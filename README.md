# README #

# About the project #

It is a project management software.It has following features


* create / update/delete users.
* two level users - normal user and admin user.
* create/update/delete projects.
* create/update/delete clients.
* multiple communications,tasks inside projects.
* assign and track the tasks.
* todolist
* task schedule report
* timeline entry report.
* save emails directly as communication.
* assign time and view report of fixed tasks.

# Language/Tools used #
PHP / MYSql / YII / Bootstrap / Ajax / Netbeans / SVN