<?php

class m131223_083358_alter_tables extends CDbMigration
{
	public function safeUp()
	{
		//alter table pm_communication
		$this->addColumn("pm_communication","communication_subject","string");
		$this->addColumn("pm_communication","project_associates_id","int(11)");
		
		$this->addForeignKey("fk_comm_projectassociates", "pm_communication", "project_associates_id", "pm_project_associates", "project_associates_id", "CASCADE", "RESTRICT");
		
		
		//table communication_attachment
		$this->createTable('pm_communication_attachment', array(
		'communication_attachment_id'=>'pk',
		'communication_id'=>'int(11) NOT NULL',//communication
		'attachment'=>'VARCHAR(50)',
		'crtd_by' =>'int(11) DEFAULT NULL',
		'crtd_dt' =>'date DEFAULT NULL',
		'updt_by' =>'int(11) DEFAULT NULL',
		'updt_dt' =>'date DEFAULT NULL',
		'updt_cnt' =>'int(11) DEFAULT NULL',
		), 'ENGINE=InnoDB');
		$this->addForeignKey("fk_communication_attachment_comm", "pm_communication_attachment", "communication_id", "pm_communication", "communication_id", "CASCADE", "RESTRICT");
		
		//end communication_attachment
		
		//table job file
		$this->createTable('pm_job_file', array(
		'file_id'=>'pk',
		'project_id'=>'int(11) NOT NULL',//project
		'job_id'=>'int(11) NOT NULL ',//job
		'file_name'=>'VARCHAR(255)',
		'type'=>'VARCHAR(10)',
		'issue'=>'VARCHAR(10)',
		'supersede'=>'VARCHAR(10)',
		'wip'=>'VARCHAR(10)',
		'crtd_by' =>'int(11) DEFAULT NULL',
		'crtd_dt' =>'date DEFAULT NULL',
		'updt_by' =>'int(11) DEFAULT NULL',
		'updt_dt' =>'date DEFAULT NULL',
		'updt_cnt' =>'int(11) DEFAULT NULL',
		), 'ENGINE=InnoDB');
		
		$this->addForeignKey("fk_jobfile_project", "pm_job_file", "project_id", "pm_project", "project_id", "CASCADE", "RESTRICT");
		$this->addForeignKey("fk_jobfile_job", "pm_job_file", "job_id", "pm_job", "job_id", "CASCADE", "RESTRICT");
		//end table job file
		
		//table task Communication
		$this->createTable('pm_task_communication', array(
		'task_communication_id'=>'pk',
		'task_id'=>'int(11) NOT NULL',//task
		'communication_id'=>'int(11) NOT NULL',//communication
		'crtd_by' =>'int(11) DEFAULT NULL',
		'crtd_dt' =>'date DEFAULT NULL',
		'updt_by' =>'int(11) DEFAULT NULL',
		'updt_dt' =>'date DEFAULT NULL',
		'updt_cnt' =>'int(11) DEFAULT NULL',
		), 'ENGINE=InnoDB');
		$this->addForeignKey("fk_taskcommunication_task", "pm_task_communication", "task_id", "pm_task", "task_id", "CASCADE", "RESTRICT");
		$this->addForeignKey("fk_taskcommunication_comm", "pm_task_communication", "communication_id", "pm_communication", "communication_id", "CASCADE", "RESTRICT");
		//end task communication
	}

	public function safeDown()
	{
		$this->dropColumn("pm_communication","communication_subject");
		$this->dropColumn("pm_communication","project_associates_id");
		$this->dropTable('pm_communication_attachment');
		$this->dropTable('pm_job_file');
		$this->dropTable('pm_task_communication');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}