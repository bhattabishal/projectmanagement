<?php

class m131215_061057_code_value_relationships extends CDbMigration
{
	public function up()
	{
		
		$this->addForeignKey("fk_project_status", "pm_project", "project_status", "pm_code_value", "code_id", "CASCADE", "RESTRICT");
		$this->addForeignKey("fk_project_type", "pm_project", "project_type", "pm_code_value", "code_id", "CASCADE", "RESTRICT");
		
		$this->addForeignKey("fk_job_cat", "pm_job", "job_category", "pm_code_value", "code_id", "CASCADE", "RESTRICT");
		$this->addForeignKey("fk_job_type", "pm_job", "job_type", "pm_code_value", "code_id", "CASCADE", "RESTRICT");
		$this->addForeignKey("fk_job_sharable", "pm_job", "sharable", "pm_code_value", "code_id", "CASCADE", "RESTRICT");
		
		$this->addForeignKey("fk_jobaction_job_status", "pm_job_action", "job_status", "pm_code_value", "code_id", "CASCADE", "RESTRICT");
		
		$this->addForeignKey("fk_branch_type", "pm_branch", "branch_type", "pm_code_value", "code_id", "CASCADE", "RESTRICT");
		$this->addForeignKey("fk_projectassociates_is_coordinator", "pm_project_associates", "is_coordinator", "pm_code_value", "code_id", "CASCADE", "RESTRICT");
	}

	public function down()
	{
		$this->dropForeignKey("fk_project_status", "pm_project");
		$this->dropForeignKey("fk_project_type", "pm_project");
		$this->dropForeignKey("fk_job_cat", "pm_job");
		$this->dropForeignKey("fk_job_type", "pm_job");
		$this->dropForeignKey("fk_job_sharable", "pm_job");
		$this->dropForeignKey("fk_jobaction_job_status", "pm_job_action");
		$this->dropForeignKey("fk_branch_type", "pm_branch");
		$this->dropForeignKey("fk_projectassociates_is_coordinator", "pm_project_associates");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}