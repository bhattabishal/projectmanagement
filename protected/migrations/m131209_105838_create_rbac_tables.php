<?php

class m131209_105838_create_rbac_tables extends CDbMigration
{
	public function up()
	{
		//create the auth item table
		$this->createTable('pm_auth_item', array(
		'name' =>'varchar(64) NOT NULL',
		'type' =>'integer NOT NULL',
		'description' =>'text',
		'bizrule' =>'text',
		'data' =>'text',
		'PRIMARY KEY (`name`)',
		), 'ENGINE=InnoDB');
		
		
		//create the auth item child table
		$this->createTable('pm_auth_item_child', array(
		'parent' =>'varchar(64) NOT NULL',
		'child' =>'varchar(64) NOT NULL',
		'PRIMARY KEY (`parent`,`child`)',
		), 'ENGINE=InnoDB');
		
		
		//the tbl_auth_item_child.parent is a reference to tbl_auth_item.name
		$this->addForeignKey("fk_auth_item_child_parent", "pm_auth_item_child", "parent", "pm_auth_item", "name", "CASCADE", "CASCADE");
		
		//the tbl_auth_item_child.child is a reference to tbl_auth_item.name
		$this->addForeignKey("fk_auth_item_child_child", "pm_auth_item_child", "child", "pm_auth_item", "name", "CASCADE", "CASCADE");
		
		//create the auth assignment table
		$this->createTable('pm_auth_assignment', array(
		'itemname' =>'varchar(64) NOT NULL',
		'userid' =>'int(11) NOT NULL',
		'bizrule' =>'text',
		'data' =>'text',
		'PRIMARY KEY (`itemname`,`userid`)',
		), 'ENGINE=InnoDB');
		
		
		//the tbl_auth_assignment.itemname is a reference
		//to tbl_auth_item.name
		$this->addForeignKey(
		"fk_auth_assignment_itemname",
		"pm_auth_assignment",
		"itemname",
		"pm_auth_item",
		"name",
		"CASCADE",
		"CASCADE"
		);
		
		
		//the tbl_auth_assignment.userid is a reference
		//to tbl_user.id
		$this->addForeignKey(
		"fk_auth_assignment_userid",
		"pm_auth_assignment",
		"userid",
		"pm_user",
		"user_id",
		"CASCADE",
		"CASCADE"
		);
	}

	public function down()
	{
		$this->truncateTable('pm_auth_assignment');
		$this->truncateTable('pm_auth_item_child');
		$this->truncateTable('pm_auth_item');
		$this->dropTable('pm_auth_assignment');
		$this->dropTable('pm_auth_item_child');
		$this->dropTable('pm_auth_item');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}