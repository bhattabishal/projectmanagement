<?php

class m131212_053022_create_job_task_tables extends CDbMigration
{
	

	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		//table job
		$this->createTable('pm_job', array(
		'job_id'=>'pk',
		'communication_id'=>'int(11) NOT NULL',//communication
		'job_category'=>'int(11) NOT NULL',//code value
		'job_details'=>'text',
		'job_type'=>'int(11) NOT NULL',//code value
		'sharable'=>'int(11) NOT NULL',//code value
		'job_due_date'=>'date NOT NULL',
		'wis'=>'decimal(18,2) DEFAULT NULL',
		'est_rps_cost'=>'decimal(18,2) DEFAULT NULL',
		'est_ao_cost'=>'decimal(18,2) DEFAULT NULL',
		'actual_rps_cost'=>'decimal(18,2) DEFAULT NULL',
		'actual_ao_cost'=>'decimal(18,2) DEFAULT NULL',
		'done_percentage'=>'decimal(18,2) DEFAULT NULL',
		'crtd_by' =>'int(11) DEFAULT NULL',
		'crtd_dt' =>'date DEFAULT NULL',
		'updt_by' =>'int(11) DEFAULT NULL',
		'updt_dt' =>'date DEFAULT NULL',
		'updt_cnt' =>'int(11) DEFAULT NULL',
		), 'ENGINE=InnoDB');
		
		$this->addForeignKey("fk_job_communication", "pm_job", "communication_id", "pm_communication", "communication_id", "CASCADE", "RESTRICT");
		
		$this->createTable('pm_communication_job', array(
		'communication_job_id'=>'pk',
		'communication_id'=>'int(11) NOT NULL',//communication
		'job_id'=>'int(11) NOT NULL',//job
		'crtd_by' =>'int(11) DEFAULT NULL',
		'crtd_dt' =>'date DEFAULT NULL',
		'updt_by' =>'int(11) DEFAULT NULL',
		'updt_dt' =>'date DEFAULT NULL',
		'updt_cnt' =>'int(11) DEFAULT NULL',
		), 'ENGINE=InnoDB');
		
		
		$this->addForeignKey("fk_communicationjob_commu", "pm_communication_job", "communication_id", "pm_communication", "communication_id", "CASCADE", "RESTRICT");
		
		$this->addForeignKey("fk_communicationjob_job", "pm_communication_job", "job_id", "pm_job", "job_id", "CASCADE", "RESTRICT");
		
		//jobcontact
		$this->createTable('pm_job_contact', array(
		'job_contact_id'=>'pk',
		'job_id'=>'int(11) NOT NULL',//job
		'client_contact_id'=>'int(11) NOT NULL',//client contact
		'crtd_by' =>'int(11) DEFAULT NULL',
		'crtd_dt' =>'date DEFAULT NULL',
		'updt_by' =>'int(11) DEFAULT NULL',
		'updt_dt' =>'date DEFAULT NULL',
		'updt_cnt' =>'int(11) DEFAULT NULL',
		), 'ENGINE=InnoDB');
		
		$this->addForeignKey("fk_jobcontact_job", "pm_job_contact", "job_id", "pm_job", "job_id", "CASCADE", "RESTRICT");
		$this->addForeignKey("fk_jobcontact_client_contact", "pm_job_contact", "client_contact_id", "pm_client_contact", "client_contact_id", "CASCADE", "RESTRICT");
		
		
		//job associate start
		$this->createTable('pm_job_associate', array(
		'job_associate_id'=>'pk',
		'job_id'=>'int(11) NOT NULL',//job
		'user_id'=>'int(11) NOT NULL',//user
		'crtd_by' =>'int(11) DEFAULT NULL',
		'crtd_dt' =>'date DEFAULT NULL',
		'updt_by' =>'int(11) DEFAULT NULL',
		'updt_dt' =>'date DEFAULT NULL',
		'updt_cnt' =>'int(11) DEFAULT NULL',
		), 'ENGINE=InnoDB');
		
		$this->addForeignKey("fk_jobassociate_job", "pm_job_associate", "job_id", "pm_job", "job_id", "CASCADE", "RESTRICT");
		$this->addForeignKey("fk_jobassociate_user", "pm_job_associate", "user_id", "pm_user", "user_id", "CASCADE", "RESTRICT");
		
		//job associate ends
		
		
		//job action starts
		$this->createTable('pm_job_action', array(
		'job_action_id'=>'pk',
		'job_id'=>'int(11) NOT NULL',//job
		'job_status'=>'int(11) NOT NULL',//code value
		'action_date'=>'date DEFAULT NULL',
		'action_remarks'=>'text',
		'user_id'=>'int(11) NOT NULL',//user
		'crtd_by' =>'int(11) DEFAULT NULL',
		'crtd_dt' =>'date DEFAULT NULL',
		'updt_by' =>'int(11) DEFAULT NULL',
		'updt_dt' =>'date DEFAULT NULL',
		'updt_cnt' =>'int(11) DEFAULT NULL',
		), 'ENGINE=InnoDB');
		
		$this->addForeignKey("fk_jobaction_job", "pm_job_action", "job_id", "pm_job", "job_id", "CASCADE", "RESTRICT");
		$this->addForeignKey("fk_jobaction_user", "pm_job_action", "user_id", "pm_user", "user_id", "CASCADE", "RESTRICT");
		
		//job action ends
		
		
		//task starts
		$this->createTable('pm_task', array(
		'task_id'=>'pk',
		'job_id'=>'int(11) NOT NULL',//job
		'job_details'=>'text  NOT NULL',
		'sharable'=>'int(11) NOT NULL',//code value
		'job_due_date'=>'date  NOT NULL',
		'est_rps_cost'=>'decimal(18,2) NOT NULL',
		'est_ao_cost'=>'decimal(18,2) NOT NULL',
		'actual_rps_cost'=>'decimal(18,2)  NOT NULL',
		'actual_ao_cost'=>'decimal(18,2)  NOT NULL',
		'done_percentage'=>'decimal(18,2) NOT NULL',	
		'assigned_to'=>'int(11) NOT NULL',//user
		'task_status'=>'int(11) NOT NULL',//code value
		'crtd_by' =>'int(11) DEFAULT NULL',
		'crtd_dt' =>'date DEFAULT NULL',
		'updt_by' =>'int(11) DEFAULT NULL',
		'updt_dt' =>'date DEFAULT NULL',
		'updt_cnt' =>'int(11) DEFAULT NULL',
		), 'ENGINE=InnoDB');
		
		$this->addForeignKey("fk_task_job", "pm_task", "job_id", "pm_job", "job_id", "CASCADE", "RESTRICT");
		$this->addForeignKey("fk_task_user", "pm_task", "assigned_to", "pm_user", "user_id", "CASCADE", "RESTRICT");
		
		//task ends
	}

	public function safeDown()
	{
		$this->dropTable('pm_job');
		$this->dropTable('pm_job_contact');
		$this->dropTable('pm_communication_job');
		$this->dropTable('pm_job_associate');
		$this->dropTable('pm_job_action');
		$this->dropTable('pm_task');
	}
	
}