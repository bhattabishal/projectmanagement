<?php

class m131205_090805_user_role_tables extends CDbMigration
{
	

	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable('pm_user',array(
			'user_id'=>'pk',
			'user_name'=>'string NOT NULL',
			'login_name'=>'string NOT NULL',
			'login_pwd'=>'string NOT NULL',
			'last_login_time'=>'datetime NOT NULL',
			'branch_id'=>'int(11) NOT NULL',
			'crtd_by' =>'int(11) DEFAULT NULL',
			'crtd_dt' =>'date DEFAULT NULL',
			'updt_by' =>'int(11) DEFAULT NULL',
			'updt_dt' =>'date DEFAULT NULL',
			'updt_cnt' =>'int(11) DEFAULT NULL',
			
		),'ENGINE=InnoDB');
		
		$this->addForeignKey("fk_user_branch", "pm_user", "branch_id", "pm_branch", "branch_id", "CASCADE", "RESTRICT");
		
		//$this->addForeignKey("fk_user_role", "pm_user", "role_id", "pm_role", "role_id", "CASCADE", "RESTRICT");
	}

	public function safeDown()
	{
		$this->dropTable('pm_user');
	}
	
}