<?php

class m131212_044715_create_communication_job_contact_associates_tables extends CDbMigration
{
	

	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		//table communication
		$this->createTable('pm_communication', array(
		'communication_id'=>'pk',
		'project_id'=>'int(11) NOT NULL',
		'communication_details'=>'text',
		'project_contact_id'=>'int(11) NOT NULL',
		'communication_date'=>'date NOT NULL',
		'user_id'=>'int(11) NOT NULL',
		'crtd_by' =>'int(11) DEFAULT NULL',
		'crtd_dt' =>'date DEFAULT NULL',
		'updt_by' =>'int(11) DEFAULT NULL',
		'updt_dt' =>'date DEFAULT NULL',
		'updt_cnt' =>'int(11) DEFAULT NULL',
		), 'ENGINE=InnoDB');
		
		$this->addForeignKey("fk_comm_project", "pm_communication", "project_id", "pm_project", "project_id", "CASCADE", "RESTRICT");
		
		
		$this->addForeignKey("fk_comm_project_contact", "pm_communication", "project_contact_id", "pm_project_contact", "project_contact_id", "CASCADE", "RESTRICT");
		
		$this->addForeignKey("fk_comm_user", "pm_communication", "user_id", "pm_user", "user_id", "CASCADE", "RESTRICT");
		
		
		//table communication contact
		$this->createTable('pm_communication_contact', array(
		'communication_contact_id'=>'pk',
		'communication_id'=>'int(11) NOT NULL',//communication
		'client_contact_id'=>'int(11) NOT NULL',//client contact
		'crtd_by' =>'int(11) DEFAULT NULL',
		'crtd_dt' =>'date DEFAULT NULL',
		'updt_by' =>'int(11) DEFAULT NULL',
		'updt_dt' =>'date DEFAULT NULL',
		'updt_cnt' =>'int(11) DEFAULT NULL',
		), 'ENGINE=InnoDB');
		
		$this->addForeignKey("fk_commcontact_commu", "pm_communication_contact", "communication_id", "pm_communication", "communication_id", "CASCADE", "RESTRICT");
		
		$this->addForeignKey("fk_commcontact_clientcontact", "pm_communication_contact", "client_contact_id", "pm_client_contact", "client_contact_id", "CASCADE", "RESTRICT");
		
		
		//table communication contact
		$this->createTable('pm_communication_associate', array(
		'communication_associate_id'=>'pk',
		'communication_id'=>'int(11) NOT NULL',//communication
		'user_id'=>'int(11) NOT NULL',//user
		'crtd_by' =>'int(11) DEFAULT NULL',
		'crtd_dt' =>'date DEFAULT NULL',
		'updt_by' =>'int(11) DEFAULT NULL',
		'updt_dt' =>'date DEFAULT NULL',
		'updt_cnt' =>'int(11) DEFAULT NULL',
		), 'ENGINE=InnoDB');
		
		$this->addForeignKey("fk_commu_associate", "pm_communication_associate", "communication_id", "pm_communication", "communication_id", "CASCADE", "RESTRICT");
		
		$this->addForeignKey("fk_commu_associate_user", "pm_communication_associate", "user_id", "pm_user", "user_id", "CASCADE", "RESTRICT");
		
	}
		
		

	public function safeDown()
	{
		$this->dropTable('pm_communication');
		$this->dropTable('pm_communication_contact');
		$this->dropTable('pm_communication_associate');
	}
	
}