<?php

class m131227_092956_alter_task_status extends CDbMigration
{
	public function up()
	{
		$this->addForeignKey("fk_task_status", "pm_task", "task_status", "pm_code_value", "code_id", "CASCADE", "RESTRICT");
	}

	public function down()
	{
		$this->dropForeignKey("fk_task_status", "pm_project");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}