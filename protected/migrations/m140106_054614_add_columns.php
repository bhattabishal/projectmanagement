<?php

class m140106_054614_add_columns extends CDbMigration
{
	public function up()
	{
		$this->addColumn("pm_branch","color_code","varchar(50)");
		
	}

	public function down()
	{
		$this->dropColumn("pm_branch","color_code");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}