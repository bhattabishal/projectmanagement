<?php

class m140108_071304_create_code_master_rln extends CDbMigration
{
	public function up()
	{
		$this->addForeignKey("fk_codevalue_mastertype", "pm_code_value", "code_type", "pm_code_value_master", "code_type", "CASCADE", "RESTRICT");
	}

	public function down()
	{
		$this->dropForeignKey("fk_codevalue_mastertype","pm_code_value");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}