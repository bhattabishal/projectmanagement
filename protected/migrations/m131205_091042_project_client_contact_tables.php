<?php

class m131205_091042_project_client_contact_tables extends CDbMigration
{
	

	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		//client table
		$this->createTable('pm_client',array(
			'client_id'=>'pk',
			'client_name'=>'string NOT NULL',
			'client_address'=>'text',
			'client_phone'=>'string DEFAULT NULL',
			'crtd_by' =>'int(11) DEFAULT NULL',
			'crtd_dt' =>'date DEFAULT NULL',
			'updt_by' =>'int(11) DEFAULT NULL',
			'updt_dt' =>'date DEFAULT NULL',
			'updt_cnt' =>'int(11) DEFAULT NULL',	
			
		),'ENGINE=InnoDB');
		
		//client contact multiple for client
		$this->createTable('pm_client_contact',array(
			'client_contact_id'=>'pk',
			'client_id'=>'int(11) DEFAULT NULL',
			'contact_name'=>'string NOT NULL',
			'contact_phone'=>'string DEFAULT NULL',
			'contact_email'=>'string DEFAULT NULL',
			'crtd_by' =>'int(11) DEFAULT NULL',
			'crtd_dt' =>'date DEFAULT NULL',
			'updt_by' =>'int(11) DEFAULT NULL',
			'updt_dt' =>'date DEFAULT NULL',
			'updt_cnt' =>'int(11) DEFAULT NULL',
			
		),'ENGINE=InnoDB');
		
		
		$this->createTable('pm_project',array(
		'project_id' => 'pk',
		'project_title' =>'string NOT NULL',
		'project_status' =>'int(11) DEFAULT NULL',
		'project_type' =>'int(11) DEFAULT NULL',
		'client_id' =>'int(11) DEFAULT NULL',
		'project_date' =>'date DEFAULT NULL',
		'crtd_by' =>'int(11) DEFAULT NULL',
		'crtd_dt' =>'date DEFAULT NULL',
		'updt_by' =>'int(11) DEFAULT NULL',
		'updt_dt' =>'date DEFAULT NULL',
		'updt_cnt' =>'int(11) DEFAULT NULL',	
		),'ENGINE=InnoDB');
		
		//table project contact
		$this->createTable('pm_project_contact',array(
			'project_contact_id'=>'pk',
			'project_id'=>'int(11) NOT NULL',
			'contact_id'=>'int(11) NOT NULL',
			'crtd_by' =>'int(11) DEFAULT NULL',
			'crtd_dt' =>'date DEFAULT NULL',
			'updt_by' =>'int(11) DEFAULT NULL',
			'updt_dt' =>'date DEFAULT NULL',
			'updt_cnt' =>'int(11) DEFAULT NULL',
			
		),'ENGINE=InnoDB');
		
		
		//project associates table
		$this->createTable('pm_project_associates',array(
			'project_associates_id'=>'pk',
			'project_id'=>'int(11) NOT NULL',
			'user_id'=>'int(11) NOT NULL',
			'crtd_by' =>'int(11) DEFAULT NULL',
			'crtd_dt' =>'date DEFAULT NULL',
			'updt_by' =>'int(11) DEFAULT NULL',
			'updt_dt' =>'date DEFAULT NULL',
			'updt_cnt' =>'int(11) DEFAULT NULL',
		),'ENGINE=InnoDB');
		
		$this->addForeignKey("fk_project_client", "pm_project", "client_id", "pm_client", "client_id", "CASCADE", "RESTRICT");
		
		//foreign key relationships between client and client contact
		//the pm_client_contact.client_id is a reference to pm_client.client_id
		$this->addForeignKey("fk_client_contact", "pm_client_contact", "client_id", "pm_client", "client_id", "CASCADE", "RESTRICT");
		
		
		//foreign key relationship between project_contact and client_contact
		$this->addForeignKey("fk_project_client_contact", "pm_project_contact", "contact_id", "pm_client_contact", "client_contact_id", "CASCADE", "RESTRICT");
		
		//foreign key relationships between project_contact and project
		$this->addForeignKey("fk_project_contact", "pm_project_contact", "project_id", "pm_project", "project_id", "CASCADE", "RESTRICT");
		
		//foreign key relationship between project_associates and project
		$this->addForeignKey("fk_project_associates", "pm_project_associates", "project_id", "pm_project", "project_id", "CASCADE", "RESTRICT");
		
		//foreign key relationship between project_associates and user
		$this->addForeignKey("fk_project_user_associates", "pm_project_associates", "user_id", "pm_user", "user_id", "CASCADE", "RESTRICT");
	}

	public function safeDown()
	{
		$this->truncateTable('pm_project');
		$this->truncateTable('pm_client');
		$this->truncateTable('pm_client_contact');
		$this->truncateTable('pm_project_contact');
		$this->truncateTable('pm_project_associates');
	}
	
}