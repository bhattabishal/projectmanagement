<?php

class m131215_053410_alter_project_associates_table extends CDbMigration
{
	public function up()
	{
		$this->addColumn("pm_project_associates","is_coordinator","int(2)");
	}

	public function down()
	{
		$this->dropColumn("pm_project_associates","is_coordinator");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}