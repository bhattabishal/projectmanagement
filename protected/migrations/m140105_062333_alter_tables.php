<?php

class m140105_062333_alter_tables extends CDbMigration
{
	

	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		
		$this->addColumn("pm_project","job_due_date","date NOT NULL");
		$this->addColumn("pm_project","wis","decimal(5,2) DEFAULT NULL");
		$this->addColumn("pm_project","est_rps_wis","decimal(5,2) DEFAULT NULL");
		$this->addColumn("pm_project","est_ao_wis","decimal(5,2) DEFAULT NULL");
		$this->addColumn("pm_project","actual_rps_hrs","decimal(5,2) DEFAULT NULL");
		$this->addColumn("pm_project","actual_ao_hrs","decimal(5,2) DEFAULT NULL");
		$this->addColumn("pm_project","done_percentage","decimal(5,2) DEFAULT NULL");
		
		
		$this->createTable('pm_checklist', array(
		'checklist_id'=>'pk',
		'job_type_id'=>'int(11) NOT NULL',//code value
		'checklist_details'=>'text NOT NULL',
		'crtd_by' =>'int(11) DEFAULT NULL',
		'crtd_dt' =>'date DEFAULT NULL',
		'updt_by' =>'int(11) DEFAULT NULL',
		'updt_dt' =>'date DEFAULT NULL',
		'updt_cnt' =>'int(11) DEFAULT NULL',
		), 'ENGINE=InnoDB');
		
		$this->addForeignKey("fk_checklist_job_type", "pm_checklist", "job_type_id", "pm_code_value", "code_id", "CASCADE", "RESTRICT");
		
		
		$this->createTable('pm_task_checklist', array(
		'task_checklist_id'=>'pk',
		'task_id'=>'int(11) NOT NULL',//task
		'checklist_details'=>'text NOT NULL',
		'status' =>'int(5) DEFAULT NULL',//code value
		'crtd_by' =>'int(11) DEFAULT NULL',
		'crtd_dt' =>'date DEFAULT NULL',
		'updt_by' =>'int(11) DEFAULT NULL',
		'updt_dt' =>'date DEFAULT NULL',
		'updt_cnt' =>'int(11) DEFAULT NULL',
		), 'ENGINE=InnoDB');
		
		$this->addForeignKey("fk_taskchecklist_task", "pm_task_checklist", "task_id", "pm_task", "task_id", "CASCADE", "RESTRICT");
		
		$this->addForeignKey("fk_taskchecklist_status", "pm_task_checklist", "status", "pm_code_value", "code_id", "CASCADE", "RESTRICT");
		
		
		$this->addColumn("pm_task","task_cat","int(5) NOT NULL");
		
		
		$this->dropForeignKey('fk_task_job','pm_task');
		$this->dropColumn("pm_task","job_id");
		
		$this->addColumn("pm_task","communication_id","int(11) NOT NULL");
		
		$this->addForeignKey("fk_taskcommunication", "pm_task", "communication_id", "pm_communication", "communication_id", "CASCADE", "RESTRICT");
		
		$this->addForeignKey("fk_taskcat", "pm_task", "task_cat", "pm_code_value", "code_id", "CASCADE", "RESTRICT");
	}

	public function safeDown()
	{
		$this->dropColumn("pm_project","job_due_date");
		$this->dropColumn("pm_project","wis");
		$this->dropColumn("pm_project","est_rps_wis");
		$this->dropColumn("pm_project","est_ao_wis");
		$this->dropColumn("pm_project","actual_rps_hrs");
		$this->dropColumn("pm_project","actual_ao_hrs");
		$this->dropColumn("pm_project","done_percentage");
		
		$this->dropTable('pm_checklist');
		$this->dropTable('pm_task_checklist');
		
		$this->dropColumn("pm_task","task_cat");
		$this->dropColumn("pm_task","communication_id");
		
		$this->addColumn("pm_task","job_id","int(11) NOT NULL");
		$this->addForeignKey("fk_task_job", "pm_task", "job_id", "pm_job", "job_id", "CASCADE", "RESTRICT");
	}
	
}