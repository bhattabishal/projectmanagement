<?php

class m131205_090045_master_tables extends CDbMigration
{
	

	
	// Use safeUp/safeDown to do migration with transaction
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable('pm_branch',array(
			'branch_id'=>'pk',
			'branch_name'=>'string DEFAULT NULL',
			'branch_code'=>'string DEFAULT NULL',
			'branch_contact'=>'string DEFAULT NULL',
			'branch_email'=>'string DEFAULT NULL',
			'branch_type'=>'int(11) NOT NULL',
			'crtd_by' =>'int(11) DEFAULT NULL',
			'crtd_dt' =>'date DEFAULT NULL',
			'updt_by' =>'int(11) DEFAULT NULL',
			'updt_dt' =>'date DEFAULT NULL',
			'updt_cnt' =>'int(11) DEFAULT NULL',
			
		),'ENGINE=InnoDB');
		
		/*$this->createTable('pm_task',array(
			'task_id'=>'pk',
			'task_name'=>'string DEFAULT NULL',
			'task_lbl'=>'string DEFAULT NULL',

			'crtd_by' =>'int(11) DEFAULT NULL',
			'crtd_dt' =>'date DEFAULT NULL',
			'updt_by' =>'int(11) DEFAULT NULL',
			'updt_dt' =>'date DEFAULT NULL',
			'updt_cnt' =>'int(11) DEFAULT NULL',
			
		),'ENGINE=InnoDB');
		
		$this->createTable('pm_role',array(
			'role_id'=>'pk',
			'role_name'=>'string DEFAULT NULL',
			'crtd_by' =>'int(11) DEFAULT NULL',
			'crtd_dt' =>'date DEFAULT NULL',
			'updt_by' =>'int(11) DEFAULT NULL',
			'updt_dt' =>'date DEFAULT NULL',
			'updt_cnt' =>'int(11) DEFAULT NULL',
			
		),'ENGINE=InnoDB');
		
		$this->createTable('pm_role_task',array(
			'role_task_id'=>'pk',
			'role_id'=>'int(11) DEFAULT NULL',
			'task_id'=>'int(11) DEFAULT NULL',
			'crtd_by' =>'int(11) DEFAULT NULL',
			'crtd_dt' =>'date DEFAULT NULL',
			'updt_by' =>'int(11) DEFAULT NULL',
			'updt_dt' =>'date DEFAULT NULL',
			'updt_cnt' =>'int(11) DEFAULT NULL',
			
		),'ENGINE=InnoDB');*/
		
		
		$this->createTable('pm_code_value',array(
			'code_id'=>'pk',
			
			'code_lbl'=>'string NOT NULL',
			'code_type'=>'string NOT NULL',
			'order'=>'int(11) NOT NULL',
			'crtd_by' =>'int(11) DEFAULT NULL',
			'crtd_dt' =>'date DEFAULT NULL',
			'updt_by' =>'int(11) DEFAULT NULL',
			'updt_dt' =>'date DEFAULT NULL',
			'updt_cnt' =>'int(11) DEFAULT NULL',
			
		),'ENGINE=InnoDB');
		
		$this->createTable('pm_code_value_master',array(
			'code_master_id'=>'pk',
			
			'code_type'=>'string NOT NULL',
			'code_type_lbl'=>'string NOT NULL',
			'edit_allowed'=>"enum('yes','no') NOT NULL",
			
		),'ENGINE=InnoDB');
		
		
		//$this->addForeignKey("fk_roletask_role", "pm_role_task", "role_id", "pm_role", "role_id", "CASCADE", "RESTRICT");
		
		//$this->addForeignKey("fk_roletask_task", "pm_role_task", "task_id", "pm_task", "task_id", "CASCADE", "RESTRICT");
		
		
	}

	public function safeDown()
	{
		$this->dropTable('pm_branch');
		//$this->dropTable('pm_task');
		//$this->dropTable('pm_role');
		//$this->dropTable('pm_role_task');
		$this->dropTable('pm_code_value');
	}
	
	
}