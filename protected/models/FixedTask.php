<?php
/**
 * This is the model class for table "pm_fixed_task".
 *
 * The followings are the available columns in table 'pm_fixed_task':
 * @property string $fixed_task_id
 * @property string $parent_id
 * @property string $title
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 */
class FixedTask extends PMActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_fixed_task';
	}
       

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('parent_id', 'length', 'max'=>11),
			array('title', 'length', 'max'=>150),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('fixed_task_id, parent_id, title', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'parent'=>array(self::BELONGS_TO, 'FixedTask', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'fixed_task_id' => 'Fixed Task',
			'parent_id' => 'Parent',
			'title' => 'Title',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Created Date',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('fixed_task_id',$this->fixed_task_id,true);
		$criteria->compare('parent_id',$this->parent_id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'sort'=>array(
                            'defaultOrder'=>'title ASC',
                        ),
		));
	}
	
	public function listParentTasks()
	{
		$tasks=FixedTask::model()->findAll('parent_id=:pid',array(':pid'=>'0'));
		$array=array();
		
		foreach($tasks as $val)
		{
			$array[$val->fixed_task_id]=$val->title;
		}
		
		return $array; 
	}
	public function listChildTasks()
	{
		$tasks=FixedTask::model()->with('parent')->findAll('t.parent_id > 0');
		$array=array();
		
		foreach($tasks as $val)
		{
			$array[$val->fixed_task_id]=$val->title.'('.$val->parent->title.')';
		}
		
		return $array;
	}
	
	
	public function listChildTasksForTimeline()
	{
		$projectsArray=  array();
		 $fixedTasks=$this->listChildTasks();
		  foreach($fixedTasks as $key=>$val)
		  {
		  	 $id=$key;
              $projectsArray[$id]=$val;
		  }
		  
		  return $projectsArray;
		
	}
        
        public function getFixedTaskProjectList()
        {
            $projects=  Project::model()->getAllProjectsNo();
            
            $projectsArray=  array();
            foreach($projects as $key=>$val)
            {
                 $id=$key.'-project';
                 $projectsArray[$id]=$val;
            }
            $fixedTasks=array();
            $fixedTasks=$this->getFixedTaskParentChild();
            
            $merge=array_merge($projectsArray,$fixedTasks);
            
            return $merge;
            
        }
        
        private function getFixedTaskParentChild($parent_id=0,&$arrayResult=array(), &$x='')
        {
            $tasks=  FixedTask::model()->findAll('parent_id=:pid',array(':pid'=>$parent_id));
           
            foreach($tasks as $result)
            {
                $id=$result->fixed_task_id;
                if($parent_id <= 0)
                {
                    $x='';
                }
                $arrayResult[$id]=$x.$result->title;
                
                //check if has child
                $ptask= FixedTask::model()->findAll('parent_id=:pid',array(':pid'=>$result->fixed_task_id));
                
                if(count($ptask) > 0)
                {
                    $x.='-';
                    $this->getFixedTaskParentChild($result->fixed_task_id,$arrayResult,$x);
                }
               
            }
            
            return $arrayResult;
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FixedTask the static model class
	 */
	 
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
