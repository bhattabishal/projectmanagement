<?php
 class ClientInstructionSheet extends CFormModel
  {
	public $communication_date;
	public $communication_id;
	public $projectId;
	public $type;

	public function rules()
		{
			return array(
					array('communication_date, communication_id', 'required'),
					array('projectId,type', 'safe'),
			      );
		}
	public function relations()
		{
			// NOTE: you may need to adjust the relation name and the related
			// class name for the relations automatically generated below.
			return array(
		'userType' => array(self::HAS_ONE, 'CodeValue',array('branch_type'=>'code_id'), 'through'=>'branch'),
		);
		}
   /*for priject wise details*/
	public function getProjectWiseDetails()
	 {
		$cond="";
		$pid=$this->projectId;
	 
		if($pid > 0)
		{
			$cond=" and pm_project.project_id='$pid'";
		}
		
		$records=Yii::app()->db->createCommand("SELECT
			pm_project.project_id,
			pm_project.project_no,
			pm_project.project_title,
			pm_project.project_status,
			CONCAT_WS('-', IF(pm_project.project_no IS NULL or pm_project.project_no = '', pm_project.project_id, pm_project.project_no), 
			pm_code_value.code_lbl) AS prj_no

			FROM
			pm_project
			INNER JOIN pm_code_value ON pm_project.project_type = pm_code_value.code_id
			WHERE
			pm_project.project_status IN (3,4)
			
			$cond
			ORDER BY
			pm_project.project_no DESC,
			pm_project.project_id DESC
			")->queryAll();
			
			return $records;
	  }
	
	/*for Type wise details*/
	public function getAoRPS($from)
	{
		if($from==0)
			{
				return "Client";
			}
			else 
			{
				$branch=Branch::model()->find('branch_id=:id',array(':id'=>$from));
			
				if($branch->branch_type==14)
				{
					return "AO";
				}
				else if($branch->branch_type==18)
				{
					return "RPS";
				}
				else
				{
					return "";
				}
			}
	    }
     }
  ?>