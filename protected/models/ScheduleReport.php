<?php
class ScheduleReport extends CFormModel
{
	public $projectId;
	public $startDt;
	public $endDt;
        public $branch_id;
        public $adminTasks;
	
	public function rules()
	{
		return array(
			
			array('startDt, endDt,branch_id', 'required'),
			array('projectId,adminTasks','safe'),
			
			
		
		);
	}
	public function getProjectWiseDetails($pid,$bid,$fxtTask)
	{
		$cond="";
		//$pid=$this->projectId;
                
                
                if($fxtTask  != "" && $fxtTask > 0)
                {
                    $cond1=" and pm_project.project_id='-1'";
						$records=Yii::app()->db->createCommand("SELECT
			pm_project.project_id,
			pm_project.project_no,
			pm_project.project_title,
			pm_project.project_status,
			CONCAT_WS('-', IF(pm_project.project_no IS NULL or pm_project.project_no = '', pm_project.project_id, pm_project.project_no), 
			pm_code_value.code_lbl) AS prj_no

			FROM
			pm_project
			INNER JOIN pm_code_value ON pm_project.project_type = pm_code_value.code_id
			WHERE
			pm_project.project_status IN (3,4)
			$cond1
			ORDER BY
			pm_project.project_no DESC,
			pm_project.project_id DESC
			")->queryAll();
			return $records;
                }
                else
                {
                    $cond=' and p.project_id > 0';
                    if($pid != "" && $pid > 0)
                    {
                            $cond.=" and p.project_id='$pid' and (u.branch_id='$bid' or p.branch_id = '$bid')";
                    }
                    else
                    {
                         $cond.="  and (u.branch_id='$bid' or p.branch_id = '$bid')";
                    }
                 
                }
                
		
		
		$records=Yii::app()->db->createCommand("SELECT DISTINCT
 p.project_id,
 p.project_no,
 p.project_title,
p.project_status,
CONCAT_WS('-', IF(p.project_no IS NULL or p.project_no = '', p.project_id, p.project_no), 
			cv.code_lbl) AS prj_no

FROM
pm_task_schedule AS ts
INNER JOIN pm_user AS u on ts.user_id = u.user_id
INNER JOIN pm_branch AS b on u.branch_id = b.branch_id

INNER JOIN pm_task AS t on ts.task_id = t.task_id
INNER JOIN pm_communication AS c on t.communication_id = c.communication_id
INNER JOIN pm_project AS p on c.project_id = p.project_id
INNER JOIN pm_code_value AS cv on p.project_type = cv.code_id

WHERE 
b.branch_type='18' AND p.project_no>0
$cond

ORDER BY
p.project_no DESC,
p.project_id DESC
")->queryAll();
			
			return $records;
	}
	
/*
	
	public function getProjectWiseDetails($pid,$bid,$fxtTask)
	{
		$cond="";
		//$pid=$this->projectId;
                
                
                if($fxtTask  != "" && $fxtTask > 0)
                {
                    $cond=" and pm_project.project_id='-1'";
                }
                else
                {
                    $cond=' and pm_project.project_id > 0';
                    if($pid != "" && $pid > 0)
                    {
                            $cond.=" and pm_project.project_id='$pid' and pm_project.branch_id='$bid'";
                    }
                    else
                    {
                         $cond.="  and pm_project.branch_id='$bid'";
                    }
               
                }
               
		
		
		$records=Yii::app()->db->createCommand("SELECT
			pm_project.project_id,
			pm_project.project_no,
			pm_project.project_title,
			pm_project.project_status,
			CONCAT_WS('-', IF(pm_project.project_no IS NULL or pm_project.project_no = '', pm_project.project_id, pm_project.project_no), 
			pm_code_value.code_lbl) AS prj_no

			FROM
			pm_project
			INNER JOIN pm_code_value ON pm_project.project_type = pm_code_value.code_id
			WHERE
			pm_project.project_status IN (3,4)
			$cond
			ORDER BY
			pm_project.project_no DESC,
			pm_project.project_id DESC
			")->queryAll();
			
			return $records;
	}
	 */
	
	public function getTotAssignedHrsFromProjectDate($pid,$date)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(tothour),0) as actualhrs FROM scheduled_total_by_date WHERE schedule_date='$date' and project_id='$pid'")->queryRow();
		return $records['actualhrs'];
	}
	
	public function getTotAssignedHrsFromFixedTaskDate($tid,$date)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(tothour),0) as actualhrs FROM scheduled_total_by_date_fixed_tasks WHERE schedule_date='$date' and task_id='$tid'")->queryRow();
		return $records['actualhrs'];
	}
	
	
	public function getTotAssignedHrsFromProject($pid)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(tothour),0) as actualhrs FROM scheduled_total_by_date WHERE  project_id='$pid'")->queryRow();
		return $records['actualhrs'];
	}
	
	
	public function getTotAssignedHrsFromTask($tid)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(tothour),0) as actualhrs FROM scheduled_total_by_date_fixed_tasks WHERE  task_id='$tid' and YEAR(schedule_date) = YEAR(CURDATE()) ")->queryRow();
		return $records['actualhrs'];
	}
	
	
	
	public function getTotWorkedHrsFromProjectDate($pid,$date)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(tothour),0) as workedhrs FROM actual_total_by_date WHERE task_date='$date' and project_id='$pid'")->queryRow();

		return $records['workedhrs'];
	}
	
	public function getTotWorkedHrsFixedTaskFromTaskDate($tid,$date)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(tothour),0) as workedhrs FROM actual_total_by_date_fixed_tasks WHERE task_date='$date' and task_id='$tid'")->queryRow();

		return $records['workedhrs'];
	}
	
	public function getTotWorkedHrsFromProject($pid)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(tothour),0) as workedhrs FROM actual_total_by_date WHERE  project_id='$pid'")->queryRow();

		return $records['workedhrs'];
	}
	
	public function getTotWorkedHrsFromTask($tid)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(tothour),0) as workedhrs FROM actual_total_by_date_fixed_tasks WHERE  task_id='$tid' and  YEAR(task_date) = YEAR(CURDATE())")->queryRow();

		return $records['workedhrs'];
	}
	
}
?>