<?php

/**
 * This is the model class for table "pm_checklist_scheme".
 *
 * The followings are the available columns in table 'pm_checklist_scheme':
 * @property string $chk_scheme_id
 * @property integer $chklist_id
 * @property integer $job_type_id
 * @property integer $checklist_stages
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 */
class ChecklistScheme extends PMActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_checklist_scheme';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('chklist_id, job_type_id, checklist_stages', 'required'),
			array( 'job_type_id, checklist_stages, crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('chk_scheme_id, chklist_id, job_type_id, checklist_stages, crtd_by, crtd_dt, updt_by, updt_dt, updt_cnt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'chklist' => array(self::BELONGS_TO, 'Checklist', 'chklist_id'),
			'jobType' => array(self::BELONGS_TO, 'CodeValue', 'job_type_id'),
			'checklistStages' => array(self::BELONGS_TO, 'CodeValue', 'checklist_stages'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'chk_scheme_id' => 'Chk Scheme',
			'chklist_id' => 'Chklist',
			'jobType.code_lbl' => 'Job Type',
			'checklistStages.code_lbl' => 'Checklist Stages',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('chk_scheme_id',$this->chk_scheme_id,true);
		$criteria->compare('chklist_id',$this->chklist_id);
		$criteria->compare('job_type_id',$this->job_type_id);
		$criteria->compare('checklist_stages',$this->checklist_stages);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);
                $criteria->with=('jobType');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                     'sort'=>array(
                            'defaultOrder'=>'code_lbl ASC',
                        ),
                    
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ChecklistScheme the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
