<?php
abstract class PMActiveRecord extends CActiveRecord
{
	protected function beforeSave()
	{
		if(null !==Yii::app()->user)
		{
			$id=Yii::app()->user->id;
		}
		else{
			$id=1;
		}
		
		if($this->isNewRecord)
		{
			$this->crtd_by=$id;
			$this->updt_cnt=0;
		}
		else
		{
			//$pid=$this->getPrimaryKey();
			$this->updt_cnt=$this->updt_cnt+1;
		}
		
		$this->updt_by=$id;
		
		//$this->updt_cnt
		
		return parent::beforeSave();
	}
	
	
	//behaviour to add date time on create,update fields in table
	public function behaviors()
	{
		return array(
		'CTimestampBehavior'=>array(
		'class'=>'zii.behaviors.CTimestampBehavior',
		'createAttribute'=>'crtd_dt',
		'updateAttribute' =>'updt_dt',
		'setUpdateOnCreate'=>true,
		),
		);
	}
}
?>