<?php

/**
 * This is the model class for table "pm_fixed_task_timeline".
 *
 * The followings are the available columns in table 'pm_fixed_task_timeline':
 * @property string $fxt_id
 * @property integer $project_id
 * @property string $type
 * @property integer $user_id
 * @property string $entry_dt
 * @property string $nt_hours
 * @property string $ot_hours
 * @property string $remarks
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 */
class FixedTaskTimeline extends PMActiveRecord
{
   public $updateType;
   public $user_search;
   public $project_search;
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_fixed_task_timeline';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, type, nt_hours,entry_dt', 'required'),
			array('project_id, user_id, crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>11),
			array('nt_hours, ot_hours', 'length', 'max'=>5),
			array('remarks', 'length', 'max'=>250),
			array('project_id','checkBudgetAllocated', 'on' => 'insert,update'),
			
			array('project_id','checkDuplicateDateEntry', 'on' => 'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('fxt_id, project_id, type,fxt_brief_id, user_id, entry_dt, nt_hours, ot_hours, remarks,project_search,user_search, crtd_by, crtd_dt, updt_by, updt_dt, updt_cnt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'project' => array(self::BELONGS_TO, 'FixedTask', 'project_id'),
		'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'fxt_id' => 'Fxt',
			'project_id' => 'Project',
			'type' => 'Type',
			'user_id' => 'User',
			'entry_dt' => 'Entry Dt',
			'nt_hours' => 'Nor Hours',
			'ot_hours' => 'Ot Hours',
			'remarks' => 'Remarks',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
		);
	}
	
	
	public function checkBudgetAllocated($attribute,$params)
	{
		$budget=FixedTaskSetting::model()->find('project_id=:pid',array(':pid'=>$this->project_id));
		
		if($budget==NULL)
		{
			$this->addError('project_id', "No budget in selected project");
			return FALSE;
		}
		return TRUE;
		
	}
	
	
	public function checkDuplicateDateEntry($attribute,$params)
	{
              $pid=$this->project_id;
              $dt=$this->entry_dt;
              $uid=Yii::app()->user->id;
		$entry=FixedTaskTimeline::model()->find('project_id=:pid and entry_dt=:dt and user_id=:uid',array(':pid'=>$this->project_id,':dt'=>$this->entry_dt,':uid'=>Yii::app()->user->id));
		
              
                
		if($entry==NULL)
		{
			return TRUE;
			
		}
		else
		{
			
			$this->addError('project_id', "Record already exists for the selected date and project.");
			return FALSE;
		}
		
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('fxt_id',$this->fxt_id,true);
		$criteria->compare('project.title',$this->project_search,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('user.user_name',$this->user_search,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('entry_dt',$this->entry_dt,true);
		$criteria->compare('nt_hours',$this->nt_hours,true);
		$criteria->compare('ot_hours',$this->ot_hours,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);
		
		$criteria -> join = 'INNER JOIN pm_user user on t.user_id = user.user_id ';
		
		
		$criteria -> join .= 'INNER JOIN pm_fixed_task project on t.project_id = project.fixed_task_id ';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FixedTaskTimeline the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
