<?php
class WeeklyProgressReport extends CFormModel
{
  
    public $projectId;
	public $userId;
	public $startDt;
	public $endDt;
	public $projectName;
	public $projectUsers;
	public $branch_id;
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('startDt, endDt', 'required'),
			// email has to be a valid email address
			array('userId,projectId', 'safe'),
			
		);
	}
	
	
public function getProjectWiseDetails($pid,$bid)
	{
		$cond="";
	
                    if($pid != "" && $pid > 0)
                    {
                            $cond=" and pm_project.project_id='$pid'";
							// and pm_project.branch_id='$bid'
                    }
                  
                
		
		
		$records=Yii::app()->db->createCommand("SELECT
			pm_project.project_id,
			pm_project.project_no,
			pm_project.project_title,
			pm_project.project_status,
			CONCAT_WS('-', IF(pm_project.project_no IS NULL or pm_project.project_no = '', pm_project.project_id, pm_project.project_no), 
			pm_code_value.code_lbl) AS prj_no

			FROM
			pm_project
			INNER JOIN pm_code_value ON pm_project.project_type = pm_code_value.code_id
			WHERE
			pm_project.project_status IN (3,4)
			$cond
			ORDER BY
			pm_project.project_no ASC,
			pm_project.project_id ASC
			")->queryAll();
			
			return $records;
	}
	

	
	public function getWorkedHrsFromTaskSchedule($pid,$uid)
	{  
	
		$records=Yii::app()->db->createCommand("select IFNULL(sum(totworkedhour),0) as totworkedhour FROM scheduled_total_by_date WHERE user_id='$uid' and project_id='$pid' AND YEARWEEK(schedule_date) = YEARWEEK(CURRENT_DATE - INTERVAL 7 DAY)")->queryRow();
		return $records['totworkedhour'];
	}
	
	public function getTotalWorkedHrsFromTaskSchedule($pid)
	{ 
		$records=Yii::app()->db->createCommand("select IFNULL(sum(totworkedhour),0) as totworkedhour FROM scheduled_total_by_date WHERE project_id='$pid'")->queryRow();
		
		return $records['totworkedhour'];
	}
	
		public function getTotalWorkedHrsByUserFromTaskSchedule($uid)
	{ 
		$records=Yii::app()->db->createCommand("select IFNULL(sum(totworkedhour),0) as totworkedhour FROM scheduled_total_by_date WHERE user_id='$uid'")->queryRow();
		
		return $records['totworkedhour'];
	}
	
		public function getTotalTillThisHrsFromTaskSchedule()
	{  
	
		$records=Yii::app()->db->createCommand("select IFNULL(sum(totworkedhour),0) as totworkedhour FROM scheduled_total_by_date")->queryRow();
		return $records['totworkedhour'];
	}
	
		public function getTotalThisHrsFromTaskSchedule()
	{  
	
		$records=Yii::app()->db->createCommand("select IFNULL(sum(totworkedhour),0) as totworkedhour FROM scheduled_total_by_date WHERE YEARWEEK(schedule_date) = YEARWEEK(CURRENT_DATE - INTERVAL 7 DAY)")->queryRow();
		return $records['totworkedhour'];
	}
	
	public function getWisFromProject($pid)
	{
		$records=Yii::app()->db->createCommand("select wis FROM pm_project WHERE project_id='$pid'")->queryRow();
		return $records['wis'];
		
	}
}
?>