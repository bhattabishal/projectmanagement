<?php

/**
 * This is the model class for table "pm_email_user".
 *
 * The followings are the available columns in table 'pm_email_user':
 * @property integer $email_user_id
 * @property integer $user_id
 * @property string $email_id
 * @property string $password
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 */
class EmailUser extends PMActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $password_repeat;
	public function tableName()
	{
		return 'pm_email_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, email_id, password, password_repeat', 'required'),
			array('user_id, crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			array('email_id, password', 'length', 'max'=>100),
			array('user_id, email_id', 'unique','on'=>'insert'),
			array('password', 'compare'),
			array('email_id', 'email'),
			
			array('password_repeat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('email_user_id, user_id, email_id, password, crtd_by, crtd_dt, updt_by, updt_dt, updt_cnt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'email_user_id' => 'Email User',
			'user_id' => 'User',
			'email_id' => 'Email',
			'password' => 'Password',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
		);
	}
	
	protected function afterValidate()
	{
		parent::afterValidate();
		if(!$this->hasErrors())
			$this->password = $this->hashPassword($this->password);
		}
	public function hashPassword($password)
	{
		return MyCustomClass::encrypt($password);
	}
	
	public function getEmailDetailsById($uid)
	{
		$criteria=new CDbCriteria;
		$criteria->select='*'; 
		$criteria->condition='user_id=:user_id';
		$criteria->params=array(':user_id'=>$uid);
		$user=EmailUser::model()->find($criteria);
		
		return $user;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('email_user_id',$this->email_user_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('email_id',$this->email_id,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);
                 $criteria->with=('user');
		
			if(!Yii::app()->user->checkAccess('viewAllUserEmails'))
			{
				$criteria->addCondition('user_id='.Yii::app()->user->id.'');
			}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'sort'=>array(
                            'defaultOrder'=>'user_name ASC',
                        ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EmailUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
