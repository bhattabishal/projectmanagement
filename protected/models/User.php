<?php

/**
 * This is the model class for table "pm_user".
 *
 * The followings are the available columns in table 'pm_user':
 * @property integer $user_id
 * @property string $user_name
 * @property string $login_name
 * @property string $login_pwd
 * @property string $last_login_time
 * @property integer $branch_id
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 *
 * The followings are the available model relations:
 * @property PmAuthItem[] $pmAuthItems
 * @property PmCommunication[] $pmCommunications
 * @property PmCommunicationAssociate[] $pmCommunicationAssociates
 * @property PmJobAction[] $pmJobActions
 * @property PmJobAssociate[] $pmJobAssociates
 * @property PmProjectAssociates[] $pmProjectAssociates
 * @property PmTask[] $pmTasks
 * @property PmBranch $branch
 */
class User extends PMActiveRecord
{
	public $login_pwd_repeat;
	public $old_password;
         public $branch_name;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_user';
	}
        
        public function scopes() 
         {
                return array(
                    'byName' => array('order' => 'user_name ASC'),
                  );
           }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_name, login_name, login_pwd, branch_id,login_pwd_repeat', 'required'),
			array('branch_id', 'numerical', 'integerOnly'=>true),
			array('user_name, login_name, login_pwd,old_password', 'length', 'max'=>255),
			array('user_name, login_name', 'unique'),
			array('login_name', 'email'),
			array('old_password', 'required' ,'on' => 'changepassword'),
			array('old_password', 'compareCurrentPassword', 'on' => 'changepassword'),
			
			array('login_pwd', 'compare'),
			
			array('login_pwd_repeat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, user_name, login_name,branch_name, login_pwd, last_login_time, branch_id', 'safe', 'on'=>'search'),
		);
		
		
		
	}
	
	/*
		compare user entered password with the old password
	*/
	public function compareCurrentPassword($attribute,$params)
	{
		$user = User::model()->findByPk(Yii::app()->user->id);

		if( md5($this->old_password) !== $user->login_pwd )
	    {
	      $this->addError($attribute,'Old password did not match');
	    }

	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pmAuthItems' => array(self::MANY_MANY, 'PmAuthItem', 'pm_auth_assignment(userid, itemname)'),
			'pmCommunications' => array(self::HAS_MANY, 'PmCommunication', 'user_id'),
			'pmCommunicationAssociates' => array(self::HAS_MANY, 'PmCommunicationAssociate', 'user_id'),
			'pmJobActions' => array(self::HAS_MANY, 'PmJobAction', 'user_id'),
			'pmJobAssociates' => array(self::HAS_MANY, 'PmJobAssociate', 'user_id'),
			'pmProjectAssociates' => array(self::HAS_MANY, 'PmProjectAssociates', 'user_id'),
			'pmTasks' => array(self::HAS_MANY, 'PmTask', 'assigned_to'),
			'branch' => array(self::BELONGS_TO, 'Branch', 'branch_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'user_name' => 'Name',
			'login_name' => 'Email',
			'login_pwd' => 'Password',
			'last_login_time' => 'Last Login Time',
			'branch_id' => 'Branch',
                        'branch.branch_name' => 'Branch',
			'login_pwd_repeat' =>'Confirm Password',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('login_name',$this->login_name,true);
		$criteria->compare('login_pwd',$this->login_pwd,true);
		$criteria->compare('last_login_time',$this->last_login_time,true);
		//$criteria->compare('branch_id',$this->branch_id);
                $criteria->compare('branch.branch_name',$this->branch_name,true);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);
                $criteria->with = array( 
                   'branch'=>array('select'=>'branch.branch_name','together'=>true), 
                   );

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'sort'=>array(
                        'attributes'=>array(
                             'branch_name'=>array(
                                   'asc'=>'branch.branch_name',
                                   'desc'=>'branch.branch_name DESC',
                            ),
                            '*',
                        ),
                    ),
		));
	}
	protected function afterValidate()
	{
		parent::afterValidate();
		if(!$this->hasErrors())
			$this->login_pwd = $this->hashPassword($this->login_pwd);
		}
	public function hashPassword($password)
	{
	return md5($password);
	}
	
	public function validatePassword($password)
	{
	return $this->hashPassword($password)===$this->login_pwd;
	}
	
	public function getUserDetailsById($uid)
	{
		$criteria=new CDbCriteria;
		$criteria->select='*'; 
		$criteria->condition='user_id=:user_id';
		$criteria->params=array(':user_id'=>$uid);
		$user=User::model()->find($criteria);
		
		return $user;
	}
	
	
	public function branchWiseUserList()
	{
		$data = User::model()->byName()->findAll();
		$userArry=array();
			 $i=0;
			 foreach ($data as $user) 
			 {
				     if($user->user_id > 0) 
					 {
					 	$branch=Branch::model()->byName()->find('branch_id=:branchID', array(':branchID'=>$user->branch_id));
						if($branch->branch_type==14)
						{
							$userArry[$user->user_id]="AO : ".$user->user_name;
						}
						
				        $options[$user->user_id] = array('selected' => 'selected');
				    }
					$i++;
				} 
				
			foreach ($data as $user) 
			 {
				     if($user->user_id > 0) 
					 {
					 	$branch=Branch::model()->byName()->find('branch_id=:branchID', array(':branchID'=>$user->branch_id));
						if($branch->branch_type!=14)
						{
							$userArry[$user->user_id]="RPS : ".$user->user_name;
						}
						
				        $options[$user->user_id] = array('selected' => 'selected');
				    }
					$i++;
				}   
				
				return $userArry;  
	}
	
	
	
	
	public function getAoList()
	{
		$data = User::model()->byName()->findAll();
		$userArry=array();
			 $i=0;
			 foreach ($data as $user) 
			 {
				     if($user->user_id > 0) 
					 {
					 	$branch=Branch::model()->byName()->find('branch_id=:branchID', array(':branchID'=>$user->branch_id));
						if($branch->branch_type==14)
						{
							$userArry[$user->user_id]=$user->user_name;
						}
						
				        //$options[$user->user_id] = array('selected' => 'selected');
				    }
					$i++;
				}  
				
				return $userArry;  
	}
	public function getRpsList()
	{
		$data = User::model()->byName()->findAll();
		$userArry=array();
			 $i=0;
			 foreach ($data as $user) 
			 {
				     if($user->user_id > 0) 
					 {
					 	$branch=Branch::model()->byName()->find('branch_id=:branchID', array(':branchID'=>$user->branch_id));
						if($branch->branch_type==18)
						{
							$userArry[$user->user_id]=$user->user_name;
						}
						
				    }
					$i++;
				}  
				
				return $userArry;  
	}
	
	public function getAoRPSList($type='')
	{
		if($type=="ao")
		{
			return $this->getAoList();
		}
		else if($type=="rps")
		{
			return $this->getRpsList();
		}
		else
		{
			$data = User::model()->byName()->findAll();//use of scopes
		    $userArry=array();
			 $i=0;
			 foreach ($data as $user) 
			 {
				     if($user->user_id > 0) 
					 {
					 	
						
						$userArry[$user->user_id]=$user->user_name;
						
						
				    }
					$i++;
				}  
				
				return $userArry;
		}
	}
	
	public function getUsersArrayList()
	{
		
		
			$data = User::model()->findAll(array('order' => 'user_name'));
		    $userArry=array();
			 $i=0;
			 foreach ($data as $user) 
			 {
				     if($user->user_id > 0) 
					 {
					 	
						
						$userArry[$i]=$user->user_id."/".$user->user_name;
						
						$i++;
				    }
					
				}  
				
				return $userArry;
		
	}
        
        /*
         * get users array working in given project
         */
        public function getUsersArrayListByProjectId($pid)
        {
            $data=Project::model()->getProjectUsers($pid);
            $userArry=array();
			 $i=0;
			 foreach ($data as $user) 
			 {
				     if($user['user_id'] > 0) 
					 {
					 	
						
						$userArry[$i]=$user['user_id']."/".$user['user_name'];
						
						$i++;
				    }
					
				}  
				
				return $userArry;
        }
	
	
	public function getAoRpsUser($id)
	{
		
				//method 2 for find
				$user=User::model()->find(array(
				    'select'=>'user_id,user_name,branch_id',
				    'condition'=>'user_id=:userID',
				    'params'=>array(':userID'=>$id),
				));
				
				//method 3 for find
				$branch=Branch::model()->find('branch_id=:branchID', array(':branchID'=>$user->branch_id));
				
				if($branch->branch_type==14)
				{
					return "AO";
				}
				else if($branch->branch_type==18)
				{
					return "RPS";
				}
	}
	
	
	public function checkIfCoordinatorsFromTask($uid,$tid)
	{
		$project=Yii::app()->db->createCommand("SELECT
		pm_communication.project_id,
		pm_project_coordinates.user_id,
		pm_project_coordinates.type
		FROM
		pm_task
		INNER JOIN pm_communication ON pm_task.communication_id = pm_communication.communication_id
		INNER JOIN pm_project_coordinates ON pm_communication.project_id = pm_project_coordinates.project_id
		WHERE
		pm_task.task_id = '$tid' AND
		pm_project_coordinates.user_id = '$uid'")->queryRow();
		
		if($project['user_id'] > 0)
		{
			return TRUE;
		}
		
		return FALSE;
		
	}
	
	public function checkUserIfCoordinator($uid)
	{
		$coordinator=Yii::app()->db->createCommand("SELECT
pm_project_coordinates.user_id,
pm_project_coordinates.type
FROM
pm_project_coordinates
WHERE user_id='$uid'")->queryRow();
		
		if($coordinator['user_id'] > 0)
		{
			return TRUE;
		}
		
		return FALSE;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
