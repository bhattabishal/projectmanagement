<?php

/**
 * This is the model class for table "pm_communication".
 *
 * The followings are the available columns in table 'pm_communication':
 * @property integer $communication_id
 * @property integer $project_id
 * @property string $communication_details
 * @property integer $project_contact_id
 * @property string $communication_date
 * @property integer $user_id
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 * @property string $communication_subject
 * @property integer $project_associates_id
 *
 * The followings are the available model relations:
 * @property PmProject $project
 * @property PmProjectAssociates $projectAssociates
 * @property PmProjectContact $projectContact
 * @property PmUser $user
 * @property PmCommunicationAssociate[] $pmCommunicationAssociates
 * @property PmCommunicationAttachment[] $pmCommunicationAttachments
 * @property PmCommunicationContact[] $pmCommunicationContacts
 * @property PmCommunicationJob[] $pmCommunicationJobs
 * @property PmJob[] $pmJobs
 * @property PmTaskCommunication[] $pmTaskCommunications
 */
class Communication extends PMActiveRecord
{
	public $is_client;
	public $task_communication_id;
	public $project_comm_id;
	public $project_search;
	public $projectCompositeNo;
	public $type;
	public $by_search;
	public $mailNo;
	public $isAttachment;
	public $attachment;
	public $updateType;
	public $associates;
	public $sendAttach;
	public $sendToClient;
	public $branch_id;
	
	
	
	//public $files;
	
	//public $taskCount;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_communication';
	}
	
	

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, communication_date,communication_subject', 'required'),
			array('project_id, crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			array('communication_subject', 'length', 'max'=>255),
			array('communication_details,comm_from,is_client,is_internal,emailId,isAttachment, comm_to,attachment', 'safe'),
			
			array('associates,sendAttach,sendToClient', 'safe'),
			
			
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('communication_id, project_id,project_search,status_text,projectCompositeNo, communication_details, communication_date, crtd_by, crtd_dt, updt_by, updt_dt, updt_cnt,is_client,is_internal,is_private,status, communication_subject,type,user_id,by_search,mailNo,associates,sendAttach,sendToClient,branch_id', 'safe', 'on'=>'search'),
			array('status', 'ifCheckTaskCompleted', 'on' => 'toggle'),
		);
	}
	
	/*
	condition before save
	1. do not allow communication to hide if any active tasks
	*/
	public function ifCheckTaskCompleted($attribute,$params)
	{
		
			$activeTasks=$this->getNotCompletedTaskCount();
		
		//echo "hi".$activeTasks;
		//die;
				//echo count($activeTasks);
				//die;
		if($activeTasks > 0 )
		{
			$this->addError($attribute, 'There are active tasks inside this communication');
			return FALSE;
			
		}
		else{
			return true;
		}
		
		
	}
	
	/*
	check if communication has tasks.It it has any tasks do not allow to delete
	*/
	public function beforeDelete()
        {
                if(parent::beforeDelete())
                {
					$task=Task::model()->findAll(array(
					    'select'=>'task_id',
					    'condition'=>'communication_id=:communication_id',
					
					    'params'=>array(':communication_id'=>$this->communication_id),
						));
						
						
						
					if(count($task) > 0)
					{
						return false;
					}
					else{
						
						return true;
					}
                        
                }
       
        }
	

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Project', 'project_id'),
			//'projectAssociates' => array(self::BELONGS_TO, 'ProjectAssociates', 'project_associates_id'),
			//'userAssociates' => array(self::HAS_ONE, 'User', array('user_id'=>'user_id'),'through'=>'projectAssociates'),
			'projectContact' => array(self::BELONGS_TO, 'ProjectContact', 'project_contact_id'),
			'clientContact' => array(self::HAS_ONE, 'ClientContact',array('contact_id'=>'client_contact_id'), 'through'=>'projectContact'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'branch' => array(self::HAS_ONE, 'Branch',array('branch_id'=>'branch_id'), 'through'=>'user'),
			'commFromBranch'=> array(self::BELONGS_TO, 'Branch', 'comm_from'),
			
			'userType' => array(self::HAS_ONE, 'CodeValue',array('branch_type'=>'code_id'), 'through'=>'branch'),
			
			'codeValueProjectType' => array(self::HAS_ONE, 'CodeValue',array('project_type'=>'code_id'), 'through'=>'project'),
			
			'pmCommunicationAssociates' => array(self::HAS_MANY, 'CommunicationAssociate', 'communication_id'),
			'pmCommunicationAttachments' => array(self::HAS_MANY, 'CommunicationAttachment', 'communication_id'),
			'pmCommunicationContacts' => array(self::HAS_MANY, 'CommunicationContact', 'communication_id'),
			'pmCommunicationJobs' => array(self::HAS_MANY, 'CommunicationJob', 'communication_id'),
			'pmJobs' => array(self::HAS_MANY, 'Job', 'communication_id'),
			'pmTaskCommunications' => array(self::HAS_MANY, 'TaskCommunication', 'communication_id'),
		);
	}
	
	public function getColor()
	{
		
		if($this->comm_from > 0)//not from client
		{
			return $this->commFromBranch->color_code;
		}
		else{
			return "";
		}
	} 
	
	
	
	
	
	public function getClass()
	{
		if(!Yii::app()->user->checkAccess('viewAllRecentCommunication'))
		{
			//do not show internal communication to other users
			if(Yii::app()->user->hasState("branchId"))
			{
				$bid=Yii::app()->user->getState("branchId");
				if($this->is_internal=="Yes")
				{
					if($this->user->branch_id==$bid)
					{
						return "self";
					}
					else{
						return "hidden";
					}
				}
				
			}
			else{
				return "self";
			}
		}
		else{
			return "self";
		}
		
	}
	
	public function getTaskCount()
	{
		$count = Yii::app()->db->createCommand()
    			->select('count(task_id) as actTask')
    			->from('pm_task')
			    ->where("task_status not in(12,26) AND communication_id = $this->communication_id")
			    ->queryRow();
				
				return $count['actTask'];
	}
	
	
	public function getTaskVariation()
	{
		$count = Yii::app()->db->createCommand()
    			->select('count(task_id) as actTask')
    			->from('pm_task')
			    ->where("task_variation='Yes' AND communication_id = $this->communication_id")
			    ->queryRow();
				
				return $count['actTask'];
	}
	
	public function getNotCompletedTaskCount()
	{
		$count = Yii::app()->db->createCommand()
    			->select('count(task_id) as actTask')
    			->from('pm_task')
			    ->where("task_status !=26 AND communication_id = $this->communication_id")
			    ->queryRow();
				
				return $count['actTask'];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'communication_id' => 'Com No',
			'project_id' => 'Prj No',
			'projectCompositeNo' =>'Prj No',
			'project.project_title' => 'Project',
			'project_search' => 'Project',
			'communication_details' => 'Details',
			'project_contact_id' => 'Prj Contact',
			'communication_date' => 'Date',
			'user.user_name'=>'By',
			'user_id' => 'User',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
			'communication_subject' => 'Subject',
			'project_associates_id' => 'Project Associates',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		
  
		$criteria->compare('t.communication_id',$this->communication_id);
		$criteria->compare('t.is_internal',$this->is_internal);
		
		$criteria->compare('project.project_title',$this->project_search,true);
		$criteria->compare('concat(project.project_no,"-",codeValueProjectType.code_lbl)',$this->projectCompositeNo,true);
		$criteria->compare('t.status',$this->status,true);
		//$criteria->compare('t.user_id',$this->by,true);
		$criteria->compare('t.communication_details',$this->communication_details,true);
		//$criteria->compare('project_contact_id',$this->project_contact_id);
		$criteria->compare('t.communication_date',$this->communication_date,true);
		$criteria->compare('user.user_name',$this->by_search,true);
		//$criteria->compare('crtd_by',$this->crtd_by);
		//$criteria->compare('crtd_dt',$this->crtd_dt,true);
		//$criteria->compare('updt_by',$this->updt_by);
		//$criteria->compare('updt_dt',$this->updt_dt,true);
		//$criteria->compare('updt_cnt',$this->updt_cnt);
		$criteria->compare('t.communication_subject',$this->communication_subject,true);
		
		//$criteria->with = array('userType'=>array('select'=>'userType.code_id','together'=>true));
			$criteria->compare('branch.branch_name',$this->branch_id,true);
			
		//$criteria->order = 't.communication_id DESC';
		//$criteria->with='project';
		$criteria->with = array(
		  'project'=>array('select'=>'project.*','together'=>true),
		  'codeValueProjectType'=>array('select'=>'codeValueProjectType.*','together'=>true),
		  'userType'=>array('select'=>'userType.*','together'=>true),
		  'user'=>array('select'=>'user.user_name','together'=>true),
		  'branch'=>array('select'=>'branch.branch_name','together'=>true),
		);

		//$criteria -> join = 'INNER JOIN pm_project project1 on t.project_id = project1.project_id ';
		//$criteria->with='codeValueProjectType';
		
		//$criteria -> join = 'INNER JOIN pm_user user1 on t.user_id = user1.user_id ';
		//$criteria -> join = 'INNER JOIN pm_branch branch1 on user1.branch_id = branch1.branch_id ';
		//$criteria -> join = 'INNER JOIN pm_code_value codevalue2 on branch1.branch_type = codevalue2.code_id ';
		
		//$criteria -> join = 'INNER JOIN pm_user user on t.crtd_by = user.user_id ';
		//$criteria->condition="t.status=0";
		
		// $criteria->addSearchCondition('concat(project.project_no,"-",codeValueProjectType.code_lbl)', $this->projectCompositeNo); 

		
		//$criteria->compare('project_associates_id',$this->project_associates_id);
		
		//only show associates project
		if(!Yii::app()->user->checkAccess('viewAllRecentCommunication'))
		{
			$model=new MyCustomClass();
		  	$pids=$model->getInvolvedProjectsByUser();
			
			//$criteria->addCondition('project1.project_id in ('.$pids.')');
			
			$criteria->addCondition('project.project_id in ('.$pids.')');
			
			$criteria->addCondition('t.is_internal=\'No\' or user.branch_id ='.Yii::app()->user->getState("branchId").'');
			
			
			
		}
		
		
		
		//filter by client,ao,rps
		if($this->type != "")
		{
			
				
			if($this->type == 0)
			{
				$criteria->addSearchCondition('comm_from','0');
			}
			else{
				
				if($this->type == 1)//ao
				{
					$aos=Branch::model()->getAoBranchIdsString();
					//$criteria->addSearchCondition("comm_from in ($aos)");
					$criteria->addCondition("comm_from in ($aos)");
				}
				else//rps
				{
						$rps=Branch::model()->getRpsBranchIdsString();
					//$criteria->addSearchCondition("comm_from in ($rps)");
					$criteria->addCondition("comm_from in ($rps)");
				}
			}
		}
		 
		 //do not show done project
if($this->status == "")
		{
			//active and ready to issue
			//$criteria->addCondition('project.project_status in (3,25)');
			$criteria->addCondition('project.project_status != 5');
			$criteria->addSearchCondition('status','0');
		}
		
		$criteria->addCondition('t.communication_id > 1');
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			   'sort'=>array(
			    'defaultOrder' => 't.communication_id DESC',
		        'attributes'=>array(
		            'project_search'=>array(
		                'asc'=>'project.project_title',
		                'desc'=>'project.project_title DESC',
		            ),
					
		            '*',
		        ),
		    ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Communication the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function getProjectCompositeNo()
	{
		$criteria=new CDbCriteria;
		$criteria->select='*'; 
		$criteria->condition='project_id=:project_id';
		$criteria->params=array(':project_id'=>$this->project_id);
		$proj=Project::model()->find($criteria);
		
		$criteria=new CDbCriteria;
		$criteria->select='*'; 
		$criteria->condition='code_id=:code_id';
		$criteria->params=array(':code_id'=>$proj->project_type);
		$code=CodeValue::model()->find($criteria);
		
		return ($proj->project_no <=0 ? $proj->project_id:$proj->project_no)."-".$code->code_lbl;
	}
	
	
	
	
	
	
}
