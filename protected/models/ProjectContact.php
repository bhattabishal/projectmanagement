<?php

/**
 * This is the model class for table "pm_project_contact".
 *
 * The followings are the available columns in table 'pm_project_contact':
 * @property integer $project_contact_id
 * @property integer $project_id
 * @property integer $contact_id
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 *
 * The followings are the available model relations:
 * @property Communication[] $communications
 * @property ClientContact $contact
 * @property Project $project
 */
class ProjectContact extends PMActiveRecord
{
	public $contact_email;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_project_contact';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, contact_id', 'required'),
			array('project_id, contact_id, crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			array('crtd_dt, updt_dt', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('project_contact_id, project_id, contact_id, crtd_by, crtd_dt, updt_by, updt_dt, updt_cnt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'communications' => array(self::HAS_MANY, 'Communication', 'project_contact_id'),
			'contact' => array(self::BELONGS_TO, 'ClientContact', 'contact_id'),
			'project' => array(self::BELONGS_TO, 'Project', 'project_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'project_contact_id' => 'Project Contact',
			'project_id' => 'Project',
			'contact_id' => 'Contact',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('project_contact_id',$this->project_contact_id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('contact_id',$this->contact_id);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getProjectContacts($pid)
	{
		$criteria2=new CDbCriteria;
		$criteria2->select='t.contact_id,tu.contact_email';  // only select the 'title' column
		$criteria2->join='Inner JOIN `pm_client_contact` as `tu` ON t.contact_id=tu.client_contact_id';
		$criteria2->condition="t.project_id=:pid";
		$criteria2->params = array(':pid'=>$pid);
		
		
		$project_contact=ProjectContact::model()->findAll($criteria2);
		
		return $project_contact;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProjectContact the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
