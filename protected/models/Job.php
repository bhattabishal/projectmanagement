<?php

/**
 * This is the model class for table "pm_job".
 *
 * The followings are the available columns in table 'pm_job':
 * @property integer $job_id
 * @property integer $communication_id
 * @property integer $job_category
 * @property string $job_details
 * @property integer $job_type
 * @property integer $sharable
 * @property string $job_due_date
 * @property string $wis
 * @property string $est_rps_cost
 * @property string $est_ao_cost
 * @property string $actual_rps_cost
 * @property string $actual_ao_cost
 * @property string $done_percentage
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 *
 * The followings are the available model relations:
 * @property PmCommunicationJob[] $pmCommunicationJobs
 * @property PmCodeValue $jobCategory
 * @property PmCommunication $communication
 * @property PmCodeValue $sharable0
 * @property PmCodeValue $jobType
 * @property PmJobAction[] $pmJobActions
 * @property PmJobAssociate[] $pmJobAssociates
 * @property PmJobContact[] $pmJobContacts
 * @property PmTask[] $pmTasks
 */
class Job extends PMActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_job';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('communication_id, job_category, job_type, sharable, job_due_date', 'required'),
			array('communication_id, job_category, job_type, sharable, crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			array('wis, est_rps_cost, est_ao_cost, actual_rps_cost, actual_ao_cost, done_percentage', 'length', 'max'=>18),
			array('job_details, crtd_dt, updt_dt', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('job_id, communication_id, job_category, job_details, job_type, sharable, job_due_date, wis, est_rps_cost, est_ao_cost, actual_rps_cost, actual_ao_cost, done_percentage, crtd_by, crtd_dt, updt_by, updt_dt, updt_cnt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pmCommunicationJobs' => array(self::HAS_MANY, 'CommunicationJob', 'job_id'),
			'jobCategory' => array(self::BELONGS_TO, 'CodeValue', 'job_category'),
			'communication' => array(self::BELONGS_TO, 'Communication', 'communication_id'),
			'sharable0' => array(self::BELONGS_TO, 'CodeValue', 'sharable'),
			'jobType' => array(self::BELONGS_TO, 'CodeValue', 'job_type'),
			'pmJobActions' => array(self::HAS_MANY, 'JobAction', 'job_id'),
			'pmJobAssociates' => array(self::HAS_MANY, 'JobAssociate', 'job_id'),
			'pmJobContacts' => array(self::HAS_MANY, 'JobContact', 'job_id'),
			'pmTasks' => array(self::HAS_MANY, 'Task', 'job_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'job_id' => 'Job',
			'communication_id' => 'Communication',
			'communication.communication_details' => 'Communication Details',
			'job_category' => 'Category',
			'jobCategory.code_lbl'=>'Category',
			'job_details' => 'Details',
			'job_type' => 'Type',
			'jobType.code_lbl'=>'Type',
			'sharable' => 'Sharable',
			'sharable0.code_lbl' => 'Sharable',
			'job_due_date' => 'Due Date',
			'wis' => 'Wis',
			'est_rps_cost' => 'Est Rps Cost',
			'est_ao_cost' => 'Est Ao Cost',
			'actual_rps_cost' => 'Actual Rps Cost',
			'actual_ao_cost' => 'Actual Ao Cost',
			'done_percentage' => 'Done Percentage',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('job_id',$this->job_id);
		$criteria->compare('communication_id',$this->communication_id);
		$criteria->compare('job_category',$this->job_category);
		$criteria->compare('job_details',$this->job_details,true);
		$criteria->compare('job_type',$this->job_type);
		$criteria->compare('sharable',$this->sharable);
		$criteria->compare('job_due_date',$this->job_due_date,true);
		$criteria->compare('wis',$this->wis,true);
		$criteria->compare('est_rps_cost',$this->est_rps_cost,true);
		$criteria->compare('est_ao_cost',$this->est_ao_cost,true);
		$criteria->compare('actual_rps_cost',$this->actual_rps_cost,true);
		$criteria->compare('actual_ao_cost',$this->actual_ao_cost,true);
		$criteria->compare('done_percentage',$this->done_percentage,true);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Job the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
