<?php

/**
 * This is the model class for table "pm_communication_attachment".
 *
 * The followings are the available columns in table 'pm_communication_attachment':
 * @property integer $communication_attachment_id
 * @property integer $communication_id
 * @property string $attachment
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 */
class CommunicationAttachment extends PMActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_communication_attachment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('communication_id', 'required'),
			array('communication_id, crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			array('attachment', 'length', 'max'=>150),
			array('crtd_dt, updt_dt', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('communication_attachment_id, communication_id, attachment, crtd_by, crtd_dt, updt_by, updt_dt, updt_cnt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'communication_attachment_id' => 'Communication Attachment',
			'communication_id' => 'Communication',
			'attachment' => 'Attachment',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('communication_attachment_id',$this->communication_attachment_id);
		$criteria->compare('communication_id',$this->communication_id);
		$criteria->compare('attachment',$this->attachment,true);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CommunicationAttachment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
