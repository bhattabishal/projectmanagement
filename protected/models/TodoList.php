<?php

/**
 * This is the model class for table "pm_todo_list".
 *
 * The followings are the available columns in table 'pm_todo_list':
 * @property string $to_do_id
 * @property integer $user_id
 * @property string $task
 * @property string $date_time
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $status
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 *  * @property CodeValue $taskStatus
 */

class TodoList extends PMActiveRecord
{

    public 	$status_search;
    public  $user_search;
    public  $from_search;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_todo_list';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, task, date_time,actual_date,status', 'required'),
			array('user_id, crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			array('from_id,remarks','safe'),
			array('status', 'checkAssignedTodo', 'on' => 'editable,update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			 array('date_time,actual_date, status', 'required', 'on' => 'update'),
			array('to_do_id, user_id, task, date_time,actual_date, crtd_by, crtd_dt, updt_by, updt_dt, updt_cnt,from_id, status_search,user_search,from_search', 'safe', 'on'=>'search'),
		);
	}
	
	
	/*
	*/
	
	public function checkAssignedTodo($attribute,$params)
	{
		$userId=$this->user_id;
		if(Yii::app()->user->id != $userId)
		{
			$this->addError($attribute,'You are not allowed to change others status.');
			return false;
		}
		
		return true;
		
	}
	
	//for color change corresponding to current shift date
	public function setRowColor()
	{
		
		$this->to_do_id;
		
		//echo $pid;
		//die;
		
		$d1=$this->date_time;
		$d2=$this->actual_date;
			    
		if($d2=="0000-00-00")
		{
			return "";
		}
		
		$diff=MyCustomClass::GetDateDifference($d1, $d2);
		
		
	if($diff > 0)

		{
			return "#fb7474";
		}

		else{
			return "";
		}
		
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
	
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'pmUsers' => array(self::BELONGS_TO, 'User', 'user_id'),
		'taskStatus' => array(self::BELONGS_TO, 'CodeValue', 'status'),
		'fromUsers' => array(self::BELONGS_TO, 'User', 'from_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'to_do_id' => 'To Do',
			'user_id' => 'User',
			'task' => 'Task',
			'date_time' => 'Date',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
			'remarks'=>'Comments',
			'status'=>'Completed',
			'from_id' => 'To',
			'taskStatus.code_lbl' => 'Completed',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$id=Yii::app()->user->id;
		
		$criteria=new CDbCriteria;

		$criteria->compare('to_do_id',$this->to_do_id,true);
		$criteria->compare('pmUsers.user_name',$this->user_search,true);
		$criteria->compare('task',$this->task,true);
		$criteria->compare('fromUsers.user_name',$this->from_search,true);
		$criteria->compare('date_time',$this->date_time,true);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);
		$criteria->compare('remarks',$this->remarks);
    	
        $criteria->compare('taskStatus.code_lbl',$this->status_search,false);
		$criteria -> join = 'INNER JOIN pm_code_value taskStatus on t.status = taskStatus.code_id ';
		$criteria -> join .= 'INNER JOIN pm_user pmUsers on t.user_id = pmUsers.user_id ';
		$criteria -> join .= 'Left JOIN pm_user fromUsers on t.from_id = fromUsers.user_id ';
		
		
		$criteria->addCondition("t.user_id='$id' or t.from_id='$id'");

	if($this->status_search == "")
	{
	
		$criteria->addCondition("t.status='2'");
		
	}
	
	
	
	
		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
                                'pagination'=>false,
				'sort'=>array(
				
				
					'defaultOrder' => 't.date_time ASC',
					'attributes'=>array(
						//'project_search'=>array(
						// 'asc'=>'project.project_title',
						//'desc'=>'project.project_title DESC',
						//),
					
						'*',
		
					),
				),
				
				
	
			));
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PmTodoList the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
