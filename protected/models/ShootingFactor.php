<?php

/**
 * This is the model class for table "pm_shooting_factor".
 *
 * The followings are the available columns in table 'pm_shooting_factor':
 * @property integer $shooting_id
 * @property integer $user_id
 * @property string $shooting_factor
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 */
class ShootingFactor extends PMActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_shooting_factor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, shooting_factor,shooting_year_month', 'required'),
			array('user_id, crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			array('shooting_factor', 'length', 'max'=>10),
			
			array('shooting_year_month','validateDuplicate', 'on' => 'insert,update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('shooting_id, user_id, shooting_factor, shooting_year_month', 'safe', 'on'=>'search'),
		);
	}
	
	
	public function validateDuplicate($attribute,$params)
	{
		if($this->isNewRecord)
		{
			$record = ShootingFactor::model()->find("user_id=:user_id AND shooting_year_month=:shooting_year_month",
			array(
			  ':user_id'=>$this->user_id,
			  ':shooting_year_month'=>$this->shooting_year_month,
			));
			
			if($record->shooting_id > 0)
			{
				$this->addError($attribute, "Duplicate entry for selected date and user.");
				return FALSE;
				
			}
		}
		else
		{
			$record = ShootingFactor::model()->find("user_id=:user_id AND shooting_year_month=:shooting_year_month and shooting_id !=':shooting_id'",
			array(
			  ':user_id'=>$this->user_id,
			  ':shooting_year_month'=>$this->shooting_year_month,
			  ':shooting_id'=>$this->shooting_id,
			));
			
			if($record->shooting_id > 0)
			{
				$this->addError($attribute, "Duplicate entry for selected date and user.");
				return FALSE;
			}
		}
		
		return TRUE;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'shooting_id' => 'Shooting',
			'user_id' => 'User',
			'shooting_factor' => 'Shooting Factor',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('shooting_id',$this->shooting_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('shooting_factor',$this->shooting_factor,true);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);
                $criteria->with=('user');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                     'sort'=>array(
                            'defaultOrder'=>'user_name ASC',
                        ),
		));
	}
	
	public function getByUserId($uid)
	{
		
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ShootingFactor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
