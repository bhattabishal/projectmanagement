<?php

/**
 * This is the model class for table "pm_client_contact".
 *
 * The followings are the available columns in table 'pm_client_contact':
 * @property integer $client_contact_id
 * @property integer $client_id
 * @property string $contact_name
 * @property string $contact_phone
 * @property string $contact_email
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 *
 * The followings are the available model relations:
 * @property Client $client
 * @property CommunicationContact[] $communicationContacts
 * @property JobContact[] $jobContacts
 * @property ProjectContact[] $projectContacts
 */
class ClientContact extends PMActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_client_contact';
	}

          public function scopes() 
        {
                return array(
                    'byName' => array('order' => 'contact_name ASC'),
                  );
        }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contact_name', 'required'),
			array('client_id, crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			array('contact_name, contact_phone, contact_email', 'length', 'max'=>255),
			array('contact_name,contact_email','unique'),
			array('contact_email', 'email'),
			//array('crtd_dt, updt_dt', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('client_contact_id, client_id, contact_name, contact_phone, contact_email, crtd_by, crtd_dt, updt_by, updt_dt, updt_cnt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'client' => array(self::BELONGS_TO, 'Client', 'client_id'),
			'communicationContacts' => array(self::HAS_MANY, 'CommunicationContact', 'client_contact_id'),
			'jobContacts' => array(self::HAS_MANY, 'JobContact', 'client_contact_id'),
			'projectContacts' => array(self::HAS_MANY, 'ProjectContact', 'contact_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'client_contact_id' => 'Client Contact',
			'client_id' => 'Client',
			'client.client_name' =>'Client',
			'contact_name' => 'Name',
			'contact_phone' => 'Phone',
			'contact_email' => 'Email',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('client_contact_id',$this->client_contact_id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('contact_name',$this->contact_name,true);
		$criteria->compare('contact_phone',$this->contact_phone,true);
		$criteria->compare('contact_email',$this->contact_email,true);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ClientContact the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
