<?php

/**
 * This is the model class for table "pm_fixed_task_setting".
 *
 * The followings are the available columns in table 'pm_fixed_task_setting':
 * @property string $id
 * @property integer $project_id
 * @property integer $year
 * @property integer $month
 * @property string $budget
 * @property string $crtd_dt
 * @property integer $crtd_by
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 */
class FixedTaskSetting extends PMActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_fixed_task_setting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, start_date,budget', 'required'),
			array('project_id, crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			array('project_id', 'numerical', 'min'=>1),
			array('budget','match','pattern'=>'/^[0-9]{1,2}(\.[0-9]{0,2})?$/'),
			array('budget', 'length', 'max'=>5),
			array('budget', 'validateBudget', 'on' => 'insert'),
                    
                        array('end_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, project_id, start_date, budget, crtd_dt, crtd_by, updt_by, updt_dt, updt_cnt', 'safe', 'on'=>'search'),
		);
	}
	
	public function validateBudget($attribute,$params)
	{
		
		
		$oldBudget=FixedTaskSetting::model()->find('start_date=:start_dt and project_id=:pid',array(':start_dt'=>$this->start_date,':pid'=>$this->project_id));
		if($oldBudget->id > 0)
		{
			$this->addError($attribute, 'The budget for the selected week is already inserted.');
			return FALSE;
		}
		
		return TRUE;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'task'=>array(self::BELONGS_TO, 'FixedTask', 'project_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_id' => 'Project',
			'start_date' => 'Start Date',
			//'task.title'=>'Title',
			'budget' => 'Budget',
			'crtd_dt' => 'Created Date',
			'crtd_by' => 'Crtd By',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('start_date',$this->start_date);
                $criteria->with=('task');
		$criteria->compare('task.title',$this->id,true);
		$criteria->compare('budget',$this->budget,true);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'sort'=>array(
                            'defaultOrder'=>'title ASC',
                        ),
		));
	}
	
	public function getMonthsArray()
    {
        for($monthNum = 1; $monthNum <= 12; $monthNum++){
            $months[$monthNum] = date('F', mktime(0, 0, 0, $monthNum, 1));
        }

        return array(0 => 'Month:') + $months;
    }
    
    public function getYearsArray()
    {
        $thisYear = date('Y', time());

        for($yearNum = $thisYear; $yearNum >= 1920; $yearNum--){
            $years[$yearNum] = $yearNum;
        }

        return array(0 => 'Year:') + $years;
    }
    
    
    
    public function getWeeksArray()
    {
    	$currentdate=MyCustomClass::addDays(date('Y-m-d'),15);
		$start = new DateTime('2014-08-01');
		$end = new DateTime($currentdate);
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($start, $interval, $end);
		
		$results=array();

		foreach ($period as $dt)
		{
		    if ($dt->format("N") == 7)
		    {
		        //echo $dt->format("d-M-Y") . "<br>\n";
		        $results[$dt->format("Y-m-d")]=$dt->format("d-M-Y");
		    }
		}
		
		return array_reverse($results);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FixedTaskSetting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
