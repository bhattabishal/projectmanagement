<?php
class PerformanceReport extends CFormModel
{
	public $userId;
	public $frommonth;
	public $tomonth;
	
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('frommonth, tomonth', 'required'),
			// email has to be a valid email address
			array('userId', 'safe'),
			
		);
	}
}
?>