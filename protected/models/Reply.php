<?php

/**
 * This is the model class for table "pm_reply".
 *
 * The followings are the available columns in table 'pm_reply':
 * @property string $reply_id
 * @property string $inbox_id
 * @property string $email_from
 * @property string $email_to
 * @property string $email_cc
 * @property string $subject
 * @property string $message
 * @property integer $crtd_by
 * @property string $crtd_dt
 */
class Reply extends PMActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $uid;
	public $msg_no;
	public $fromName;
	public $fromAddress;
	public $mail_to;
	public $mail_cc;
	public $mainSubject;
	public $mainMessage;
	public $mail_date; 
	 public $updateType;
	 public $files;
	
	public function tableName()
	{
		return 'pm_reply';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email_from, email_to, message', 'required'),
			array('email_to,email_cc', 'ifCheckValidEmail', 'on' => 'compose'),
			//array('crtd_by', 'numerical', 'integerOnly'=>true),
			array('inbox_id', 'length', 'max'=>11),
			array('email_from', 'length', 'max'=>50),
			array('email_to', 'length', 'max'=>150),
			array('email_cc, subject', 'length', 'max'=>250),
			array('uid,msg_no,fromName, fromAddress, mail_to,mail_cc,mainSubject,mainMessage,mail_date,files', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('reply_id, inbox_id, email_from, email_to, email_cc, subject, message, crtd_by, crtd_dt', 'safe', 'on'=>'search'),
			
			
		);
	}
	
	/*
	custom validation to check for single or multiple email(seperated by comma) ids if they are valid email ids
	*/
	public function ifCheckValidEmail($attribute,$params)
	{
		$return=TRUE;
		
		//echo $this->email_to;
		//die;
			
			if($this->email_to != "" && strpos($this->email_to,',') !== false)
			{
				$emailTo=explode(',',trim($this->email_to));
				foreach($emailTo as $val)
				{
					if(!filter_var($val, FILTER_VALIDATE_EMAIL))
					{
						$this->addError('email_to', 'To Email address is invalid');
						$return=FALSE;
						
					}
				}
			}
			else
			{
				
				$emailTo=$this->email_to;
				if($emailTo != "")
				{
					if(!filter_var($emailTo, FILTER_VALIDATE_EMAIL))
					{
						$this->addError('email_to', 'To Email address is invalid');
						$return=FALSE;
					}
				}
				
			}
		
		
			if($this->email_cc != "" && strpos($this->email_cc,',') !== false)
			{
				$emailCc=explode(',',trim($this->email_cc));
				foreach($emailCc as $val)
				{
					if(!filter_var($val, FILTER_VALIDATE_EMAIL))
					{
						$this->addError('email_cc', 'CC Email address is invalid');
						$return=FALSE;
						
					}
				}
			}
			else
			{
				
				$emailCc=$this->email_cc;
				if($emailCc != "")
				{
					if(!filter_var($emailCc, FILTER_VALIDATE_EMAIL))
					{
						$this->addError('email_cc', 'CC Email address is invalid');
						$return=FALSE;
					}
				}
				
			}
		
		
		return $return;
		
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'reply_id' => 'Reply',
			'inbox_id' => 'Inbox',
			'email_from' => 'From',
			'email_to' => 'To',
			'email_cc' => 'Cc',
			'subject' => 'Subject',
			'message' => 'Message',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('reply_id',$this->reply_id,true);
		$criteria->compare('inbox_id',$this->inbox_id,true);
		$criteria->compare('email_from',$this->email_from,true);
		$criteria->compare('email_to',$this->email_to,true);
		$criteria->compare('email_cc',$this->email_cc,true);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reply the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
