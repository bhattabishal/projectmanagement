<?php

/**
 * This is the model class for table "pm_inbox".
 *
 * The followings are the available columns in table 'pm_inbox':
 * @property string $inbox_id
 * @property string $msg_no
 * @property string $from
 * @property string $mail_from
 * @property string $mail_to
 * @property string $mail_cc
 * @property string $subject
 * @property string $message
 * @property string $mail_date
 * @property integer $crtd_by
 * @property string $crtd_dt
 */
class Inbox extends PMActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $attachments;
	public function tableName()
	{
		return 'pm_inbox';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('msg_no, fromAddress, mail_to, message, mail_date', 'required'),
			//array('crtd_by', 'numerical', 'integerOnly'=>true),
			array('msg_no, fromAddress,fromName', 'length', 'max'=>150),
			//array('fromName,fromAddress, mail_to', 'length', 'max'=>150),
			array('subject,mail_to', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('inbox_id,uid, msg_no, fromName,fromAddress, mail_to, mail_cc, subject, message, mail_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'pmInboxAttachments' => array(self::HAS_MANY, 'InboxAttachment', 'inbox_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'inbox_id' => 'Inbox',
			'msg_no' => 'Msg No',
			'fromAddress' => 'From',
			'fromName' => 'Mail From',
			'mail_to' => 'Mail To',
			'mail_cc' => 'Mail Cc',
			'subject' => 'Subject',
			'message' => 'Message',
			'attachments'=>'Attachments',
			'mail_date' => 'Mail Date',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('inbox_id',$this->inbox_id,true);
		$criteria->compare('msg_no',$this->msg_no,true);
		$criteria->compare('fromAddress',$this->fromAddress,true);
		$criteria->compare('fromName',$this->fromName,true);
		$criteria->compare('mail_to',$this->mail_to,true);
		$criteria->compare('mail_cc',$this->mail_cc,true);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('mail_date',$this->mail_date,true);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		
		if(!Yii::app()->user->checkAccess('viewAllArchives'))
		{
			$criteria->addCondition('crtd_by='.Yii::app()->user->id.'');
		}
		
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Inbox the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
