<?php

/**
 * This is the model class for table "pm_project".
 *
 * The followings are the available columns in table 'pm_project':
 * @property string $project_id
 * @property string $project_title
 * @property integer $project_status
 * @property integer $project_type
 * @property integer $client_id
 * @property string $project_date
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 */
class Project extends PMActiveRecord
{
	public $client_contact_id;
	public $project_associates_id;
	public $coord_ao;
	public $coord_rps;
	public $project_comments;
	public $project_comments_ao;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_project';
	}
	
	//public function setClient_contact_id()
	//{
		//return '';
	//}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_title,project_no, project_status,branch_id, project_type, client_id, project_date,wis,est_rps_wis,est_ao_wis,actual_rps_hrs,actual_ao_hrs,done_percentage,client_contact_id,project_associates_id,coord_ao,coord_rps', 'required'),
			array('project_status, project_type, client_id,project_no', 'numerical', 'integerOnly'=>true),
			array('wis,est_rps_wis,est_ao_wis,actual_rps_hrs,actual_ao_hrs,done_percentage', 'numerical', 'integerOnly'=>FALSE),
			array('project_title', 'length', 'max'=>150),
			array('project_title','unique'),
			array('done_percentage', 'numerical', 'integerOnly'=>false,'max'=>100),
			array('project_comments', 'safe'),
			
			//array('project_status','in','range'=>self::getProjectStatusAllowedRange()),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('project_id, project_no,project_title, project_status, project_type, client_id, project_date,project_comments,project_comments_ao', 'safe', 'on'=>'search'),
		);
	}
	
	/*
	validation before delete
	if no task created or communication inside project then we can delete the project
	but we only checked here if has communication or not since task created inside communication
	*/
	public function beforeDelete()
        {
                if(parent::beforeDelete())
                {
					$comm=Communication::model()->findAll(array(
					    'select'=>'project_id',
					    'condition'=>'project_id=:project_id',
					
					    'params'=>array(':project_id'=>$this->project_id),
						));
						
						
						
					if(count($comm) > 0)
					{
						return false;
					}
					else{
						
						return true;
					}
                        
                }
       
        }
	
	//public static function getProjectStatusAllowedRange()
	//{
		//return array(1,2,3);
	//}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'pmCommunications' => array(self::HAS_MANY, 'PmCommunication', 'project_id'),
			'projectType' => array(self::BELONGS_TO, 'CodeValue', 'project_type'),
			'client' => array(self::BELONGS_TO, 'Client', 'client_id'),
			'projectStatus' => array(self::BELONGS_TO, 'CodeValue', 'project_status'),
			'created' => array(self::BELONGS_TO, 'User', 'crtd_by'),
			'pmProjectAssociates' => array(self::HAS_MANY, 'ProjectAssociates', 'project_id'),
			'pmProjectContacts' => array(self::HAS_MANY, 'ProjectContact', 'project_id'),
			'pmProjectContactsContacts' => array(self::HAS_MANY, 'ClientContact',array('contact_id'=>'client_contact_id'), 'through'=>'pmProjectContacts'),
			'pmProjectComments' => array(self::HAS_MANY, 'ProjectComments', 'project_id'),
			'pmCommunications' => array(self::HAS_MANY, 'Communication', 'project_id'),
			'pmTasks' => array(self::HAS_MANY,'Task',array('communication_id'=>'communication_id'), 'through'=>'pmCommunications'),
			'pmProjectCommentsUser' => array(self::HAS_ONE, 'User',array('crtd_by'=>'user_id'), 'through'=>'pmProjectComments'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'project_id' => 'No',
			'project_title' => 'Title',
			'projectStatus.code_lbl' => 'Status',
			'projectType.code_lbl' => 'Type',
			'project_comments' => 'Comments',
			'project_associates_id' => 'Project Associates',
			'client_id' =>'Client',
			'client_contact_id'=>'Client Contact',
			'client.client_name' => 'Client Name',
			'job_due_date' => 'Due date',
			'client.client_id' => 'Client',
			'created.user_name' => 'Created By',
			'project_date' => 'Date',
			'done_percentage'=>'Done %',
			'branch_id'=>'Branch',
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('project_id',$this->project_id,true);
		$criteria->compare('project_title',$this->project_title,true);
		$criteria->compare('project_status',$this->project_status);
		$criteria->compare('project_type',$this->project_type);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('project_date',$this->project_date,true);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Project the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getProjectNoById($pid)
	{
		$criteria=new CDbCriteria;
		$criteria->select='*'; 
		$criteria->condition='project_id=:project_id';
		$criteria->params=array(':project_id'=>$pid);
		$proj=Project::model()->find($criteria);
		
		$criteria=new CDbCriteria;
		$criteria->select='*'; 
		$criteria->condition='code_id=:code_id';
		$criteria->params=array(':code_id'=>$proj->project_type);
		$code=CodeValue::model()->find($criteria);
		
		return ($proj->project_no <=0 ? $proj->project_id:$proj->project_no)."-".$code->code_lbl;
	}
	
	/*
         * get users working in given project
         */
	public function getProjectUsers($pid)
	{
		$result= Yii::app()->db->createCommand("SELECT

		DISTINCT pm_task_schedule.user_id,pm_user.user_name

		FROM
		pm_task_schedule
		INNER JOIN pm_task ON pm_task_schedule.task_id = pm_task.task_id
		INNER JOIN pm_communication ON pm_task.communication_id = pm_communication.communication_id
		INNER JOIN pm_project ON pm_communication.project_id = pm_project.project_id
		INNER JOIN pm_user ON pm_task_schedule.user_id = pm_user.user_id
		WHERE pm_project.project_id=$pid")->queryAll();
		
		return $result;
		
		
	}
	
	public function getAllProjectsNo($limit=0,$offset=0,$fixed=0,$bid=0)
	{
		$criteria=new CDbCriteria;
		$criteria->select='*'; 
		//$criteria->condition='project_status != 5';//excluding done projects
		$append="";
		if($fixed <= 0)
		{
			$criteria->condition='project_id > 0';//excluding admin projects
                        $append=" and";
		}
		
		
		if($limit > 0)
		{
			$criteria->limit=$limit;
		}
		if($offset > 0)
		{
			$criteria->offset=$offset;
		}
                
                if($bid > 0)
                {
                    $criteria->condition.=" $append branch_id ='$bid'";
                }
                
		$criteria->order='project_no ASC';
		$projs=Project::model()->findAll($criteria);
		
		
		$rtnarry=array();
		foreach($projs as $proj)
		{
			$criteria=new CDbCriteria;
			$criteria->select='*'; 
			
			$criteria->condition='code_id=:code_id';
			$criteria->params=array(':code_id'=>$proj->project_type);
			$code=CodeValue::model()->find($criteria);
			
			$rtnarry[$proj->project_id]=($proj->project_no <=0 ? $proj->project_id:$proj->project_no)."-".$code->code_lbl."(".$proj->project_title.")";
		}
		
		
		
		return $rtnarry;
	}
        
        public function getProjectNoTitle()
        {
            return $this->project_no."-".$this->projectType->code_lbl.'('.$this->project_title.")";
        }
		
	/////////////same typegetAllProjectsNo
	
	public function getAllBranchUsersProjectsNo($limit=0,$offset=0,$fixed=0,$bid=0)
	{
		$qry="SELECT DISTINCT
 p.project_id,p.project_no,p.project_title,cv.code_lbl,p.branch_id,
 concat(p.project_no,'-','cv.code_lbl','(',p.project_title,')')as projectNoTitle

FROM
pm_task_schedule AS ts
INNER JOIN pm_user AS u on ts.user_id = u.user_id
INNER JOIN pm_branch AS b on u.branch_id = b.branch_id

INNER JOIN pm_task AS t on ts.task_id = t.task_id
INNER JOIN pm_communication AS c on t.communication_id = c.communication_id
INNER JOIN pm_project AS p on c.project_id = p.project_id
INNER JOIN pm_code_value AS cv on p.project_type = cv.code_id

WHERE 
b.branch_type='18' AND p.project_no>0 AND t.task_status <> 26 AND (u.branch_id='$bid' or p.branch_id = '$bid')
ORDER BY
p.project_no ASC";
	
	
	$records=Yii::app()->db->createCommand($qry)->queryAll();
       $project=array();
	  		
	  		foreach($records as $row)
	  		{
				$project[$row['project_id']]=$row['project_no'].'-'.$row['code_lbl']."(".$row['project_title'].')';
			}
	return $project;
		
	}
	
		public function getAllBranchUsersProjectsNo1($limit=0,$offset=0,$fixed=0,$bid=0)
	{
		$qry="SELECT DISTINCT
 p.project_id,p.project_no,cv.code_lbl,p.project_title,p.branch_id,
 concat(p.project_no,'-','cv.code_lbl','(',p.project_title,')')as projectNoTitle

FROM
pm_task_schedule AS ts
INNER JOIN pm_user AS u on ts.user_id = u.user_id
INNER JOIN pm_branch AS b on u.branch_id = b.branch_id

INNER JOIN pm_task AS t on ts.task_id = t.task_id
INNER JOIN pm_communication AS c on t.communication_id = c.communication_id
INNER JOIN pm_project AS p on c.project_id = p.project_id
INNER JOIN pm_code_value AS cv on p.project_type = cv.code_id

WHERE 
b.branch_type='18' AND p.project_no>0 AND (u.branch_id='$bid' or p.branch_id = '$bid')
ORDER BY
p.project_no ASC";
	
	
	$records=Yii::app()->db->createCommand($qry)->queryAll();
       $project=array();
	  		
	  		foreach($records as $row)
	  		{
				$project[$row['project_id']]=$row['project_no'].'-'.$row['code_lbl']."(".$row['project_title'].')';
			}
	return $project;
		
	}
	
	
        
      //assciates and coordinates
	public function getAssociatedAndCoordinatedProjectsNo($bid=0)
	{
		if(Yii::app()->user->checkAccess('viewAllProject'))
		{
                    if($bid > 0)
                    {
                        return $this->getAllProjectsNo(0,0,0,$bid);
                    }
                    else
                    {
                        return $this->getAllProjectsNo(0,0,0);
                    }
			
		}
		else
		{
                    $cond="";
                    if($bid > 0)
                    {
                        $cond=" and pm_project.branch_id='$bid'";
                    }
			$id=Yii::app()->user->id;
			$sql="select * from((SELECT
pm_code_value.code_lbl,
pm_project.project_id,
pm_project.project_no,
pm_project.project_title
FROM
pm_project_coordinates
INNER JOIN pm_project ON pm_project_coordinates.project_id = pm_project.project_id
INNER JOIN pm_code_value ON pm_project.project_type = pm_code_value.code_id
WHERE pm_project_coordinates.user_id='$id' $cond
)

UNION

(SELECT
pm_code_value.code_lbl,
pm_project.project_id,
pm_project.project_no,
pm_project.project_title
FROM
pm_project_associates
INNER JOIN pm_project ON pm_project_associates.project_id = pm_project.project_id
INNER JOIN pm_code_value ON pm_project.project_type = pm_code_value.code_id
WHERE
pm_project_associates.user_id = '$id' $cond))t1
ORDER BY project_no DESC,
code_lbl ASC";
			//check associcates
			$records=Yii::app()->db->createCommand($sql)->queryAll();
			
	  		$projects=array();
	  		
	  		foreach($records as $row)
	  		{
				$projects[$row['project_id']]=$row['project_no'].'-'.$row['code_lbl']."(".$row['project_title'].')';
			}
			
			return $projects;
		}
	}

	/*
	before saving validation for unique project no
	*/
	public function beforeSave()
        {
                if(parent::beforeSave())
                {
					
		
					if($this->isNewRecord)
					{
						
						
						$oldpno=Project::model()->findAll(array(
					    'select'=>'project_id',
					    'condition'=>'project_no=:project_no and project_type=:project_type',
					
					    'params'=>array(':project_no'=>$this->project_no,
						':project_type'=>$this->project_type),
						));
						//echo count($oldpno);
						if(count($oldpno)>0)
						{
							$this->addError('project_no', 'The project no for selected type is already in use');
							 return false;
						}
						else{
							
						}
						 return true;
					
						
					}
					else{
							
						
						$criteria=new CDbCriteria;
						$criteria->select='project_id'; 
						$criteria->condition="project_no=:project_no AND project_type=:project_type";
						$criteria->addCondition("project_id <> $this->project_id");
						$criteria->params = array(
						  ':project_no' => $this->project_no,
						  ':project_type' => $this->project_type,
						   //':project_id ' => $this->project_id,
						);
						
						$oldpno=Project::model()->findAll($criteria);
						//echo count($oldpno);
						if(count($oldpno)>0)
						{
							$this->addError('project_no', 'The project no for selected type is already in use');
							 return false;
						}
						 return true;
					}
					
					
					
                        
                }
       
        }
		
		/*
		get all or selected project details from project table
		*/
		public function getAllOrSelectedProjectDetails($pid=0)
		{
			$cond='';
			if($pid > 0)
			{
				$cond=" where  pm_project.project_id='$pid'";
			}
			$qry="SELECT
pm_code_value.code_lbl,
pm_project.project_id,
concat(pm_project.project_no,'-',pm_code_value.code_lbl) as pno,
pm_project.project_title
FROM
pm_project
INNER JOIN pm_code_value ON pm_project.project_type = pm_code_value.code_id $cond";

			$records=Project::model()->findAll($criteria);
			
			return $records;
 
		}
		
		
}
