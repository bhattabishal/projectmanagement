<?php
class UserWiseScheduleReport extends CFormModel
{
	public $projectId;
	public $startDt;
	public $endDt;
     
    public $adminTasks;
	public $user_id;
	public $branch_id;
	public $task_id;
	public function rules()
	{
		return array(
			
			array('startDt, endDt,user_id', 'required'),
			array('projectId,adminTasks','safe'),
			
			
		
		);
	}

	

	public function getTotAssignedHrsFromProjectDate($pid,$date,$uid)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(tothour),0) as actualhrs FROM scheduled_total_by_date WHERE schedule_date='$date' and project_id='$pid' and user_id='$uid'")->queryRow();
		return $records['actualhrs'];
	}
	
	public function getTotAssignedHrsFromFixedTaskDate($tid,$date)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(tothour),0) as actualhrs FROM scheduled_total_by_date_fixed_tasks WHERE schedule_date='$date' and task_id='$tid' ")->queryRow();
		return $records['actualhrs'];
	}
	
	
	public function getTotAssignedHrsFromProject($pid,$uid)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(tothour),0) as actualhrs FROM scheduled_total_by_date WHERE  project_id='$pid' and user_id='$uid'")->queryRow();
		return $records['actualhrs'];
	}
	
	
	public function getTotAssignedHrsFromTask($tid)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(tothour),0) as actualhrs FROM scheduled_total_by_date_fixed_tasks WHERE  task_id='$tid' and YEAR(schedule_date) = YEAR(CURDATE()) ")->queryRow();
		return $records['actualhrs'];
	}
	
	
	
	public function getTotWorkedHrsFromProjectDate($pid,$date,$uid)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(tothour),0) as workedhrs FROM actual_total_by_date WHERE task_date='$date' and project_id='$pid' and user_id ='$uid'")->queryRow();

		return $records['workedhrs'];
	}
	
	public function getTotWorkedHrsFixedTaskFromTaskDate($tid,$date)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(tothour),0) as workedhrs FROM actual_total_by_date_fixed_tasks WHERE task_date='$date' and task_id='$tid'")->queryRow();

		return $records['workedhrs'];
	}
	
	public function getTotWorkedHrsFromProject($pid,$uid)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(tothour),0) as workedhrs FROM actual_total_by_date WHERE  project_id='$pid' and user_id='$uid'")->queryRow();

		return $records['workedhrs'];
	}
	
	public function getTotWorkedHrsFromTask($tid)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(tothour),0) as workedhrs FROM actual_total_by_date_fixed_tasks WHERE  task_id='$tid' and  YEAR(task_date) = YEAR(CURDATE())")->queryRow();

		return $records['workedhrs'];
	}
	
	public function getFixedTaskByUser($uid,$date,$tid)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(normal_hour+overtime_hour),0) as actualhrs FROM pm_actual_timeline WHERE  user_id='$uid' AND task_date ='$date' AND task_id='$tid'")->queryRow();
		return $records['actualhrs'];
	}
		public function getFixedTotalTaskByUser($uid,$tid)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(normal_hour+overtime_hour),0) as actualhrs FROM pm_actual_timeline WHERE  user_id='$uid' AND task_id='$tid' AND YEAR(task_date) = YEAR(CURDATE())")->queryRow();
		return $records['actualhrs'];
	}

	public function getTotalShootingFactor($uid)
	{
		$records=Yii::app()->db->createCommand("select IFNULL(sum(shooting_factor),0) as shoot FROM pm_shooting_factor WHERE user_id='$uid'")->queryRow();
		return $records['shoot'];
	}
	public function getShootingFactorByUser($uid,$edate)
	{
		$records=Yii::app()->db->createCommand("SELECT shooting_factor as shoot FROM pm_shooting_factor WHERE user_id ='$uid' and shooting_year_month='$edate'")->queryRow();
		return $records['shoot'];
	}
	//UserWiseScheduleReport
	
 public function getInvolvedProjectsByUser($pid,$uid,$fxtTask,$tid)
  {
 
 	$records="";
   if($fxtTask  != "" && $fxtTask > 0)
                {
                  $records=MyCustomClass::getFixedProjectsByUserId($uid,$tid);
                }
				else 
				{
				       if($uid > 0 && $pid != "" && $pid >0)
						{
						$records=MyCustomClass::getProjectsByUserId($uid,$pid);
						
						}
						
						else
						{
							$records=MyCustomClass::getInvolvedProjectsByUserId($uid,$pid);
						}
	
		}
	
	 return $records;
  	
  
  }


}
?>