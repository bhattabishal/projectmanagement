<?php

Class ClientInstruction extends CFormModel
{
	public $projectNo;
	public $communicationNo;
	public $subject;
	public $description;
	public $allocatedHours;
	public $comments;
	public $date;
	
	
	
	
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('projectNo',$this->projectNo);
		$criteria->compare('communicationNo',$this->communicationNo);
		$criteria->compare('subject',$this->subject);
		$criteria->compare('description',$this->description);
		$criteria->compare('allocatedHours',$this->allocatedHours,true);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

?>