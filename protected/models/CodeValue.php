<?php

/**
 * This is the model class for table "pm_code_value".
 *
 * The followings are the available columns in table 'pm_code_value':
 * @property integer $code_id
 * @property string $code_lbl
 * @property string $code_type
 * @property integer $order
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 *
 * The followings are the available model relations:
 * @property Branch[] $branches
 * @property Checklist[] $checklists
 * @property Job[] $jobs
 * @property Job[] $jobs1
 * @property Job[] $jobs2
 * @property JobAction[] $jobActions
 * @property Project[] $projects
 * @property Project[] $projects1
 * @property ProjectAssociates[] $projectAssociates
 * @property Task[] $tasks
 * @property Task[] $tasks1
 * @property Task[] $tasks2
 * @property TaskChecklist[] $taskChecklists
 */
class CodeValue extends CActiveRecord
{
     public function scopes() 
        {
                return array(
                    'byLabel' => array('order' => 'code_lbl ASC'),
                    
                  );
        }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_code_value';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code_lbl', 'required'),
			array('order, crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			array('code_lbl, code_type', 'length', 'max'=>255),
			array('crtd_dt, updt_dt', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('code_id, code_lbl, code_type, order, crtd_by, crtd_dt, updt_by, updt_dt, updt_cnt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'branches' => array(self::HAS_MANY, 'Branch', 'branch_type'),
			'checklists' => array(self::HAS_MANY, 'Checklist', 'job_type_id'),
			'jobs' => array(self::HAS_MANY, 'Job', 'job_category'),
			'jobs1' => array(self::HAS_MANY, 'Job', 'sharable'),
			'jobs2' => array(self::HAS_MANY, 'Job', 'job_type'),
			'jobActions' => array(self::HAS_MANY, 'JobAction', 'job_status'),
			'projects' => array(self::HAS_MANY, 'Project', 'project_status'),
			'projects1' => array(self::HAS_MANY, 'Project', 'project_type'),
			'projectAssociates' => array(self::HAS_MANY, 'ProjectAssociates', 'is_coordinator'),
			'tasks' => array(self::HAS_MANY, 'Task', 'task_cat'),
			'tasks1' => array(self::HAS_MANY, 'Task', 'sharable'),
			'tasks2' => array(self::HAS_MANY, 'Task', 'task_status'),
			'taskChecklists' => array(self::HAS_MANY, 'TaskChecklist', 'status'),
			'codeMaster' => array(self::BELONGS_TO, 'CodeValueMaster', 'code_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'code_id' => 'Code',
			'code_lbl' => 'Code Lbl',
			'code_type' => 'Code Type',
			'order' => 'Order',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('code_id',$this->code_id);
		$criteria->compare('code_lbl',$this->code_lbl,true);
		$criteria->compare('code_type',$this->code_type);
		$criteria->compare('order',$this->order);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);

    	return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/*
	@param code_type
	*/
	public function getValuesArryByType($type)
	{
                $values=CodeValue::model()->byLabel()->findAll(array('condition'=>"t.code_type='$type'"));// used scopes 'byLabel'
		//$values=CodeValue::model()->findAll(array('order'=>'order', 'condition'=>'code_type=:x', 'params'=>array(':x'=>$type)));
		//$values=CodeValue::model()->findAll(array('order'=>'t.order', 'condition'=>"t.code_type='$type'"));
		
		$rtnarry=array();
		foreach($values as $val)
		{
			$rtnarry[$val->code_id]=$val->code_lbl;
		}
		
		return $rtnarry;
	}
	
	/*
	same as above only difference is the text(code_lbl) in array key instead of code_id.
	In some old models text based search is enabled and may meshup the code if used above function
	this function is used in minimal cases to search the grid by text based value
	
	@param code_type
	*/
	public function getTextValuesArryByType($type)
	{
			$cond="";
			if($type=="job_status")
			{
				$cond="and code_id not in (27)";
			}
		//$values=CodeValue::model()->findAll(array('order'=>'t.order', 'condition'=>"t.code_type='$type' $cond"));
		 $values=CodeValue::model()->byLabel()->findAll(array('condition'=>"t.code_type='$type'"));// used scopes 'byLabel'
		
		$rtnarry=array();
		foreach($values as $val)
		{
			$rtnarry[$val->code_lbl]=$val->code_lbl;
		}
		
		return $rtnarry;
	}
	
	/*
	used to get the value,i.e many have auto increment number as value but some are user defined which can be changed later
	@param code_type,code_id
	*/
	public function getCodeTypeValue($type,$id)
	{
		$values=CodeValue::model()->find(array('condition'=>"t.code_type='$type'  and t.code_id='$id'"));
		return $values->value;
	}
	


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CodeValue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
