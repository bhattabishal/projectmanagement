<?php

/**
 * This is the model class for table "pm_project".
 *
 * The followings are the available columns in table 'pm_project':
 * @property integer $project_id
 * @property string $project_title
 * @property integer $project_status
 * @property integer $project_type
 * @property integer $client_id
 * @property integer $client_id
 * @property string $project_date
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 *
 * The followings are the available model relations:
 * @property PmCommunication[] $pmCommunications
 * @property PmCodeValue $projectType
 * @property PmBranch $branch
 * @property PmClient $client
 * @property PmCodeValue $projectStatus
 * @property PmProjectAssociates[] $pmProjectAssociates
 * @property PmProjectContact[] $pmProjectContacts
 */
class HomeForm extends CActiveRecord
{
	
	
	public $projectCompositeNo;
	public $project_comments;
	public $project_comments_ao;
	public $coordinatorfilter;
	public $clientDueDate;
	public $rps_coord;
	public $ao_coord;
	
	
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_project';
	}
	//public function defaultScope()
	//{ 
		//$model=new MyCustomClass();
		//$pids=$model->getAssociatesProjectsByUser();
		//if(!Yii::app()->user->checkAccess('viewAllProject'))
		//{
			//return array(
               // 'condition'=>'t.project_id in ('.implode(",",$pids).')',         
           // ); 
		//}        
                
	//}  
	
	/*
	public function getabc()
	{
		return $this->project_id;
	}*/
	
        /*
         * return color based on project client due date.if no client due date as it was added later, task due date is considered for the row color
         * if due date is less than or equal to current date then color =#fb7474
         * if one day remaning for due date then color=#fdc152
         * if two days remaining for due date then color=#ffff00
         * else color is =#fffbf0
         */
        
	public function setRowColor()
	{
		
		$pid=$this->project_id;
		
		//echo $pid;
		//die;
		
		$duedate = Yii::app()->db->createCommand()
    			->select('MIN(pm_task.job_due_date) as due_date')
    			->from('pm_task')
			    ->join('pm_communication', 'pm_task.communication_id = pm_communication.communication_id')
			    ->where("pm_task.task_status not in(12,26) AND pm_communication.project_id = $pid")
			    ->queryRow();
			    
		$clientDueDate=Yii::app()->db->createCommand()
    			->select('MIN(pm_task.job_client_due_date) as due_date')
    			->from('pm_task')
			    ->join('pm_communication', 'pm_task.communication_id = pm_communication.communication_id')
			    ->where("pm_task.task_status not in(12,26) AND pm_communication.project_id = $pid")
			    ->queryRow();
			    
			    
			    $date="";
			    
			    if($clientDueDate['due_date'] =="")
			    {
					$date=$duedate['due_date'];
				}
				else
				{
					$date=$clientDueDate['due_date'];
				}
		 
		
		//$insertdate="0000-00-00";
		////if($duedate['due_date'] != "")
		//{
			//$insertdate=$duedate['due_date'];
		//}
		
		
		//update project status
		//Project::model()->updateByPk($pid, array(
		   // 'job_due_date' => $insertdate
		//));	
		if($date == "")
		{
			return "";
		}
		
		
		$now=date("Y-m-d");
		$customClass=new MyCustomClass();
		
		//compare due dates
		
		if($date <= $now)
		{
			return "#fb7474";
		}
		else if($customClass->GetDateDifference($date,$now)==1)
		{
			return "#fdc152";
		}
		else if($customClass->GetDateDifference($date,$now)==2)
		{
			return "#ffff00";
		}
		else{
			return "#fffbf0";
		}
		
	}
	

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('project_title', 'required'),
			//array('project_status, project_type, client_id, crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			//array('project_title', 'length', 'max'=>255),
			array('project_comments,project_comments_ao,clientDueDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('project_id, branch_id,project_title, project_status,projectCompositeNo, project_type, client_id, project_date,project_comments_ao,coordinatorfilter,ao_coord,rps_coord', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'pmCommunications' => array(self::HAS_MANY, 'PmCommunication', 'project_id'),
			'projectType' => array(self::BELONGS_TO, 'CodeValue', 'project_type'),
			'client' => array(self::BELONGS_TO, 'Client', 'client_id'),
			'projectStatus' => array(self::BELONGS_TO, 'CodeValue', 'project_status'),
			'updtUser' => array(self::BELONGS_TO, 'User', 'updt_by'),
			'pmProjectAssociates' => array(self::HAS_MANY, 'ProjectAssociates', 'project_id'),
			'branch' => array(self::BELONGS_TO, 'Branch', 'branch_id'),
			
			'aoCoord'=>array(self::HAS_ONE, 'ProjectCoordinates', 'project_id',
                        'condition'=>"aoCoord.type='ao'"),
			'rpsCoord'=>array(self::HAS_ONE, 'ProjectCoordinates', 'project_id',
                        'condition'=>"rpsCoord.type='rps'"),
			'aoCoordUser' => array(self::HAS_ONE, 'User',array('user_id'=>'user_id'), 'through'=>'aoCoord'),
			'rpsCoordUser' => array(self::HAS_ONE, 'User',array('user_id'=>'user_id'), 'through'=>'rpsCoord')
			

			//'pmProjectComments' => array(self::HAS_MANY, 'ProjectComments', 'project_id'),
			
			//'pmProjectCommentsUser' => array(self::HAS_ONE, 'User',array('crtd_by'=>'user_id'), 'through'=>'pmProjectComments'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'project_id' => 'No',
			'branch_id' => 'Branch',
			'project_title' => 'Name',
			'project_status' => 'Status',
			'project_type' => 'Type',
			'client_id' => 'Client',
			'project_comments' => 'Comments',
			'project_date' => 'Date',
			'done_percentage'=>'Done %',
			'coordinatorfilter'=>'Coor',
			'clientDueDate' => 'Client Due Date'
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$model=new MyCustomClass();
		$criteria=new CDbCriteria;
        $criteria->with = array('projectType','client','projectStatus','aoCoordUser','rpsCoordUser');
		$criteria->compare('t.project_id',$this->project_id,true);
		$criteria->compare('t.branch_id',$this->branch_id,true);
		$criteria->compare('t.project_title',$this->project_title,true);
		//$criteria->compare('projectStatus.code_lbl',$this->project_status,true);
		$criteria->compare('t.project_status',$this->project_status,false);
		$criteria->compare('projectType.code_lbl',$this->project_type,true);
		$criteria->compare('client.client_name',$this->client_id,true);
		$criteria->compare('t.project_date',$this->project_date,true);
		$criteria->compare('concat(project_no,"-",projectType.code_lbl)',$this->projectCompositeNo,true);
		//$criteria->order="project_id DESC";
		$criteria->compare('aoCoordUser.user_name',$this->ao_coord,true);
		$criteria->compare('rpsCoordUser.user_name',$this->rps_coord,true);
	
	
		if(!Yii::app()->user->checkAccess('viewAllProject'))
		{
			//check user if coordinators
			$pids=$model->getInvolvedProjectsByUser();
			
			//$criteria->condition='t.project_id in ('.$pids.')';
			$criteria->addCondition('t.project_id in ('.$pids.')');
		}
		
		if($this->project_status == "")
		{
			//$criteria->addSearchCondition('status','0');
			$criteria->addCondition('t.project_status !=5');
		}
		
		$criteria->addCondition('t.project_id > 0');

		//return new CActiveDataProvider($this, array(
			//'criteria'=>$criteria,
		//));
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			   'sort'=>array(
			   'defaultOrder' => 't.project_id DESC',
		        'attributes'=>array(
		            //'project_title'=>array(
		               // 'asc'=>'project_title ASC',
		                //'desc'=>'project_title DESC',
		          //  ),
					//'project_status'=>array(
		               // 'asc'=>'project_status ASC',
		               // 'desc'=>'project_status DESC',
		            //),
					//'project_type'=>array(
		                //'asc'=>'projectType.code_lbl ASC',
		               // 'desc'=>'projectType.code_lbl DESC',
		           // ),
					//'client_id'=>array(
		                //'asc'=>'client.client_name ASC',
		               // 'desc'=>'client.client_name DESC',
		           // ),
					//'project_date'=>array(
		              //  'asc'=>'project_date ASC',
		               // 'desc'=>'project_date DESC',
		          //  ),
					//'job_due_date'=>array(
		               // 'asc'=>'job_due_date ASC',
		               // 'desc'=>'job_due_date DESC',
		           // ),
					//'done_percentage'=>array(
		                //'asc'=>'done_percentage ASC',
		                //'desc'=>'done_percentage DESC',
		            //),
					//'project_comments'=>array(
		              //  'asc'=>'project_comments ASC',
		               // 'desc'=>'project_comments DESC',
		            //),
		            '*',
		        ),
		    ),
		));
		
	}
	
	
	/*public function renderClientDueDate($data,$row)
	{
		$pid=$data->project_id;
		
		$taskCount=Yii::app()->db->createCommand()
    			->select('pm_task.job_client_due_date  as due_date')
    			->from('pm_task')
			    ->join('pm_communication', 'pm_task.communication_id = pm_communication.communication_id')
			    ->where("pm_communication.project_id = $pid")
			    ->queryAll();
		
		$duedate = Yii::app()->db->createCommand()
    			->select('MIN(pm_task.job_client_due_date ) as due_date,pm_task.task_id')
    			->from('pm_task')
			    ->join('pm_communication', 'pm_task.communication_id = pm_communication.communication_id')
			    ->where('pm_task.task_status not in (12,26) AND pm_communication.project_id = '.$pid)
				->group('pm_task.job_client_due_date ,pm_task.task_id')
				->limit(1)
			    ->queryRow();
		 
		
		$insertdate="0000-00-00";
		if($duedate['due_date'] != "")
		{
			$insertdate=$duedate['due_date'];
		}
		
		//update project status
		Project::model()->updateByPk($pid, array(
		    'job_client_due_date' => $insertdate
		));
			
		
		if(count($taskCount) <= 0)
		{
			return "...no task...";
		}
		
		if($duedate['due_date'] == "")
		{
			
			return "all tasks done";
		}
		
		$tid=$duedate['task_id'];
				
				$task = Yii::app()->db->createCommand()
    			->select('pm_task.task_id,
                                  pm_task.assigned_to,
                                  pm_task.task_status,
                                  pm_communication.communication_id,
                                  pm_project.project_no,
                                  pm_code_value.code_lbl')
    			->from('pm_task')
			    ->join('pm_communication', 'pm_task.communication_id = pm_communication.communication_id')
				  ->join('pm_project', 'pm_communication.project_id = pm_project.project_id')
				    ->join('pm_code_value', 'pm_project.project_type = pm_code_value.code_id')
			    ->where("pm_task.task_id = $tid")
			    ->queryRow();
				
				$table="No:".$task['project_no']."-".$task['code_lbl']."-".$task['communication_id']."-".$task['task_id'];
		     return $duedate['due_date']."</br>(".$table.")";
		
	}*/
	
	
	/*
	 $data ... the current row data   
     $row ... the row index 
	*/
	public function renderCoordAo($data,$row)
	{
		
		
		 $pid=$data->project_id;
		 		
		//we can findall the records using method 1 or method 2
		$criteria=new CDbCriteria;
		$criteria->select='*';  // 
		$criteria->condition="project_id=:project_id AND type='ao'";
		$criteria->params=array(':project_id'=>$pid);
		$project_associates=ProjectCoordinates::model()->findAll($criteria); // $params is not needed
		//print_r($project_associates);
		
		$aos=array();
	
		
		$i=0;
		foreach($project_associates as $records)
		{
			
				$user_id=$records->user_id;
				
				
				//method 2 for find
				$user=User::model()->find(array(
				    'select'=>'user_id,user_name,branch_id',
				    'condition'=>'user_id=:userID',
				    'params'=>array(':userID'=>$user_id),
				));
				
				
					$aos[$i]=$user->user_name;
				
				
		
			$i++;
		}
		return implode(",",$aos);
		//$this->_rps=implode(",",$rps);
	}
	
	
	public function getProjectCompositeNo()
	{
		
		
		return ($this->project_no <=0 ? $this->project_id:$this->project_no)."-".$this->projectType->code_lbl;
	}
	
	
	/*
	public function getProjectComments()
	{
		$pid=$this->project_id;
		
		
		$project_comments=Yii::app()->db->createCommand()
    			->select('pm_project_comments.comments,pm_user.user_name,pm_project_comments.crtd_dt')
    			->from('pm_project_comments')
			    ->join('pm_user', 'pm_project_comments.crtd_by = pm_user.user_id')
			    ->where("pm_project_comments.project_id = $pid")
				->order('comment_id DESC')
				->limit(1)
			    ->queryRow();
				
				if(count($project_comments) <= 0)
				{
					return "";
				}
		
		return $project_comments['user_name']."(".$project_comments['crtd_dt'].")".$project_comments['comments'];
	}*/
	

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HomeForm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
}
