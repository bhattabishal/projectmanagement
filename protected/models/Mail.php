<?php
class Mail extends CFormModel
{
    public $date;//when was it sent 
    public $fromName;
	public $fromAddress;
	public $from;//who sent it 
	public $to;//array(email=>name)
	public $toString;
    public $subject;//the messages subject 
	public $messagePlain;
	public $messageHtml;
	public $messageOriginal;
	public $messageId;//Message-ID 
	public $messageNo;//message sequence number in the maibox 
	public $message;
	public $size;//size in bytes 
	public $recent;//this message is flagged as recent 
	public $flagged;//this message is flagged 
	public $answered;//this message is flagged as answered 
	public $deleted;// this message is flagged for deletion 
	public $seen;//this message is flagged as already read 
	public $cc;//array(email=>name)
	public $replyTo;//array(email=>name)
	public $draft;// this message is flagged as being a draft 
	public $attachment;
	public $uid;//UID the message has in the mailbox 
	
	
	/*
	public function getDate()
	{
		return $this->date;
	}
	
	public function setDate($date)
	{
		$this->date=$date;
	}
	
	public function getFrom()
	{
		return $this->from;
	}
	
	public function setFrom($from)
	{
		$this->from=$from;
	}
	public function getSubject()
	{
		return $this->subject;
	}
	
	public function setSubject($subject)
	{
		$this->subject=$subject;
	}
	
	public function getMessagePlain()
	{
		return $this->messagePlain;
	}
	
	public function setMessagePlain($message)
	{
		$this->messagePlain=$message;
	}
	
	public function getMessageId()
	{
		return $this->messageId;
	}
	
	public function setMessageId($messageId)
	{
		$this->messageId=$messageId;
	}
	public function getMessageOriginal()
	{
		return $this->messageOriginal;
	}
	
	public function setMessageOriginal($message)
	{
		$this->messageOriginal=$message;
	}
	
	public function getAttachment()
	{
		return $this->attachment;
	}
	
	public function setAttachment($attachment)
	{
		$this->attachment=$attachment;
	}
	
	*/
	
	
}
?>