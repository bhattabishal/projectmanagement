<?php

/**
* This is the model class for table "pm_actual_timeline".
*
* The followings are the available columns in table 'pm_actual_timeline':
* @property string $timeline_id
* @property integer $task_id
* @property string $normal_hour
* @property string $cusion_hour
* @property string $overtime_hour
* @property string $task_date
* @property string $remarks
* @property integer $crtd_by
* @property string $crtd_dt
* @property integer $updt_by
* @property string $updt_dt
* @property integer $updt_cnt
*/
class ActualTimeline extends PMActiveRecord{
	public $taskCompositeNo;
	public $isAlert;
	public $error_known;
	public $error_unknown;
	public $nameText;
        public $timelineId;//it is used in grid update as grid id is set to task id for error update
        
	/**
	* @return string the associated database table name
	*/
	public function tableName(){
		return 'pm_actual_timeline';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules(){
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('task_id,user_id, task_date, remarks ,percentage_completed,normal_hour, overtime_hour', 'required','on' => 'insert,update,transfer'),
			array('task_id,user_id,error_known,error_unknown', 'numerical', 'integerOnly'=>true),
			array('normal_hour, overtime_hour','match','pattern'=>'/^[0-9]{1,2}(\.[0-9]{0,2})?$/'),
			
			array('task_date', 'validateDate', 'on' => 'insert'),
			array('task_id', 'validateTask', 'on' => 'insert'),
			///array('normal_hour', 'validateHour', 'on' => 'insert'),
			
			//array('overtime_hour', 'validateHour', 'on' => 'insert'),
			array('user_id', 'validateRps'),
			array('percentage_completed', 'validatePercentage', 'on' => 'insert,update'),
			array('error_known,error_unknown','validateIfCompleted', 'on' => 'insert,update,batchSave'),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			
			array('timeline_id, task_id,normal_hour, cusion_hour, overtime_hour,schedule_id_completed,isAlert,task_type,admin_timeline_id,audited', 'safe'),
			
			
			array('timeline_id, task_id,user_id, normal_hour, cusion_hour, overtime_hour, task_date, remarks, crtd_by, crtd_dt, percentage_completed, nameText, taskCompositeNo,audited', 'safe', 'on'=>'search'),
		);
	}
	
	/*
	if completed percentage is 100%,then required
	*/
	
	public function validateIfCompleted($attribute,$params)
	{
		if($this->percentage_completed >= 100)
		{
			if($this->error_known == "" && $this->error_unknown=="")
			{
				$this->addError($attribute,"No of error is required.");
			}
		}
		
		return TRUE;
	}
	
        
        	public function validateRps($attribute,$params)
	{
            $checkRps= Yii::app()->db->createCommand()
		->select('assigned_to')
		->from('pm_task')
		->where("task_id =:task_id",array(':task_id'=>$this->task_id))
		->queryRow();
		if($this->user_id=$checkRps->assigned_to)
		{
		     $this->addError($attribute,"You cannot select same user");
                     return FALSE;
		}
		
		return TRUE;
	}
	/*
	check if for the selected date and task there is remaining work.else inform user to check the schedule and enter timeline
	*/
	
	public function validateTask($attribute,$params)
	{
		$todaysRecord= Yii::app()->db->createCommand()
		->select('*')
		->from('pm_task_schedule')
		->where("task_id =:task_id",array(':task_id'=>$this->task_id))
		->andWhere("user_id =:user_id",array(':user_id'=>$this->user_id))
		->andWhere("schedule_date = :schedule_date",array(':schedule_date'=>$this->task_date))
		->andWhere("completed_status = :completed_status",array(':completed_status' => '0'))
		->queryRow();
		
		if($todaysRecord['schedule_id']=="" || $todaysRecord['schedule_id']<= 0)
		{
			$this->addError($attribute, "No assigned work found on selected date and task.Please check the schedule chart.");
			return FALSE;
		}
		return TRUE;
	}
	
	/*
	user is allowed to enter the timeline only for the current date or a day before
	*/
	public function validateDate($attribute,$params){
		
		
		$dateDifference=MyCustomClass::GetDateDifference($this->task_date);
		//$dateDifference=1;
		if($dateDifference > 0 || $dateDifference < -1){
			$this->addError($attribute, "Timeline entry allowed only for current date or a day before.");
		}
			
		
	}
	
	/*
	normally percentage is not allowed to be less then what entered earlier.but in case of extended_work the percentage can be less than earlier 
	
	*/
	public function validatePercentage($attribute,$params){
		
		if($this->percentage_completed <= 0 || $this->percentage_completed > 100 )
		{
			$this->addError($attribute, "Percentage completed should be between 1-100.");
				return FALSE;
		}
		
		if($this->percentage_completed > 90 && $this->percentage_completed < 100 )
		{
			$this->addError($attribute, "Percentage completed should be 100 after 90.");
				return FALSE;
		}
		
		$extendedRecord=TaskSchedule::model()->find('task_id=:task_id and extended_status=:extended_status',array(':task_id'=>$this->task_id,':extended_status'=>'1'));
		
		$previousEntry=ActualTimeline::model()->find(array('order'=>'timeline_id DESC','condition'=>'task_id=:task_id','params'=>array(':task_id'=>$this->task_id)));
		
		if(floatval($this->percentage_completed) < floatval($previousEntry->percentage_completed)){
			if(count($extendedRecord) <= 0){
				$this->addError($attribute, "Percentage completed is less than what was entered previous.");
				return FALSE;
			}
		}
		
		return TRUE;
	}
	
	
	/*
	check if n/c/w hour doesnot exceed the defined allowed time
	*/
	public function  validateHour($attribute,$params){
		//$selectedDateRecord=ActualTimeline::model()->countByAttributes(array(
		//'user_id'=>$this->user_id,
		//'task_date'=>$this->task_date
		//));
		
		$record=Yii::app()->db->createCommand()
		->select('IFNULL(sum(normal_hour),0) as nh,IFNULL(sum(cusion_hour),0) as ch,IFNULL(sum(overtime_hour),0) as oh')
		->from('pm_actual_timeline')
		->where('pm_actual_timeline.user_id=:userid',array(':userid'=>$this->user_id))
		->andWhere('pm_actual_timeline.task_date=:task_date',array(':task_date'=>$this->task_date))
		->group('task_date')
		->queryRow();
		
		$definedNH=CodeValue::model()->getCodeTypeValue('work_hour',28);
		$definedCH=CodeValue::model()->getCodeTypeValue('work_hour',29);
		$definedOH=CodeValue::model()->getCodeTypeValue('work_hour',30);
		
		
		
		if($this->normal_hour=="" && $this->overtime_hour==""){
			$this->addError($attribute, "Please enter any available hours.");
			return FALSE;
		}
		
		//echo floatval($record['nh'])+floatval($this->normal_hour);
		//die;
		
		//check for normal hour,cusion hour and overtime hour,28,29,30(work_hour)
			
		if((floatval($record['nh'])+floatval($record['ch'])+floatval($this->normal_hour)) > (floatval($definedNH)+floatval($definedCH))){
			$this->addError($attribute, "Normal Hour is not available.");
			return FALSE;
		}
		//elseif((floatval($record['ch'])+floatval($this->cusion_hour)) > floatval($definedCH))
		//{
		//$this->addError($attribute, "Cusion Hour is not available.");
		//}
		elseif((floatval($record['oh'])+floatval($this->overtime_hour)) > floatval($definedOH)){
			$this->addError($attribute, "Overtime Hour is not available.");
			return FALSE;
		}
		else{
			return TRUE;
		}
			
			
		
	}

	/**
	* @return array relational rules.
	*/
	public function relations(){
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'task' => array(self::BELONGS_TO, 'Task', 'task_id'),
			'communication' => array(self::BELONGS_TO, 'Communication', array('communication_id'=>'communication_id'), 'through'=>'task'),
			'project' => array(self::HAS_ONE, 'Project',array('project_id'=>'project_id'), 'through'=>'communication'),
			'codeValueProjectType' => array(self::HAS_ONE, 'CodeValue',array('project_type'=>'code_id'), 'through'=>'project'),
		);
	}

	/**
	* @return array customized attribute labels (name=>label)
	*/
	public function attributeLabels(){
		return array(
			'timeline_id' => 'Timeline',
			'task_id' => 'Task',
                        'user_id' => 'Assigned to',
			'normal_hour' => 'Normal Hour',
			'cusion_hour' => 'Cusion Hour',
			'overtime_hour' => 'OT Hour',
			'task_date' => 'Task Date',
			'percentage_completed' =>'Completed (%)',
			'remarks' => 'Remarks',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
			'isAlert' =>'Alert Task',
			'error_known'=>'Error know but does',
			'error_unknown'=>'Error dont know',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	*
	* Typical usecase:
	* - Initialize the model fields with values from filter form.
	* - Execute this method to get CActiveDataProvider instance which will filter
	* models according to data in model fields.
	* - Pass data provider to CGridView, CListView or any similar widget.
	*
	* @return CActiveDataProvider the data provider that can return the models
	* based on the search/filter conditions.
	*/
	public function search(){
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.timeline_id',$this->timeline_id,true);
		$criteria->compare('t.task_id',$this->task_id);
		$criteria->compare('t.normal_hour',$this->normal_hour,true);
		$criteria->compare('t.cusion_hour',$this->cusion_hour,true);
		$criteria->compare('t.overtime_hour',$this->overtime_hour,true);
		$criteria->compare('t.task_date',$this->task_date,true);
		$criteria->compare('t.remarks',$this->remarks,true);
		$criteria->compare('t.crtd_by',$this->crtd_by);
		$criteria->compare('t.crtd_dt',$this->crtd_dt,true);
		$criteria->compare('t.updt_by',$this->updt_by);
		$criteria->compare('t.updt_dt',$this->updt_dt,true);
		$criteria->compare('t.updt_cnt',$this->updt_cnt);
		$criteria->compare('user.user_name',$this->nameText,true);
		$criteria->compare('percentage_completed',$this->percentage_completed,true);
		$criteria->compare('concat(project.project_no,"-",codeValueProjectType.code_lbl)',$this->taskCompositeNo,true);
		$criteria->with = array('codeValueProjectType'=>array('alias'=>'codeValueProjectType','select'=>'codeValueProjectType.code_lbl','together'=>true));
		
		$criteria -> join = 'INNER JOIN pm_user user on t.user_id = user.user_id ';
		
		$criteria->addCondition("t.task_type='normal'");
                
                if(!Yii::app()->user->checkAccess('viewAllTask'))
		{
			$model=new MyCustomClass();
		   	$pids=$model->getInvolvedProjectsByUser();
			//$criteria->condition='project.project_id in ('.$pids.')';
			$criteria->addCondition('project.project_id in ('.$pids.')');
		}

		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize' => 50),
				'sort'=>array(
					'defaultOrder' => 't.task_date DESC,t.timeline_id DESC',
					'attributes'=>array(
						//'project_search'=>array(
						// 'asc'=>'project.project_title',
						//'desc'=>'project.project_title DESC',
						//),
					
						'*',
					),
				),
			));
	}
	
	
	/*
	check the date to shift the task due date
	first the task completed percentage is updated
	*/
	protected function afterSave(){
		parent::afterSave();
		
		
		
		
	}
	
	protected function beforeSave(){
		parent::beforeSave();
		
		$definedNH=CodeValue::model()->getCodeTypeValue('work_hour',28);
		$definedCH=CodeValue::model()->getCodeTypeValue('work_hour',29);
		$nh=floatval($this->normal_hour);
		
		if($nh > floatval($definedNH)){
			$extraHr=floatval($nh)-floatval($definedNH);
			$j=0;
			while($extraHr >0 && $definedCH > 0  ){
				$definedCH=$definedCH-1;
				$extraHr=$extraHr-1;
				$j=$j+1;
			}
			
			
			$this->cusion_hour=$j;
			
		}
		
		return true;
	}
	
	public function getTaskCompositeNo(){
		
		
		//return ($this->project->project_no <=0 ? $this->project->project_id:$this->project->project_no)."-".$this->codeValueProjectType->code_lbl."-".$this->communication->communication_id."-".$this->task_id;
		return ($this->project->project_no <=0 ? $this->project->project_id:$this->project->project_no)."-".$this->codeValueProjectType->code_lbl;
	}

	/**
	* Returns the static model of the specified AR class.
	* Please note that you should have this exact method in all your CActiveRecord descendants!
	* @param string $className active record class name.
	* @return ActualTimeline the static model class
	*/
	public static function model($className=__CLASS__){
		return parent::model($className);
	}
}
