<?php

/**
 * This is the model class for table "pm_branch".
 *
 * The followings are the available columns in table 'pm_branch':
 * @property integer $branch_id
 * @property string $branch_name
 * @property string $branch_code
 * @property string $branch_contact
 * @property string $branch_email
 * @property integer $branch_type
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 *
 * The followings are the available model relations:
 * @property PmUser[] $pmUsers
 */
class Branch extends PMActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_branch';
	}
        
        
         public function scopes() 
        {
                return array(
                    'byName' => array('order' => 'branch_name ASC'),
                  );
        }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('branch_type', 'required'),
			array('branch_type, crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			array('branch_name, branch_code, branch_contact, branch_email', 'length', 'max'=>255),
			array('crtd_dt, updt_dt', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('branch_id, branch_name, branch_code, branch_contact, branch_email, branch_type, crtd_by, crtd_dt, updt_by, updt_dt, updt_cnt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pmUsers' => array(self::HAS_MANY, 'PmUser', 'branch_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'branch_id' => 'Branch',
			'branch_name' => 'Branch Name',
			'branch_code' => 'Branch Code',
			'branch_contact' => 'Branch Contact',
			'branch_email' => 'Branch Email',
			'branch_type' => 'Branch Type',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('branch_id',$this->branch_id);
		$criteria->compare('branch_name',$this->branch_name,true);
		$criteria->compare('branch_code',$this->branch_code,true);
		$criteria->compare('branch_contact',$this->branch_contact,true);
		$criteria->compare('branch_email',$this->branch_email,true);
		$criteria->compare('branch_type',$this->branch_type);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	public function getBranchAndClientDropDown()
	{
		$records = Branch::model()->byName()->findAll();
		
		$array=array();
		$array['0']='Client';
		
		foreach($records as $record)
		{
			$array[$record->branch_id]=$record->branch_name;
		}
		
		return $array;
	}
	
	public function getAoBranchIdsString()
	{
		$records = Branch::model()->findAll('branch_type=:type',array(':type'=>14));
		
		$array=array();
		
		
		foreach($records as $record)
		{
			$array[]=$record->branch_id;
		}
		
		return implode(',',$array);
	}
	
	
	public function getRpsBranchIdsString()
	{
		$records = Branch::model()->findAll('branch_type=:type',array(':type'=>18));
		
		$array=array();
		
		
		foreach($records as $record)
		{
			$array[]=$record->branch_id;
		}
		
		return implode(',',$array);
	}

//for search in home view
	public function getBranchName()
	{
                $records=Branch::model()->byName()->findAll();// used scopes 'byName'
		//$records = Branch::model()->findAll(array('order' => 'branch_id'));
		
		$array=array();
		
		foreach($records as $record)
		{
			$array[$record->branch_id]=$record->branch_name;
		}
		
		return $array;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Branch the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
