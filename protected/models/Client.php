<?php

/**
 * This is the model class for table "pm_client".
 *
 * The followings are the available columns in table 'pm_client':
 * @property integer $client_id
 * @property string $client_name
 * @property string $client_address
 * @property string $client_phone
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 *
 * The followings are the available model relations:
 * @property PmClientContact[] $pmClientContacts
 * @property PmProject[] $pmProjects
 */
class Client extends PMActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_client';
	}

        
          public function scopes() 
        {
                return array(
                    'byName' => array('order' => 'client_name ASC'),
                  );
        }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_name', 'required'),
			array('crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			array('client_name, client_phone', 'length', 'max'=>255),
			array('client_address,crtd_dt, updt_dt', 'safe'),
			array('client_name','unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('client_name, client_address, client_phone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pmClientContacts' => array(self::HAS_MANY, 'PmClientContact', 'client_id'),
			'pmProjects' => array(self::HAS_MANY, 'PmProject', 'client_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'client_id' => 'Client',
			'client_name' => 'Name',
			'client_address' => 'Address',
			'client_phone' => 'Phone',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('client_name',$this->client_name,true);
		$criteria->compare('client_address',$this->client_address,true);
		$criteria->compare('client_phone',$this->client_phone,true);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort'=>array(
			    'defaultOrder' => 'client_name ASC',
		        'attributes'=>array(
		            	
		            '*',
		        ),
		    ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Client the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
