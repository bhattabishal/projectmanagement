<?php
/* @var $this JobController */
/* @var $model Job */

//$this->breadcrumbs=array(
	//'Jobs'=>array('index'),
	//$model->job_id,
//);

//$this->menu=array(
	//array('label'=>'List Job', 'url'=>array('index')),
	//array('label'=>'Create Job', 'url'=>array('create')),
	//array('label'=>'Update Job', 'url'=>array('update', 'id'=>$model->job_id)),
	//array('label'=>'Delete Job', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->job_id),'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>'Manage Job', 'url'=>array('admin')),
//);
?>

<!--<h1>View Job #<?php //echo $model->job_id; ?></h1>-->

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'job_id',
		'communication.communication_details',
		'jobCategory.code_lbl',
		'job_details',
		'jobType.code_lbl',
		'sharable0.code_lbl',
		'job_due_date',
		'wis',
		'est_rps_cost',
		'est_ao_cost',
		'actual_rps_cost',
		'actual_ao_cost',
		'done_percentage',
		//'crtd_by',
		//'crtd_dt',
		//'updt_by',
		//'updt_dt',
		//'updt_cnt',
	),
)); ?>
