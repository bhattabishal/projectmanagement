<?php
/* @var $this JobController */
/* @var $data Job */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->job_id), array('view', 'id'=>$data->job_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('communication_id')); ?>:</b>
	<?php echo CHtml::encode($data->communication_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_category')); ?>:</b>
	<?php echo CHtml::encode($data->job_category); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_details')); ?>:</b>
	<?php echo CHtml::encode($data->job_details); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_type')); ?>:</b>
	<?php echo CHtml::encode($data->job_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sharable')); ?>:</b>
	<?php echo CHtml::encode($data->sharable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_due_date')); ?>:</b>
	<?php echo CHtml::encode($data->job_due_date); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('wis')); ?>:</b>
	<?php echo CHtml::encode($data->wis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('est_rps_cost')); ?>:</b>
	<?php echo CHtml::encode($data->est_rps_cost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('est_ao_cost')); ?>:</b>
	<?php echo CHtml::encode($data->est_ao_cost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actual_rps_cost')); ?>:</b>
	<?php echo CHtml::encode($data->actual_rps_cost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actual_ao_cost')); ?>:</b>
	<?php echo CHtml::encode($data->actual_ao_cost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('done_percentage')); ?>:</b>
	<?php echo CHtml::encode($data->done_percentage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_by')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_dt')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_by')); ?>:</b>
	<?php echo CHtml::encode($data->updt_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_dt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_cnt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_cnt); ?>
	<br />

	*/ ?>

</div>