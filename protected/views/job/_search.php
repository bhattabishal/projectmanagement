<?php
/* @var $this JobController */
/* @var $model Job */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'job_id'); ?>
		<?php echo $form->textField($model,'job_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'communication_id'); ?>
		<?php echo $form->textField($model,'communication_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'job_category'); ?>
		<?php echo $form->textField($model,'job_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'job_details'); ?>
		<?php echo $form->textArea($model,'job_details',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'job_type'); ?>
		<?php echo $form->textField($model,'job_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sharable'); ?>
		<?php echo $form->textField($model,'sharable'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'job_due_date'); ?>
		<?php echo $form->textField($model,'job_due_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wis'); ?>
		<?php echo $form->textField($model,'wis',array('size'=>18,'maxlength'=>18)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'est_rps_cost'); ?>
		<?php echo $form->textField($model,'est_rps_cost',array('size'=>18,'maxlength'=>18)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'est_ao_cost'); ?>
		<?php echo $form->textField($model,'est_ao_cost',array('size'=>18,'maxlength'=>18)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'actual_rps_cost'); ?>
		<?php echo $form->textField($model,'actual_rps_cost',array('size'=>18,'maxlength'=>18)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'actual_ao_cost'); ?>
		<?php echo $form->textField($model,'actual_ao_cost',array('size'=>18,'maxlength'=>18)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'done_percentage'); ?>
		<?php echo $form->textField($model,'done_percentage',array('size'=>18,'maxlength'=>18)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'crtd_by'); ?>
		<?php echo $form->textField($model,'crtd_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'crtd_dt'); ?>
		<?php echo $form->textField($model,'crtd_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updt_by'); ?>
		<?php echo $form->textField($model,'updt_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updt_dt'); ?>
		<?php echo $form->textField($model,'updt_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updt_cnt'); ?>
		<?php echo $form->textField($model,'updt_cnt'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->