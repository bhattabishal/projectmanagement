<?php
/* @var $this JobController */
/* @var $model Job */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'job-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'communication_id'); ?>
		<?php echo $model->communication->communication_details; ?>
		<?php //echo $form->error($model,'communication_id'); ?>
	</div>

	<div class="row">
		<?php
		$records = CHtml::listData(CodeValue::model()->findAll("code_type= 'job_cat'"), 'code_id', 'code_lbl');
		?>
		<?php echo $form->labelEx($model,'job_category'); ?>
		<?php echo $form->dropDownList($model,'job_category',$records,array('empty' => 'Select category')); ?>
		<?php echo $form->error($model,'job_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'job_details'); ?>
		<?php echo $form->textArea($model,'job_details',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'job_details'); ?>
	</div>

	<div class="row">
		<?php
		$records = CHtml::listData(CodeValue::model()->findAll("code_type= 'job_type'"), 'code_id', 'code_lbl');
		?>
		<?php echo $form->labelEx($model,'job_type'); ?>
		<?php echo $form->dropDownList($model,'job_type',$records,array('empty' => 'Select type')); ?>
		<?php echo $form->error($model,'job_type'); ?>
	</div>

	<div class="row">
		<?php
		$records = CHtml::listData(CodeValue::model()->findAll("code_type= 'status'"), 'code_id', 'code_lbl');
		?>
		<?php echo $form->labelEx($model,'sharable'); ?>
		<?php echo $form->dropDownList($model,'sharable',$records,array('empty' => 'Select sharable')); ?>
		<?php echo $form->error($model,'sharable'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'job_due_date'); ?>
		<?php 
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $model,
		    'attribute'=>'job_due_date',
		    //'flat'=>true,//remove to hide the datepicker
		    'options'=>array(
		        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
				'dateFormat' => 'yy-mm-dd',
				'showButtonPanel' => true,      // show button panel
		    ),
		    'htmlOptions'=>array(
		        'style'=>''
		    ),
		));
	  ?>
		<?php echo $form->error($model,'job_due_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wis'); ?>
		<?php echo $form->textField($model,'wis',array('size'=>18,'maxlength'=>18)); ?>
		<?php echo $form->error($model,'wis'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'est_rps_cost'); ?>
		<?php echo $form->textField($model,'est_rps_cost',array('size'=>18,'maxlength'=>18)); ?>
		<?php echo $form->error($model,'est_rps_cost'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'est_ao_cost'); ?>
		<?php echo $form->textField($model,'est_ao_cost',array('size'=>18,'maxlength'=>18)); ?>
		<?php echo $form->error($model,'est_ao_cost'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'actual_rps_cost'); ?>
		<?php echo $form->textField($model,'actual_rps_cost',array('size'=>18,'maxlength'=>18)); ?>
		<?php echo $form->error($model,'actual_rps_cost'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'actual_ao_cost'); ?>
		<?php echo $form->textField($model,'actual_ao_cost',array('size'=>18,'maxlength'=>18)); ?>
		<?php echo $form->error($model,'actual_ao_cost'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'done_percentage'); ?>
		<?php echo $form->textField($model,'done_percentage',array('size'=>18,'maxlength'=>18)); ?>
		<?php echo $form->error($model,'done_percentage'); ?>
	</div>

	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->