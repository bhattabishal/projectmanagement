<?php
$this->breadcrumbs=array(
	'Task Communications'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List TaskCommunication','url'=>array('index')),
array('label'=>'Manage TaskCommunication','url'=>array('admin')),
);
?>

<h1>Create TaskCommunication</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>