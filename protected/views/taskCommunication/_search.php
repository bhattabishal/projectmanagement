<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'task_communication_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'task_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'communication_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'crtd_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'crtd_dt',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updt_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updt_dt',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updt_cnt',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
