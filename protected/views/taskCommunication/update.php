<?php
$this->breadcrumbs=array(
	'Task Communications'=>array('index'),
	$model->task_communication_id=>array('view','id'=>$model->task_communication_id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List TaskCommunication','url'=>array('index')),
	array('label'=>'Create TaskCommunication','url'=>array('create')),
	array('label'=>'View TaskCommunication','url'=>array('view','id'=>$model->task_communication_id)),
	array('label'=>'Manage TaskCommunication','url'=>array('admin')),
	);
	?>

	<h1>Update TaskCommunication <?php echo $model->task_communication_id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>