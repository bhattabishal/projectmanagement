<?php
$this->breadcrumbs=array(
	'Task Communications'=>array('index'),
	$model->task_communication_id,
);

$this->menu=array(
array('label'=>'List TaskCommunication','url'=>array('index')),
array('label'=>'Create TaskCommunication','url'=>array('create')),
array('label'=>'Update TaskCommunication','url'=>array('update','id'=>$model->task_communication_id)),
array('label'=>'Delete TaskCommunication','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->task_communication_id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage TaskCommunication','url'=>array('admin')),
);
?>

<h1>View TaskCommunication #<?php echo $model->task_communication_id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'task_communication_id',
		'task_id',
		'communication_id',
		'crtd_by',
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
),
)); ?>
