<?php
$this->breadcrumbs=array(
	'Task Communications',
);

$this->menu=array(
array('label'=>'Create TaskCommunication','url'=>array('create')),
array('label'=>'Manage TaskCommunication','url'=>array('admin')),
);
?>

<h1>Task Communications</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
