<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'task-communication-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'task_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'communication_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'crtd_by',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'crtd_dt',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'updt_by',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'updt_dt',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'updt_cnt',array('class'=>'span5')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
