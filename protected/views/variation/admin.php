	<div id='menub'>
		<?php
		$this->widget(
		'bootstrap.widgets.TbButton',
		array(
		'buttonType'=>'link',
		    'type'=>'primary',
		    'icon'=>'file',
		    'label'=>'Variation',
		    'loadingText'=>'loading...',
		    'htmlOptions'=>array('id'=>'buttonStateful'),
		'url'=>'variation/excel',
		)
		);
		?>
  </div>

 <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'variation-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type'=>' bordered',
	//'template' => "{summary}\n{items}\n{exportbuttons}\n{pager}",//for excel
	'columns'=>array(
	   array(
	    'header' => 'Project No',
	   	'name'=>'projectCompositeNo',
	   	'value'=>'$data->getProjectCompositeNo()',
	   ),
	    'task_id',
		'variation_dt',
		'variation_no',
		array(
			'header'=>'Description',
		
            'value'=>'$data->description',
		),
	//	'description',		
		'variation_hours',
		
		array(
		'name'=>'status',
		'header' => 'Status',
		'value'=>'$data->getStatus()',
		'class' => 'bootstrap.widgets.TbEditableColumn2',
		'filter'=>array('2'=>'Approved', '1'=>'Partial Approved', '0'=>'Pending'),
	    'headerHtmlOptions' => array('style' => 'width: 110px'),
		'editable' => array(
		'type' => 'select',
		'source' => $this->createUrl('variation/getStatusValues'),
		'url' =>  $this->createUrl('variation/updateComments'),
		)
		),
	
		
	
		 array(
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'name' => 'comments',
			//'value' =>'comments',
			'type'=>'raw',
			'header'=>'Comments',
			'headerHtmlOptions' => array('style' => 'width: 110px'),
			'editable' => array(
			 	'type' => 'textarea',
				'url' => $this->createUrl('variation/updateComments'),
				'placement' => 'left',
			)
			),
		/*
		'comments',
		'crtd_by',
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
		*/
		array(
            'class'=>'EJuiDlgsColumn',
            'template'=>'{update}{delete}',
			'htmlOptions' => array('style'=>'width:56px'),
			'deleteButtonUrl'=>'Yii::app()->createUrl("/variation/delete", array("id" => $data->variation_id))',
			 'buttons'=>array(
                       // 'view'=>
                          //   array(
                       // 'label'=>'ajax dialog view',
						//'options'=>array(
						//'class'=>'commView',
						//)
                   // ),
					'update'=>
                             array(
                        'options'=>array(
						'class'=>'commUpdate',
						)
                    ),
					'delete'=>
                             array(
                        'options'=>array(
						'class'=>'commDelete'.$uniqid,
						)
                    ),
					
					
                            ),
							//'viewDialog'=>array(
            // 'controllerRoute' => 'variation/view', //=default
            // 'actionParams' => array('id' => '$data->primaryKey'), //=default
            // 'dialogTitle' => 'View Variation',
			  ///'closeOnAction' => true,
			// 'refreshGridId' => 'variation-grid',
                //         'hideTitleBar' => false, 
            // 'dialogWidth' => '80%',
        	// 'dialogHeight' => '300',
        //),
		'updateDialog'=>array(
		'controllerRoute' => 'variation/update', //=default
            'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update Variation',
			  'closeOnAction' => true,
			 'refreshGridId' => 'variation-grid',
			 'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						  'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ),
		),
			),
	),
 )); ?>
