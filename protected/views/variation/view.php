<?php
/* @var $this VariationController */
/* @var $model Variation */

$this->breadcrumbs=array(
	'Variations'=>array('index'),
	$model->variation_id,
);

$this->menu=array(
	array('label'=>'List Variation', 'url'=>array('index')),
	array('label'=>'Create Variation', 'url'=>array('create')),
	array('label'=>'Update Variation', 'url'=>array('update', 'id'=>$model->variation_id)),
	array('label'=>'Delete Variation', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->variation_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Variation', 'url'=>array('admin')),
);
?>

<h1>View Variation #<?php echo $model->variation_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'variation_id',
		'project_id',
		'task_id',
		'description',
		'variation_hours',
		'status',
		'comments',
		'crtd_by',
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
	),
)); ?>
