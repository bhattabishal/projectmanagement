<?php
/* @var $this VariationController */
/* @var $model Variation */
/* @var $form CActiveForm */
?>

<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'variation-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	

	<div class="row col2">
		<?php echo $form->labelEx($model,'task_id'); ?>
		<?php 
		
		$records = Task::model()->findAll(array('order' => 'task_id'));
		$list=array();
		foreach($records as $record)
		{
			$list[$record->task_id]=Task::model()->getTaskName($record->task_id);
		}
		//$list = CHtml::listData($records, 'client_id', 'client_name');

		//echo $form->textField($model,'task_id'); 
		echo $form->dropDownList($model,'task_id',$list,array('empty' => 'Select task'));
		?>
		<?php echo $form->error($model,'task_id'); ?>
	</div>

	<div style="clear: both"></div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'variation_hours'); ?>
		<?php echo $form->textField($model,'variation_hours',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'variation_hours'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php 
		$list=array();
		$list['0']='Pending';
		$list['1']='Partial Approved';
		$list['2']='Approved';
		echo $form->dropDownList($model,'status',$list,array('empty' => 'Select Status'));
		?>
		<?php echo $form->error($model,'status'); ?>
	</div>
	<div style="clear: both"></div>
	<div class="row col2">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
	<div class="row col2">
		<?php echo $form->labelEx($model,'comments'); ?>
		<?php echo $form->textArea($model,'comments',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'comments'); ?>
	</div>
<div style="clear: both"></div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->