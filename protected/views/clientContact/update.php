<?php
/* @var $this ClientContactController */
/* @var $model ClientContact */

$this->breadcrumbs=array(
	'Client Contacts'=>array('index'),
	$model->client_contact_id=>array('view','id'=>$model->client_contact_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ClientContact', 'url'=>array('index')),
	array('label'=>'Create ClientContact', 'url'=>array('create')),
	array('label'=>'View ClientContact', 'url'=>array('view', 'id'=>$model->client_contact_id)),
	array('label'=>'Manage ClientContact', 'url'=>array('admin')),
);
?>

<h1>Update ClientContact <?php echo $model->client_contact_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>