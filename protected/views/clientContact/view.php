<?php
/* @var $this ClientContactController */
/* @var $model ClientContact */

$this->breadcrumbs=array(
	'Client Contacts'=>array('index'),
	$model->client_contact_id,
);

$this->menu=array(
	array('label'=>'List ClientContact', 'url'=>array('index')),
	array('label'=>'Create ClientContact', 'url'=>array('create')),
	array('label'=>'Update ClientContact', 'url'=>array('update', 'id'=>$model->client_contact_id)),
	array('label'=>'Delete ClientContact', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->client_contact_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ClientContact', 'url'=>array('admin')),
);
?>

<!--<h1>View ClientContact #<?php //echo $model->client_contact_id; ?></h1>-->

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'client_contact_id',
		'client.client_name',
		'contact_name',
		'contact_phone',
		'contact_email',
	),
)); ?>
