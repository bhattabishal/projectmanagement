<div class="headingalign">
<?php
$uniqid=md5(uniqid());
//echo CHtml::tag('h3',array(),'RELATIONAL DATA EXAMPLE ROW : "'.$id.'"');
echo CHtml::tag('h6',array(),'Client Contact Details');
?>
</div>

<div class="btnalign">
<?php
//for new btn
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'clientContact/create',
		'actionParams' => array('cid'=>$cid),
        'dialogTitle' => 'Create Client Contact',
		'id' => 'newClientContact',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
		//'renderOpenButton' => false,
        'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New Client Contact',
        'closeButtonText' => 'Close',
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'gridClientContact', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                           'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
//end new btn
?>
</div>

<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', array(
'type'=>'striped bordered',
'id' => 'gridClientContact',
//'id'=>'grid-'.$uniqid,
//'pagerCssClass'=>'pagination pager-'.$uniqid,
'dataProvider' => $gridDataProvider,
'enableSorting' => false,
'enablePagination'=>false,
//'template' => "{items}",
'columns'=>array(
	
	'contact_name',
	'contact_phone',
	'contact_email',
	array(
            'class'=>'EJuiDlgsColumn',
			'htmlOptions' => array('style'=>'width:56px'),
			'deleteButtonUrl'=>'Yii::app()->createUrl("/clientContact/delete", array("id" => $data->client_contact_id))',
           	//'updateButtonImageUrl'=>Yii::app()->baseUrl .'images/viewdetaildialog.png',
            'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
						'options'=>array(
						'class'=>'clientContactView',
						)
                    ),
					'update'=>
                             array(
                        'options'=>array(
						'class'=>'clientContactUpdate',
						)
                    ),
					'delete'=>
                             array(
                        'options'=>array(
						'class'=>'clientContactDelete',
						)
                    ),
					
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'clientContact/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View Client Contact',
			 'closeOnAction' => true,
			 'refreshGridId' => 'gridClientContact',
                         'hideTitleBar' => false, 
            'dialogWidth' => '80%',
        		'dialogHeight' => '300',
        ),
		'updateDialog'=>array(
             'controllerRoute' => 'clientContact/update', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update Client Contact',
			 'closeOnAction' => true,
			 'refreshGridId' => 'gridClientContact',
			  'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
             //'dialogWidth' => 800, //use the value from the dialog config
             //'dialogHeight' => 600,
        ),
		
           
        ),
),
//'columns' => array_merge(array(array('class'=>'bootstrap.widgets.TbImageColumn')),$gridColumns),
));
?>