<?php
/* @var $this ClientContactController */
/* @var $model ClientContact */

$this->breadcrumbs=array(
	'Client Contacts'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ClientContact', 'url'=>array('index')),
	array('label'=>'Create ClientContact', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#client-contact-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Client Contacts</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'client-contact-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'client_contact_id',
		'client_id',
		'contact_name',
		'contact_phone',
		'contact_email',
		'crtd_by',
		/*
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
