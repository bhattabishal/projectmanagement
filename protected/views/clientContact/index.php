<?php
/* @var $this ClientContactController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Client Contacts',
);

$this->menu=array(
	array('label'=>'Create ClientContact', 'url'=>array('create')),
	array('label'=>'Manage ClientContact', 'url'=>array('admin')),
);
?>

<h1>Client Contacts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
