<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_name'); ?>
		<?php echo $form->textField($model,'user_name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'login_name'); ?>
		<?php echo $form->textField($model,'login_name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'login_pwd'); ?>
		<?php echo $form->textField($model,'login_pwd',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_login_time'); ?>
		<?php echo $form->textField($model,'last_login_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'branch_id'); ?>
		<?php echo $form->textField($model,'branch_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'crtd_by'); ?>
		<?php echo $form->textField($model,'crtd_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'crtd_dt'); ?>
		<?php echo $form->textField($model,'crtd_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updt_by'); ?>
		<?php echo $form->textField($model,'updt_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updt_dt'); ?>
		<?php echo $form->textField($model,'updt_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updt_cnt'); ?>
		<?php echo $form->textField($model,'updt_cnt'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->