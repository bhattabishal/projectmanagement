<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h1>Manage Users</h1>-->

<!--<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>-->

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<!--<div class="search-form" style="display:none">
<?php //$this->renderPartial('_search',array(
	//'model'=>$model,
//)); ?>
</div>--><!-- search-form -->

<div class="btnalign">
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'user/create',
        'dialogTitle' => 'Create user',
        'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New user',
        'closeButtonText' => 'Close',
		'id' => 'newUser',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
		//'renderOpenButton' => false,
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'user-grid', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
?>
</div>

<?php 
$uniqid=md5(uniqid());
$this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => 'striped bordered',
	'columns'=>array(
		//'user_id',
		array(
            'header'=>'#',
            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
			//'class'=>'bootstrap.widgets.TbRelationalColumn',
			//'url' => $this->createUrl('clientContact/loadByClient'),
			//'uniqid'=>$uniqid,
			//'afterAjaxUpdate' => 'js:function(tr,rowid,data){
					//}'
        ),
		
		'user_name',
		'login_name',
		//'login_pwd',
		'last_login_time',
		//'branch.branch_name',
            array(
		'name'=>'branch_name',
		'header'=>'Branch Name',
		'value'=>'$data->branch->branch_name',
		'htmlOptions' => array('style'=>'width:200px'),
		//'filter'=>CodeValue::model()->getValuesArryByType('is_client'), 
		
		
		),
		/*
		'crtd_by',
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
		*/
		array(
			'class'=>'EJuiDlgsColumn',
			'htmlOptions' => array('style'=>'width:60px'),
			'deleteButtonUrl'=>'Yii::app()->createUrl("/user/delete", array("id" => $data->user_id))',
			'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
                    ),
					
					
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'user/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View user',
			 'closeOnAction' => true,
			 'refreshGridId' => 'user-grid',
                         'hideTitleBar' => false, 
              'dialogWidth' => '80%',
        		'dialogHeight' => '300',
        ),
		'updateDialog'=>array(
             'controllerRoute' => 'user/update', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update user',
			 'closeOnAction' => true,
			  'refreshGridId' => 'user-grid',
			  'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
             //'dialogWidth' => 800, //use the value from the dialog config
             //'dialogHeight' => 600,
        ),
		),
	),
)); ?>
