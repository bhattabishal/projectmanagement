<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php //echo $form->errorSummary($model); ?>
	
	<div class="row ">
		<?php echo $form->labelEx($model,'old_password'); ?>
		<?php echo $form->passwordField($model,'old_password',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'old_password'); ?>
	</div>
	
	<div class="row ">
		<?php echo $form->labelEx($model,'login_pwd'); ?>
		<?php echo $form->passwordField($model,'login_pwd',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'login_pwd'); ?>
	</div>
	
	<div class="row ">
		<?php echo $form->labelEx($model,'login_pwd_repeat'); ?>
		<?php echo $form->passwordField($model,'login_pwd_repeat',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'login_pwd_repeat'); ?>
	</div>
	
	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->