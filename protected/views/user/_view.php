<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->user_id), array('view', 'id'=>$data->user_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_name')); ?>:</b>
	<?php echo CHtml::encode($data->user_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('login_name')); ?>:</b>
	<?php echo CHtml::encode($data->login_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('login_pwd')); ?>:</b>
	<?php echo CHtml::encode($data->login_pwd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_login_time')); ?>:</b>
	<?php echo CHtml::encode($data->last_login_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_id')); ?>:</b>
	<?php echo CHtml::encode($data->branch_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_by')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_by); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_dt')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_by')); ?>:</b>
	<?php echo CHtml::encode($data->updt_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_dt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_cnt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_cnt); ?>
	<br />

	*/ ?>

</div>