<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php //echo $form->errorSummary($model); ?>

	<div class="row col2">
		<?php echo $form->labelEx($model,'user_name'); ?>
		<?php echo $form->textField($model,'user_name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'user_name'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'login_name'); ?>
		<?php echo $form->textField($model,'login_name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'login_name'); ?>
	</div>
	<div style="clear:both"></div>
	<div class="row col2">
		<?php echo $form->labelEx($model,'login_pwd'); ?>
		<?php echo $form->passwordField($model,'login_pwd',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'login_pwd'); ?>
	</div>
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'login_pwd_repeat'); ?>
		<?php echo $form->passwordField($model,'login_pwd_repeat',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'login_pwd_repeat'); ?>
	</div>
	<div style="clear:both"></div>

	<div class="row col2">
		<?php
		$records = CHtml::listData(Branch::model()->findAll(), 'branch_id', 'branch_name');
		?>
		<?php echo $form->labelEx($model,'branch_id'); ?>
		<?php echo $form->dropDownList($model,'branch_id',$records,array('empty' => 'Select branch')); ?>
		<?php echo $form->error($model,'branch_id'); ?>
	</div>

	<div style="clear:both"></div>
	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->