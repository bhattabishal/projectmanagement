<?php
/* @var $this ChecklistSchemeController */
/* @var $model ChecklistScheme */

$this->breadcrumbs=array(
	'Checklist Schemes'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ChecklistScheme', 'url'=>array('index')),
	array('label'=>'Create ChecklistScheme', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#checklist-scheme-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Checklist Schemes</h1>

<!--<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>-->

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<!--<div class="search-form" style="display:none">
<?php //$this->renderPartial('_search',array(
	//'model'=>$model,
//)); ?>
</div>--><!-- search-form -->
<div class="btnalign">
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'checklistScheme/create',
        'dialogTitle' => 'Create Checklist scheme',
        'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New checklist scheme',
        'closeButtonText' => 'Close',
		'id' => 'newChecklistScheme',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
		//'renderOpenButton' => false,
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'checklistscheme-grid', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
?>
</div>
<?php 
$uniqid=md5(uniqid());
$this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'checklistscheme-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => 'striped bordered',
	'columns'=>array(
		//'chk_scheme_id',
		array(
            'header'=>'#',
            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
			//'class'=>'bootstrap.widgets.TbRelationalColumn',
			//'url' => $this->createUrl('clientContact/loadByClient'),
			//'uniqid'=>$uniqid,
			//'afterAjaxUpdate' => 'js:function(tr,rowid,data){
					//}'
        ),
		
		'jobType.code_lbl',
		'checklistStages.code_lbl',
                'chklist.checklist_details',
		//'crtd_by',
		//'crtd_dt',
		/*
		'updt_by',
		'updt_dt',
		'updt_cnt',
		*/
			array(
			'class'=>'EJuiDlgsColumn',
			'htmlOptions' => array('style'=>'width:60px'),
			'deleteButtonUrl'=>'Yii::app()->createUrl("/checklistScheme/delete", array("id" => $data->chk_scheme_id))',
			'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
                    ),
					
					
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'checklistScheme/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View checklist',
			 'closeOnAction' => true,
			 'refreshGridId' => 'checklistscheme-grid',
                         'hideTitleBar' => false, 
              'dialogWidth' => '80%',
        		'dialogHeight' => '300',
        ),
		'updateDialog'=>array(
             'controllerRoute' => 'checklistScheme/update', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update checklist',
			 'closeOnAction' => true,
			  'refreshGridId' => 'checklistscheme-grid',
			  'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
             //'dialogWidth' => 800, //use the value from the dialog config
             //'dialogHeight' => 600,
        ),
		),
	),
)); ?>
