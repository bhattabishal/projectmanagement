<?php
/* @var $this ChecklistSchemeController */
/* @var $data ChecklistScheme */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('chk_scheme_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->chk_scheme_id), array('view', 'id'=>$data->chk_scheme_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('chklist_id')); ?>:</b>
	<?php echo CHtml::encode($data->chklist_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_type_id')); ?>:</b>
	<?php echo CHtml::encode($data->job_type_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('checklist_stages')); ?>:</b>
	<?php echo CHtml::encode($data->checklist_stages); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_by')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_dt')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_by')); ?>:</b>
	<?php echo CHtml::encode($data->updt_by); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_dt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_cnt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_cnt); ?>
	<br />

	*/ ?>

</div>