<?php
/* @var $this ChecklistSchemeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Checklist Schemes',
);

$this->menu=array(
	array('label'=>'Create ChecklistScheme', 'url'=>array('create')),
	array('label'=>'Manage ChecklistScheme', 'url'=>array('admin')),
);
?>

<h1>Checklist Schemes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
