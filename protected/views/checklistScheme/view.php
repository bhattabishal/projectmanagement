<?php
/* @var $this ChecklistSchemeController */
/* @var $model ChecklistScheme */

$this->breadcrumbs=array(
	'Checklist Schemes'=>array('index'),
	$model->chk_scheme_id,
);

$this->menu=array(
	array('label'=>'List ChecklistScheme', 'url'=>array('index')),
	array('label'=>'Create ChecklistScheme', 'url'=>array('create')),
	array('label'=>'Update ChecklistScheme', 'url'=>array('update', 'id'=>$model->chk_scheme_id)),
	array('label'=>'Delete ChecklistScheme', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->chk_scheme_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ChecklistScheme', 'url'=>array('admin')),
);
?>

<h1>View ChecklistScheme #<?php echo $model->chk_scheme_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'chk_scheme_id',
		'chklist_id',
		'job_type_id',
		'checklist_stages',
		'crtd_by',
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
	),
)); ?>
