<?php
/* @var $this ChecklistSchemeController */
/* @var $model ChecklistScheme */

$this->breadcrumbs=array(
	'Checklist Schemes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ChecklistScheme', 'url'=>array('index')),
	array('label'=>'Manage ChecklistScheme', 'url'=>array('admin')),
);
?>

<h1>Create ChecklistScheme</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>