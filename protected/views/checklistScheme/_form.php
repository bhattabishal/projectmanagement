<?php
/* @var $this ChecklistSchemeController */
/* @var $model ChecklistScheme */
/* @var $form CActiveForm */
?>

<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'checklist-scheme-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>--><!---->

	<?php echo $form->errorSummary($model); ?>

	<div class="row col2">
		<?php
		$records = CHtml::listData(CodeValue::model()->findAll("code_type= 'checklist_stages'"), 'code_id', 'code_lbl');
		?>
		<?php  echo $form->labelEx($model,'checklist_stages'); ?>
		<?php echo $form->dropDownList($model,'checklist_stages',$records,array('empty' => 'Select stages')); ?>
		<?php echo $form->error($model,'checklist_stages'); ?>
	</div>

	<div class="row col2">
		<?php
		$records = CHtml::listData(CodeValue::model()->findAll("code_type= 'project_type'"), 'code_id', 'code_lbl');
		?>
		<?php echo $form->labelEx($model,'job_type_id'); ?>
		<?php echo $form->dropDownList($model,'job_type_id',$records,array('empty' => 'Select project type')); ?>
		<?php echo $form->error($model,'job_type_id'); ?>
	</div>
	<div style="clear:both"></div>
	<div class="row col2" style="width:100%">
		<?php
		$records = CHtml::listData(Checklist::model()->findAll(), 'checklist_id', 'checklist_details');
		$options=array();
		?>
		<?php echo $form->labelEx($model,'chklist_id'); ?>
		<?php echo $form->dropDownList($model,'chklist_id',$records,array('multiple' => 'multiple','options' => $options,'class'=>'biglist'));  ?>
		
		<?php echo $form->error($model,'chklist_id'); ?>
		
	</div>

	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->