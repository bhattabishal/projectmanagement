<?php
/* @var $this ChecklistSchemeController */
/* @var $model ChecklistScheme */

$this->breadcrumbs=array(
	'Checklist Schemes'=>array('index'),
	$model->chk_scheme_id=>array('view','id'=>$model->chk_scheme_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ChecklistScheme', 'url'=>array('index')),
	array('label'=>'Create ChecklistScheme', 'url'=>array('create')),
	array('label'=>'View ChecklistScheme', 'url'=>array('view', 'id'=>$model->chk_scheme_id)),
	array('label'=>'Manage ChecklistScheme', 'url'=>array('admin')),
);
?>

<h1>Update ChecklistScheme <?php echo $model->chk_scheme_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>