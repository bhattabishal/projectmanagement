<?php
/* @var $this CodeValueMasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Code Value Masters',
);

$this->menu=array(
	array('label'=>'Create CodeValueMaster', 'url'=>array('create')),
	array('label'=>'Manage CodeValueMaster', 'url'=>array('admin')),
);
?>

<h1>Code Value Masters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
