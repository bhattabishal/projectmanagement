<?php
/* @var $this CodeValueMasterController */
/* @var $model CodeValueMaster */



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#code-value-master-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<!--<div class="search-form" style="display:none">
<?php 
//$this->renderPartial('_search',array(
	//'model'=>$model,
//)); ?>
</div>--><!-- search-form -->



<?php
$uniqid=md5(uniqid());
 $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'code-value-master-grid',
	'dataProvider'=>$model->search(),
//	'filter'=>$model,
	'type' => 'striped bordered',
	'columns'=>array(
		array(
            'header'=>'#',
            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		  //  'code_master_id',
		 
			'class'=>'bootstrap.widgets.TbRelationalColumn',
			'url' => $this->createUrl('codeValue/loadByMaster'),
			'uniqid'=>$uniqid,
			'afterAjaxUpdate' => 'js:function(tr,rowid,data){
					}'
		    
        ),
		
		
				
		//'code_master_id',
		//'code_type',
		'code_type_lbl',
		//'edit_allowed',
		//array(
			//'class'=>'CButtonColumn',
		//),
		array(
		
          'class'=>'EJuiDlgsColumn',
			'template'=>'{update}',
			'buttons'=>array(
                        'update'=>
                             array(
                        'label'=>'ajax dialog update',
						'options'=>array(
						'class'=>'codeValueMasterUpdate'.$uniqid,
						)
                    ),
					),
		'updateDialog'=>array(
             'controllerRoute' => 'codeValueMaster/update', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update Code Label',
			 'closeOnAction' => true,
			  'refreshGridId' => 'codeValueMaster-form-grid',
			  'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
          
        ),
		
           
        ),
	
	),
)); ?>
