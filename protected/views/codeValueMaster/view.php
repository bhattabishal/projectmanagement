<?php
/* @var $this CodeValueMasterController */
/* @var $model CodeValueMaster */

$this->breadcrumbs=array(
	'Code Value Masters'=>array('index'),
	$model->code_master_id,
);

$this->menu=array(
	array('label'=>'List CodeValueMaster', 'url'=>array('index')),
	array('label'=>'Create CodeValueMaster', 'url'=>array('create')),
	array('label'=>'Update CodeValueMaster', 'url'=>array('update', 'id'=>$model->code_master_id)),
	array('label'=>'Delete CodeValueMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->code_master_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CodeValueMaster', 'url'=>array('admin')),
);
?>

<h1>View CodeValueMaster #<?php echo $model->code_master_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'code_master_id',
		'code_type',
		'code_type_lbl',
		'edit_allowed',
	),
)); ?>
