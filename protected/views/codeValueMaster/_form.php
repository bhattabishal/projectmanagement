<?php
/* @var $this CodeValueMasterController */
/* @var $model CodeValueMaster */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'code-value-master-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

<!--	<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'code_type'); ?>
		<?php echo $form->textField($model,'code_type',array('size'=>60,'maxlength'=>150,'disabled'=>'true')); ?>
		<?php echo $form->error($model,'code_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'code_type_lbl'); ?>
		<?php echo $form->textField($model,'code_type_lbl',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'code_type_lbl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'edit_allowed'); ?>
		<?php echo $form->textField($model,'edit_allowed',array('size'=>3,'maxlength'=>3,'disabled'=>'true')); ?>
		<?php echo $form->error($model,'edit_allowed'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->