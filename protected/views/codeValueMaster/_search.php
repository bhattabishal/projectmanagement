<?php
/* @var $this CodeValueMasterController */
/* @var $model CodeValueMaster */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'code_master_id'); ?>
		<?php echo $form->textField($model,'code_master_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'code_type'); ?>
		<?php echo $form->textField($model,'code_type',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'code_type_lbl'); ?>
		<?php echo $form->textField($model,'code_type_lbl',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'edit_allowed'); ?>
		<?php echo $form->textField($model,'edit_allowed',array('size'=>3,'maxlength'=>3)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->