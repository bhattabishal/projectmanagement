<?php
/* @var $this CodeValueMasterController */
/* @var $model CodeValueMaster */

$this->breadcrumbs=array(
	'Code Value Masters'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CodeValueMaster', 'url'=>array('index')),
	array('label'=>'Manage CodeValueMaster', 'url'=>array('admin')),
);
?>

<h1>Create CodeValueMaster</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>