<?php
/* @var $this CodeValueMasterController */
/* @var $data CodeValueMaster */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('code_master_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->code_master_id), array('view', 'id'=>$data->code_master_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('code_type')); ?>:</b>
	<?php echo CHtml::encode($data->code_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('code_type_lbl')); ?>:</b>
	<?php echo CHtml::encode($data->code_type_lbl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('edit_allowed')); ?>:</b>
	<?php echo CHtml::encode($data->edit_allowed); ?>
	<br />


</div>