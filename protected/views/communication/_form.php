<?php
/* @var $this CommunicationController */
/* @var $model Communication */
/* @var $form CActiveForm */
?>




	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php echo $form->errorSummary($model); ?>

	<div class="row col2">
		
		<?php echo $form->labelEx($model,'project_id'); ?>
		<?php echo $model->project->project_title; ?>
		<?php //echo $form->error($model,'project_id'); ?>
	</div>
	<div class="row col2">
		<?php echo $form->labelEx($model,'communication_subject'); ?>
		<?php echo $form->textField($model,'communication_subject',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'communication_subject'); ?>
	</div>
	<div style="clear:both"></div>
	
		<div class="row col2" style="width:97%">
		<?php echo $form->labelEx($model,'communication_details'); ?>
		<?php echo $form->textArea($model,'communication_details',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'communication_details'); ?>
	</div>
	<div style="clear:both"></div>

	

	
	<!--<div class="row col2">
		<?php
		//$records = CHtml::listData(Branch::model()->findAll(array('order' => 'branch_id')), 'branch_id', 'branch_name');
		?>
		<?php //echo $form->labelEx($model,'comm_from'); ?>
		<?php //echo $form->dropDownList($model,'comm_from',$records,array('empty' => 'Select Communication From')); ?>
		<?php //echo $form->error($model,'comm_from'); ?>
	</div>-->

	<div class="row col2">
		<?php
			//$checked=false;
			//if(!$model->isNewRecord)
			//{
				//if($model->comm_from <= 0)
				//{
					//$checked=true;
				//}
			//}
		?>
		<?php //echo $form->labelEx($model,'is_client'); ?>
		<?php //echo $form->checkBox($model,'is_client',array('checked'=>$checked,'value'=>1)); ?>
		<?php //echo $form->error($model,'is_client'); ?>
		
		<?php
		$list = Branch::model()->getBranchAndClientDropDown();?>
		<?php echo $form->labelEx($model,'comm_from'); ?>
		<?php echo $form->dropDownList($model,'comm_from',$list); ?>
		<?php echo $form->error($model,'comm_from'); ?>
		
			
			
	</div>

<div class="row col2">
		<?php
			$checked=false;
			if(!$model->isNewRecord)
			{
				if($model->is_internal=="Yes")
				{
					$checked=true;
				}
			}
		?>
		<?php echo $form->labelEx($model,'is_internal'); ?>
		<?php echo $form->checkBox($model,'is_internal',array('checked'=>$checked,'value'=>1)); ?>
		<?php echo $form->error($model,'is_internal'); ?>
	</div>
	<div style="clear:both"></div>
	<div class="row col2" >
		<?php echo $form->labelEx($model,'communication_date'); ?>
		<?php 
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $model,
		    'attribute'=>'communication_date',
		    //'flat'=>true,//remove to hide the datepicker
		    'options'=>array(
		        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
				'dateFormat' => 'yy-mm-dd',
				'showButtonPanel' => true,      // show button panel
		    ),
		    'htmlOptions'=>array(
		        'style'=>''
		    ),
		));
	  ?>
		<?php echo $form->error($model,'communication_date'); ?>
	</div>
	

	<div class="row col2">
		<?php
			if(!$model->isNewRecord)
			{
				$files = Yii::app()->db->createCommand()
    			->select('communication_attachment_id,attachment')
    			->from('pm_communication_attachment')
			    ->where("communication_id = $model->communication_id")
			    ->queryAll();
				if(count($files) > 0)
				{
					echo '<div id="files_wrap_list" class="MultiFile-list">';
					foreach($files as $key=>$val)
					{
						//$file = '/images/'. $val['communication_attachment_id'] . '/'. $val['attachment'];
			            //echo '<div id="forAjaxRefresh">';
			           // echo $val['attachment'];
			           // echo "<br />";
			           // echo CHtml::ajaxLink('remove', array('delimg', 'id'=>$val['communication_attachment_id']), array('update'=>'#forAjaxRefresh'));
			            //echo '</div>';
						
						echo '<div class="MultiFile-label" id="forAjaxRefresh'.$val['communication_attachment_id'].'">';
							//echo '<a class="MultiFile-remove" href="#files_wrap">x</a>';
							echo CHtml::ajaxLink('x', array('deleteFile', 'cfid'=>$val['communication_attachment_id'],'file'=>$val['attachment']), array('update'=>'#forAjaxRefresh'.$val['communication_attachment_id']),array('id'=>'delId'.$val['communication_attachment_id'],));
							echo '<span class="MultiFile-title" title="File selected: '.$val['attachment'].'">'.$val['attachment'].'</span>';
						echo '</div>';
					}
					echo '</div>';
				}
			}
		
		
		?>
		<?php
  $this->widget('CMultiFileUpload', array(
     'model'=>$model,
     'name'=>'files',
     'accept'=>'jpg|png|gif|pdf|zip|rar|doc|dwg|docx|xls|xlsx|ppt|pptx|jpeg|mp3',
     'options'=>array(
        // 'onFileSelect'=>'function(e, v, m){ alert("onFileSelect - "+v) }',
        // 'afterFileSelect'=>'function(e, v, m){ alert("afterFileSelect - "+v) }',
        // 'onFileAppend'=>'function(e, v, m){ alert("onFileAppend - "+v) }',
        // 'afterFileAppend'=>'function(e, v, m){ alert("afterFileAppend - "+v) }',
        // 'onFileRemove'=>'function(e, v, m){ alert("onFileRemove - "+v) }',
        // 'afterFileRemove'=>'function(e, v, m){ alert("afterFileRemove - "+v) }',
     ),
     'denied'=>'File is not allowed',
     'max'=>10, // max 10 files
 
 
  ));
?>
	</div>
	<div style="clear:both"></div>
	
	

	

	
	
	






