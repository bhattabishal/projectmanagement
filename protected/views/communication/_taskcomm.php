
<div class="headingalign">
<?php
$uniqid=md5(uniqid());
//echo CHtml::tag('h3',array(),'RELATIONAL DATA EXAMPLE ROW : "'.$id.'"');
echo CHtml::tag('h6',array(),'Task Communication Details');
?>
</div>

<div class="btnalign">
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'communication/create',
		'actionParams' => array('pid'=>0,'tid'=>$tid),
        'dialogTitle' => 'Create Task Communication',
       'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New task communication',
        'closeButtonText' => 'Close',
		'id' => 'newTaskComm',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'gridTaskComm', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
//end new btn
?>
</div>
<?php
//print_r($data->getAttributes());
//$color_code = "#ff0000";
$this->widget('bootstrap.widgets.TbExtendedGridView', array(
'type'=>' bordered',
'id' => 'gridTaskComm',
//'id'=>'grid-'.$uniqid,
//'pagerCssClass'=>'pagination pager-'.$uniqid,
'dataProvider' => $gridDataProvider,
'enableSorting' => false,
'enablePagination'=>false,
//'enableSorting' => false,
//'enablePagination'=>false,
//'emptyText' =>'No records found',
//'rowCssClass'=>'none',
//'rowHtmlOptionsExpression'=>'array("style"=>"background-color:{$data->branch->color_code}","class"=>"self")',
//'template' => "{items}",
'columns'=>array(
	
	array(
            'header'=>'#',
			'value'=> function($data,$row) use ($sno){
	                return $sno."-".$data->communication_id;
	            },
           // 'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
			//'class'=>'bootstrap.widgets.TbRelationalColumn',
			//'url' => $this->createUrl('clientContact/loadByClient'),
			//'uniqid'=>$uniqid,
			//'afterAjaxUpdate' => 'js:function(tr,rowid,data){
					//}'
        ),
	'communication_date',
	'communication_subject',
	'communication_details',
	//'branch.color_code',
		array(
            'class'=>'EJuiDlgsColumn',
			'htmlOptions' => array('style'=>'width:56px'),
			'deleteButtonUrl'=>'Yii::app()->createUrl("/taskCommunication/delete", array("id" => $data->task_communication_id))',
           	//'updateButtonImageUrl'=>Yii::app()->baseUrl .'images/viewdetaildialog.png',
            'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
						'options'=>array(
						'class'=>'taskCommView',
						)
                    ),
					'update'=>
                             array(
                        'options'=>array(
						'class'=>'taskCommUpdate',
						)
                    ),
					'delete'=>
                             array(
                        'options'=>array(
						'class'=>'taskCommDelete',
						)
                    ),
					
					
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'communication/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View Communication',
			  'closeOnAction' => true,
			 'refreshGridId' => 'gridTaskComm',
                         'hideTitleBar' => false, 
             'dialogWidth' => '80%',
        	 'dialogHeight' => '300',
        ),
		'updateDialog'=>array(
             'controllerRoute' => 'communication/update', //=default
             'actionParams' => array('id' => '$data->primaryKey','pid'=>0,'tid'=>$tid), //=default
             'dialogTitle' => 'Update Communication',
			  'closeOnAction' => true,
			 'refreshGridId' => 'gridTaskComm',
			 'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
             //'dialogWidth' => 800, //use the value from the dialog config
             //'dialogHeight' => 600,
        ),
		
           
        ),
),
//'columns' => array_merge(array(array('class'=>'bootstrap.widgets.TbImageColumn')),$gridColumns),
));
?>