<div class="form wide">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'communication-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'), // ADD THIS
	'enableClientValidation' => false,
	//'clientOptions' => array(
                            //'validateOnSubmit' => true,
                            //'afterValidate' => 'js:checkIfEmailToClient',
                   // ),
 	'stateful' => true,


)); 

?>

<?php



$this->widget('bootstrap.widgets.TbWizard', array(
    'type' => 'pills', // 'tabs' or 'pills'
            'options' => array(
                    //'onTabShow' => 'js:function(tab, navigation, index) { if((index+1) > 1) {alert("Tab #" + (index+1));} }',
                    //'onTabClick' => 'js:function(tab, navigation, index) {return false;}',
            ),
            'tabs' => array(
                array('label' => 'Communication Info', 'content' => $this->renderPartial('_form', array('model' => $model, 'form' => $form), true), 'active' => true),
				
				array('label' => 'Email Info', 'content' => $this->renderPartial('_commemail', array('model' => $model, 'form' => $form,), true)),
				

               
            ),
        )
        );
?>

<!--for tiny mce-->
	<?php $this->widget('application.extensions.tinymce.SladekTinyMce'); ?>
 
<script>
    tinymce.init({
    selector: "textarea#Contracts_contractName",
    menubar: false,
    width: 900,
    height: 300,
   toolbar1: "undo redo | bold | italic underline | alignleft aligncenter alignright alignjustify ", 
   toolbar2: "outdent indent | hr | sub sup | bullist numlist | formatselect fontselect fontsizeselect | cut copy paste pastetext pasteword | search replace ", 
 
 }); 
 </script>
 
<script type="text/javascript">
 
    tinymce.init({
    selector: "textarea#Communication_communication_details",
    theme: "modern",
    //width: 900,
   // height: 300,
    //plugins: [
       //  "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
        // "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        // "save table contextmenu directionality emoticons template paste textcolor"
  // ],
   content_css: "css/content.css",
   menubar:false,
   
    toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent"

   //toolbar: "bold italic | alignleft aligncenter alignright alignjustify | outdent indent | forecolor backcolor", 
   //style_formats: [
       // {title: 'Bold text', inline: 'b'},
       // {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
       // {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
       // {title: 'Example 1', inline: 'span', classes: 'example1'},
       // {title: 'Example 2', inline: 'span', classes: 'example2'},
       
    //]
 }); 
 </script>
	<!--end tinymce-->



<?php $this->endWidget(); ?>

</div>


<script type="text/javascript">

/*

function setBtnId()
{
	document.getElementById("emailsendbtn").value="1";
}
function setBtnIdNull()
{
	document.getElementById("emailsendbtn").value="0";
}
function checkIfEmailToClient(form, data, hasError)
{
	var btnId=document.getElementById("emailsendbtn").value;

	//alert(hasError);
	 if(!hasError && btnId==1)
	 {
	 	var y=confirm("Do you want to send email to client?");
		
		if(y)
		{
			document.getElementById("emailtoclient").value="1";
		}
		else{
			document.getElementById("emailtoclient").value="0";
		}
		
		
	 }
	
	  return true;
	
	
}
*/
</script>