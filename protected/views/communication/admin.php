
<!--include javascript files for print-->
<?php
$baseUrl = Yii::app()->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($baseUrl.'/js/custom.js');
$cs->registerScriptFile($baseUrl.'/printArea/jquery.PrintArea.js');

$cs->registerCssFile($baseUrl.'/printArea/PrintArea.css');
$cs->registerCssFile($baseUrl.'/printArea/media_all.css');
$cs->registerCssFile($baseUrl.'/printArea/empty.css');
$cs->registerCssFile($baseUrl.'/printArea/noPrint.css');
$cs->registerCssFile($baseUrl.'/printArea/media_none.css');
$cs->registerCssFile($baseUrl.'/printArea/no_rel_no_media.css');
$cs->registerCssFile($baseUrl.'/printArea/no_rel.css');
?>

<?php 
$color_code = "#ff0000";
//$this->widget('bootstrap.widgets.TbExtendedGridView', array(
$this->widget('application.extensions.eexcel.EExcelView', array(
	'id'=>'communication-grid',
	'dataProvider'=>$model->search(),
	 'title'=>'Title',//for excel export
   	'grid_mode' => 'grid',
	'filename'=>'comm_report',
     'autoWidth'=>true,//for excel export
	'type'=>' bordered',
	// 'fixedHeader' => true,
//'headerOffset' => 40,
	'filter'=>$model,
	'rowCssClass'=>'none',
	//'rowHtmlOptionsExpression'=>'array("style"=>"background-color:{$data->getColor()}","class"=>"{$data->getClass()}")',
	'rowHtmlOptionsExpression'=>'array("style"=>"background-color:{$data->getColor()}")',
	'template' => "{summary}\n{items}\n{exportbuttons}\n{pager}",//for excel
	'columns'=>array(
		
		//array(
           // 'header'=>'#',
			//'value'=>'$data->communication_id',
       // ),
	   array(
			'class' => 'bootstrap.widgets.TbToggleColumn',
			'toggleAction' => 'communication/toggleBookmark',
			'checkedIcon' =>'iconbookmarklight',
			'uncheckedIcon' =>'iconbookmark',
			'checkedButtonLabel'=>'Bookmark',
			'uncheckedButtonLabel'=>'Unbookmark',
			'name' => 'bookmark',
			'header' => '',
			'filter'=>'',
			'excelcolumn' => false,
			),

	  
		array(
			'name'=>'projectCompositeNo',
			//'value'=>'$data->getProjectCompositeNo()',
			'value' => 'CHtml::link($data->getProjectCompositeNo(), Yii::app()
 ->createUrl("home/selectedAdmin",array("pno"=>$data->getProjectCompositeNo())))',
        'type'  => 'raw',
		
		),
		 'communication_id',
		 //'is_internal',
		// array(
		 	//'name'=>'is_internal',
			//'header'=>'Int',
			//'filter'=>array('No'=>'No', 'Yes'=>'Yes'),
		// ),
		array(
	'name'=>'branch_id',
	'value'=>'$data->branch->branch_name',
	'header' => 'Branch',
	//'filter'=>Branch::model()->getBranchName('branch_id'),
	//'headerHtmlOptions' => array('style' => 'width: 110px'),
	),
		
		
		array( 'name'=>'project_search', 'value'=>'$data->project->project_title' ),
		//'communication_date',
                array(
                    'name' => 'communication_date',
                    'htmlOptions' => array('style'=>'width:50px'),
                ),
		'communication_subject',
		//'user.user_name',
		array(
		'name' =>'by_search',
		'header' =>'by',
		'value'=>'$data->user->user_name',
	
		),
		array(
			'name' => 'type',
			  'header' => 'Type',
			  'value' =>function($data,$row) {
			  		if($data->comm_from == 0)
					{
						return "Client";
					}
					else{
						$id=$data->comm_from;
						$row=Yii::app()->db->createCommand("SELECT
pm_code_value.code_id
FROM
pm_branch
INNER JOIN pm_code_value ON pm_branch.branch_type = pm_code_value.code_id where pm_branch.branch_id='$id'")->queryRow();

						if($row['code_id']==14)
						{
							return "Ao";
						}
						else if($row['code_id']==18)
						{
							return "RPS";
						}
						else
						{
							return "";
						}
					}
			  	} ,
			  'filter'=>array(2=>'RPS', 1=>'AO',0=>'Client'),
			  'type' => 'raw',

		),
		array(
			'name' => 'communication_details',
			  'header' => 'Details',
			  'value' => function($data,$row)
			  {
			  	return '<div class="comm_details" id="PrintArea_'.$data->communication_id.'">'.$data->communication_details.'</div>'.'<button type="button" class="printbtn" id="'.$data->communication_id.'">Print</button>';
			  	//return $data->communication_details;
			  },
			  'filter'=>'',
			  'type' => 'raw',
			  'htmlOptions' => array('style'=>'width:586px'),

		),
		array(
			'header'=>'Attachments',
			'type' =>'html',
			'excelcolumn' => false, 
			// call the method 'renderCoordAo' from the model
            'value'=>function($data,$row) {
	                $files = Yii::app()->db->createCommand()
    			->select('attachment')
    			->from('pm_communication_attachment')
			    ->where("communication_id = $data->communication_id")
			    ->queryAll();
				if(count($files) > 0)
			{
				$placesLinks = array();
				foreach($files as $key=>$val)
				{
					$placesLinks[] =CHtml::link($val['attachment'], Yii::app()->createUrl("home/downloadFile", array("file"=>$val['attachment'])));
				}
				
				return implode(', ', $placesLinks);
			}
			else{
				return "..no files..";
			}
	            },
		),
		array(
			'header'=>'AT',
			// call the method 'renderCoordAo' from the model
            'value'=>'$data->getTaskCount()',
		),
		array(
			'header'=>'Varn',
			// call the method 'renderCoordAo' from the model
            'value'=>'$data->getTaskVariation()',
		),
		
		 array(
'class' => 'bootstrap.widgets.TbToggleColumn',
'toggleAction' => 'communication/toggleStatus',
'checkedButtonLabel'=>'Unhide',
'uncheckedButtonLabel'=>'Hide',
'name' => 'status',
'header' => 'Sts',
'excelcolumn' => false, 
'filter'=>array(0=>'Unhidden', 1=>'hidden'),
'htmlOptions' => array('style'=>'width:86px'),
),
'mail_sent',
 
		array(
            'class'=>'EJuiDlgsColumn',
			'htmlOptions' => array('style'=>'width:56px'),
			'deleteButtonUrl'=>'Yii::app()->createUrl("/communication/delete", array("id" => $data->communication_id))',
			 'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
						'options'=>array(
						'class'=>'commView',
						)
                    ),
					'update'=>
                             array(
                        'options'=>array(
						'class'=>'commUpdate',
						)
                    ),
					'delete'=>
                             array(
                        'options'=>array(
						'class'=>'commDelete',
						)
                    ),
					
					
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'communication/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View Communication',
			  'closeOnAction' => true,
			 'refreshGridId' => 'communication-grid',
                         'hideTitleBar' => false, 
             'dialogWidth' => '80%',
        	 'dialogHeight' => '300',
        ),
		'updateDialog'=>array(
		'controllerRoute' => 'communication/updateCommOnly', //=default
            'actionParams' => array('id' => '$data->primaryKey','pid'=>'$data->project_id'), //=default
             'dialogTitle' => 'Update Communication',
			  'closeOnAction' => true,
			 'refreshGridId' => 'communication-grid',
			 'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						  'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ),
		),
			),
		/*
		'crtd_by',
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
		'communication_subject',
		'project_associates_id',
		*/
		
	),
)); ?>

<script>
    $(document).ready(function(){

       

        $(".printbtn").click(function(){

      	var id = this.id;
		var mode = "popup";//iframe,popup
		var close = true;
		 var print ="div#PrintArea_" + id;
		 var keepAttr = [];
		 var extraCss="";
		 
		// keepAttr.push("Class");
		 //keepAttr.push("ID");
		// keepAttr.push("Style");
		 var headElements =  '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>'
		 ;
		 var options = { mode : mode,strict:false, popClose : close, extraCss : extraCss, retainAttr : keepAttr, extraHead : headElements };
		 $(print).printArea(options);
		
        });

        
    });

  </script>
