<?php
/* @var $this CommunicationController */
/* @var $model Communication */
/* @var $form CActiveForm */
?>


<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'communication-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'), // ADD THIS
	'enableClientValidation' => true,
	'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'afterValidate' => 'js:checkIfEmailToClient',
                    ),
 	'stateful' => false,


)); 

?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php echo $form->errorSummary($model); ?>

	<div class="row col2">
		
		<?php echo $form->labelEx($model,'project_id'); ?>
		
		<?php 
		$records = CHtml::listData(Project::model()->findAll(array('order' => 'project_title','condition'=>"project_status != '5'")), 'project_id', 'project_title');
		echo $form->dropDownList($comm,'project_id',$records,array('empty' => 'Select project')); 
		 ?>
	</div>
	<div class="row col2">
		<?php echo $form->labelEx($model,'communication_subject'); ?>
		<?php echo $form->textField($model,'communication_subject',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'communication_subject'); ?>
	</div>
	<div style="clear:both"></div>
	
		<div class="row col2">
		<?php echo $form->labelEx($model,'communication_details'); ?>
		<?php echo $form->textArea($model,'communication_details',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'communication_details'); ?>
	</div>
	

	

	<div class="row col2">
		<?php echo $form->labelEx($model,'communication_date'); ?>
		<?php 
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $model,
		    'attribute'=>'communication_date',
		    //'flat'=>true,//remove to hide the datepicker
		    'options'=>array(
		        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
				'dateFormat' => 'yy-mm-dd',
				'showButtonPanel' => true,      // show button panel
		    ),
		    'htmlOptions'=>array(
		        'style'=>''
		    ),
		));
	  ?>
		<?php echo $form->error($model,'communication_date'); ?>
	</div>

	<div class="row col2">
		<?php
			$checked=false;
			if(!$model->isNewRecord)
			{
				if($model->comm_from <= 0)
				{
					$checked=true;
				}
			}
		?>
		<?php echo $form->labelEx($model,'is_client'); ?>
		<?php echo $form->checkBox($model,'is_client',array('checked'=>$checked,'value'=>1)); ?>
		<?php echo $form->error($model,'is_client'); ?>
	</div>

<div class="row col2">
		<?php
			$checked=false;
			if(!$model->isNewRecord)
			{
				if($model->is_internal=="Yes")
				{
					$checked=true;
				}
			}
		?>
		<?php echo $form->labelEx($model,'is_internal'); ?>
		<?php echo $form->checkBox($model,'is_internal',array('checked'=>$checked,'value'=>1)); ?>
		<?php echo $form->error($model,'is_internal'); ?>
	</div>
	<div style="clear:both"></div>
	
	

	<div class="row col2">
		<?php
			if(!$model->isNewRecord)
			{
				$files = Yii::app()->db->createCommand()
    			->select('communication_attachment_id,attachment')
    			->from('pm_communication_attachment')
			    ->where("communication_id = $model->communication_id")
			    ->queryAll();
				if(count($files) > 0)
				{
					echo '<div id="files_wrap_list" class="MultiFile-list">';
					foreach($files as $key=>$val)
					{
						//$file = '/images/'. $val['communication_attachment_id'] . '/'. $val['attachment'];
			            //echo '<div id="forAjaxRefresh">';
			           // echo $val['attachment'];
			           // echo "<br />";
			           // echo CHtml::ajaxLink('remove', array('delimg', 'id'=>$val['communication_attachment_id']), array('update'=>'#forAjaxRefresh'));
			            //echo '</div>';
						
						echo '<div class="MultiFile-label" id="forAjaxRefresh'.$val['communication_attachment_id'].'">';
							//echo '<a class="MultiFile-remove" href="#files_wrap">x</a>';
							echo CHtml::ajaxLink('x', array('deleteFile', 'cfid'=>$val['communication_attachment_id'],'file'=>$val['attachment']), array('update'=>'#forAjaxRefresh'.$val['communication_attachment_id']),array('id'=>'delId'.$val['communication_attachment_id'],));
							echo '<span class="MultiFile-title" title="File selected: '.$val['attachment'].'">'.$val['attachment'].'</span>';
						echo '</div>';
					}
					echo '</div>';
				}
			}
		
		
		?>
		<?php
  $this->widget('CMultiFileUpload', array(
     'model'=>$model,
     'name'=>'files',
     'accept'=>'jpg|png|gif|pdf|zip|rar|doc|dwg|docx|xls|xlsx|ppt|pptx|jpeg|mp3',
     'options'=>array(
        // 'onFileSelect'=>'function(e, v, m){ alert("onFileSelect - "+v) }',
        // 'afterFileSelect'=>'function(e, v, m){ alert("afterFileSelect - "+v) }',
        // 'onFileAppend'=>'function(e, v, m){ alert("onFileAppend - "+v) }',
        // 'afterFileAppend'=>'function(e, v, m){ alert("afterFileAppend - "+v) }',
        // 'onFileRemove'=>'function(e, v, m){ alert("onFileRemove - "+v) }',
        // 'afterFileRemove'=>'function(e, v, m){ alert("afterFileRemove - "+v) }',
     ),
     'denied'=>'File is not allowed',
     'max'=>10, // max 10 files
 
 
  ));
?>
	</div>
	<input type="hidden" name="emailtoclient" id="emailtoclient" value="0"/>
	<input  type="hidden" name="emailsendbtn" id="emailsendbtn" value="0"/>
	

	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn','onclick'=>'js:setBtnIdNull()')); ?>
		<?php
			//if($model->isNewRecord)
			//{
				echo CHtml::submitButton($model->isNewRecord ? 'Create and Mail' : 'Save and Mail',array(
				'class' => 'btn',
				'onclick'=>'js:setBtnId()'
				));
			//}
			//echo CHtml::submitButton(Yii::t('tr','Create and Mail'),array('class' => 'btn'),array('confirm'=>Yii::t('tr','Please confirm printing'),));
			if(!$model->isNewRecord)
			{
				echo CHtml::submitButton('Save as New',array(
				'class' => 'btn',
				'onclick'=>'js:setBtnIdNull()',
				));
			}
			
			if(!$model->isNewRecord)
			{
				echo CHtml::submitButton('Save as New and Mail',array(
				'class' => 'btn',
				'onclick'=>'js:setBtnId()'
				));
			}
		?>
		<?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn')); ?>
	</div>
	
	<!--for tiny mce-->
	<?php $this->widget('application.extensions.tinymce.SladekTinyMce'); ?>
 
<script>
    tinymce.init({
    selector: "textarea#Contracts_contractName",
    menubar: false,
    width: 900,
    height: 300,
   toolbar1: "undo redo | bold | italic underline | alignleft aligncenter alignright alignjustify ", 
   toolbar2: "outdent indent | hr | sub sup | bullist numlist | formatselect fontselect fontsizeselect | cut copy paste pastetext pasteword | search replace ", 
 
 }); 
 </script>
 
<script type="text/javascript">
 
    tinymce.init({
    selector: "textarea#Communication_communication_details",
    theme: "modern",
    //width: 900,
   // height: 300,
    //plugins: [
       //  "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
        // "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        // "save table contextmenu directionality emoticons template paste textcolor"
  // ],
   content_css: "css/content.css",
   menubar:false,
   
    toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent"

   //toolbar: "bold italic | alignleft aligncenter alignright alignjustify | outdent indent | forecolor backcolor", 
   //style_formats: [
       // {title: 'Bold text', inline: 'b'},
       // {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
       // {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
       // {title: 'Example 1', inline: 'span', classes: 'example1'},
       // {title: 'Example 2', inline: 'span', classes: 'example2'},
       
    //]
 }); 
 </script>
	<!--end tinymce-->

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
function setBtnId()
{
	document.getElementById("emailsendbtn").value="1";
}
function setBtnIdNull()
{
	document.getElementById("emailsendbtn").value="0";
}
function checkIfEmailToClient(form, data, hasError)
{
	var btnId=document.getElementById("emailsendbtn").value;

	//alert(hasError);
	 if(!hasError && btnId==1)
	 {
	 	var y=confirm("Do you want to send email to client?");
		
		if(y)
		{
			document.getElementById("emailtoclient").value="1";
		}
		else{
			document.getElementById("emailtoclient").value="0";
		}
		
		
	 }
	
	  return true;
	
	
}
</script>