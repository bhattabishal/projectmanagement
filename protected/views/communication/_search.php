<?php
/* @var $this CommunicationController */
/* @var $model Communication */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'communication_id'); ?>
		<?php echo $form->textField($model,'communication_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'project_id'); ?>
		<?php echo $form->textField($model,'project_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'communication_details'); ?>
		<?php echo $form->textArea($model,'communication_details',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'project_contact_id'); ?>
		<?php echo $form->textField($model,'project_contact_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'communication_date'); ?>
		<?php echo $form->textField($model,'communication_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'crtd_by'); ?>
		<?php echo $form->textField($model,'crtd_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'crtd_dt'); ?>
		<?php echo $form->textField($model,'crtd_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updt_by'); ?>
		<?php echo $form->textField($model,'updt_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updt_dt'); ?>
		<?php echo $form->textField($model,'updt_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updt_cnt'); ?>
		<?php echo $form->textField($model,'updt_cnt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'communication_subject'); ?>
		<?php echo $form->textField($model,'communication_subject',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'project_associates_id'); ?>
		<?php echo $form->textField($model,'project_associates_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->