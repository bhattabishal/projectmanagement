<?php
/* @var $this CommunicationController */
/* @var $model Communication */

$this->breadcrumbs=array(
	'Communications'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Communication', 'url'=>array('index')),
	array('label'=>'Create Communication', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#communication-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Communications</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'communication-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'communication_id',
		'project_id',
		'communication_details',
		'project_contact_id',
		'communication_date',
		'user_id',
		/*
		'crtd_by',
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
		'communication_subject',
		'project_associates_id',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
