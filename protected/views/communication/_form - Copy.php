<?php
/* @var $this CommunicationController */
/* @var $model Communication */
/* @var $form CActiveForm */
?>

<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'communication-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); 

?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php //echo $form->errorSummary($model); ?>

	<div class="row col2">
		<?php
		//$records = CHtml::listData(CodeValue::model()->findAll("code_type ='project_status'"), 'code_id', 'code_lbl');
		//$records = CHtml::listData($model->project,'project.project_id', 'project.project_title');
		?>
		<?php echo $form->labelEx($model,'project_id'); ?>
		<?php echo $model->project->project_title; ?>
		<?php //echo $form->error($model,'project_id'); ?>
	</div>
	<div class="row col2">
		<?php echo $form->labelEx($model,'communication_subject'); ?>
		<?php echo $form->textField($model,'communication_subject',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'communication_subject'); ?>
	</div>
	<div style="clear:both"></div>
	
	

	<div class="row col2">
		<?php
		$records = CHtml::listData(Communication::model()->with('projectContact','clientContact')->findAll(), 'projectContact.project_contact_id', 'clientContact.contact_name');
		?>
		<?php echo $form->labelEx($model,'project_contact_id'); ?>
		<?php echo $form->dropDownList($model,'project_contact_id',$records,array('empty' => 'Select contact')); ?>
		<?php echo $form->error($model,'project_contact_id'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'communication_date'); ?>
		<?php 
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $model,
		    'attribute'=>'communication_date',
		    //'flat'=>true,//remove to hide the datepicker
		    'options'=>array(
		        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
				'dateFormat' => 'yy-mm-dd',
				'showButtonPanel' => true,      // show button panel
		    ),
		    'htmlOptions'=>array(
		        'style'=>''
		    ),
		));
	  ?>
		<?php echo $form->error($model,'communication_date'); ?>
	</div>
<div style="clear:both"></div>

	<div class="row col2">
		<?php
		$records = CHtml::listData(User::model()->findAll(), 'user_id', 'user_name');
		?>
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->dropDownList($model,'user_id',$records,array('empty' => 'Select user')); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row col2">
		<?php
		$records = CHtml::listData(Communication::model()->with('projectAssociates','userAssociates')->findAll(), 'projectAssociates.project_associates_id', 'userAssociates.user_name');
		?>
		<?php echo $form->labelEx($model,'project_associates_id'); ?>
		<?php echo $form->dropDownList($model,'project_associates_id',$records,array('empty' => 'Select associates')); ?>
		<?php echo $form->error($model,'project_associates_id'); ?>
	</div>
<div style="clear:both"></div>
	<div class="row col2">
		<?php echo $form->labelEx($model,'communication_details'); ?>
		<?php echo $form->textArea($model,'communication_details',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'communication_details'); ?>
	</div>
	

	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->