<?php
/* @var $this CommunicationController */
/* @var $model Communication */

$this->breadcrumbs=array(
	'Communications'=>array('index'),
	$model->communication_id,
);

$this->menu=array(
	array('label'=>'List Communication', 'url'=>array('index')),
	array('label'=>'Create Communication', 'url'=>array('create')),
	array('label'=>'Update Communication', 'url'=>array('update', 'id'=>$model->communication_id)),
	array('label'=>'Delete Communication', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->communication_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Communication', 'url'=>array('admin')),
);
?>



<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'communication_id',
		'project.project_title',
		'communication_subject',
		//'communication_details',
		array(
			'name'=>'communication_details',
			'value'=>$data->communication_details,
			'type'=>'raw',
		),
		'communication_date',
		//'clientContact.contact_name',
		
		'user.user_name',
		
		//'userAssociates.user_name',
	),
)); ?>
