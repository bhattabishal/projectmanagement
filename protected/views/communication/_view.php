<?php
/* @var $this CommunicationController */
/* @var $data Communication */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('communication_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->communication_id), array('view', 'id'=>$data->communication_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('project_id')); ?>:</b>
	<?php echo CHtml::encode($data->project_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('communication_details')); ?>:</b>
	<?php echo CHtml::encode($data->communication_details); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('project_contact_id')); ?>:</b>
	<?php echo CHtml::encode($data->project_contact_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('communication_date')); ?>:</b>
	<?php echo CHtml::encode($data->communication_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_by')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_by); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_dt')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_by')); ?>:</b>
	<?php echo CHtml::encode($data->updt_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_dt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_cnt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_cnt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('communication_subject')); ?>:</b>
	<?php echo CHtml::encode($data->communication_subject); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('project_associates_id')); ?>:</b>
	<?php echo CHtml::encode($data->project_associates_id); ?>
	<br />

	*/ ?>

</div>