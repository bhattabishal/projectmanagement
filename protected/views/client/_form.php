<?php
/* @var $this ClientContactController */
/* @var $model ClientContact */
/* @var $form CActiveForm */
?>

<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'client-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php //echo $form->errorSummary($model); ?>


	<div class="row col2">
		<?php echo $form->labelEx($model,'client_name'); ?>
		<?php echo $form->textField($model,'client_name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'client_name'); ?>
	</div>
	
	
	
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'client_phone'); ?>
		<?php echo $form->textField($model,'client_phone',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'client_phone'); ?>
	</div>

	<div style="clear:both"></div>
	<div class="row col2">
		<?php echo $form->labelEx($model,'client_address'); ?>
		<?php echo $form->textArea($model,'client_address',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'client_address'); ?>
	</div>
	
	<div style="clear:both"></div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->