<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'client_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'client_name',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textAreaRow($model,'client_address',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php echo $form->textFieldRow($model,'client_phone',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'crtd_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'crtd_dt',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updt_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updt_dt',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updt_cnt',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
