<?php
$this->breadcrumbs=array(
	'Clients'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Client','url'=>array('index')),
array('label'=>'Create Client','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('client-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<!--<h1>Manage Clients</h1>-->

<!--<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>-->

<!--<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div>--><!-- search-form -->


<?php
//$this->widget(
//'bootstrap.widgets.TbButton',
//array(
//'label' => 'New client',
 //'url' => 'client/create',
//)
//);
?>

<div class="btnalign">
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'client/create',
        'dialogTitle' => 'Create client',
        'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New client',
        'closeButtonText' => 'Close',
		'id' => 'newClient',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
		//'renderOpenButton' => false,
        'closeOnAction' =>true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'client-grid', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
?>
</div>


<?php 
$uniqid=md5(uniqid());
$this->widget('bootstrap.widgets.TbExtendedGridView',array(
'id'=>'client-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'type' => 'striped bordered',
'columns'=>array(
		//'client_id',
		array(
            'header'=>'#',
            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
			'class'=>'bootstrap.widgets.TbRelationalColumn',
			'url' => $this->createUrl('clientContact/loadByClient'),
			'uniqid'=>$uniqid,
			'afterAjaxUpdate' => 'js:function(tr,rowid,data){
					}'
        ),
		
		
                 array(
                     'name'=>'client_name',
                     'value'=>'$data->client_name',
                     'htmlOptions' => array('style'=>'width:200px'), 
                 ),
		
    array(
                     'name'=>'client_phone',
                     'value'=>'$data->client_phone',
                     'htmlOptions' => array('style'=>'width:100px'), 
                 ),
		'client_address',
		array(
            'class'=>'EJuiDlgsColumn',
			'htmlOptions' => array('style'=>'width:60px'),

			'deleteButtonUrl'=>'Yii::app()->createUrl("/client/delete", array("id" => $data->client_id))',
			
           	//'updateButtonImageUrl'=>Yii::app()->baseUrl .'images/viewdetaildialog.png',
            'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
                    ),
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'client/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View client',
			 'closeOnAction' => true,
			 'refreshGridId' => 'client-grid',
                         'hideTitleBar' => false, 
              'dialogWidth' => '80%',
        		'dialogHeight' => '300',
        ),
		'updateDialog'=>array(
             'controllerRoute' => 'client/update', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update client',
			 'closeOnAction' => true,
			  'refreshGridId' => 'client-grid',
			  'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
             //'dialogWidth' => 800, //use the value from the dialog config
             //'dialogHeight' => 600,
        ),
		
           
        ),
		/*'crtd_by',
		'crtd_dt',
		
		'updt_by',
		'updt_dt',
		'updt_cnt',
		*/
//array(
//'class'=>'bootstrap.widgets.TbButtonColumn',
//),
),
)); ?>
