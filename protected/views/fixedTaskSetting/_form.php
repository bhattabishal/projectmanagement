<?php
/* @var $this FixedTaskSettingController */
/* @var $model FixedTaskSetting */
/* @var $form CActiveForm */
?>

<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fixed-task-setting-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row col2">
		<?php echo $form->labelEx($model,'project_id'); ?>
		<?php 
		//echo $form->textField($model,'project_id');
		echo $form->dropDownList($model,'project_id',FixedTask::model()->listChildTasks(),array('empty' => 'Select  Task'));
		 ?>
		
		<?php echo $form->error($model,'project_id'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'start_date'); ?>
		<?php 
			//echo $form->textField($model,'year');
			echo $form->dropDownList($model,'start_date',$model->getWeeksArray(),array('empty' => 'Select  Week'));
		?>
		<?php echo $form->error($model,'start_date'); ?>
	</div>

	
	<div class="row col2">
		<?php echo $form->labelEx($model,'budget'); ?>
		<?php echo $form->textField($model,'budget',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'budget'); ?>
	</div>

	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->