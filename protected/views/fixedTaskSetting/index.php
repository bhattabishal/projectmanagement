<?php
/* @var $this FixedTaskSettingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fixed Task Settings',
);

$this->menu=array(
	array('label'=>'Create FixedTaskSetting', 'url'=>array('create')),
	array('label'=>'Manage FixedTaskSetting', 'url'=>array('admin')),
);
?>

<h1>Fixed Task Settings</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
