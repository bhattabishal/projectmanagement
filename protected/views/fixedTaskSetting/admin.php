
<div class="btnalign">
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'fixedTaskSetting/create',
        'dialogTitle' => 'Create Budget',
        'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New Budget',
        'closeButtonText' => 'Close',
		'id' => 'newClient',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
		//'renderOpenButton' => false,
        'closeOnAction' =>true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'fixed-task-setting-grid', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
?>
</div>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'fixed-task-setting-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => 'striped bordered',
	'columns'=>array(
		
		'task.title',
		'start_date',
		'end_date',
		'budget',
		'crtd_dt',
		/*
		'crtd_by',
		'updt_by',
		'updt_dt',
		'updt_cnt',
		*/
		array(
			'class'=>'EJuiDlgsColumn',
			'template'=>'{update}{delete}',
			'deleteButtonUrl'=>'Yii::app()->createUrl("/fixedTaskSetting/delete", array("id" => $data->id))',
			'buttons'=>array(
                        
					'update'=>
                             array(
                        'options'=>array(
						'class'=>'commUpdate',
						)
                    ),
					'delete'=>
                             array(
                        'options'=>array(
						'class'=>'commDelete'.$uniqid,
						)
                    ),
					
					
                            ),
                            'updateDialog'=>array(
             'controllerRoute' => 'fixedTaskSetting/update', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update Budget',
			  'closeOnAction' => true,
			 'refreshGridId' => 'fixed-task-setting-grid',
			 'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
             //'dialogWidth' => 800, //use the value from the dialog config
             //'dialogHeight' => 600,
        ),
		
		),
	),
)); ?>
