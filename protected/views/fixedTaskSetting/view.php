<?php
/* @var $this FixedTaskSettingController */
/* @var $model FixedTaskSetting */

$this->breadcrumbs=array(
	'Fixed Task Settings'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FixedTaskSetting', 'url'=>array('index')),
	array('label'=>'Create FixedTaskSetting', 'url'=>array('create')),
	array('label'=>'Update FixedTaskSetting', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FixedTaskSetting', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FixedTaskSetting', 'url'=>array('admin')),
);
?>

<h1>View FixedTaskSetting #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'project_id',
		'year',
		'month',
		'budget',
		'crtd_dt',
		'crtd_by',
		'updt_by',
		'updt_dt',
		'updt_cnt',
	),
)); ?>
