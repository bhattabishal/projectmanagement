<?php
/* @var $this TaskChecklistController */
/* @var $model TaskChecklist */
/* @var $form CActiveForm */
?>

<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'task-checklist-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php //echo $form->errorSummary($model); ?>

	<div class="row col2">
		<?php echo $form->labelEx($model,'task_id'); ?>
		<?php echo $model->task->job_details; ?>
		<?php echo $form->error($model,'task_id'); ?>
	</div>
	
	<div class="row col2">
		<?php
		$records = CHtml::listData(CodeValue::model()->byLabel()->findAll("code_type= 'job_status' and code_id not in (26)"), 'code_id', 'code_lbl');
		?>
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',$records,array('empty' => 'Select status')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'checklist_details'); ?>
		<?php echo $form->textArea($model,'checklist_details',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'checklist_details'); ?>
	</div>

	

	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->