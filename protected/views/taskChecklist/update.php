<?php
/* @var $this TaskChecklistController */
/* @var $model TaskChecklist */

$this->breadcrumbs=array(
	'Task Checklists'=>array('index'),
	$model->task_checklist_id=>array('view','id'=>$model->task_checklist_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TaskChecklist', 'url'=>array('index')),
	array('label'=>'Create TaskChecklist', 'url'=>array('create')),
	array('label'=>'View TaskChecklist', 'url'=>array('view', 'id'=>$model->task_checklist_id)),
	array('label'=>'Manage TaskChecklist', 'url'=>array('admin')),
);
?>

<!--<h1>Update TaskChecklist <?php //echo $model->task_checklist_id; ?></h1>-->

<?php $this->renderPartial('_form', array('model'=>$model)); ?>