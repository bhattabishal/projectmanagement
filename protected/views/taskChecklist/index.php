<?php
/* @var $this TaskChecklistController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Task Checklists',
);

$this->menu=array(
	array('label'=>'Create TaskChecklist', 'url'=>array('create')),
	array('label'=>'Manage TaskChecklist', 'url'=>array('admin')),
);
?>

<h1>Task Checklists</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
