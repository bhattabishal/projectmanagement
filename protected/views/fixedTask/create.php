<?php
/* @var $this FixedTaskController */
/* @var $model FixedTask */

$this->breadcrumbs=array(
	'Fixed Tasks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FixedTask', 'url'=>array('index')),
	array('label'=>'Manage FixedTask', 'url'=>array('admin')),
);
?>

<h1>Create FixedTask</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>