<?php
/* @var $this FixedTaskController */
/* @var $model FixedTask */

$this->breadcrumbs=array(
	'Fixed Tasks'=>array('index'),
	$model->title=>array('view','id'=>$model->fixed_task_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FixedTask', 'url'=>array('index')),
	array('label'=>'Create FixedTask', 'url'=>array('create')),
	array('label'=>'View FixedTask', 'url'=>array('view', 'id'=>$model->fixed_task_id)),
	array('label'=>'Manage FixedTask', 'url'=>array('admin')),
);
?>

<h1>Update FixedTask <?php echo $model->fixed_task_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>