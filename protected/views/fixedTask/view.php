<?php
/* @var $this FixedTaskController */
/* @var $model FixedTask */

$this->breadcrumbs=array(
	'Fixed Tasks'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List FixedTask', 'url'=>array('index')),
	array('label'=>'Create FixedTask', 'url'=>array('create')),
	array('label'=>'Update FixedTask', 'url'=>array('update', 'id'=>$model->fixed_task_id)),
	array('label'=>'Delete FixedTask', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->fixed_task_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FixedTask', 'url'=>array('admin')),
);
?>

<h1>View FixedTask #<?php echo $model->fixed_task_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'fixed_task_id',
		'parent_id',
		'title',
		'crtd_by',
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
	),
)); ?>
