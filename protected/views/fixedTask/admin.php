<div class="btnalign">
<?php



EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'fixedTask/create',
        'dialogTitle' => 'Create Task',
        'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'Create',
        'closeButtonText' => 'Close',
		'id' => 'newClient',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
		//'renderOpenButton' => false,
        'closeOnAction' =>true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'fixed-task-grid', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
?>
</div>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'fixed-task-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => 'striped bordered',
	'columns'=>array(
		
		array(
			'name'=>'parent_id',
			'value'=>function($data,$row){
	                if($data->parent_id <= 0)
	                {
						return "Root";
					}
					else
					{
						return $data->parent->title;
					}
	            },
		),
		
		'title',		
		'crtd_dt',
		
		/*
		'updt_dt',
		'updt_cnt',
		*/
		array(
			'class'=>'EJuiDlgsColumn',
			'template'=>'{update}{delete}',
			'deleteButtonUrl'=>'Yii::app()->createUrl("/fixedTask/delete", array("id" => $data->fixed_task_id))',
			'buttons'=>array(
                        
					'update'=>
                             array(
                        'options'=>array(
						'class'=>'commUpdate',
						)
                    ),
					'delete'=>
                             array(
                        'options'=>array(
						'class'=>'commDelete',
						)
                    ),
					
					
                            ),
                            'updateDialog'=>array(
             'controllerRoute' => 'fixedTask/update', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update Task',
			  'closeOnAction' => true,
			 'refreshGridId' => 'gridComm',
			 'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
             //'dialogWidth' => 800, //use the value from the dialog config
             //'dialogHeight' => 600,
        ),
		
		),
	),
)); ?>
