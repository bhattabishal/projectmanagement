<?php
/* @var $this FixedTaskController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fixed Tasks',
);

$this->menu=array(
	array('label'=>'Create FixedTask', 'url'=>array('create')),
	array('label'=>'Manage FixedTask', 'url'=>array('admin')),
);
?>

<h1>Fixed Tasks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
