<body>
    <h3>Error <?php echo $data['code']; ?> occurred</h3>
    <h4><?php echo nl2br(CHtml::encode($data['message'])); ?></h4>
    <?php if(Yii::app()->user->hasFlash('error')):?>
    <div class="flash-error">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif;?>
</body>