
<?php
/*
$totHour=$rps_hour;
$totNormalHour=$rps_hour-$ot_hour;

$totOTHours=$ot_hour;

$definedNH=CodeValue::model()->getCodeTypeValue('work_hour',28);
$definedCH=CodeValue::model()->getCodeTypeValue('work_hour',29);
$definedOH=CodeValue::model()->getCodeTypeValue('work_hour',30);
		
$i=1;
echo '<h5>Generated Schedule</h5>';	
echo '<table class="table-striped table-bordered">';	
echo '<tr>';
echo '<td> S.N.</td>';
echo '<td>Date</td>';
echo '<td>Normal Hours</td>';
echo '<td>OT Hours</td>';
echo '</tr>';
while (strtotime($due_date) >= strtotime($date)) 
{
	$currentDate=$due_date;
	
	if($totHour <= 0)
	{
		break;
	}
	
	$record=Yii::app()->db->createCommand()
		->select('IFNULL(sum(normal_hour),0) as nh,IFNULL(sum(cusion_hour),0) as ch,IFNULL(sum(overtime_hour),0) as oh')
		->from('pm_task_schedule')
		->where('pm_task_schedule.user_id=:userid',array(':userid'=>$user_id))
		->andWhere('pm_task_schedule.completed_status=0')
		->andWhere('pm_task_schedule.schedule_date=:schedule_date',array(':schedule_date'=>$currentDate))
		->group('schedule_date')
		->queryRow();
		
		if($task_id > 0)
		{
			$record=Yii::app()->db->createCommand()
			->select('IFNULL(sum(normal_hour),0) as nh,IFNULL(sum(cusion_hour),0) as ch,IFNULL(sum(overtime_hour),0) as oh')
			->from('pm_task_schedule')
			->where('pm_task_schedule.user_id=:userid',array(':userid'=>$user_id))
			->andWhere('pm_task_schedule.completed_status=0')
			->andWhere("pm_task_schedule.task_id !=$task_id")
			->andWhere('pm_task_schedule.schedule_date=:schedule_date',array(':schedule_date'=>$currentDate))
			->group('schedule_date')
			->queryRow();
		}
		
		$remainingNormal=floatval($definedNH)-floatval($record['nh']);
		
		$remainingOT=floatval($definedOH)- floatval($record['oh']);
		
		if($ot_hour > 0)
		{
			$remaining=$remainingNormal+$remainingOT;
		}
		else
		{
			$remaining=$remainingNormal;
		}
		
		
		if($remaining > 0 && $totHour > 0)
		{
			$j=0;
			while($remainingNormal >0 && $totNormalHour > 0  )
			{
				$remainingNormal=$remainingNormal-1;
				$totNormalHour=$totNormalHour-1;
				$totHour=$totHour-1;
				$j=$j+1;
			}
			
			$k=0;
			if($ot_hour > 0)
			{
				while ($remainingOT > 0 && $totOTHours > 0)
				{
					$remainingOT=$remainingOT-1;
					$totHour=$totHour-1;
					$totOTHours=$totOTHours-1;
					$k=$k+1;
				}
			}
			
			
		
			?>
			<!--<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo $currentDate; ?></td>
				<td><?php echo $j; ?></td>
				<td><?php echo $k; ?></td>
			</tr>-->
			<?php
		}
		
		$i++;
	
	$due_date = date ("Y-m-d", strtotime("-1 day", strtotime($due_date)));
}
echo '<tr>';
echo '<td colspan="3" align="right"><b>Total Hours</b> </td>';
echo '<td>';
echo $rps_hour-$totHour;
echo '</td>';
echo '</tr>';
echo '</table>';
*/

echo '<h5>Generated Schedule</h5>';	
echo '<table class="table-striped table-bordered">';	
echo '<tr>';
echo '<td> S.N.</td>';
echo '<td>Date</td>';
echo '<td>Normal Hours Available</td>';
echo '<td>OT Hours Available</td>';
echo '<td>Normal Hours</td>';
echo '<td>OT Hours</td>';
echo '</tr>';

$i=1;
$totHour=0;
foreach($taskSchedule as $schedule)
{
	$nclass="";
	$otclass="";
	if(floatval($schedule['normal_hour']) > floatval($schedule['actualNH']))
	{
		$nclass='class="busy"';
	}
	
	if(floatval($schedule['overtime_hour']) > floatval($schedule['actualOH']))
	{
		$otclass='class="busy"';
	}
	
	?>
	<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo $schedule['schedule_date']; ?></td>
				<td><?php echo $schedule['actualNH']; ?></td>
				<td><?php echo $schedule['actualOH']; ?></td>
				<td <?php echo $nclass; ?>><?php echo $schedule['normal_hour']; ?></td>
				<td <?php echo $otclass; ?>><?php echo $schedule['overtime_hour']; ?></td>
			</tr>
	<?php
	$totHour+=$schedule['normal_hour']+$schedule['overtime_hour'];
        $i++;
}
$totclass="";
if($totHour != $rps_hour)
{
	$totclass='class="busy"';
}
echo '<tr>';
echo '<td colspan="5" align="right">';
echo '<b>Total Hours</b>';
echo '</td>';
echo '<td '.$totclass.'>';
echo '<b>';
echo $totHour;
echo '</b></td>';
echo '</tr>';
echo '</table>';
?>
<style>
	td.busy
	{
		border: 2px solid #ff0000;
	}
</style>
