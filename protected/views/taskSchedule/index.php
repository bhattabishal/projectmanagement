<?php
/* @var $this TaskScheduleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Task Schedules',
);

$this->menu=array(
	array('label'=>'Create TaskSchedule', 'url'=>array('create')),
	array('label'=>'Manage TaskSchedule', 'url'=>array('admin')),
);
?>

<h1>Task Schedules</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
