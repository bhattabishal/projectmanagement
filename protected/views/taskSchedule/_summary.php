<div style="margin: 5px 0 5px 10px;font-weight:bold">Assigned Tasks Details</div>

<table class="table">
	<tr>
		<td>#</td>
		<td>Date</td>
		<td>Task No</td>
		<td>Total Normal Hours</td>
	<!--	<td>Total Cusion Hours</td>-->
		<td>Total Overtime Hours</td>
	</tr>
	<?php
	if(count($results) > 0)
	{
		$i=1;
		foreach($results as $result)
		{
			?>
			<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo $result['task_date']; ?></td>
				<td><?php echo $result['project_number']; ?></td>
				<td><?php echo $result['sum_normal_hour']; ?></td>
			<!--	<td><?php //echo $result['sum_cusion_hour']; ?></td>-->
				<td><?php echo $result['sum_overtime_hour']; ?></td>
			</tr>
			<?php
			$i++;
		}
		
	}
	else
	{
		?>
		<tr>
			<td colspan="6" align="middle">No Task Assigned</td>
		</tr>
		<?php
	}
	?>
</table>