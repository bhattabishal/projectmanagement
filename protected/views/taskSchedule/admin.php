<div class="btnalign">
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'taskSchedule/create',
        'dialogTitle' => 'Create Schedule',
        'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New Schedule',
        'closeButtonText' => 'Close',
		'id' => 'newClient',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
		//'renderOpenButton' => false,
        'closeOnAction' =>true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'task-schedule-grid', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'task-schedule-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => 'striped bordered',
	'columns'=>array(
		array(
		'header'=>'#',
            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		),
		array(
		'name'=>'projectCompositeNo',
		'header'=>'Project No',
		'value'=>'$data->getProjectCompositeNo()',
		)
		,
		array(
		'name'=>'communication_no',
		'header'=>'Comm No',
		'value'=>'$data->communication->communication_id',
		),
		array(
		'name' =>'user_search',
		'header' =>'Assigned To',
		'value'=>'$data->user->user_name',
	
		),
		
		'task_id',
		
		
		'schedule_date',
		'normal_hour',
		//'cusion_hour',
		'overtime_hour',
		array(
			'name'=>'task.is_planned',
			'header'=>'Is Planned',
			'value'=>function($data,$row) {
			  		if($data->task->is_planned == 0)
					{
						return "No";
					}
					else{					
							return "Yes";					
					}
			  	}
		),
		/*
		'remarks',
		'crtd_dt',
		'crtd_by',
		'updt_dt',
		'updt_by',
		'updt_cnt',
		*/
		array(
            'class'=>'EJuiDlgsColumn',
			'htmlOptions' => array('style'=>'width:60px'),

			'deleteButtonUrl'=>'Yii::app()->createUrl("/taskSchedule/delete", array("id" => $data->schedule_id))',
			
           	//'updateButtonImageUrl'=>Yii::app()->baseUrl .'images/viewdetaildialog.png',
            'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
                    ),
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'taskSchedule/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View Schedule',
			 'closeOnAction' => true,
			 'refreshGridId' => 'task-schedule-grid',
                         'hideTitleBar' => false, 
              'dialogWidth' => '80%',
        		'dialogHeight' => '300',
        ),
		'updateDialog'=>array(
             'controllerRoute' => 'taskSchedule/update', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update Schedule',
			 'closeOnAction' => true,
			  'refreshGridId' => 'task-schedule-grid',
			  'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
             //'dialogWidth' => 800, //use the value from the dialog config
             //'dialogHeight' => 600,
        ),
		
           
        ),
	),
)); ?>
