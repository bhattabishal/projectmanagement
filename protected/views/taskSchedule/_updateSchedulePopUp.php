<?php
/*
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
                'id'=>'jobDialog',
                'options'=>array(
                                    'title'=>Yii::t('studentBasicinfo','Update Schedule'),
                                    'autoOpen'=>true,
                                    'modal'=>'true',
                                    'width'=>1050,
         							'height'=>auto,
                                   
                                    ),
                ) );
                */
?>

<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'task-schedule-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row col2">
		<?php echo $form->labelEx($model,'task_id'); ?>
		<?php 
		$records = Task::model()->getUserAssignedActiveTaskList(0); 
                
                $disabled="disabled";
               // if($model->isNewRecord)
                //{
                      //$disabled="";
                //}
			
			echo $form->dropDownList($model,'task_id',$records,array('empty' => 'Select task','disabled'=>'disabled')); 
		?>
		<?php echo $form->error($model,'task_id'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'schedule_date'); ?>
		<?php 
		
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $model,
		    'attribute'=>'schedule_date',
			
		    //'flat'=>true,//remove to hide the datepicker
		    'options'=>array(
		        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
				'dateFormat' => 'yy-mm-dd',
				'showButtonPanel' => true,      // show button panel
		    ),
		    'htmlOptions'=>array(
		        'style'=>''
		    ),
		));
		 ?>
		<?php echo $form->error($model,'schedule_date'); ?>
	</div>
	
	<div class="row col2">
		<?php
		$records = User::model()->getRpsList();
		?>
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php 
		//echo $form->dropDownList($model,'assigned_to',$records,array('empty' => 'Select User'));
		
		echo $form->dropDownList($model,'user_id',$records,array('empty' => 'Select user','disabled'=>$disabled));  
		
		?>
		
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'normal_hour'); ?>
		<?php echo $form->textField($model,'normal_hour'); ?>
		<?php echo $form->error($model,'normal_hour'); ?>
	</div>


	<div class="row col2">
		<?php echo $form->labelEx($model,'overtime_hour'); ?>
		<?php echo $form->textField($model,'overtime_hour'); ?>
		<?php echo $form->error($model,'overtime_hour'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'remarks'); ?>
		<?php echo $form->textArea($model,'remarks',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'remarks'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<?php //$this->endWidget('zii.widgets.jui.CJuiDialog');?>