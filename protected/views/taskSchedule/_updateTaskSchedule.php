<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
                'id'=>'jobDialog',
                'options'=>array(
                                    'title'=>Yii::t('studentBasicinfo','Task Transfer'),
                                    'autoOpen'=>true,
                                    'modal'=>'true',
                                    'width'=>800,
         			    'height'=>auto,
                                   
                                    ),
                ) );
?>

<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'task-schedule-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>


	<div class="row col2">
		<?php echo $form->labelEx($model,'task_id'); ?>
		<?php  
                        $disabled="disabled";
			echo $form->textField($model,'task_id',array('disabled'=>'disabled')); 
		?>
		<?php echo $form->error($model,'task_id'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'schedule_date'); ?>
		<?php 
		
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $model,
		    'attribute'=>'schedule_date',
			
		    //'flat'=>true,//remove to hide the datepicker
		    'options'=>array(
		        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
				'dateFormat' => 'yy-mm-dd',
				'showButtonPanel' => true,      // show button panel
		    ),
		    'htmlOptions'=>array(
		        'style'=>''
		    ),
		));
		 ?>
		<?php echo $form->error($model,'schedule_date'); ?>
	</div>
	
	<div class="row col2">
		<?php
		$records = User::model()->getRpsList();
		?>
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php 
		//echo $form->dropDownList($model,'assigned_to',$records,array('empty' => 'Select User'));
		
		echo $form->dropDownList($model,'user_id',$records,array('empty' => 'Select user'));  
		
		?>
		
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'normal_hour'); ?>
		<?php echo $form->textField($model,'normal_hour'); ?>
		<?php echo $form->error($model,'normal_hour'); ?>
	</div>


	<div class="row col2">
		<?php echo $form->labelEx($model,'overtime_hour'); ?>
		<?php echo $form->textField($model,'overtime_hour'); ?>
		<?php echo $form->error($model,'overtime_hour'); ?>
	</div>
    
    <div class="row col2">
		<?php echo $form->labelEx($model,'completed_status'); ?>
		<?php echo $form->textField($model,'completed_status'); ?>
		<?php echo $form->error($model,'completed_status'); ?>
	</div>
     
    <div class="row col2">
		<?php echo $form->labelEx($model,'cusion_hour'); ?>
		<?php echo $form->textField($model,'cusion_hour'); ?>
		<?php echo $form->error($model,'cusion_hour'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'remarks'); ?>
		<?php echo $form->textArea($model,'remarks',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'remarks'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn')); ?>
	</div>

<?php $this->endWidget(); ?>
        
        
        <?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>

</div><!-- form -->