<!-- this div are for the popup loading  DO NOT DELETE-->           
        <div id="studentBasicinfo">
        <div id="jobDialog" style="display:none;"></div>
        </div>     
        <!-- this div are for the popup loading -->  
<?php


$task_alert=Yii::app()->db->createCommand("SELECT
pm_user.user_name,
pm_task_alert.*
FROM
pm_task_alert
INNER JOIN pm_user ON pm_task_alert.crtd_by = pm_user.user_id
WHERE
pm_task_alert.fixed_by <= 0")->queryAll();




$baseUrl = Yii::app()->baseUrl; 
$cs = Yii::app()->getClientScript();
//$cs->registerScriptFile('https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js');
//$cs->registerScriptFile($baseUrl.'/fixedHeader/libs/jquery-migrate-1.2.1.min.js');
//$cs->registerScriptFile($baseUrl.'/fixedHeader/libs/jquery.scrollTo.js');
//$cs->registerScriptFile($baseUrl.'/fixedHeader/libs/jquery.dataTables.js');
//$cs->registerScriptFile($baseUrl.'/fixedHeader/libs/dataTables.scroller.js');
//$cs->registerScriptFile($baseUrl.'/fixedHeader/libs/FixedColumns.js');
//$cs->registerScriptFile($baseUrl.'/fixedHeader/main.js');
//$cs->registerCssFile($baseUrl.'/fixedHeader/styles.css');


$cs->registerScriptFile($baseUrl.'/fixedHeader/js/jquery.fixedheadertable.js');
$cs->registerCssFile($baseUrl.'/fixedHeader/css/fixedheadertable.css');
$cs->registerCssFile($baseUrl.'/fixedHeader/css/custom.css');

//$cs->registerScriptFile($baseUrl.'/fixedHeader/js/jquery.fixedheadertable.js');

$cs->registerScriptFile($baseUrl.'/htmltable_export/tableExport.js');
$cs->registerScriptFile($baseUrl.'/htmltable_export/jquery.base64.js');

$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/libs/sprintf.js');
$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/jspdf.js');
$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/libs/base64.js');

//$first_day_this_month = date('m-01-Y');
//$last_day_this_month  = date('m-t-Y');

$definedNH=CodeValue::model()->getCodeTypeValue('work_hour',28);
$definedCH=CodeValue::model()->getCodeTypeValue('work_hour',29);
$definedOH=CodeValue::model()->getCodeTypeValue('work_hour',30);

//get the min max date of active tasks(i.e task which are not completed)
$date_range=Yii::app()->db->createCommand("SELECT
min(pm_task_schedule.schedule_date) as mindate,
max(pm_task_schedule.schedule_date) as maxdate

FROM
pm_task_schedule
INNER JOIN pm_task ON pm_task_schedule.task_id = pm_task.task_id
WHERE
pm_task.task_status <> 26")->queryRow();






$startDt="";
$endDt="";
$days=0;
if($date_range['mindate'] != "")
{
	$startDt=$date_range['mindate'];
    $endDt=$date_range['maxdate'];
    
    $days=MyCustomClass::GetDateDifference($endDt,$startDt);
    $days+=1;//for current date the difference is 0 so make it one.we need total days count
    if($days < 30)
    {
		$diff=30-$days;
		$endDt=MyCustomClass::addDays($endDt,$diff);
	}
}
else
{
	$startDt=MyCustomClass::subractDays(date('Y-m-d'),2);
	$endDt=MyCustomClass::addDays(date('Y-m-d'),28);
	$days=MyCustomClass::GetDateDifference($endDt,$startDt);
	$days+=1;//for current date the difference is 0 so make it one.we need total days count
}



$loopStartDt=$startDt;
$loopEndDt=$endDt;



$uid=Yii::app()->user->id;
$user_info=User::model()->getAoRpsUser($uid);

$cond="";
if($user_info=='RPS')
{
	//$cond=" and pm_user.user_id=$uid";
}

$task_records=Yii::app()->db->createCommand("SELECT
pm_user.user_id,
pm_user.user_name,
CONCAT_WS('-', IF(pm_project.project_no IS NULL or pm_project.project_no = '', pm_project.project_id, pm_project.project_no), 
pm_code_value.code_lbl, pm_communication.communication_id, pm_task.task_id) AS task_no,
pm_task.task_id,
pm_task.job_start_date,
pm_task.job_due_date,
pm_task.is_planned,
pm_task.est_rps_cost,
IFNULL((select sum(worked_normal_hr+worked_ot_hr) FROM pm_task_schedule as intbl WHERE intbl.task_id=pm_task.task_id),0) as totworkedhr
FROM
pm_user
INNER JOIN pm_branch ON pm_user.branch_id = pm_branch.branch_id
INNER JOIN pm_task ON pm_user.user_id = pm_task.assigned_to
INNER JOIN pm_communication ON pm_task.communication_id = pm_communication.communication_id
INNER JOIN pm_project ON pm_communication.project_id = pm_project.project_id
INNER JOIN pm_code_value ON pm_project.project_type = pm_code_value.code_id
WHERE
pm_branch.branch_type = 18 AND
pm_task.task_status <> 26
$cond
ORDER BY
pm_user.user_name ASC,
pm_task.task_id DESC
")->queryAll();


?>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function(){		
		});


//]]>


</script>
<div style="margin-bottom: 10px">
	<div>
		<div style=" width:60px; float:left;">Working</div>
		<div class="working" style="width: 50px; float:left;">&nbsp; </div>
		<div style=" width:60px; float:left; margin-left: 10px">Available</div>
		<div class="available" style="width: 50px; float:left;">&nbsp; </div>
		<div style=" width:60px; float:left; margin-left: 10px">Planned</div>
		<div class="planned" style="width: 50px; float:left;">&nbsp; </div>
		<div style="width: 100px; float:left; margin-left: 10px">Maximum NH - </div>
		<div style="width: 20px; float:left;"><?php echo $definedNH; ?></div>
		<div style="width: 100px; float:left; margin-left: 10px">Maximum CH - </div>
		<div style="width: 20px; float:left;"><?php echo $definedCH; ?></div>
		<div style="width: 100px; float:left;margin-left: 10px">Maximum OTH - </div>
		<div style="width: 20px; float:left;"><?php echo $definedOH; ?></div>
		
		<div><a href="#" onClick ="
		$('#myDemoTable').tableExport({
			type:'excel',
			escape:'false',
			ignoreColumn: [2],
			});
		">Export to Excel</a></div>
		
		<div style="clear: both"></div>
		
	</div>
</div>


<!--for task flash-->
<div>
<?php
if(count($task_alert) > 0)
{
	?>
	
		<?php
		foreach($task_alert as $alert)
		{
			$alertModel=TaskAlert::model()->findByPk($alert['alert_id']);
			?>
			<div style="float: left;width:250px; border: 1px solid #000; padding: 5px;margin-left: 10px">
				<div style="width: 100px; float:left;">
					<div>Alert By</div>
					<div>Task No</div>
					<div>Mark Fixed</div>
				</div>
				<div style="width:150px;float:left">
					<div><?php echo $alert['user_name']; ?></div>
					<div><?php echo $alert['task_id']; ?></div>
					<div>
						<?php
						$this->widget(
						    'bootstrap.widgets.TbEditableField',
						    array(
						    'type' => 'select',
						    'emptytext'=>'-',
						    //'text'=>$alertModel,
						  	'placement'=>'right',
						    'model' => $alertModel,
						    'attribute' => 'fixed_by', // $model->name will be editable
						    'source' => $this->createUrl('task/getAlertValues'),
						    'url' => $this->createUrl('task/updateAlert'), //url for submit data
						    )
						    );
						?>
					</div>
				</div>
				<div style="clear: both"></div>
			</div>
			<?php
		}
		?>
		<div style="clear: both"></div>
	
	<?php
}

?>
</div>
<!--end task flash-->
  <div class="outerbox">
            <div class="innerbox">
                <table class="bluetable" id="myDemoTable" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th>Name</th>
          
            <th>Task</th>
            <th>&nbsp;</th>
        	<?php
        		while (strtotime($loopStartDt) <= strtotime($loopEndDt))
        		{
					$currentDate=$loopStartDt;
					//if(date('w', strtotime($currentDate)) != 6)
					{
						
					
					?>
					<th><?php echo date('D-M-j-Y',strtotime($currentDate)); ?></th>	
					<?php
					}
					$loopStartDt = date ("Y-m-d", strtotime("+1 day", strtotime($loopStartDt)));
				}
        	?>
        </tr>
     </thead>
     <tfoot>
     	<tr>
     		<?php
     			echo '<td colspan="" align="right"><b>Total</b></td>';
     			echo '<td>-</td>';
				echo '<td>-</td>';
					 	$loopStartDt=$startDt;
						$loopEndDt=$endDt;
						while (strtotime($loopStartDt) <= strtotime($loopEndDt))
						{
							$currentDate=$loopStartDt;
							$dateRecord2=Yii::app()->db->createCommand()
							->select('IFNULL(sum(normal_hour-worked_normal_hr),0) as nh,IFNULL(sum(cusion_hour),0) as ch,IFNULL(sum(overtime_hour-worked_ot_hr),0) as oh')
							->from('pm_task_schedule')
							->join('pm_task','pm_task_schedule.task_id=pm_task.task_id')
							->andWhere("pm_task.task_status != '26'")
							->andWhere("pm_task_schedule.completed_status != '1'")
							->andWhere('pm_task_schedule.schedule_date=:schedule_date',array(':schedule_date'=>$currentDate))
							->group('schedule_date')
							->queryRow();
							
							
							$loopStartDt = date ("Y-m-d", strtotime("+1 day", strtotime($loopStartDt)));
							
							
							
							
							echo '<td><b>';
							echo floatval($dateRecord2['nh'])+floatval($dateRecord2['oh']);
							echo '</b></td>';
						}
     		?>
     	</tr>
     </tfoot>
     <tbody>
     	<?php
     		$oldUserName="";
     		$oldUserId=0;
     		foreach($task_records as $taskRecord)
     		{
     			$task_id=$taskRecord['task_id'];
     			$user_id=$taskRecord['user_id'];
     			$job_start_dt=$taskRecord['job_start_date'];
     			$job_end_dt=$taskRecord['job_due_date'];
     			
     			
     			
     			
     			echo '<tr>';
     			
				if($oldUserName==$taskRecord['user_name'])
				{
					echo '<td>';
					echo '-';
					echo '</td>';
				}
				else if($oldUserName !=$taskRecord['user_name'] && $oldUserName=="")
				{
					echo '<td><b>';
					//end sum
					echo $taskRecord['user_name'];
					echo '</b></td>';
				}
				else
				{
					
					//give the sum
					 echo '<td colspan="" align="right" class="totLoad">Total Load</td>';
					 echo '<td>-</td>';
				echo '<td>-</td>';
					 	$loopStartDt=$startDt;
						$loopEndDt=$endDt;
						while (strtotime($loopStartDt) <= strtotime($loopEndDt))
						{
							$currentDate=$loopStartDt;
							$dateRecord2=Yii::app()->db->createCommand()
							->select('IFNULL(sum(normal_hour-worked_normal_hr),0) as nh,IFNULL(sum(cusion_hour),0) as ch,IFNULL(sum(overtime_hour-worked_ot_hr),0) as oh')
							->from('pm_task_schedule')
							->join('pm_task','pm_task_schedule.task_id=pm_task.task_id')
							->where('pm_task_schedule.user_id=:userid',array(':userid'=>$oldUserId))
							->andWhere("pm_task.task_status != '26'")
							->andWhere("pm_task_schedule.completed_status != '1'")
							->andWhere('pm_task_schedule.schedule_date=:schedule_date',array(':schedule_date'=>$currentDate))
							->group('schedule_date')
							->queryRow();
							
							
							$loopStartDt = date ("Y-m-d", strtotime("+1 day", strtotime($loopStartDt)));
							
							
							$circle="";
							if(floatval($dateRecord2['nh']) > floatval($definedNH))
							{
								$circle='class="circle"';
							}
							if(floatval($dateRecord2['oh']) > floatval($definedOH))
							{
								$circle='class="circle"';
							}
							
							echo '<td  class="totLoad"><div '.$circle.'>';
							echo floatval($dateRecord2['nh'])+floatval($dateRecord2['oh']);
							echo '</div></td>';
						}
					echo '</tr>';
					echo '<tr>';
					echo '<td><b>';
					//end sum
					echo $taskRecord['user_name'];
					echo '</b></td>';
					
				}
				$planned="";
				if(intval($taskRecord['is_planned']) > 0)
				{
					$planned='class="planned"';
				}
				
				echo '<td '.$planned.'>';
				$taskModel=Task::model()->findByPk($taskRecord['task_id']);
				if(intval($taskRecord['is_planned']) > 0)
				{
					$int_unique_id=MyCustomClass::uniqueRandom();
					/*
							$this->widget(
						    'bootstrap.widgets.TbEditableField',
						    array(
						    'type' => 'select',
						    'emptytext'=>'-',
						    'text'=>$taskRecord['task_no'],
						  	'placement'=>'right',
						    'model' => $taskModel,
						    'attribute' => 'is_planned', // $model->name will be editable
						    'source' => $this->createUrl('taskSchedule/getPlannedValues'),
						    'url' => $this->createUrl('taskSchedule/updatePlanned'), //url for submit data
						    )
						    );
						    */
						   echo CHtml::ajaxLink(
                            $taskRecord['task_no']."(".$taskRecord['est_rps_cost'].")",
                            Yii::app()->createUrl('task/taskPlannedPopUp',array('id'=>$taskRecord['task_id'])),
							
                            array('update'=>'#jobDialog'),
                           array('id'=>'showJobDialog'.$taskRecord['task_id']."$int_unique_id",'class'=>'editable-click')
                            );
						    
				}
				else
				{
					echo $taskRecord['task_no']."(".$taskRecord['est_rps_cost']."-".$taskRecord['totworkedhr'].")";
				}
				
				echo '</td>';
				
				echo '<td><div>';
				//new schedule button
				/*
				EQuickDlgs::iframeLink(
				    array(
				        'controllerRoute' => 'taskSchedule/createReportDialog',
				        'actionParams' => array('user' => "$user_id",'task'=>"$task_id"),
				        'dialogTitle' => 'Create Schedule',
				        'dialogWidth' => '80%',
				        'dialogHeight' => '500',
				        'openButtonText' => 'New',
				       
				        'closeButtonText' => 'Close',
						'id' => "newClients$task_id",
						'openButtonHtmlOptions'=>array('class'=>'btn',),
						//'renderOpenButton' => false,
				        'closeOnAction' =>true, //important to invoke the close action in the actionCreate
				       // 'refreshGridId' => 'rptTable', //the grid with this id will be refreshed after closing
						'iframeHtmlOptions' => array(
				                                                            'width' => '100%',
				                                                            'height' => '100%',
				                                                    ),
				    )
				);
				*/
				
				$int_unique_id=MyCustomClass::uniqueRandom();
				 EQuickDlgs::iframeButton(
						    array(
						        'controllerRoute' => 'taskSchedule/createReportDialog',
						        'actionParams' => array('user' => "$user_id",'task'=>"$task_id"),
						        'dialogTitle' => 'Create Schedule',
						        //'class'=>'',
						        'dialogWidth' => '80%',
						        'dialogHeight' => '500',
						        'openButtonText' => 'new',
						        'closeButtonText' => 'Close',
								'id' => $int_unique_id,
								 'openButtonType'=>'link',
								'openButtonHtmlOptions'=>array('class'=>'btn','id'=>$int_unique_id,'name'=>$int_unique_id),
								//'renderOpenButton' => false,
						        'closeOnAction' =>true, //important to invoke the close action in the actionCreate
						       // 'refreshGridId' => 'rptTable', //the grid with this id will be refreshed after closing
								'iframeHtmlOptions' => array(
						                                                            'width' => '100%',
						                                                            'height' => '100%',
						                                                    ),
						    )
						);
				
				echo '</div></td>';
				//new schedule button
				
				//for date loop
				$loopStartDt=$startDt;
				$loopEndDt=$endDt;
				while (strtotime($loopStartDt) <= strtotime($loopEndDt))
				{
					$currentDate=$loopStartDt;
					
					//get work load of current task of current user of given date
					$dateRecord=Yii::app()->db->createCommand()
					->select('IFNULL(sum(normal_hour-worked_normal_hr),0) as nh,IFNULL(sum(cusion_hour),0) as ch,IFNULL(sum(overtime_hour-worked_ot_hr),0) as oh,schedule_id')
					->from('pm_task_schedule')
					->where('pm_task_schedule.user_id=:userid',array(':userid'=>$user_id))
					->andWhere('pm_task_schedule.task_id=:task_id',array(':task_id'=>$task_id))
					->andWhere("pm_task_schedule.completed_status != '1'")
					->andWhere('pm_task_schedule.schedule_date=:schedule_date',array(':schedule_date'=>$currentDate))
					->group('schedule_date')
					->queryRow();
					
					
					
					
					
					$loopStartDt = date ("Y-m-d", strtotime("+1 day", strtotime($loopStartDt)));
					
					$colorCell="";
					$normalAvailable="working";
					$otAvailable="working";
					
					if(MyCustomClass::check_in_range($job_start_dt,$job_end_dt,$currentDate))
					{
						$colorCell='jobAssigned';
					}
					
					if(floatval($dateRecord['nh']) < $definedNH)
					{
						$normalAvailable='available';
					}
					
					
					
					if(floatval($dateRecord['oh']) < $definedOH)
					{
						$otAvailable='available';
					}
					
					
					if(intval($taskRecord['is_planned']) > 0 && ($dateRecord['nh'] != "" || $dateRecord['oh'] != ""))
					{
						$otAvailable='';
						$normalAvailable='';
						$colorCell='planned';
					}
					
					if($dateRecord['nh'] == "")
					{
						$normalAvailable='';
					}
					
					if($dateRecord['oh'] == "")
					{
						$otAvailable='';
					}
					
					
					//$taskScheduleModel=TaskSchedule::model()->findByPk($dateRecord['schedule_id']);
					//$taskScheduleModel->scenario = 'editable';
	     			//$taskScheduleModel->task_id=$task_id;
	     			//$taskScheduleModel->user_id=$user_id;
	     			//$taskScheduleModel->schedule_id=$dateRecord['schedule_id'];
					//$taskScheduleModel->schedule_date=$currentDate;
					//$taskScheduleModel->normal_hour=$dateRecord['nh'];
					//$taskScheduleModel->overtime_hour=$dateRecord['oh'];
					
					?>
					<td class="<?php echo $colorCell; ?>" >
						
						<div class="nh <?php echo $colorCell." ".$normalAvailable; ?>">
						<?php 
						
						//if($currentDate >= date('Y-m-d') && $dateRecord['nh']!= "")
						if( $dateRecord['nh']!= "")
						{
							/*
							$this->widget(
						    'bootstrap.widgets.TbEditableField',
						    array(
						    'type' => 'text',
						    'emptytext'=>'-',
						    'model' => $taskScheduleModel,
						    'attribute' => 'normal_hour', // $model->name will be editable
						    'url' => $this->createUrl('taskSchedule/updateHours'), //url for submit data
						    )
						    );
						    */
						   
                            
                            
                          // echo  CHtml::ajaxLink(
                           // $dateRecord['nh'],
                           // Yii::app()->createUrl('taskSchedule/taskSchedulePopUp',array('id'=>$dateRecord['schedule_id'])),
							
                           // array('update'=>'#jobDialog'),
                           // array('id'=>'showJobDialog'.$dateRecord['schedule_id'].$int_unique_id,'class'=>'editable-click')
                           // );
                           
                            
                          EQuickDlgs::iframeLink(
						    array(
						        'controllerRoute' => 'taskSchedule/taskSchedulePopUp',
						        'actionParams' => array('id' => $dateRecord['schedule_id']),
						        'dialogTitle' => 'Create Schedule',
						        //'class'=>'',
						        'dialogWidth' => '80%',
						        'dialogHeight' => '500',
						        'openButtonText' => $dateRecord['nh'],
						        'closeButtonText' => 'Close',
								'id' => $dateRecord['schedule_id'],
								 'openButtonType'=>'link',
								'openButtonHtmlOptions'=>array('class'=>'btnss','id'=>$dateRecord['schedule_id'],'name'=>$dateRecord['schedule_id']),
								//'renderOpenButton' => false,
						        'closeOnAction' =>true, //important to invoke the close action in the actionCreate
						       // 'refreshGridId' => 'rptTable', //the grid with this id will be refreshed after closing
								'iframeHtmlOptions' => array(
						                                                            'width' => '100%',
						                                                            'height' => '100%',
						                                                    ),
						    )
						);
						    
						    // echo $dateRecord['nh']; 
						}
						else
						{
							echo $dateRecord['nh']; 
						}
						    
						    
						?>
						</div>	
						
						
					<!--</td>-->
					
				<!--	<td class="<?php //echo $colorCell." ".$otAvailable; ?>">	-->
				<div class="oh <?php echo $colorCell." ".$otAvailable; ?>">
						<?php //echo $dateRecord['oh'];							
						if($currentDate >= date('Y-m-d') && $dateRecord['oh']!= "" )
						{
							/*
							$this->widget(
						    'bootstrap.widgets.TbEditableField',
						    array(
						    'type' => 'text',
						    'emptytext'=>'-',
						    'model' => $taskScheduleModel,
						    'attribute' => 'overtime_hour', // $model->name will be editable
						    'url' => $this->createUrl('taskSchedule/updateHours'), //url for submit data
						    )
						    );
						    
						     */
						     echo $dateRecord['oh'];   
						}
						else
						{
							echo $dateRecord['oh']; 
						}
						 ?>
					</div>
					</td>
					<?php
					
				}
				//end date loop
				
				echo '</tr>';
				$oldUserName=$taskRecord['user_name'];
				$oldUserId=$user_id;
			}
			//for last record sum
			echo '<tr>';
			 echo '<td align="right"  class="totLoad">Total Load</td>';
			 echo '<td>-</td>';
			echo '<td>-</td>';
					 $loopStartDt=$startDt;
						$loopEndDt=$endDt;
						while (strtotime($loopStartDt) <= strtotime($loopEndDt))
						{
							$currentDate=$loopStartDt;
							$dateRecord=Yii::app()->db->createCommand()
							->select('IFNULL(sum(normal_hour-worked_normal_hr),0) as nh,IFNULL(sum(cusion_hour),0) as ch,IFNULL(sum(overtime_hour-worked_ot_hr),0) as oh')
							->from('pm_task_schedule')
							->join('pm_task','pm_task_schedule.task_id=pm_task.task_id')
							->where('pm_task_schedule.user_id=:userid',array(':userid'=>$oldUserId))
							->andWhere("pm_task.task_status != '26'")
							->andWhere("pm_task_schedule.completed_status != '1'")
							->andWhere('pm_task_schedule.schedule_date=:schedule_date',array(':schedule_date'=>$currentDate))
							->group('schedule_date')
							->queryRow();
							
							
							$loopStartDt = date ("Y-m-d", strtotime("+1 day", strtotime($loopStartDt)));
							
							$circle="";
							if(floatval($dateRecord['nh']) > floatval($definedNH))
							{
								$circle='class="circle"';
							}
							
							if(floatval($dateRecord['oh']) > floatval($definedOH))
							{
								$circle='class="circle"';
							}
							
							echo '<td  class="totLoad"><div '.$circle.'>';
							echo floatval($dateRecord['nh'])+floatval($dateRecord['oh']);
							echo '</div></td>';
						}	
			echo '</tr>';
			
     	?>
     </tbody>
     
</table>
 </div>
  </div>
<style type="text/css">
td.jobAssigned
{
	border-bottom:2px solid #7e89fc; padding-bottom:2px;
	
}
div.planned,td.planned
{
	background-color: #f8ff64 !important;
}
div.available
{
	background-color: #ffacac !important;
}
div.working
{
	background-color: #59fb4d !important;
}
td.completed
{
	background-color: #ff1c00;
}
.circle
{
width:30px;
height:30px;
border-radius:15px;
font-size:12px;
color:#fff;
line-height:30px;
text-align:center;
background:#ff0000
}
td div.nh,td div.oh
{
	width:100%;
	text-align: center;
}
td.totLoad
{
	font-size: 12px !important;
	font-weight: bold !important;
}
</style>




<div class="clear"></div>
<script language="javascript">
	$(document).ready(function() {
                $('#myDemoTable').fixedHeaderTable({
                    altClass : 'odd',
                    footer : true,
                    fixedColumns : 2
                });
                
                
                /*
			    $("#myDemoTable td").each(function()
			    {
			    	
			        if($.trim($(this).find('div.nh').text()).length <= 0)
			        {
			           
			            var text = $.trim($(this).find('div.nh').text());
			            text += "..";

			            $(this).text(text);
			        }
			        
			         if($.trim($(this).find('div.oh').text()).length <= 0)
			        {
			           
			            var text = $.trim($(this).find('div.oh').text());
			            text += "..";

			            $(this).text(text);
			        }
			    });
*/
                
            });
</script>