<?php
$this->breadcrumbs=array(
	'Tests'=>array('index'),
	$model->test_id,
);

$this->menu=array(
array('label'=>'List Test','url'=>array('index')),
array('label'=>'Create Test','url'=>array('create')),
array('label'=>'Update Test','url'=>array('update','id'=>$model->test_id)),
array('label'=>'Delete Test','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->test_id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Test','url'=>array('admin')),
);
?>

<h1>View Test #<?php echo $model->test_id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'test_id',
		'test_name',
		'test_address',
		'test_email',
		'crtd_by',
),
)); ?>
