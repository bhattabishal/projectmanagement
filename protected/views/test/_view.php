<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('test_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->test_id),array('view','id'=>$data->test_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('test_name')); ?>:</b>
	<?php echo CHtml::encode($data->test_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('test_address')); ?>:</b>
	<?php echo CHtml::encode($data->test_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('test_email')); ?>:</b>
	<?php echo CHtml::encode($data->test_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_by')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_by); ?>
	<br />


</div>