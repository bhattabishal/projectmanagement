<?php
/* @var $this BranchController */
/* @var $data Branch */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->branch_id), array('view', 'id'=>$data->branch_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_name')); ?>:</b>
	<?php echo CHtml::encode($data->branch_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_code')); ?>:</b>
	<?php echo CHtml::encode($data->branch_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_contact')); ?>:</b>
	<?php echo CHtml::encode($data->branch_contact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_email')); ?>:</b>
	<?php echo CHtml::encode($data->branch_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_type')); ?>:</b>
	<?php echo CHtml::encode($data->branch_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_by')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_by); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_dt')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_by')); ?>:</b>
	<?php echo CHtml::encode($data->updt_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_dt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_cnt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_cnt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('color_code')); ?>:</b>
	<?php echo CHtml::encode($data->color_code); ?>
	<br />

	*/ ?>

</div>