<?php
/* @var $this BranchController */
/* @var $model Branch */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'branch-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'branch_name'); ?>
		<?php echo $form->textField($model,'branch_name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'branch_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'branch_code'); ?>
		<?php echo $form->textField($model,'branch_code',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'branch_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'branch_contact'); ?>
		<?php echo $form->textField($model,'branch_contact',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'branch_contact'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'branch_email'); ?>
		<?php echo $form->textField($model,'branch_email',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'branch_email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'branch_type'); ?>
		<?php echo $form->textField($model,'branch_type'); ?>
		<?php echo $form->error($model,'branch_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'crtd_by'); ?>
		<?php echo $form->textField($model,'crtd_by'); ?>
		<?php echo $form->error($model,'crtd_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'crtd_dt'); ?>
		<?php echo $form->textField($model,'crtd_dt'); ?>
		<?php echo $form->error($model,'crtd_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_by'); ?>
		<?php echo $form->textField($model,'updt_by'); ?>
		<?php echo $form->error($model,'updt_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_dt'); ?>
		<?php echo $form->textField($model,'updt_dt'); ?>
		<?php echo $form->error($model,'updt_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_cnt'); ?>
		<?php echo $form->textField($model,'updt_cnt'); ?>
		<?php echo $form->error($model,'updt_cnt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'color_code'); ?>
		<?php echo $form->textField($model,'color_code',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'color_code'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->