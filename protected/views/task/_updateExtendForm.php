<?php
/* @var $this TaskController */
/* @var $model Task */
/* @var $form CActiveForm */
?>

<div class="form wide">

<?php $form=$this->beginWidget('DynamicTabularForm', array(
    'defaultRowView1'=>'_taskScheduleTabularForm',
	'id'=>'task-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>FALSE,
)); ?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php 
	
		echo $form->errorSummary($model); 
	
	
	
	?>

	<div class="row col2">
		<?php echo $form->labelEx($model,'communication_id'); ?>
		<?php
		//$comm_details=Communication::model()->findAllByPk($model->communication_id);
		?>
		<?php echo $model->communication->getProjectCompositeNo()."-".$model->communication->communication_id; ?>
		<?php echo $form->error($model,'communication_id'); ?>
	</div>

	
	
	

	<div class="row col2">
		<?php
		//$records = CHtml::listData(User::model()->findAll(), 'user_id', 'user_name');
		$records =User::model()->getRpsList();
		?>
		<?php echo $form->labelEx($model,'assigned_to'); ?>
		<?php 
		//echo $form->dropDownList($model,'assigned_to',$records,array('empty' => 'Select User'));
		
		echo $form->dropDownList($model,'assigned_to',$records,array('ajax'=>array(
		'type'=>'POST', //request type
		'url'=>$this->createUrl('taskSchedule/dynamicTaskAssignment'), //url to call.
		'update'=>'.assignedTasks',
		'data'=>array('user_id'=>'js:this.value') 
		),'empty' => 'Select User'));  
		
		?>
		
		<?php echo $form->error($model,'assigned_to'); ?>
		
		<?php //$this->widget('bootstrap.widgets.TbButton', array(
    //'label'=>'view schedule',
   // 'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
   // 'size'=>'mini', // null, 'large', 'small' or 'mini'
    //'htmlOptions'=>array('id'=>'butnAssignedTo'),
///)); ?>
<?php
EQuickDlgs::ajaxIcon(
            Yii::app()->baseUrl .'images/view.png',
            array(
                'controllerRoute' => 'taskSchedule/report', //'member/view'
               // 'actionParams' => array('id'=>$model->id), //array('id'=>$model->member->id),
                'dialogTitle' => 'Scheduled Tasks',
                'dialogWidth' => '95%',
                'dialogHeight' => '400',
                'openButtonText' => 'View Schedule',
                'closeButtonText' => 'Close',
            )
        );
?>
	</div>
	
	
<div style="clear:both"></div>
	
	
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'job_due_date'); ?>
		<?php 
		
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $model,
		    'attribute'=>'job_due_date',
		   // 'flat'=>true,//remove to hide the datepicker
		    'options'=>array(
		        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
				'dateFormat' => 'yy-mm-dd',
				'showButtonPanel' => true,      // show button panel
		    ),
		    'htmlOptions'=>array(
		        'style'=>''
		    ),
		));
		
	  ?>
		<?php echo $form->error($model,'job_due_date'); ?>
	</div>
	
	<div class="row col2">
		<?php 
			//echo $currentTime=date('H');
		echo $form->labelEx($model,'est_rps_cost'); 
		?>
		<?php echo $form->textField($model,'est_rps_cost',array('size'=>18,'maxlength'=>18)); ?>
		<?php echo $form->error($model,'est_rps_cost'); ?>
	</div>
	
	<div style="clear:both"></div>
	<div class="row col2">
		<?php echo $form->labelEx($model,'totalOverTimeHours'); ?>
		<?php echo $form->textField($model,'totalOverTimeHours',array('size'=>18,'maxlength'=>18,'placeholder'=> 'fill if applicable')); ?>
		<?php echo $form->error($model,'totalOverTimeHours'); ?>
		
		<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Generate Schedule',
    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'mini', // null, 'large', 'small' or 'mini'
    'htmlOptions'=>array('id'=>'btnGenerateSchedule'),
)); ?>
	</div>
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'est_ao_cost'); ?>
		<?php echo $form->textField($model,'est_ao_cost',array('size'=>18,'maxlength'=>18,'placeholder'=> 'fill if applicable')); ?>
		<?php echo $form->error($model,'est_ao_cost'); ?>
	</div>
	
	<div style="clear:both"></div>
	<!--task assignment-->
	<div id="taskAssignmentForm" style="padding: 0 10px 0 10px;">
		<div id="showhide" style="cursor: pointer; text-decoration: underline;" onclick="showHide();">Show assigned tasks</div>
		<div class="assignedTasks" style="display:none;margin-top: 10px">
			<?php
				if(isset($model->assigned_to) && $model->assigned_to > 0)
				{
					$results=TaskSchedule::model()->getActiveTaskScheduleSummary($model->assigned_to);
					$this->renderPartial('//taskSchedule/_summary',array(
						'results'=>$results,
					),false,false);
				}
			?>
		</div>
		
		<div  class="taskSchedule" >
			
		</div>
		<?php echo $form->error($model,'validation'); ?>
		
		<?php
			
			//echo $form->rowForm1($taskScheduleModel);
		?>
	
	
		
		
		
		
		
		
	</div>
	
	
	<!--task assignment-->
	<div style="clear:both"></div>
	
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'job_details'); ?>
		<?php echo $form->textArea($model,'job_details',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'job_details'); ?>
	</div>
	<div style="clear:both"></div>
	<input  type="hidden" name="taskId" id="task_id" value="<?php echo $model->task_id; ?>"/>
	<input  type="hidden" name="extended" id="extended" value="<?php echo $model->extendedTask; ?>"/>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn')); ?>
	</div>
<!--for tiny mce-->
	<?php $this->widget('application.extensions.tinymce.SladekTinyMce'); ?>
 
<script>
    tinymce.init({
    selector: "textarea#Contracts_contractName",
    menubar: false,
    width: 900,
    height: 300,
   toolbar1: "undo redo | bold | italic underline | alignleft aligncenter alignright alignjustify ", 
   toolbar2: "outdent indent | hr | sub sup | bullist numlist | formatselect fontselect fontsizeselect | cut copy paste pastetext pasteword | search replace ", 
 
 }); 
 </script>
 
<script type="text/javascript">
 
    tinymce.init({
    selector: "textarea#Task_job_details",
    theme: "modern",
    //width: 900,
   // height: 300,
    //plugins: [
       //  "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
        // "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        // "save table contextmenu directionality emoticons template paste textcolor"
  // ],
   content_css: "css/content.css",
   menubar:false,
   
    toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent"

   //toolbar: "bold italic | alignleft aligncenter alignright alignjustify | outdent indent | forecolor backcolor", 
   //style_formats: [
       // {title: 'Bold text', inline: 'b'},
       // {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
       // {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
       // {title: 'Example 1', inline: 'span', classes: 'example1'},
       // {title: 'Example 2', inline: 'span', classes: 'example2'},
       
    //]
 }); 
 </script>
	<!--end tinymce-->
<?php $this->endWidget(); ?>

</div><!-- form -->

	
	
<script type="text/javascript">
	$( "#Task_est_rps_cost,#Task_totalOverTimeHours" ).keyup(function() {
		ajaxScheduleLoad();
	});
	
	function ajaxScheduleLoad()
	{
		$.ajax({
      type: "POST",
      url:    "<?php echo Yii::app()->createUrl('taskSchedule/dynamicTaskSchedule'); ?>",
      data:  {
      	user_id:$('#Task_assigned_to').val(),
      	due_date:$("#Task_job_due_date").val(),
      	rps_hour:$("#Task_est_rps_cost").val(),
      	ot_hour:$("#Task_totalOverTimeHours").val(),
      	task_id:$('#task_id').val(),
      	extended:$('#extended').val(),
      	},
      success: function(msg){
           $('.taskSchedule').html(msg);
          },
      error: function(xhr){
      alert("failure"+xhr.readyState+this.url)

      }
    });
	}
	
	
	$('#btnGenerateSchedule').click(function() {
    ajaxScheduleLoad();
});


  function showHide()
  {
		if($(".assignedTasks").is(":visible"))
		{
			$(".assignedTasks").hide();	
			$('#showhide').text('Show assigned tasks');
		}
		else{
			$(".assignedTasks").removeClass("hidden");
			$(".assignedTasks").show();
			$('#showhide').text('Hide assigned tasks');
		}
	}
        
   
</script>
