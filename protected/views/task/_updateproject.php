<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
                'id'=>'jobDialog',
                'options'=>array(
                                    'title'=>Yii::t('studentBasicinfo','Update Task Status Details'),
                                    'autoOpen'=>true,
                                    'modal'=>'true',
                                    'width'=>550,
         							'height'=>auto,
                                   
                                    ),
                ) );
?>



    <?php
    $this->widget('bootstrap.widgets.TbEditableDetailView', array(
    'data' => $model,
	//'id'  =>'region-details',    //you can define any default params for child EditableFields
   'url' => $this->createUrl('task/editTaskStatus'), //common submit url for all fields
   	'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken), //params for all fields
    'emptytext' => 'no value',
    //'apply' => false, //you can turn off applying editable to all attributes
    'attributes' => array(
  
   
    array( //select loaded from ajax.
    'name' => 'task_status',
	'value'=> $model->taskStatus->code_lbl,
    'editable' => array(
    'type' => 'select',
	//'source' => Editable::source(Group::model()->findAll(), 'group_id', 'group_name')
    'source' => $this->createUrl('home/getTaskStatusValues'),
	'mode'=>'inline',
    )
    ),
    array( //select loaded from ajax.
    'name' => 'est_rps_cost',
    'editable' => false,
    ),
    array( //select loaded from ajax.
    'name' => 'actual_rps_cost',
    'editable' => array(
    'type' => 'text',
	'inputclass' => 'input-small',
	'emptytext' => 'special emptytext', 
	'mode'=>'inline',
    )
    ),
     array( //select loaded from ajax.
    'name' => 'est_ao_cost',
    'editable' =>false,
    ),
	array( //select loaded from ajax.
    'name' => 'actual_ao_cost',
    'editable' => array(
    'type' => 'text',
	'inputclass' => 'input-small',
	'emptytext' => 'special emptytext', 
	'mode'=>'inline',
    )
    ),
 
   array( //select loaded from ajax.
    'name' => 'done_percentage',
    'editable' => array(
    'type' => 'text',
	'inputclass' => 'input-small',
	'emptytext' => 'special emptytext', 
	'mode'=>'inline',
    )
    ),
   
   
    )
    ));
    ?>

<?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>