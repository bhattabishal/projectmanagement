<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
                'id'=>'jobDialog',
                'options'=>array(
                                    'title'=>Yii::t('studentBasicinfo','Update Planned Task'),
                                    'autoOpen'=>true,
                                    'modal'=>'true',
                                    'width'=>550,
         							'height'=>auto,
                                   
                                    ),
                ) );
?>



    <?php
    $this->widget(
						    'bootstrap.widgets.TbEditableField',
						    array(
						    'type' => 'select',
						    'emptytext'=>'-',
						  	'placement'=>'right',
						    'model' => $model,
						    'mode'=>'inline',
						    'attribute' => 'is_planned', // $model->name will be editable
						    'source' => $this->createUrl('taskSchedule/getPlannedValues'),
						    'url' => $this->createUrl('taskSchedule/updatePlanned'), //url for submit data
						    )
						    );
    ?>

<?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>