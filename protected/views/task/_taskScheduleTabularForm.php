<?php
/* @var $this TaskScheduleController */
/* @var $model TaskSchedule */
/* @var $form CActiveForm */
?>
<?php $row_id = "taskSchedule-" . $key ?>
<div class='row-fluid custom'  id="<?php echo $row_id ?>">
<?php
    echo $form->hiddenField($model, "[$key]schedule_id");
    echo $form->updateTypeField($model, $key, "updateType", array('key' => $key));
    ?>


	

	 <div class="span2">
		<?php echo $form->labelEx($model,"[$key]schedule_date"); ?>
		<?php 
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $model,
		    'attribute'=>"[$key]schedule_date",
		    //'flat'=>true,//remove to hide the datepicker
		    'options'=>array(
		        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
				'dateFormat' => 'yy-mm-dd',
				'showButtonPanel' => true,      // show button panel
		    ),
		    'htmlOptions'=>array(
		        'class'=>'medium'
		    ),
		));
	  ?>
		<?php echo $form->error($model,"[$key]schedule_date"); ?>
	</div>
	
	 

	 <div class="span-custom-small">
		<?php echo $form->labelEx($model,"[$key]normal_hour"); ?>
		<?php echo $form->textField($model,"[$key]normal_hour",array('class'=>'small')); ?>
		<?php echo $form->error($model,"[$key]normal_hour"); ?>
	</div>

	 <div class="span-custom-small">
		<?php echo $form->labelEx($model,"[$key]cusion_hour"); ?>
		<?php echo $form->textField($model,"[$key]cusion_hour",array('class'=>'small')); ?>
		<?php echo $form->error($model,"[$key]cusion_hour"); ?>
	</div>

	 <div class="span-custom-small">
		<?php echo $form->labelEx($model,"[$key]overtime_hour"); ?>
		<?php echo $form->textField($model,"[$key]overtime_hour",array('class'=>'small')); ?>
		<?php echo $form->error($model,"[$key]overtime_hour"); ?>
	</div>

	 <div class="span3" style="width: 17%">
		<?php echo $form->labelEx($model,"[$key]remarks"); ?>
		<?php echo $form->textField($model,"[$key]remarks",array('size'=>20,'maxlength'=>250,'style'=>'width:220px')); ?>
		<?php echo $form->error($model,"[$key]remarks"); ?>
	</div >
	
		<div  class="span1">
 
            <?php echo $form->deleteRowButton1($row_id, $key); ?>
        </div>
	
	



</div><!-- form -->