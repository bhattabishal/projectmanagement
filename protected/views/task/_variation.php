<div class="row col2">
			<?php echo $form->labelEx($variationModel,'description'); ?>
			<?php echo $form->textArea($variationModel,'description',array('rows'=>6, 'cols'=>50)); ?>
			<?php echo $form->error($variationModel,'description'); ?>
		</div>
		
		<div class="row col2">
			<?php echo $form->labelEx($variationModel,'variation_hours'); ?>
			<?php echo $form->textField($variationModel,'variation_hours',array('size'=>18,'maxlength'=>18)); ?>
			<?php echo $form->error($variationModel,'variation_hours'); ?>
		</div>