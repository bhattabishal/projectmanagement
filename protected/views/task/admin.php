
<?php
$uniqid=md5(uniqid());
?>
<h1>Tasks</h1>

<!--<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>-->

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php //$this->renderPartial('_search',array(
	//'model'=>$model,
//)); 
?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'task-grid',
	// 'fixedHeader' => true,
		//'headerOffset' => 40,
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type'=>' bordered',
	 //'rowCssClass'=>'none',
	 'rowHtmlOptionsExpression'=>'array("style"=>"background-color:{$data->setRowColor()}","class"=>"self")',
	'columns'=>array(
	
	array(
			//'class'=>'CButtonColumn',
				'class'=>'bootstrap.widgets.TbRelationalColumn',
				'cssClass'=>'main-taskchecklist tbrelational-column',
				'uniqid'=>$uniqid,
				//'htmlOptions' =>array( 'class' => 'tbrelational-column2' ), 
				 'header' => '#',
				'url' => $this->createUrl('home/checklist'),
				'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
				'afterAjaxUpdate' => 'js:function(tr,rowid,data){console.log(rowid)}'			
		),
		array(
			'name'=>'projectCompositeNo',
			//'value'=>'$data->getProjectCompositeNo()',
			'value' => 'CHtml::link($data->getProjectCompositeNo(), Yii::app()
 ->createUrl("home/selectedAdmin",array("pno"=>$data->getProjectCompositeNo())))',
        'type'  => 'raw',
		),
		
	
		
		array( 
                    'name'=>'communication_id',
                    'headerHtmlOptions' => array('style' => 'width: 15px'),
                    ),
		'task_id',
		//'project.project_id',
		//array( 'name'=>'project_search', 'value'=>'$data->project->project_id' ),
		array(
	'name'=>'branch_id',
	'value'=>'$data->branch->branch_name',
	'header' => 'Branch',
	//'filter'=>Branch::model()->getBranchName('branch_id'),
	//'headerHtmlOptions' => array('style' => 'width: 110px'),
	),
	 
			 array(
			'name' => 'ao_coord',
		    'header'=>'Coord Ao', 
			 'value'=>'$data->aoCoordUser->user_name', 
			
			),
		array(
		    'name'=>'rps_coord',
			'header'=>'Coord Rps',
			 'value'=>'$data->rpsCoordUser->user_name', 
		),
		
		
		array( 'name'=>'project_text_search',
                    'value'=>'$data->project->project_title',
                    'headerHtmlOptions' => array('style' => 'width: 90px'),
            ),
		
		//'job_details',
		array(
		    'name'=>'job_details',
			// call the method 'renderCoordAo' from the model
	            'type'=>'raw',
                    'value'=>'$data->job_details',
                    'headerHtmlOptions' => array('style' => 'width: 130px'),
		),
		//'sharable0.code_lbl',
		'job_due_date',
		'job_client_due_date',
		//'est_rps_cost',		
		//'est_ao_cost',
		//'actual_rps_cost',
		//'actual_ao_cost',
		'done_percentage',
		//'assignedTo.user_name',
		array( 'name'=>'assigned_to_search', 'value'=>'$data->assignedTo->user_name' ),
		//'taskStatus.code_lbl',
		//array( 'name'=>'status_search', 'value'=>'$data->taskStatus->code_lbl' ),
		
		 array(
		'name' => 'status_search',
		'org_name' =>'task_status',
		'value'=> '$data->taskStatus->code_lbl',
		'header' => 'Status',
		'filter'=>CodeValue::model()->getTextValuesArryByType('job_status'),
		'class' => 'bootstrap.widgets.MyStatusTbEditableColumn',
		'headerHtmlOptions' => array('style' => 'width:40px'),
		'editable' => array(
		'type' => 'select',
		'source' => $this->createUrl('home/getTaskStatusValues'),
		'url' =>  $this->createUrl('task/editTaskStatus'),
		)
		),
		'jobType.code_lbl',
		 array(
		'class' => 'bootstrap.widgets.TbToggleColumn',
		'toggleAction' => 'task/toggle',
		'checkedButtonLabel'=>'Unhide',
		'uncheckedButtonLabel'=>'Hide',
		'name' => 'is_hidden',
		'header' => '',
		'filter'=>''
		),
		array(
			'name'=>'task_variation',
                        'header' => 'Var',
			'filter'=>array('No'=>'No', 'Yes'=>'Yes'),
			'value' => '$data->task_variation',
        	
		),
		
		array(
            'class'=>'EJuiDlgsColumn',
			'htmlOptions' => array('style'=>'width:60px'),
			'deleteButtonUrl'=>'Yii::app()->createUrl("/task/delete", array("id" => $data->task_id))',
			 'buttons'=>array(
			 			
			 			
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
						'options'=>array(
						'class'=>'taskView',
						)
                    ),
					'update'=>
                             array(
                        'options'=>array(
						'class'=>'taskUpdate',
						)
                    ),
					'delete'=>
                             array(
                        'options'=>array(
						'class'=>'taskDelete'.$uniqid,
						)
                    ),
					
					
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'task/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View Task',
			  'closeOnAction' => true,
			 'refreshGridId' => 'task-grid',
                         'hideTitleBar' => false, 
             'dialogWidth' => '80%',
        	 'dialogHeight' => '300',
        ),
		'updateDialog'=>array(
		//'controllerRoute' => 'task/updateTaskOnly', //=default
		'controllerRoute' => 'task/update', //=default
            'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update Task',
			  'closeOnAction' => true,
			 'refreshGridId' => 'task-grid',
			 'dialogWidth' => '97%',
        		'dialogHeight' => '600',
                         'hideTitleBar' => false,
						  'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
		),
		
		
		
			),
			/*
			array(
            'class'=>'EJuiDlgsColumn',
            'headerHtmlOptions'=>array('class'=>''),
            'htmlOptions' => array('style'=>'width:16px !important'),
            'template'=>'{update}',
            'updateButtonImageUrl'=>Yii::app()->baseUrl .'/images/expand-icon.png',
            'updateDialog'=>array(
               'controllerRoute' => 'task/updateExtend', 
               'actionParams' => array('id'=>'$data->primaryKey'),
               'dialogTitle' => 'Extend Task Schedule',
               'dialogWidth' => '97%', //override the value from the dialog config
               'dialogHeight' => '600',
                 'hideTitleBar' => false,
						  'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
            ),            
          ),
          */
	),
)); ?>
