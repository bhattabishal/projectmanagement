<?php
/* @var $this TaskController */
/* @var $data Task */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('task_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->task_id), array('view', 'id'=>$data->task_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_id')); ?>:</b>
	<?php echo CHtml::encode($data->job_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_details')); ?>:</b>
	<?php echo CHtml::encode($data->job_details); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sharable')); ?>:</b>
	<?php echo CHtml::encode($data->sharable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_due_date')); ?>:</b>
	<?php echo CHtml::encode($data->job_due_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('est_rps_cost')); ?>:</b>
	<?php echo CHtml::encode($data->est_rps_cost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('est_ao_cost')); ?>:</b>
	<?php echo CHtml::encode($data->est_ao_cost); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('actual_rps_cost')); ?>:</b>
	<?php echo CHtml::encode($data->actual_rps_cost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actual_ao_cost')); ?>:</b>
	<?php echo CHtml::encode($data->actual_ao_cost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('done_percentage')); ?>:</b>
	<?php echo CHtml::encode($data->done_percentage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('assigned_to')); ?>:</b>
	<?php echo CHtml::encode($data->assigned_to); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('task_status')); ?>:</b>
	<?php echo CHtml::encode($data->task_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_by')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_dt')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_by')); ?>:</b>
	<?php echo CHtml::encode($data->updt_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_dt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_cnt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_cnt); ?>
	<br />

	*/ ?>

</div>