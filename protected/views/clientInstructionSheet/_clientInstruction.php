	<?php
	$baseUrl = Yii::app()->baseUrl; 
	$cs = Yii::app()->getClientScript();
	
	$cs->registerScriptFile($baseUrl.'/fixedHeader/js/jquery.fixedheadertable.js');
	$cs->registerScriptFile($baseUrl.'/htmltable_export/tableExport.js');
	$cs->registerScriptFile($baseUrl.'/htmltable_export/jquery.base64.js');
	$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/libs/sprintf.js');
	$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/jspdf.js');
	$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/libs/base64.js');
	
	$reportModel=new ClientInstructionSheet;
	 ?>	
				<!--search fields-->
			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'search-form',
				'enableClientValidation'=>true,
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
				),
			));
			 ?>
	 <div class="outerbox">
	       <table border="1" class="table-bordered">
	          <div>
	            <tr>
				  <td> Project: </td>
					 <td style="margin-bottom:5px;">
					   <?php
	                       $rec=Project::model()->getAllProjectsNo(0,0,0);
						   echo $form->dropDownList($model,'projectId',$rec,array('empty' => 'All')); 
						?>
					  </td>
				  <td> Type: </td>
					  <td style="margin-bottom:5px;">
						<?php
						    echo $form->dropDownList($model,'type',array(''=>'select','1'=>'AO','0'=>'Client','2'=>'RPS')); 
						?>
					  </td>
				  <td><input type="submit" value="Search" class="btn"/></td>
			   </tr>
		     </div>
	     </table>
	  <?php $this->endWidget(); ?>
		<!--end search fields-->
	  <table>
		<div>
	    	<a href="#"  onClick ="
			  $('#myDemoTable').tableExport({
				type:'excel',
				escape:'false',
				//ignoreColumn: [2],
				htmlContent:'true',
				});
			"><input  type="button" value="Export to Excel" class="btn"/></a>
	    </div>
	 </table>
		
			
<!--table starts-->
      <div class="innerbox">
	     <table class="bluetable" id="myDemoTable" cellspacing="0" style="border: 1px solid black;border-top:0px solid black;border-right:1px solid black;">
			<thead>
			   <tr>
				 <th colspan="8" style="border:1px solid black; border-left:0px;border-top:1px solid black;border-right:1px solid black;border-bottom:0px solid black;"><b>Client Instructiopn Sheet</b>
				 </th>
				</tr>
				<tr>
				   <td style="border:1px solid black;border-right:0px; border-left:0px;border-top:0px solid black;border-bottom:1px solid black;"></td>
					<td style="border:1px solid black;border-right:0px; border-left:0px;border-top:0px solid black;border-bottom:1px solid black;"></td>
					<td style="border:1px solid black;border-right:0px; border-left:0px;border-top:0px solid black;border-bottom:1px solid black;"></td>							
					<td style="border:1px solid black;border-right:0px; border-left:0px;border-top:0px solid black;border-bottom:1px solid black;"></td>
					<td style="border:1px solid black;border-right:0px; border-left:0px;border-top:0px solid black;border-bottom:1px solid black;"></td>
					<td style="border:1px solid black;border-right:0px; border-left:0px;border-top:0px solid black;border-bottom:1px solid black;"></td>
					<td style="border:1px solid black;border-right:0px; border-left:0px;border-top:0px solid black;border-bottom:1px solid black;"></td>
					<td style="border:1px solid black;border-right:0px; border-left:0px;border-top:0px solid black;border-bottom:1px solid black;"></td>
				</tr>
			</thead>
		<!--table body start-->
			<tbody>
				<?php
					$cond="";
					if($model->projectId != "" && $model->projectId > 0)
					{
						$cond=" where project_id= '$model->projectId'";
					}
					$projects=Yii::app()->db->createCommand("SELECT * from pm_project $cond")->queryAll();
					foreach($projects as $project)
					{
						$pid=$project['project_id'];
						?>
						<tr>
							<td align="center" style="font-weight: bold;font-size:15px;border-top:1px solid black;"><?php echo $project['project_title'];  ?></td>
						    <td style="border-top:1px solid black;"></td>
							<td style="border-top:1px solid black;"></td>
							<td style="border-top:1px solid black;"></td>
							<td style="border-top:1px solid black;"></td>
							<td style="border-top:1px solid black;"></td>
							<td style="border-top:1px solid black;"></td>
							<td style="border-top:1px solid black;"></td>
						</tr>
						
						<tr>
							<td style="border: 0.5px solid black; border-left:0px; border-top:1px solid black;border-bottom:1px solid black;font-weight: bold;font-size:12px;">Comm. No.</td>
							<td style="border: 0.5px solid black;border-left:0px; border-top:1px solid black;border-bottom:1px solid black;font-weight: bold;font-size:12px;">Subject</td>
							<td style="border: 0.5px solid black;border-left:0px; border-top:1px solid black;border-bottom:1px solid black;font-weight: bold;font-size:12px;">Date</td>
							<td style="border: 0.5px solid black;border-left:0px; border-top:1px solid black;border-bottom:1px solid black;font-weight: bold;font-size:12px;">Type</td>							
							<td style="border: 0.5px solid black;border-left:0px; border-top:1px solid black;border-bottom:1px solid black;font-weight: bold;font-size:12px;">Task Number</td>
							<td style="border: 0.5px solid black;border-left:0px; border-top:1px solid black;border-bottom:1px solid black;font-weight: bold;font-size:12px;">Task Description</td>
							<td style="border: 0.5px solid black;border-left:0px; border-top:1px solid black;border-bottom:1px solid black;font-weight: bold;font-size:12px;">Allocated Hour</td>
							<td style="border: 0.5px solid black;border-left:0px; border-top:1px solid black;border-bottom:1px solid black;font-weight: bold;font-size:12px;border-right:0px solid black;">Comments</td>
						</tr>
						
						<?php
							$communication=null;
							if($model->type != "" && $model->type >= 0)
							{
							  if($model->type=='0')//client
								{
						          $communication = Communication::model()->findAll(
	                              array(
	                              'select'=>'communication_id,communication_subject,communication_date,comm_from',
	                              'condition'=>"comm_from='0' and project_id='$pid'",	                             
	                               ));
								 }
								else if($model->type==1)//ao
										{
						           $communication = Communication::model()->findAll(
										array(
										'select'=>'communication_id,communication_subject,communication_date,comm_from',
									    'join' => 'INNER JOIN pm_branch ON t.comm_from = pm_branch.branch_id', 
									    'condition' => "pm_branch.branch_type = '14' and project_id='$pid'",
											 
											));			
										}
								else if($model->type==2)//rps
								   {
					                $communication = Communication::model()->findAll(
									array(
									'select'=>'communication_id,communication_subject,communication_date,comm_from',
								    'join' => 'INNER JOIN pm_branch ON t.comm_from = pm_branch.branch_id', 
								    'condition' => "pm_branch.branch_type = '18'and project_id='$pid'",
										 
										));	
									}
							
							}
							else
								{
									$communication=Communication::model()->findAll('project_id=:pid',array(':pid'=>$pid));
								}
								
							if($communication != null)
							   {
									foreach($communication as $comm)
									{
										$cid=$comm->communication_id;
									if($cid > 0)
									{
										$type=$model->getAoRPS($comm->comm_from);
										?>
								<tr>
								    <td style="border-right:1px solid black;"><?php echo $comm->communication_id; ?></td>
								    <td style="border-right:1px solid black;"><?php echo $comm->communication_subject; ?></td>
									<td style="border-right:1px solid black;"><?php echo $comm->communication_date; ?></td>
									<td style="border-right:1px solid black;"><?php echo $type; ?></td>
									<td style="border-right:1px solid black;">-</td>
									<td style="border-right:1px solid black;">-</td>
									<td style="border-right:1px solid black;">-</td>
									<td style="border-right:1px solid black;">-</td>
								</tr>
										
										<?php
									$tasks=Task::model()->findAll('communication_id=:cid',array(':cid'=>$cid));
										 if($tasks != null)
										 {
										 	foreach($tasks as $task)
											{
												$tid=$task->task_id;
												if($tid > 0)
												{
													?>
										<tr>
											<td style="border-right:1px solid black;">-</td>
							     			<td style="border-right:1px solid black;">-</td>
											<td style="border-right:1px solid black;">-</td>
											<td style="border-right:1px solid black;">-</td>
											<td style="border-right:1px solid black;"><?php echo $task->task_id; ?></td>
											<td style="border-right:1px solid black;"><?php echo $task->job_details; ?></td>
											<td style="border-right:1px solid black;"><?php echo $task->est_rps_cost; ?></td>
											<td style="border-right:1px solid black;">-</td>
										</tr>
													<?php
													
											}
										}
									 }
							      }
					    	  }
					     }
					 }
					?>
				</tbody>
			</table>
		</div>
	</div>