<?php
/* @var $this ProjectController */
/* @var $model Project */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'project_id'); ?>
		<?php echo $form->textField($model,'project_id',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'project_title'); ?>
		<?php echo $form->textField($model,'project_title',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'project_status'); ?>
		<?php echo $form->textField($model,'project_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'project_type'); ?>
		<?php echo $form->textField($model,'project_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'client_id'); ?>
		<?php echo $form->textField($model,'client_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'project_date'); ?>
		<?php echo $form->textField($model,'project_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'crtd_by'); ?>
		<?php echo $form->textField($model,'crtd_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'crtd_dt'); ?>
		<?php echo $form->textField($model,'crtd_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updt_by'); ?>
		<?php echo $form->textField($model,'updt_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updt_dt'); ?>
		<?php echo $form->textField($model,'updt_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updt_cnt'); ?>
		<?php echo $form->textField($model,'updt_cnt'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->