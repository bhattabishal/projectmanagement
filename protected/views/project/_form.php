<?php
/* @var $this ProjectController */
/* @var $model Project */
/* @var $form CActiveForm */
?>

<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'project-form',
	
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php 
	if($model->isNewRecord)
	{
		echo $form->errorSummary(array($model,$commModel,$taskModel)); 
	}
	else
	{
		echo $form->errorSummary($model); 
	}
	
	?>

	<div class="row col2">
		<?php echo $form->labelEx($model,'project_no'); ?>
		<?php echo $form->textField($model,'project_no',array('size'=>60,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'project_no'); ?>
	</div>
	
	<div class="row col2">
		<?php
		$records = CHtml::listData(Branch::model()->byName()->findAll(), 'branch_id', 'branch_name');
		
		?>
		<?php echo $form->labelEx($model,'branch_id'); ?>
		<?php echo $form->dropDownList($model,'branch_id',$records,array('empty' => 'Select Branch')); ?>
		<?php echo $form->error($model,'branch_id'); ?>
	</div>
	<div style="clear:both"></div>
	<div class="row col2">
		<?php
		$records = CHtml::listData(CodeValue::model()->byLabel()->findAll(array('condition'=>"code_type= 'project_type'")), 'code_id', 'code_lbl');
		//$records = CHtml::listData(CodeValue::model()->findAll("code_type= 'project_type'"), 'code_id', 'code_lbl');
		?>
		<?php echo $form->labelEx($model,'project_type'); ?>
		<?php echo $form->dropDownList($model,'project_type',$records,array('empty' => 'Select project type')); ?>
		<?php echo $form->error($model,'project_type'); ?>
	</div>
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'project_title'); ?>
		<?php echo $form->textField($model,'project_title',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'project_title'); ?>
	</div>
	<div style="clear:both"></div>
	
	<div class="row col2">
		<?php
		$records = Client::model()->byName()->findAll();
		$list = CHtml::listData($records, 'client_id', 'client_name');
		?>
		<?php echo $form->labelEx($model,'client_id'); ?>
		<?php 
		
		//if(isset($model->client_id)) $cid = $model->client_id; else $cid = 0;
		
		echo $form->dropDownList($model,'client_id',$list,array('ajax'=>array(
		'type'=>'POST', //request type
		'url'=>$this->createUrl('project/dynamicClientContact'), //url to call.
		'update'=>'#Project_client_contact_id',
		'data'=>array('client_id'=>'js:this.value') 
		),'empty' => 'Select client')); 
			
		?>
		<?php echo $form->error($model,'client_id'); ?>
	</div>
	
	
	
	
	
	<div class="row col2">
	<?php echo $form->labelEx($model,'client_contact_id'); ?>
	<?php
	
	 $list = array();
	 $options=array();
	 
	 //for insert operation on both before and after validation
	 if($model->isNewRecord)
	 {
	 	
		if(isset($model->client_id) && $model->client_id > 0)
		{
			$data = ClientContact::model()->byName()->findAll( array('condition'=>"client_id=$model->client_id")); 
			$list = CHtml::listData($data, 'client_contact_id', 'contact_name');
			
			if(isset($model->client_contact_id) && sizeof($model->client_contact_id) > 0)
			{
				foreach ($model->client_contact_id as $key=>$val) 
			 	{
				     if($val > 0) 
					 {
				        $options[$val] = array('selected' => 'selected');
				    }
				} 
			}
			
			 
		}
	 }
	 else{
	 	$data = ClientContact::model()->byName()->findAll(array('order' => 'contact_name','condition'=>"client_id=$model->client_id")); 
		$list = CHtml::listData($data, 'client_contact_id', 'contact_name');
			
		$data2 = ProjectContact::model()->findAll("project_id=$model->project_id");
		
		if(isset($model->client_contact_id) && sizeof($model->client_contact_id) > 0)
			{
				foreach ($model->client_contact_id as $key=>$val) 
			 	{
				     if($val > 0) 
					 {
				        $options[$val] = array('selected' => 'selected');
				    }
				} 
			}
			else{
				 foreach ($data2 as $optionVal) 
			 	{
				     if($optionVal->contact_id > 0) 
					 {
				        $options[$optionVal->contact_id] = array('selected' => 'selected');
				    }
				}  
			}
		
	 }
	 
	// echo $cid;
	// die;
	
	
	
	
	 
		//echo CHtml::dropDownList('client_contact_id', $model->client_contact_id,$list, array('multiple' => 'multiple','style'=>'width:45%'));
		echo $form->dropDownList($model,'client_contact_id',$list,array('multiple' => 'multiple','options' => $options)); 
		
	?>
	</div>
	<div style="clear:both"></div>
	
	<div  class="row col2">
		<?php echo $form->labelEx($model,'project_associates_id'); ?>
		<?php
			$data = User::model()->branchWiseUserList(); 
			//$list = CHtml::listData($data, 'user_id', 'user_name');
			$options=array();
			
			if($model->isNewRecord)
			{
				//nothing to do ,yii will handle 
			}
			else{
				$data3 = ProjectAssociates::model()->findAll("project_id=$model->project_id");
				if(isset($model->project_associates_id) && count($model->project_associates_id) > 0)
				{
					foreach ($model->project_associates_id as $key=>$val) 
				 	{
					     if($val > 0) 
						 {
					        $options[$val] = array('selected' => 'selected');
					    }
					} 
				}
				else{
					foreach ($data3 as $optionVal) 
					 {
						     if($optionVal->user_id > 0) 
							 {
						        $options[$optionVal->user_id] = array('selected' => 'selected');
						    }
					}  
				}
			}
			
			
			  
				
			echo $form->dropDownList($model,'project_associates_id',$data,array('multiple' => 'multiple','options' => $options)); 
			
		
		?>
		<?php echo $form->error($model,'project_associates_id'); ?>
	</div>
	
	
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'coord_ao'); ?>
		<?php
			$data = User::model()->getAoList(); 
			//$list = CHtml::listData($data, 'user_id', 'user_name');
			$options=array();
			
			if($model->isNewRecord)
			{
				//nothing to do ,yii will handle 
			}
			else{
				$data3 = ProjectCoordinates::model()->findAll("project_id=$model->project_id and type='ao'");
				if(isset($model->coord_ao) && count($model->coord_ao) > 0)
				{
					foreach ($model->coord_ao as $key=>$val) 
				 	{
					     if($val > 0) 
						 {
					        $options[$val] = array('selected' => 'selected');
					    }
					} 
				}
				else{
					foreach ($data3 as $optionVal) 
					 {
						     if($optionVal->user_id > 0) 
							 {
						        $options[$optionVal->user_id] = array('selected' => 'selected');
						    }
					}  
				}
			}
			
			
			  
				
			echo $form->dropDownList($model,'coord_ao',$data,array('options' => $options)); 
			
		
		?>
		<?php echo $form->error($model,'coord_rps'); ?>
	</div>
	<div style="clear:both"></div>
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'coord_rps'); ?>
		<?php
			$data = User::model()->getRpsList(); 
			//$list = CHtml::listData($data, 'user_id', 'user_name');
			$options=array();
			
			if($model->isNewRecord)
			{
				//nothing to do ,yii will handle 
			}
			else{
				$data3 = ProjectCoordinates::model()->findAll("project_id=$model->project_id and type='rps'");
				if(isset($model->coord_rps) && count($model->coord_rps) > 0)
				{
					foreach ($model->coord_rps as $key=>$val) 
				 	{
					     if($val > 0) 
						 {
					        $options[$val] = array('selected' => 'selected');
					    }
					} 
				}
				else{
					foreach ($data3 as $optionVal) 
					 {
						     if($optionVal->user_id > 0) 
							 {
						        $options[$optionVal->user_id] = array('selected' => 'selected');
						    }
					}  
				}
			}
			
			
			  
				
			echo $form->dropDownList($model,'coord_rps',$data,array('options' => $options)); 
			
		
		?>
		<?php echo $form->error($model,'coord_rps'); ?>
	</div>
	
	<div class="row col2">
		<?php
		$records = CHtml::listData(CodeValue::model()->byLabel()->findAll("code_type ='project_status'"), 'code_id', 'code_lbl');
		//$list = CHtml::listData($records, 'client_id', 'client_name');
		?>
		<?php echo $form->labelEx($model,'project_status'); ?>
		<?php echo $form->dropDownList($model,'project_status',$records,array('empty' => 'Select project status')); ?>
		<?php echo $form->error($model,'project_status'); ?>
	</div>
	
	<div style="clear:both"></div>
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'project_date'); ?>
		<?php 
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $model,
		    'attribute'=>'project_date',
			
		    //'flat'=>true,//remove to hide the datepicker
		    'options'=>array(
		        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
				'dateFormat' => 'yy-mm-dd',
				'showButtonPanel' => true,      // show button panel
		    ),
		    'htmlOptions'=>array(
		        'style'=>''
		    ),
		));
	  ?>
		<?php echo $form->error($model,'project_date'); ?>
	</div>
	

	<!--<div class="row col2">
		<?php //echo $form->labelEx($model,'job_due_date'); ?>
		<?php 
		//$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			//'model' => $model,
		    //'attribute'=>'job_due_date',
			
		    //'flat'=>true,//remove to hide the datepicker
		    //'options'=>array(
		       // 'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
				//'dateFormat' => 'yy-mm-dd',
				//'showButtonPanel' => true,      // show button panel
		   // ),
		   // 'htmlOptions'=>array(
		      //  'style'=>''
		   // ),
		//));
	  ?>
		<?php //echo $form->error($model,'job_due_date'); ?>
	</div>-->
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'wis'); ?>
		<?php echo $form->textField($model,'wis',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'wis'); ?>
	</div>
	<div style="clear:both"></div>
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'est_rps_wis'); ?>
		<?php echo $form->textField($model,'est_rps_wis',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'est_rps_wis'); ?>
	</div>
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'est_ao_wis'); ?>
		<?php echo $form->textField($model,'est_ao_wis',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'est_ao_wis'); ?>
	</div>
	<div style="clear:both"></div>
	<div class="row col2">
		
		<?php echo $form->labelEx($model,'actual_rps_hrs'); ?>
		<?php
		   // $this->widget(
		    //'bootstrap.widgets.TbTimePicker',
			   // array(
				//'model' => $model,
		    //'attribute'=>'job_due_date',
			   // 'name' => 'actual_rps_hrs',
			   // )
		   // );
		?>
		<?php echo $form->textField($model,'actual_rps_hrs',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'actual_rps_hrs'); ?>
	</div>
	
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'actual_ao_hrs'); ?>
		<?php echo $form->textField($model,'actual_ao_hrs',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'actual_ao_hrs'); ?>
	</div>
	<div style="clear:both"></div>
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'done_percentage'); ?>
		<?php echo $form->textField($model,'done_percentage',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'done_percentage'); ?>
	</div>
	<div style="clear:both"></div>
	
	<!--<div class="row col2">
		<?php //echo $form->labelEx($model,'project_comments'); ?>
		<?php ////echo $form->textArea($model,'project_comments',array('rows'=>6, 'cols'=>50)); ?>
		<?php //echo $form->error($model,'project_comments'); ?>
	</div>
	<div style="clear:both"></div>-->
	

	

	

	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->