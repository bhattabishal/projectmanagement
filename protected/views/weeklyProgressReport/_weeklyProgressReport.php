<?php
$baseUrl = Yii::app()->baseUrl; 
$cs = Yii::app()->getClientScript();

//$cs->registerScriptFile($baseUrl.'/fixedHeader/js/jquery.fixedheadertable.js');
//$cs->registerCssFile($baseUrl.'/fixedHeader/css/fixedheadertable.css');
//$cs->registerCssFile($baseUrl.'/fixedHeader/css/custom.css');

$cs->registerScriptFile($baseUrl.'/dataTables/media/js/jquery.dataTables.js');
$cs->registerScriptFile($baseUrl.'/dataTables/extensions/FixedColumns/js/dataTables.fixedColumns.min.js');
$cs->registerCssFile($baseUrl.'/dataTables/media/css/jquery.dataTables.css');
$cs->registerCssFile($baseUrl.'/dataTables/extensions/FixedColumns/css/dataTables.fixedColumns.min.css');

$cs->registerScriptFile($baseUrl.'/htmltable_export/tableExport.js');
$cs->registerScriptFile($baseUrl.'/htmltable_export/jquery.base64.js');

$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/libs/sprintf.js');
$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/jspdf.js');
$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/libs/base64.js');

$reportModel=new WeeklyProgressReport;

?>

<script type="text/javascript" language="javascript" class="init">
//<![CDATA[
var table ;
$(document).ready(function() {
    var table =$('#myDemoTable').dataTable(
    {
    	"dom": "<'row-fluid'r><'row-fluid't><'row-fluid'<'span6'i><'span6'p>>",
		"scrollY": 400,
        "scrollX": true,
        "ordering": false,
        "info":     false,
        'bFilter':false,
        "scrollCollapse": true,
        "paging": false
	});
	new $.fn.dataTable.FixedColumns(table, {
		leftColumns: 2,
		rightColumns: 6,
		heightMatch:'auto',
		
		
	} );
} );
//]]>


</script>


<!--search fields-->
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'search-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
 <h2 style="text-align:center;">Weekly Progress Report</h2>
 <table border="1" class="table-bordered">
   <tr>
   <!--  <td>Branch</td>
        <td>

            <?php
            $rec = Branch::model()->findAll();
            $list = CHtml::listData($rec, 'branch_id', 'branch_name');

            //echo $form->dropDownList($model, 'branch_id', $list, array('empty' => 'All'));
             echo $form->dropDownList($model,'branch_id',$list,array('ajax'=>array(
                                        'type'=>'POST', //request type
									
                                        'url'=>$this->createUrl('project/dynamicRegularProjectByBranch'), //url to call.
                                        'update'=>'#WeeklyProgressReport_projectId',
                                        'data'=>array('branch_id'=>'js:this.value') 
                                        ),
										
										'empty' => 'Select Branch',
									'style' => 'width:110px;'
										)
										); 
            ?>
			</td>
			<td>Project</td>
        <td>

            <?php
           

            //echo $form->dropDownList($model, 'projectId', $rec, array('empty' => 'All'));
            ?>
            <?php
			                              $bid=0;
                                        if(isset($model->branch_id) && $model->branch_id > 0)
                                        {
                                            $bid=$model->branch_id;
                                        }
					 $rec = Project::model()->getAllProjectsNo(0, 0, 0, $bid);
					
					echo $form->dropDownList($model,'projectId',$rec,array(
					'empty' => 'All',
					'style' => 'width:210px;'
					)); 
					?>-->
					
			 <td>Project</td>
        <td>

            <?php
           

            //echo $form->dropDownList($model, 'projectId', $rec, array('empty' => 'All'));
            ?>
            <?php
					 $rec = Project::model()->getAllProjectsNo(0, 0, 0);
					
					echo $form->dropDownList($model,'projectId',$rec,array(
					'empty' => 'All',
					'style' => 'width:210px;'
					)); 
					?>

        </td>
    
			
			<td><input type="submit" value="Search" class="btn"/></td>
   </tr>
 </table>
 <?php $this->endWidget(); ?>
<!--end search fields-->
<a href="#"  onClick ="
        $('#myDemoTable').tableExport({
            type: 'excel',
            escape: 'false',
            //ignoreColumn: [2],
            htmlContent: 'true',
        });
   ">Export to Excel</a>
 <!--main table-->
<?php
$lastRow="";
?>

 <table class="table-striped table-bordered" id="myDemoTable" cellpadding="0" cellspacing="0">
    <thead>
			
		<tr>
		    <th colspan="2">Project</th>
		
		<?php
		   $projectUsers=User::model()->getUsersArrayList();
		    $totUsers=count($projectUsers);
			?>
			<th colspan="<?php echo $totUsers; ?>" style=" text-align:center;">Users</th>
			
		
		   <th  colspan="6" style=" text-align:center;"><b>Total</b></th>
	      
		</tr>
		  <tr>
		      <th>Number</th>
		  <th>Name</th>
		  
				 <?php
				 $lastRow='<tr><td></td><td></td>';
				foreach($projectUsers as $userId=>$userLabel)
			{
			
				$userarry=explode("/",$userLabel);
				echo '<th>'.$userarry[1].'</th>';
				$lastRow.='<td></td>';
			}
			?>
				  <th>This </br> Week</th>
				  <th>Till last </br> week</th>
				  <th>Till this </br> week</th>
				  <th>WIS</th>
				  <th>% </br> Complete</th>
				  <th>Remark</th>
				    <?php
                                        $lastRow.='<td></td><td></td><td></td><td></td><td></td><td></td>';
                                        ?>
			  </tr>
			 
	</thead>
	
	<tbody>
	
           <?php
        		if($projectRecords!= NULL)
        		{
				 foreach ($projectRecords as $prj) {
        $pid = $prj['project_id'];
				     		?>
								<tr>
									    <td><?php echo $prj['prj_no']; ?></td>
                                        <td><?php echo $prj['project_title']; ?></td>
										
										<?php
								$thisWeek=0;
								for($i=0;$i<$totUsers;$i++)
								{
								$userarry=$projectUsers[$i];
								$userarry=explode("/",$userarry);
								$uid=$userarry[0];
								$taskDetails = $reportModel->getWorkedHrsFromTaskSchedule($pid,$uid);
							             	echo '<td>';
			                         //    echo $taskDetails;
											  echo $taskDetails > 0?$taskDetails:"";
			                                echo '</td>';
											$thisWeek=$thisWeek+$taskDetails;
								}
											?>	
											<!--for this week-->
												
											<td>
										   <?php echo $thisWeek;  ?>
										   </td>
										   
										<!--   for till last week-->
										<?php
									$tillLastWeek=$reportModel->getTotalWorkedHrsFromTaskSchedule($pid);
											
											$tillLastWeek=$tillLastWeek-$thisWeek;
													
												?>
										<td>
		                                	<?php echo $tillLastWeek; ?>
		                            	</td>
										<!--for till this week-->
										<?php
									$tillThisWeek=$reportModel->getTotalWorkedHrsFromTaskSchedule($pid);
							           			
										?>
										<td>
		                                	<?php echo $tillThisWeek; ?>
		                            	</td>
									<!--	for wis-->
									<?php
                                        $wis=$reportModel->getWisFromProject($pid);
											   
									?>
										<td>
									
										<?php echo $wis; ?>
		                                	
		                            	</td>
									<!--	for comleted-->
										<td>
										<?php 
										if($wis == 0)
										{
											$complete ="#DIV/0!";
										}
										else
										{
										$complete=$tillThisWeek/$wis;
											//	$complete = "20.2307";
										$complete = number_format($complete, 2, '.', ''); 
										//echo $complete;
										}
										
										?>
		                                	<?php echo $complete; ?>
		                            	</td>
										<td>
		                                	<?php echo $remark; ?>
		                            	</td>
									</tr>
									
									
									<?php
							
							}
							 echo $lastRow;	
							  ?>
				           
							 <tr>
							 <td>Hrs worked on the Job (A)</td>
							 <td></td>
								 <?php
				 
			               for($i=0;$i<$totUsers;$i++)
								{
								$userarry=$projectUsers[$i];
								$userarry=explode("/",$userarry);
								$uid=$userarry[0];
								$totWrkOfUser = $reportModel->getTotalWorkedHrsByUserFromTaskSchedule($uid);
							             	echo '<td>';
			                         //    echo $taskDetails;
											echo $totWrkOfUser > 0?$totWrkOfUser:"";
			                                echo '</td>';
								}
		              	?>
							<?php
							$totThisWeek = $reportModel->getTotalThisHrsFromTaskSchedule();
							             	echo '<td>';
			                         //    echo $taskDetails;
											echo $totThisWeek > 0?$totThisWeek:"";
			                                echo '</td>';
							?>
							  <?php
							   $totTillThisWeek = $reportModel->getTotalTillThisHrsFromTaskSchedule();
							   $totTillLastWeek = $totTillThisWeek-$totThisWeek ;
							    echo '<td>';
								echo $totTillLastWeek > 0?$totTillLastWeek:"";
								 echo '</td>';
							 ?>
							 <?php
							   $totTillThisWeek = $reportModel->getTotalTillThisHrsFromTaskSchedule();
							    echo '<td>';
								echo $totTillThisWeek > 0?$totTillThisWeek:"";
								 echo '</td>';
							 ?>
							 
							
							 
							 <td></td> <td></td> <td></td>
							 </tr>
							  <tr>
							 <td>Hrs spent on General works (B)</td>
							 <td></td>
								 <?php
				 
			for($i=0;$i<$totUsers;$i++)
								{
								$userarry=$projectUsers[$i];
								$userarry=explode("/",$userarry);
								$uid=$userarry[0];
								$taskDetails2 = 0;
							             	echo '<td>';
			                         //    echo $taskDetails;
											  echo $taskDetails2 > 0?$taskDetails2:"";
			                                echo '</td>';
								}
		              	?>
							 <td></td> <td></td> <td></td> <td></td> <td></td> <td></td>
							 </tr>
							  <tr>
							 <td>Total hrs worked (C)</td>
							 <td></td>
								 <?php
				 
			for($i=0;$i<$totUsers;$i++)
								{
								$userarry=$projectUsers[$i];
								$userarry=explode("/",$userarry);
								$uid=$userarry[0];
								$taskDetails1 = $reportModel->getTotalWorkedHrsByUserFromTaskSchedule($uid);
								$taskDetails3 = $taskDetails1+$taskDetails2;
							             	echo '<td>';
			                         //    echo $taskDetails;
											  echo $taskDetails3 > 0?$taskDetails3:"";
			                                echo '</td>';
								}
		              	?>
							 <?php
							$totThisWeekB = $totThisWeek+$taskDetails2;
							             	echo '<td>';
			                         //    echo $taskDetails;
											echo $totThisWeekB > 0?$totThisWeekB:"";
			                                echo '</td>';
							?>
							 <td></td> <td></td> <td></td> <td></td> <td></td>
							 </tr>
							 <?php
							 echo $lastRow;	
							 ?>
							 
							   <tr>
							 <td>Working days for the week</td>
							 <td>6</td>
								 <?php
				 
			for($i=0;$i<$totUsers;$i++)
								{
								$userarry=$projectUsers[$i];
								$userarry=explode("/",$userarry);
								$uid=$userarry[0];
								$taskDetails1 = 0;
							             	echo '<td>';
			                         //    echo $taskDetails;
											  echo $taskDetails1 > 0?$taskDetails1:"";
			                                echo '</td>';
								}
		              	?>
							 <td></td> <td></td> <td></td> <td></td> <td></td> <td></td>
							 </tr>
							   <tr>
							 <td>Official holiday</td>
							 <td></td>
								 <?php
				 
			for($i=0;$i<$totUsers;$i++)
								{
								$userarry=$projectUsers[$i];
								$userarry=explode("/",$userarry);
								$uid=$userarry[0];
								$taskDetails1 = 0;
							             	echo '<td>';
			                         //    echo $taskDetails;
											  echo $taskDetails1 > 0?$taskDetails1:"";
			                                echo '</td>';
								}
		              	?>
							 <td></td> <td></td> <td></td> <td></td> <td></td> <td></td>
							 </tr>
							   <tr>
							 <td>Casual leave taken</td>
							 <td></td>
								 <?php
				 
			for($i=0;$i<$totUsers;$i++)
								{
								$userarry=$projectUsers[$i];
								$userarry=explode("/",$userarry);
								$uid=$userarry[0];
								$taskDetails1 = 0;
							             	echo '<td>';
			                         //    echo $taskDetails;
											  echo $taskDetails1 > 0?$taskDetails1:"";
			                                echo '</td>';
								}
		              	?>
							 <td></td> <td></td> <td></td> <td></td> <td></td> <td></td>
							 </tr>
							   <tr>
							 <td>Sunday leave taken</td>
							 <td></td>
								 <?php
				 
			for($i=0;$i<$totUsers;$i++)
								{
								$userarry=$projectUsers[$i];
								$userarry=explode("/",$userarry);
								$uid=$userarry[0];
								$taskDetails1 = 0;
							             	echo '<td>';
			                         //    echo $taskDetails;
											  echo $taskDetails1 > 0?$taskDetails1:"";
			                                echo '</td>';
								}
		              	?>
							 <td></td> <td></td> <td></td> <td></td> <td></td> <td></td>
							 </tr>
							   <tr>
							 <td>Total days worked</td>
							 <td></td>
								 <?php
				 
			for($i=0;$i<$totUsers;$i++)
								{
								$userarry=$projectUsers[$i];
								$userarry=explode("/",$userarry);
								$uid=$userarry[0];
								$taskDetails1 = 6;
							             	echo '<td>';
			                         //    echo $taskDetails;
											  echo $taskDetails1 > 0?$taskDetails1:"";
			                                echo '</td>';
								}
		              	?>
							 <td></td> <td></td> <td></td> <td></td> <td></td> <td></td>
							 </tr>
							   <tr>
							 <td>Normal hrs of work/day</td>
							 <td>7.25</td>
								 <?php
				 
			for($i=0;$i<$totUsers;$i++)
								{
								$userarry=$projectUsers[$i];
								$userarry=explode("/",$userarry);
								$uid=$userarry[0];
								$taskDetails1 = 0;
							             	echo '<td>';
			                         //    echo $taskDetails;
											  echo $taskDetails1 > 0?$taskDetails1:"";
			                                echo '</td>';
								}
		              	?>
							 <td></td> <td></td> <td></td> <td></td> <td></td> <td></td>
							 </tr>
							   <tr>
							 <td>Working hrs/week (D)</td>
							 <td></td>
								 <?php
				 
			for($i=0;$i<$totUsers;$i++)
								{
								$userarry=$projectUsers[$i];
								$userarry=explode("/",$userarry);
								$uid=$userarry[0];
								$taskDetails1 = 0;
							             	echo '<td>';
			                         //    echo $taskDetails;
											  echo $taskDetails1 > 0?$taskDetails1:"";
			                                echo '</td>';
								}
		              	?>
							 <td></td> <td></td> <td></td> <td></td> <td></td> <td></td>
							 </tr>
							   <tr>
							 <td>Overtime (C-D)</td>
							 <td></td>
								 <?php
				 
			for($i=0;$i<$totUsers;$i++)
								{
								$userarry=$projectUsers[$i];
								$userarry=explode("/",$userarry);
								$uid=$userarry[0];
								$taskDetails1 = 0;
							             	echo '<td>';
			                         //    echo $taskDetails;
											  echo $taskDetails1 > 0?$taskDetails1:"";
			                                echo '</td>';
								}
		              	?>
							 <td></td> <td></td> <td></td> <td></td> <td></td> <td></td>
							 </tr>
							  <?php
							 echo $lastRow;	
							 ?>
							<?php
	                 	}
	         	?>
		 
  
		</tbody>
		
	
 </table>