<?php
/* @var $this FixedTaskTimelineBriefController */
/* @var $model FixedTaskTimelineBrief */
/* @var $form CActiveForm */
?>

<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fixed-task-timeline-brief-form',
     
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

        
        <div style="border: 1px solid #cccccc; margin-bottom: 20px">
            <div class="row" style="width:400px; float: left">
                <?php echo $form->labelEx($model,'entry_dt'); ?>
		<?php 
               
                 $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => "entry_dt",
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => 'yy-mm-dd',
                        'showButtonPanel' => true,
                    ),
                    'htmlOptions' => array(
                        'style' => 'height:20px;'
                    ),
                ));
                ?>
		<?php echo $form->error($model,'entry_dt'); ?>
            </div>
            <div class="row" style="width:400px; float: left">
                <?php echo $form->labelEx($model,'user_id'); ?>
		<?php 
                    //echo $form->textField($model,'user_id'); 
                $list=User::model()->getAoRPSList();
                  echo $form->dropDownList($model, 'user_id',$list);
                ?>
		<?php echo $form->error($model,'user_id'); ?>
            </div>
            <div style="clear:both"></div>
        </div>

	

	<div class="row buttons">
		<?php echo CHtml::submitButton('View'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->