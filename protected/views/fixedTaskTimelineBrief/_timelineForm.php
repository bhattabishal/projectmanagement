<?php
/* @var $this TaskScheduleController */
/* @var $model TaskSchedule */
/* @var $form CActiveForm */
 $list=  FixedTask::model()->listChildTasksForTimeline();
?>
<?php $row_id = "fixedTaskTimeline-" . $key ?>
<div class='row-fluid custom'  id="<?php echo $row_id ?>">
<?php
    echo $form->hiddenField($model, "[$key]fxt_id");
    echo $form->updateTypeField($model, $key, "updateType", array('key' => $key));
    ?>

	 <div class="span3">
		<?php echo $form->labelEx($model,"[$key]project_id"); ?>
		<?php 
		
                 echo $form->dropDownList($model, "[$key]project_id",$list);
	  ?>
		<?php echo $form->error($model,"[$key]project_id"); ?>
	</div>
	
	 

	 <div class="span-custom-small">
		<?php echo $form->labelEx($model,"[$key]nt_hours"); ?>
		<?php echo $form->textField($model,"[$key]nt_hours",array('class'=>'small')); ?>
		<?php echo $form->error($model,"[$key]nt_hours"); ?>
	</div>

	 <div class="span-custom-small">
		<?php echo $form->labelEx($model,"[$key]ot_hours"); ?>
		<?php echo $form->textField($model,"[$key]ot_hours",array('class'=>'small')); ?>
		<?php echo $form->error($model,"[$key]ot_hours"); ?>
	</div>

	 <div class="span3">
		<?php echo $form->labelEx($model,"[$key]remarks"); ?>
		<?php echo $form->textField($model,"[$key]remarks",array('size'=>20,'maxlength'=>250,'style'=>'width:220px')); ?>
		<?php echo $form->error($model,"[$key]remarks"); ?>
	</div >
	
		<div  class="span1">
 
            <?php echo $form->deleteRowButton1($row_id, $key); ?>
        </div>
	
	



</div><!-- form -->