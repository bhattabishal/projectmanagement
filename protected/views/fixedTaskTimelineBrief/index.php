<?php
/* @var $this FixedTaskTimelineBriefController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fixed Task Timeline Briefs',
);

$this->menu=array(
	array('label'=>'Create FixedTaskTimelineBrief', 'url'=>array('create')),
	array('label'=>'Manage FixedTaskTimelineBrief', 'url'=>array('admin')),
);
?>

<h1>Fixed Task Timeline Briefs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
