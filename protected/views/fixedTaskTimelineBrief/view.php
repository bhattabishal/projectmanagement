<?php
/* @var $this FixedTaskTimelineBriefController */
/* @var $model FixedTaskTimelineBrief */

$this->breadcrumbs=array(
	'Fixed Task Timeline Briefs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FixedTaskTimelineBrief', 'url'=>array('index')),
	array('label'=>'Create FixedTaskTimelineBrief', 'url'=>array('create')),
	array('label'=>'Update FixedTaskTimelineBrief', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FixedTaskTimelineBrief', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FixedTaskTimelineBrief', 'url'=>array('admin')),
);
?>

<h1>View FixedTaskTimelineBrief #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'entry_dt',
		'user_id',
		'tot_normal_hrs',
		'tot_ot_hrs',
		'crtd_dt',
		'crtd_by',
		'updt_dt',
		'updt_by',
		'updt_cnt',
	),
)); ?>
