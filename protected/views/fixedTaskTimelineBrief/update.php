<?php
/* @var $this FixedTaskTimelineBriefController */
/* @var $model FixedTaskTimelineBrief */

$this->breadcrumbs=array(
	'Fixed Task Timeline Briefs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FixedTaskTimelineBrief', 'url'=>array('index')),
	array('label'=>'Create FixedTaskTimelineBrief', 'url'=>array('create')),
	array('label'=>'View FixedTaskTimelineBrief', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FixedTaskTimelineBrief', 'url'=>array('admin')),
);
?>

<h1>Update FixedTaskTimelineBrief <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>