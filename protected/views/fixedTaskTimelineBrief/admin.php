<?php
/* @var $this FixedTaskTimelineBriefController */
/* @var $model FixedTaskTimelineBrief */

$this->breadcrumbs=array(
	'Fixed Task Timeline Briefs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List FixedTaskTimelineBrief', 'url'=>array('index')),
	array('label'=>'Create FixedTaskTimelineBrief', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fixed-task-timeline-brief-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Fixed Task Timeline Briefs</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fixed-task-timeline-brief-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'entry_dt',
		'user_id',
		'tot_normal_hrs',
		'tot_ot_hrs',
		'crtd_dt',
		/*
		'crtd_by',
		'updt_dt',
		'updt_by',
		'updt_cnt',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
