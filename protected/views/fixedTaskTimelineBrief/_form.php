<?php
/* @var $this FixedTaskTimelineBriefController */
/* @var $model FixedTaskTimelineBrief */
/* @var $form CActiveForm */
?>

<div class="form wide">
    
    <?php
    $form1=$this->beginWidget('CActiveForm', array(
	'id'=>'fixed-task-timeline-brief-form1',
      
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); 
    
    ?>
    <div style="border: 1px solid #cccccc; margin-bottom: 20px">
            <div class="row" style="width:400px; float: left">
                <?php echo $form1->labelEx($model,'entry_dt'); ?>
		<?php 
               
                 $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => "entry_dt",
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => 'yy-mm-dd',
                        'showButtonPanel' => true,
                    ),
                    'htmlOptions' => array(
                        'style' => 'height:20px;'
                    ),
                ));
                ?>
		<?php echo $form1->error($model,'entry_dt'); ?>
            </div>
            <!--<div class="row" style="width:400px; float: left">
                <?php //echo $form1->labelEx($model,'user_id'); ?>
		<?php 
                    //echo $form->textField($model,'user_id'); 
                //$list=User::model()->getAoRPSList();
                  //echo $form1->dropDownList($model, 'user_id',$list);
                ?>
		<?php //echo $form1->error($model,'user_id'); ?>
            </div>-->
           <!-- <div  class="row" style="width:100px; float: left">
                 <?php //echo CHtml::submitButton( 'View',array('class'=>'btn')); ?>
            </div>-->
            <div style="clear:both"></div>
        </div>
    
   
    <?php
   $this->endWidget(); 
    ?>

<?php 

$form=$this->beginWidget('DynamicTabularForm', array(
	'id'=>'fixed-task-timeline-brief-form',
        'defaultRowView1'=>'_timelineForm',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	
        
        

	<div class="row">
		  <?php 
                  
                  echo $form->rowForm1($modelTimeline); 
                  
                  ?>
	</div>
        <?php 
       
         echo $form->hiddenField($model, 'user_id');
        ?>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('name'=>'btnSave','class'=>'btn')); ?>
	</div>

<?php 
$this->endWidget();

?>

</div><!-- form -->