<?php
/* @var $this ChecklistController */
/* @var $model Checklist */

$this->breadcrumbs=array(
	'Checklists'=>array('index'),
	$model->checklist_id=>array('view','id'=>$model->checklist_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Checklist', 'url'=>array('index')),
	array('label'=>'Create Checklist', 'url'=>array('create')),
	array('label'=>'View Checklist', 'url'=>array('view', 'id'=>$model->checklist_id)),
	array('label'=>'Manage Checklist', 'url'=>array('admin')),
);
?>

<h1>Update Checklist <?php echo $model->checklist_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>