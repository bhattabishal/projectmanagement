<?php
/* @var $this ChecklistController */
/* @var $data Checklist */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('checklist_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->checklist_id), array('view', 'id'=>$data->checklist_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_type_id')); ?>:</b>
	<?php echo CHtml::encode($data->job_type_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('checklist_details')); ?>:</b>
	<?php echo CHtml::encode($data->checklist_details); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('checklist_stages')); ?>:</b>
	<?php echo CHtml::encode($data->checklist_stages); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_by')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_dt')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_by')); ?>:</b>
	<?php echo CHtml::encode($data->updt_by); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_dt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_cnt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_cnt); ?>
	<br />

	*/ ?>

</div>