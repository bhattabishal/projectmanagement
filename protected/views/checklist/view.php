<?php
/* @var $this ChecklistController */
/* @var $model Checklist */

$this->breadcrumbs=array(
	'Checklists'=>array('index'),
	$model->checklist_id,
);

$this->menu=array(
	array('label'=>'List Checklist', 'url'=>array('index')),
	array('label'=>'Create Checklist', 'url'=>array('create')),
	array('label'=>'Update Checklist', 'url'=>array('update', 'id'=>$model->checklist_id)),
	array('label'=>'Delete Checklist', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->checklist_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Checklist', 'url'=>array('admin')),
);
?>

<h1>View Checklist #<?php echo $model->checklist_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'checklist_id',
		'job_type_id',
		'checklist_details',
		'checklist_stages',
		'crtd_by',
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
	),
)); ?>
