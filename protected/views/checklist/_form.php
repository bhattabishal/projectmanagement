<?php
/* @var $this ChecklistController */
/* @var $model Checklist */
/* @var $form CActiveForm */
?>

<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'checklist-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php //echo $form->errorSummary($model); ?>

	
	<!--<div class="row col2">
		<?php
		//$records = CHtml::listData(CodeValue::model()->findAll("code_type= 'project_type'"), 'code_id', 'code_lbl');
		?>
		<?php //echo $form->labelEx($model,'job_type_id'); ?>
		<?php //echo $form->dropDownList($model,'job_type_id',$records,array('empty' => 'Select project type')); ?>
		<?php //echo $form->error($model,'job_type_id'); ?>
	</div>-->
	<!--<div class="row col2">
		<?php
		//$records = CHtml::listData(CodeValue::model()->findAll("code_type= 'checklist_stages'"), 'code_id', 'code_lbl');
		?>
		<?php // echo $form->labelEx($model,'checklist_stages'); ?>
		<?php //echo $form->dropDownList($model,'checklist_stages',$records,array('empty' => 'Select stages')); ?>
		<?php //echo $form->error($model,'checklist_stages'); ?>
	</div>-->
	
	<div style="clear:both"></div>
	<div class="row col2">
		<?php echo $form->labelEx($model,'checklist_details'); ?>
		<?php echo $form->textArea($model,'checklist_details',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'checklist_details'); ?>
	</div>
	
	
	<div style="clear:both"></div>
	<!--<div class="row">
		<?php //echo $form->labelEx($model,'crtd_by'); ?>
		<?php //echo $form->textField($model,'crtd_by'); ?>
		<?php //echo $form->error($model,'crtd_by'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'crtd_dt'); ?>
		<?php //echo $form->textField($model,'crtd_dt'); ?>
		<?php //echo $form->error($model,'crtd_dt'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'updt_by'); ?>
		<?php //echo $form->textField($model,'updt_by'); ?>
		<?php //echo $form->error($model,'updt_by'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'updt_dt'); ?>
		<?php //echo $form->textField($model,'updt_dt'); ?>
		<?php //echo $form->error($model,'updt_dt'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'updt_cnt'); ?>
		<?php //echo $form->textField($model,'updt_cnt'); ?>
		<?php //echo $form->error($model,'updt_cnt'); ?>
	</div>-->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->