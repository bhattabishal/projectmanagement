<?php
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();

//$cs->registerScriptFile($baseUrl.'/fixedHeader/js/jquery.fixedheadertable.js');
//$cs->registerCssFile($baseUrl.'/fixedHeader/css/fixedheadertable.css');
//$cs->registerCssFile($baseUrl.'/fixedHeader/css/custom.css');



$cs->registerScriptFile($baseUrl . '/dataTables/media/js/jquery.dataTables.js');
$cs->registerScriptFile($baseUrl . '/dataTables/extensions/FixedColumns/js/dataTables.fixedColumns.min.js');
$cs->registerCssFile($baseUrl . '/dataTables/media/css/jquery.dataTables.min.css');
$cs->registerCssFile($baseUrl . '/dataTables/extensions/FixedColumns/css/dataTables.fixedColumns.min.css');

/* data tables not used
  //$cs->registerCssFile($baseUrl.'/dataTables/examples/resources/syntax/shCore.css');
  //$cs->registerCssFile($baseUrl.'/dataTables/examples/resources/demo.css');
  //$cs->registerCssFile($baseUrl.'/dataTables/extensions/TableTools/css/dataTables.tableTools.css');
  //$cs->registerScriptFile($baseUrl.'/dataTables/extensions/FixedHeader/js/dataTables.fixedHeader.js');
  //$cs->registerScriptFile($baseUrl.'/dataTables/extensions/KeyTable/js/dataTables.keyTable.js');
  //$cs->registerScriptFile($baseUrl.'/dataTables/media/js/complete.min.js');
  //cs->registerScriptFile($baseUrl.'/dataTables/media/js/jquery.js');
  //$cs->registerScriptFile($baseUrl.'/dataTables/examples/resources/syntax/shCore.js');
  //$cs->registerScriptFile($baseUrl.'/dataTables/examples/resources/demo.js');
  //$cs->registerScriptFile($baseUrl.'/dataTables/extensions/TableTools/js/dataTables.tableTools.js');
 */

$cs->registerScriptFile($baseUrl . '/htmltable_export/tableExport.js');
$cs->registerScriptFile($baseUrl . '/htmltable_export/jquery.base64.js');

//$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/libs/sprintf.js');
//$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/jspdf.js');
//$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/libs/base64.js');



$reportModel = new ScheduleReport;



$days = MyCustomClass::GetDateDifference($model->endDt, $model->startDt);
$days+=1; //for current date the difference is 0 so make it one.we need total days count
?>


<script type="text/javascript" language="javascript" class="init">
//<![CDATA[
    var table;
    $(document).ready(function () {
        var table = $('#myDemoTable').dataTable(
                {
                    "dom": "<'row-fluid'r><'row-fluid't><'row-fluid'<'span6'i><'span6'p>>",
                    "scrollY": 400,
                    "scrollX": true,
                    "ordering": false,
                    "info": false,
                    'bFilter': false,
                    "scrollCollapse": true,
                    "paging": false
                });
        new $.fn.dataTable.FixedColumns(table, {
            leftColumns: 2,
            rightColumns: 4,
            heightMatch: 'auto',
        });
    });
//]]>


</script>


<!--search fields-->
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'search-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
        ));
?>
<table border="1" class="table-bordered">
    <tr>
        <td>Branch</td>
        <td>

            <?php
            $rec = Branch::model()->byName()->findAll();//use scopes for order in asc
            $list = CHtml::listData($rec, 'branch_id', 'branch_name');

            //echo $form->dropDownList($model, 'branch_id', $list, array('empty' => 'All'));
             echo $form->dropDownList($model,'branch_id',$list,array('ajax'=>array(
                                        'type'=>'POST', //request type
									
                                        'url'=>$this->createUrl('project/dynamicProjectByBranch'), //url to call.
                                        'update'=>'#ScheduleReport_projectId',
                                        'data'=>array('branch_id'=>'js:this.value') 
                                        ),
										
										'empty' => 'Select Branch',
									'style' => 'width:110px;'
										)
										); 
            ?>



        </td>
        <td>Project</td>
        <td>

            <?php
           

            //echo $form->dropDownList($model, 'projectId', $rec, array('empty' => 'All'));
            ?>
            <?php
                                        $bid=0;
                                        if(isset($model->branch_id) && $model->branch_id > 0)
                                        {
                                            $bid=$model->branch_id;
                                        }
					 $rec = Project::model()->getAllBranchUsersProjectsNo1(0,0,0,$bid);
					
					echo $form->dropDownList($model,'projectId',$rec,array(
					'empty' => 'All',
					'style' => 'width:110px;'
					)); 
					?>

        </td>
        <td>Fixed Task  <?php echo $form->checkBox($model,'adminTasks'); ?></td>
       
        <td>StartDate</td>
        <td>
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'startDt',
                //'flat'=>true,//remove to hide the datepicker
                'options' => array(
                    'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                    'dateFormat' => 'yy-mm-dd',
                    'showButtonPanel' => true, // show button panel
                ),
                'htmlOptions' => array(
                    'style'=>'width:106px'
                ),
            ));
            ?>
        </td>
        <td>End Date</td>
        <td>
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'endDt',
                //'flat'=>true,//remove to hide the datepicker
                'options' => array(
                    'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                    'dateFormat' => 'yy-mm-dd',
                    'showButtonPanel' => true, // show button panel
                ),
               'htmlOptions' => array(
			   'style'=>'width:106px'
                ),
				
            ));
            ?>
        </td>
        <td><input type="submit" value="Search" class="btn"/></td>
    </tr>

</table>
<?php $this->endWidget(); ?>
<!--end search fields-->

<a href="#"  onClick ="
        $('#myDemoTable').tableExport({
            type: 'excel',
            escape: 'false',
            //ignoreColumn: [2],
            htmlContent: 'true',
        });
   ">Export to Excel</a>



<?php
$lastRow="";
?>


<div class="outerbox">
    <div class="innerbox">
        <table id="myDemoTable" class="table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th colspan="2">Project</th>


                    <?php
                    $loopStartDt = $model->startDt;
                    $loopEndDt = $model->endDt;
                    $dates = array();
                    while (strtotime($loopStartDt) <= strtotime($loopEndDt)) {
                        $currentDate = $loopStartDt;
                        $dates[] = $currentDate;
                        //if(date('w', strtotime($currentDate)) != 6)
                        {
                            ?>
                            <th colspan="2"><?php echo date('D-M-j-Y', strtotime($currentDate)); ?></th>	
                            <?php
                        }
                        $loopStartDt = date("Y-m-d", strtotime("+1 day", strtotime($loopStartDt)));
                    }
                    ?>
                    <th colspan="2">Total</th>
                    <th colspan="2">Grand Total</th>
                </tr>
                <tr>
                    <th>No</th>
                    <th>Title</th>
<?php
$lastRow='<tr><td></td><td></td>';
foreach ($dates as $key => $val) {
    echo '<th>Sc</th>';
    echo '<th>Ac</th>';
    $lastRow.='<td></td><td></td>';
}
?>
                    <th>Sc</th>
                    <th>Ac</th>
                    <th>Sc</th>
                    <th>Ac</th>
                    <?php
                     $lastRow.='<td></td><td></td><td></td><td></td></tr>';
                    ?>
                </tr>
            </thead>
            <tbody>
<?php
if ($projectRecords != NULL) {

    foreach ($projectRecords as $prj) {
        $totAct = 0;
        $totSche = 0;
        $pid = $prj['project_id'];
        if ($pid < 0) {
            $tasks = Task::model()->findAll('communication_id=:cid', array(':cid' => '-1'));
            if ($tasks != null) {
                foreach ($tasks as $task) {
                    $tid = $task->task_id;
                    ?>
                                    <tr>
                                        <td><?php echo $prj['prj_no']; ?> (<?php echo $prj['project_title']; ?>)</td>
                                        <td><?php echo $task->job_details; ?></td>
                                    <?php
                                    foreach ($dates as $key => $val) {
                                        $date = $val;
                                        $scheduled = $reportModel->getTotAssignedHrsFromFixedTaskDate($tid, $date);
                                        $actual = $reportModel->getTotWorkedHrsFixedTaskFromTaskDate($tid, $date);
                                        $totAct+=$actual;
                                        $totSche+=$scheduled;
                                        echo '<td>';
                                        echo $scheduled > 0 ? $scheduled : "";
                                        echo '</td>';
                                        echo '<td>';
                                        echo $actual > 0 ? $actual : "";
                                        echo '</td>';
                                    }
                                    ?>
                                        <td>
                                        <?php echo $totSche; ?>
                                        </td>
                                        <td>
                                        <?php echo $totAct; ?>
                                        </td>
                                        <td> <?php echo $reportModel->getTotAssignedHrsFromTask($tid); ?></td>
                                        <td><?php echo $reportModel->getTotWorkedHrsFromTask($tid); ?></td>
                                    </tr>
                                        <?php
                                    }
                                }
                            } else {
                                ?>
                            <tr>
                                <td><?php echo $prj['prj_no']; ?></td>
                                <td><?php echo $prj['project_title']; ?></td>
            <?php
            foreach ($dates as $key => $val) {
                $date = $val;
                $scheduled = $reportModel->getTotAssignedHrsFromProjectDate($pid, $date);
                $actual = $reportModel->getTotWorkedHrsFromProjectDate($pid, $date);
                $totAct+=$actual;
                $totSche+=$scheduled;
                echo '<td>';
                echo $scheduled > 0 ? $scheduled : "";
                echo '</td>';
                echo '<td>';
                echo $actual > 0 ? $actual : "";
                echo '</td>';
            }
            ?>
                                <td>
                                <?php echo $totSche; ?>
                                </td>
                                <td>
                                <?php echo $totAct; ?>
                                </td>
                                <td> <?php echo $reportModel->getTotAssignedHrsFromProject($pid); ?></td>
                                <td><?php echo $reportModel->getTotWorkedHrsFromProject($pid); ?></td>
                            </tr>
                                <?php
                            }
                        }
                        echo $lastRow;
                    }
                    ?>
            </tbody>
        </table>
    </div>
</div>


<!--<script language="javascript">
        $(document).ready(function() {
              $('#myDemoTable').fixedHeaderTable({
                   altClass : 'odd',
                  footer : false,
                  fixedColumns : 1
               
                });
   
                
            });
</script>-->