<?php
$baseUrl = Yii::app()->baseUrl; 
$cs = Yii::app()->getClientScript();

$cs->registerScriptFile($baseUrl.'/fixedHeader/js/jquery.fixedheadertable.js');
$cs->registerCssFile($baseUrl.'/fixedHeader/css/fixedheadertable.css');
$cs->registerCssFile($baseUrl.'/fixedHeader/css/custom.css');



$cs->registerScriptFile($baseUrl.'/htmltable_export/tableExport.js');
$cs->registerScriptFile($baseUrl.'/htmltable_export/jquery.base64.js');

$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/libs/sprintf.js');
$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/jspdf.js');
$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/libs/base64.js');
?>


<!--search fields-->
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'search-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
	<table border="1" class="table-bordered">
		<tr>
			<td>User</td>
			<td>
				
					<?php
					$rec=User::model()->getAoRPSList();
					
					echo $form->dropDownList($model,'userId',$rec,array('empty' => 'All')); 
					?>
				
			</td>
			<td>From Month</td>
			<td>
				<?php
					$rec=MyCustomClass::getMonthsList();
					
					echo $form->dropDownList($model,'frommonth',$rec,array('empty' => 'All')); 
					?>
			</td>
			
			<td>To Month</td>
			<td>
				<?php
					$rec=MyCustomClass::getMonthsList();
					
					echo $form->dropDownList($model,'tomonth',$rec,array('empty' => 'All')); 
					?>
			</td>
			
			<td><input type="submit" value="Search" class="btn"/></td>
		</tr>
		
	</table>
<?php $this->endWidget(); ?>
<!--end search fields-->

<a href="#"  onClick ="
		$('#myDemoTable').tableExport({
			type:'excel',
			escape:'false',
			//ignoreColumn: [2],
			htmlContent:'true',
			});
		">Export to Excel</a>

<!--table starts-->
<table class="bluetable" id="myDemoTable" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th colspan="13"><b>Performance Report</b></th>
		</tr>
	</thead>
	<tbody>
<?php
	$cond="";
	if($model->userId != "" && $model->userId > 0)
	{
		$cond=" where user_id= '$model->userId'";
	}
	$users=Yii::app()->db->createCommand("SELECT * from pm_user $cond")->queryAll();
	foreach($users as $user)
	{
		$uid=$user['user_id'];
		?>
		<tr>
			<td colspan="13" align="center" style="font-weight: bold;font-size:14px;"><?php echo $user['user_name']; ?></td>
		</tr>
		<?php
		$startMonth=$model->frommonth;
		$tomonth=$model->tomonth;
		$start=strtotime($startMonth);
		$end = strtotime($tomonth);
		while($end >= $start)
		{
		    $month=date('Y-m-d', $end);
		    ?>
		    <!--<tr>
				<td colspan="12" align="center"><?php// echo $month; ?> </td>
			</tr>-->
			<tr>
				<td rowspan="2" style="background-color: #00ff00"><b><?php echo date("F Y", strtotime($month)) ; ?></b></td>				
				<td><b>Hour Assigned</b></td>
				<td><b>Hour Taken</b></td>
				<td><b>Hour Logged</b></td>
				<td colspan="3"><b>Error</b></td>
				<td><b>ERRORS IN 100% WORK DONE I.E %</b></td>
				<td><b>GOALS IN 100%  ASSIGNED I.E %</b></td>
				<td><b>SPEED HOURS ASSIGNED/HOURS TAKEN I.E %</b></td>
				<td></td>
				<td colspan="2"><b>Manager Assesment</b></td>
			</tr>
			<tr>
				
				<td></td>
				<td></td>
				<td></td>
				<td><b>Total</b></td>
				<td><b>Knows But Happens</b></td>
				<td><b>Doesn't Know</b></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><b>Absent</b></td>
				<td><b>Discipline Assesment (Punctuality 30, Cooperation 35, Helpfulness 35)</b></td>
			</tr>
		    <?php
		    $weeks=MyCustomClass::getWeeks($month);
		    $totHourAssigned=0;
		    $totHourTaken=0;
		     $totHourLogged=0;
		    $totError=0;
		    $totKnown=0;
		    $totUnKnown=0;
		    $i=1;
		    foreach($weeks as $key=>$val)
		    {
		    	$weekarray=explode("/",$val);
		    	$stDt=$weekarray[0];
		    	$enDt=$weekarray[1];
		    	
		    	$errors=Yii::app()->db->createCommand("SELECT sum(error_known) as error_known,sum(error_unknown) as error_unknown FROM `errors` WHERE user_id='$uid' and task_date BETWEEN '$stDt' AND '$enDt'")->queryRow();


					$assigned=Yii::app()->db->createCommand("SELECT sum(tothour) as tothour FROM 
					assigned_completed_sum
	               WHERE task_date between '$stDt' and '$enDt' and user_id='$uid'")->queryRow();

					$looged=Yii::app()->db->createCommand("SELECT
sum(normal_hour+overtime_hour) as tothour from pm_actual_timeline
where
pm_actual_timeline.user_id='$uid' and
pm_actual_timeline.task_date between '$stDt' and '$enDt' ")->queryRow();

					$taken=Yii::app()->db->createCommand("SELECT sum(tothour) as tothour FROM 
					actual_completed_sum_user_task
	               WHERE task_date between '$stDt' and '$enDt' and user_id='$uid'")->queryRow();

				$totHourAssigned+=$assigned['tothour'];
				$totHourTaken+=$taken['tothour'];
				$totHourLogged+=$looged['tothour'];
				$totKnown+=$errors['error_known'];
				$totUnKnown+=$errors['error_unknown'];
				
				?>
				<tr>
					<td><b><?php echo MyCustomClass::getWeeksCount($i) . "(".$stDt.'/'.$enDt.")";?></b></td>
					<td <?php if($assigned['tothour']<=0){ echo 'style="background-color:#FF0000"'; } ?>>
						<?php echo $assigned['tothour']; ?>
						
					</td>
					<td <?php if($taken['tothour']<=0){ echo 'style="background-color:#FF0000"'; } ?>>
						<?php echo $taken['tothour']; ?>
							
					</td>
					<td <?php if($looged['tothour']<=0){ echo 'style="background-color:#FF0000"'; } ?>>
						<?php echo $looged['tothour']; ?>
							
					</td>
					<td <?php if(($errors['error_known']+$errors['error_unknown'])<=0){ echo 'style="background-color:#FF0000"'; } ?>>
					<?php echo $errors['error_known']+$errors['error_unknown']; ?>
						
					</td>
					<td <?php if($errors['error_known']<=0){ echo 'style="background-color:#FF0000"'; } ?>>
						<?php echo $errors['error_known']; ?>
							
					</td>
					<td <?php if($errors['error_unknown']<=0){ echo 'style="background-color:#FF0000"'; } ?>>
						<?php echo $errors['error_unknown']; ?>
						
					</td>
					<td><?php 
					if($assigned['tothour'] > 0)
					{
						echo MyCustomClass::roundDecimal(((($errors['error_known']+$errors['error_unknown'])/$assigned['tothour'])*100));
						echo " %"; 
					}
					
					?></td>
					<td></td>
					<td><?php 
					if($taken['tothour'] > 0)
					{
						echo MyCustomClass::roundDecimal(((($assigned['tothour'])/$taken['tothour'])*100)); 
						echo " %"; 
					}
					
					?></td>
					<td></td>
					<td>0</td>
					<td>0</td>
				</tr>
				
				<?php
				$i++;
			}
		     $end = strtotime("-1 month", $end);
		     
		     ?>
		     <tr>
				<td><b>TOTAL</b></td>
				<td><?php echo $totHourAssigned; ?></td>
				<td><?php echo $totHourTaken; ?></td>
				<td><?php echo $totHourLogged; ?></td>
				<td><?php echo $totKnown+$totUnKnown; ?></td>
				<td><?php echo $totKnown; ?></td>
				<td><?php echo $totUnKnown; ?></td>
				<td><?php 
					if($totHourAssigned > 0)
					{
						echo MyCustomClass::roundDecimal(((($totKnown+$totUnKnown)/$totHourAssigned)*100));
						echo " %";  
					}
					
				?></td>
				<td></td>
				<td><?php 
					if($totHourTaken > 0)
					{
						echo MyCustomClass::roundDecimal((($totHourAssigned/$totHourTaken)*100));
						echo " %"; 
					}
					
					?></td>
				<td></td>
				<td><b>Total=1 Days</b></td>
				<td></td>
			</tr>
			<tr>
					<td colspan="13"></td>
				</tr>
		     <?php
		}
	}
?>
</tbody>
</table>
<!--table ends-->

<script language="javascript">
	$(document).ready(function() {
              $('#myDemoTable').fixedHeaderTable({
                   altClass : 'odd',
                  footer : false,
                 // fixedColumns : 1
               
                });
   
                
            });
</script>