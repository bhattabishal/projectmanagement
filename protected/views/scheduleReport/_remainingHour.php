<?php
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl . '/dataTables/media/js/jquery.dataTables.js');
$cs->registerScriptFile($baseUrl . '/dataTables/extensions/FixedColumns/js/dataTables.fixedColumns.min.js');
$cs->registerCssFile($baseUrl . '/dataTables/media/css/jquery.dataTables.min.css');
$cs->registerCssFile($baseUrl . '/dataTables/extensions/FixedColumns/css/dataTables.fixedColumns.min.css');
$cs->registerScriptFile($baseUrl . '/htmltable_export/tableExport.js');
$cs->registerScriptFile($baseUrl . '/htmltable_export/jquery.base64.js');
?>

<script type="text/javascript" language="javascript" class="init">
//<![CDATA[
    var table;
    $(document).ready(function () {
        var table = $('#myDemoTable').dataTable(
                {
                    "dom": "<'row-fluid'r><'row-fluid't><'row-fluid'<'span6'i><'span6'p>>",
                    "scrollY": 400,
                    "scrollX": true,
                    "ordering": false,
                    "info": false,
                    'bFilter': false,
                    "scrollCollapse": true,
                    "paging": false
                });
        new $.fn.dataTable.FixedColumns(table, {
            leftColumns: 2,
            rightColumns: 1,
            heightMatch: 'auto',
        });
    });
//]]>


</script>


<!--search fields-->
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'search-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
	<table border="1" class="table-bordered">
		<tr>
                        <td>Branch</td>
        <td>

            <?php
            $rec = Branch::model()->byName()->findAll();
            $list = CHtml::listData($rec, 'branch_id', 'branch_name');

            //echo $form->dropDownList($model, 'branch_id', $list, array('empty' => 'All'));
             echo $form->dropDownList($model,'branch_id',$list,array('ajax'=>array(
                                        'type'=>'POST', //request type
									
                                        'url'=>$this->createUrl('project/dynamicProjectByBranch'), //url to call.
                                        'update'=>'#ScheduleReport_projectId',
                                        'data'=>array('branch_id'=>'js:this.value') 
                                        ),
					'style' => 'width:150px;')); 
            ?>



        </td>
			<td>Project</td>
			<td>
				
					<?php
					 $bid=0;
                                        if(isset($model->branch_id) && $model->branch_id > 0)
                                        {
                                            $bid=$model->branch_id;
                                        }
					 $rec = Project::model()->getAllBranchUsersProjectsNo1(0,0,0,$bid);
					
					echo $form->dropDownList($model,'projectId',$rec,array(
					'empty' => 'All',
					'style' => 'width:150px;'
					)); 
					?>
				
			</td>
			
			
			<td>To</td>
			<td>
				<?php
				    $this->widget('zii.widgets.jui.CJuiDatePicker',array(
					'model' => $model,
				    'attribute'=>'endDt',
				    //'flat'=>true,//remove to hide the datepicker
				    'options'=>array(
				        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
						'dateFormat' => 'yy-mm-dd',
						'showButtonPanel' => true,      // show button panel
				    ),
				    
				));
				?>
			</td>
			
			<td><input type="submit" value="Search" class="btn"/></td>
		</tr>
		
	</table>
<?php $this->endWidget(); ?>
<!--end search fields-->


<?php

$totUsers=count($projectUsers);
$lastRow='';
?>
 <a href="#"  onClick ="
        $('#myDemoTable').tableExport({
            type: 'excel',
            escape: 'false',
            //ignoreColumn: [2],
            htmlContent: 'true',
        });
   ">Export to Excel</a>
<div class="outerbox">
    <div class="innerbox">
<table class="table-striped table-bordered" id="myDemoTable" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?php echo $model->endDt; ?></th>
            <th>&nbsp;</th> 
            <th colspan="<?php echo count($projectUsers)+4; ?>"> </th>
            <th>&nbsp;</th>
            
            <th colspan="<?php echo count($projectUsers)+3; ?>">Shooting</th>
            <th>&nbsp;</th> 
        </tr>
        <tr>
            <th>Project No</th>
            <th>Title</th>
            <?php
            $lastRow='<tr><td></td><td></td>';
            $shootingUserRows='';
            foreach($projectUsers as $userId=>$userLabel)
            {
                $userarry=explode("/",$userLabel);
                echo '<th>'.$userarry[1].'</th>';
                $shootingUserRows.='<th>'.$userarry[1].'</th>';
                 $lastRow.='<td></td><td></td>';
            }
            ?>
            <th>Total</th>
            <th>WIS i.e Given By AO</th>
            <th>Allocated i.e Given By Supervisor</th>
            <th>Remain</th>
            <th>&nbsp;</th>
            <?php
                echo $shootingUserRows;
                $lastRow.='<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>';
            ?>
            <th>Total</th>
            <th>WIS i.e Given By AO</th>
            <th>Allocated i.e Given By Supervisor</th>
            <th>Remain</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($projects as $record)
        {
            $pid=$record['project_id'];
            $currentDate=$model->endDt;
            ?>
            <tr>
                <td><?php echo $record['prj_no']; ?></td>
                <td><?php echo $record['project_title']; ?></td>
                <?php
                $daytot=0;
                $shootingTotal=0;
                $shootingValueRows='';
                for($i=0;$i<$totUsers;$i++)
                {
                    $userarry=$projectUsers[$i];
                    $userarry=explode("/",$userarry);
                    $uid=$userarry[0];
                    $taskDetails=TaskSchedule::model()->getActualTakenHourByUserProjectDate($uid,$pid,$currentDate);
                  
                    echo '<td>'.$taskDetails['remaining_hour'] .'</td>';
                    $shootingValueRows.='<td>'.$taskDetails['shootinghour'].'</td>';
                    $daytot+=$taskDetails['remaining_hour'];
                    $shootingTotal+=$taskDetails['shootinghour'];
                }
                echo '<td>'.$daytot.'</td>';
                $projectDetails=Project::model()->findByPk($pid);
                $taskHour=Task::model()->getTotalTaskHourByProjectId($pid);
                ?>
                <td> <?php echo $projectDetails->wis; ?></td>
                <td> <?php echo $taskHour['tottaskhour']; ?></td>
                <td><?php echo $projectDetails->wis-$taskHour['tottaskhour']; ?></td>
                
                 <td></td>
                 <?php
                 echo $shootingValueRows;
                 echo '<td>'.$shootingTotal.'</td>';
                 ?>
                 <td> <?php echo $projectDetails->wis; ?></td>
                 <td> <?php echo $taskHour['tottaskhour']; ?></td>
                 <td><?php echo $projectDetails->wis-$taskHour['tottaskhour']; ?></td>
                 
            </tr>
            <?php
        }
        echo $lastRow;
        ?>
    </tbody>
</table>
       
        </div>
</div>