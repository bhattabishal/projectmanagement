<?php
/* @var $this ReplyController */
/* @var $model Reply */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'reply-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'inbox_id'); ?>
		<?php echo $form->textField($model,'inbox_id',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'inbox_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email_from'); ?>
		<?php echo $form->textField($model,'email_from',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'email_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email_to'); ?>
		<?php echo $form->textField($model,'email_to',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'email_to'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email_cc'); ?>
		<?php echo $form->textField($model,'email_cc',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'email_cc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'subject'); ?>
		<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'message'); ?>
		<?php echo $form->textArea($model,'message',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'message'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'crtd_by'); ?>
		<?php echo $form->textField($model,'crtd_by'); ?>
		<?php echo $form->error($model,'crtd_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'crtd_dt'); ?>
		<?php echo $form->textField($model,'crtd_dt'); ?>
		<?php echo $form->error($model,'crtd_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_by'); ?>
		<?php echo $form->textField($model,'updt_by'); ?>
		<?php echo $form->error($model,'updt_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_dt'); ?>
		<?php echo $form->textField($model,'updt_dt'); ?>
		<?php echo $form->error($model,'updt_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_cnt'); ?>
		<?php echo $form->textField($model,'updt_cnt'); ?>
		<?php echo $form->error($model,'updt_cnt'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->