<?php
/* @var $this ReplyController */
/* @var $model Reply */

$this->breadcrumbs=array(
	'Replies'=>array('index'),
	$model->reply_id=>array('view','id'=>$model->reply_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Reply', 'url'=>array('index')),
	array('label'=>'Create Reply', 'url'=>array('create')),
	array('label'=>'View Reply', 'url'=>array('view', 'id'=>$model->reply_id)),
	array('label'=>'Manage Reply', 'url'=>array('admin')),
);
?>

<h1>Update Reply <?php echo $model->reply_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>