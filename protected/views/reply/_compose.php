<?php
/* @var $this ReplyController */
/* @var $model Reply */
/* @var $form CActiveForm */
?>

<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'reply-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'htmlOptions' => array('enctype' => 'multipart/form-data'), // ADD THIS
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.For multiple recipients please seperate by comma</p>

	<?php echo $form->errorSummary($model); ?>

	

	<div class="row col2">
		<?php echo $form->labelEx($model,'email_from'); ?>
		<?php echo $form->textField($model,'email_from',array('size'=>50,'maxlength'=>50,'readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'email_from'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'email_to'); ?>
		<?php echo $form->textField($model,'email_to',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'email_to'); ?>
	</div>
	<div style="clear:both"></div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'email_cc'); ?>
		<?php echo $form->textField($model,'email_cc',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'email_cc'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'subject'); ?>
		<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>
	<div style="clear:both"></div>

	<div class="row">
		<?php echo $form->labelEx($model,'message'); ?>
		<?php echo $form->textArea($model,'message',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'message'); ?>
	</div>

	<div style="clear:both"></div>
	<?php
  $this->widget('CMultiFileUpload', array(
     'model'=>$model,
     'name'=>'files',
     'accept'=>'jpg|png|gif|pdf|zip|rar|doc|dwg|docx|xls|xlsx|ppt|pptx|jpeg|mp3',
     'options'=>array(
        // 'onFileSelect'=>'function(e, v, m){ alert("onFileSelect - "+v) }',
        // 'afterFileSelect'=>'function(e, v, m){ alert("afterFileSelect - "+v) }',
        // 'onFileAppend'=>'function(e, v, m){ alert("onFileAppend - "+v) }',
        // 'afterFileAppend'=>'function(e, v, m){ alert("afterFileAppend - "+v) }',
        // 'onFileRemove'=>'function(e, v, m){ alert("onFileRemove - "+v) }',
        // 'afterFileRemove'=>'function(e, v, m){ alert("afterFileRemove - "+v) }',
     ),
     'denied'=>'File is not allowed',
     'max'=>10, // max 10 files
 
 
  ));
?>
<div style="clear:both"></div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Send' : 'Send',array('class' => 'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<!--for tiny mce-->
	<?php $this->widget('application.extensions.tinymce.SladekTinyMce'); ?>
 

 
<script type="text/javascript">
 
    tinymce.init({
    selector: "textarea#Reply_message",
    theme: "modern",
	preformatted:true,
    width: 1010,
    height: 300,
    //plugins: [
       //  "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
        // "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        // "save table contextmenu directionality emoticons template paste textcolor"
  // ],
   content_css: "css/content.css",
   menubar:false,
   
    toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent"

   //toolbar: "bold italic | alignleft aligncenter alignright alignjustify | outdent indent | forecolor backcolor", 
   //style_formats: [
       // {title: 'Bold text', inline: 'b'},
       // {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
       // {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
       // {title: 'Example 1', inline: 'span', classes: 'example1'},
       // {title: 'Example 2', inline: 'span', classes: 'example2'},
       
    //]
 }); 
 </script>
	<!--end tinymce-->