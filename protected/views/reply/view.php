

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'email_from',
		'email_to',
		'email_cc',
		'subject',
		array(
			'name'=>'message',
			'value'=>$model->message,
			'type'=>'raw',
		),

	),
)); ?>
