<?php
/* @var $this InboxController */
/* @var $data Inbox */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('inbox_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->inbox_id), array('view', 'id'=>$data->inbox_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uid')); ?>:</b>
	<?php echo CHtml::encode($data->uid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('msg_no')); ?>:</b>
	<?php echo CHtml::encode($data->msg_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fromName')); ?>:</b>
	<?php echo CHtml::encode($data->fromName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fromAddress')); ?>:</b>
	<?php echo CHtml::encode($data->fromAddress); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mail_to')); ?>:</b>
	<?php echo CHtml::encode($data->mail_to); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mail_cc')); ?>:</b>
	<?php echo CHtml::encode($data->mail_cc); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('subject')); ?>:</b>
	<?php echo CHtml::encode($data->subject); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('message')); ?>:</b>
	<?php echo CHtml::encode($data->message); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mail_date')); ?>:</b>
	<?php echo CHtml::encode($data->mail_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_by')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_dt')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_by')); ?>:</b>
	<?php echo CHtml::encode($data->updt_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_dt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_cnt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_cnt); ?>
	<br />

	*/ ?>

</div>