
<?php
$uniqid=md5(uniqid()); 
$this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'reply-grid',
	 'type'=>'striped bordered',
	'dataProvider'=>$gridDataProvider,
	'columns'=>array(
	
	array(
            'header'=>'#',
            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        ),
		'email_from',
		'email_to',
		'email_cc',
		'subject',
		
			array(
			'class'=>'EJuiDlgsColumn',
			'template'=>'{view}',
			'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
                    ),
					
					
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'reply/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View Reply',
			 'closeOnAction' => true,
			 'refreshGridId' => 'reply-grid',
                         'hideTitleBar' => false, 
              'dialogWidth' => '80%',
        		'dialogHeight' => '300',
        ),
		),
	),
)); ?>
