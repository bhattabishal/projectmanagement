<?php

$this->widget('bootstrap.widgets.TbWizard', array(
    'type' => 'pills', // 'tabs' or 'pills'
            'options' => array(
            ),
            'tabs' => array(
                array('label' => 'Archived Mails', 'content' => $this->renderPartial('archive', array('model' => $inboxmodel), true), 'active' => true),
				
				array('label' => 'Sent Mails', 'content' => $this->renderPartial('send', array('model' => $mailmodel), true)),
		      
            ),
        )
        );
?>

