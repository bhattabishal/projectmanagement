
<?php
$uniqid=md5(uniqid()); 
$this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'inbox-grid',
	 'type'=>'striped bordered',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
	
	array(
            'header'=>'#',
            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
			'class'=>'bootstrap.widgets.TbRelationalColumn',
			'url' => $this->createUrl('inbox/loadReply'),
			'uniqid'=>$uniqid,
			'afterAjaxUpdate' => 'js:function(tr,rowid,data){
					}'
        ),
		array(
		
		'name'=>'fromName',
		'value' =>function($data,$row) {
			if($data->fromName=="")
			{
				return $data->fromAddress;
			}
			else
			{
				return $data->fromName."(".$data->fromAddress.")";
			}
			},
		),
		'mail_to',
		'subject',
		'mail_date',
		array(
			'class'=>'EJuiDlgsColumn',
			'template'=>'{view}',
			'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
						'options'=>array(
						'class'=>'inboxView'.$uniqid,
						)
                    ),
					
					
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'inbox/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View Archive',
			 'closeOnAction' => true,
			 'refreshGridId' => 'inbox-grid',
                         'hideTitleBar' => false, 
              'dialogWidth' => '80%',
        		'dialogHeight' => '300',
        ),
	
           
        ),
	),
)); ?>
