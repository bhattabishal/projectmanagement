<?php
/* @var $this ReplyController */
/* @var $model Reply */


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#reply-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'reply-grid',
	'type'=>'striped bordered',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
	
	array(
            'header'=>'#',
            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
			
        ),
	//	'reply_id',
	//	'inbox_id',
		'email_from',
		'email_to',
		'email_cc',
		'subject',
		/*
		'message',
		'crtd_by',
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
