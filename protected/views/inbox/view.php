

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
		
		'name'=>'fromName',
		'value' =>function($model) {
			if($model->fromName=="")
			{
				return $model->fromAddress;
			}
			else
			{
				return $model->fromName."(".$model->fromAddress.")";
			}
			},
		),
	
		'mail_to',
		'mail_cc',
		'subject',
		'mail_date',
		array(
			'name'=>'message',
			'value'=>$model->message,
			'type'=>'raw',
		),
	/*	array(
		'name'=>'file',
		'header'=>'Attachments',
		'value'=>function($model)
		{
			$attachments=EmailAttachment::model()->findAll("attachment_id=$model->attachment_id");
			if(count($attachments) > 0)
			{
				$placesLinks = array();
				foreach($attachments as $file)
				{
					$placesLinks[] =CHtml::link($file->file, Yii::app()->createUrl("inbox/downloadFile", array("file"=>$file->file)));
				}
				return implode(', ', $placesLinks);
			}
			else{
				return "..no attachments..";
			}
			
		}
		,
		'type'=>'raw',
		),
		
	*/
	
	),
)); ?>
