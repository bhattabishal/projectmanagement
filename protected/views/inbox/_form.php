<?php
/* @var $this InboxController */
/* @var $model Inbox */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'inbox-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'uid'); ?>
		<?php echo $form->textField($model,'uid',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'uid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'msg_no'); ?>
		<?php echo $form->textField($model,'msg_no',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'msg_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fromName'); ?>
		<?php echo $form->textField($model,'fromName',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'fromName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fromAddress'); ?>
		<?php echo $form->textField($model,'fromAddress',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'fromAddress'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mail_to'); ?>
		<?php echo $form->textField($model,'mail_to',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'mail_to'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mail_cc'); ?>
		<?php echo $form->textArea($model,'mail_cc',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'mail_cc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'subject'); ?>
		<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'message'); ?>
		<?php echo $form->textArea($model,'message',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'message'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mail_date'); ?>
		<?php echo $form->textField($model,'mail_date'); ?>
		<?php echo $form->error($model,'mail_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'crtd_by'); ?>
		<?php echo $form->textField($model,'crtd_by'); ?>
		<?php echo $form->error($model,'crtd_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'crtd_dt'); ?>
		<?php echo $form->textField($model,'crtd_dt'); ?>
		<?php echo $form->error($model,'crtd_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_by'); ?>
		<?php echo $form->textField($model,'updt_by'); ?>
		<?php echo $form->error($model,'updt_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_dt'); ?>
		<?php echo $form->textField($model,'updt_dt'); ?>
		<?php echo $form->error($model,'updt_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_cnt'); ?>
		<?php echo $form->textField($model,'updt_cnt'); ?>
		<?php echo $form->error($model,'updt_cnt'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->