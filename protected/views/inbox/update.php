<?php
/* @var $this InboxController */
/* @var $model Inbox */

$this->breadcrumbs=array(
	'Inboxes'=>array('index'),
	$model->inbox_id=>array('view','id'=>$model->inbox_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Inbox', 'url'=>array('index')),
	array('label'=>'Create Inbox', 'url'=>array('create')),
	array('label'=>'View Inbox', 'url'=>array('view', 'id'=>$model->inbox_id)),
	array('label'=>'Manage Inbox', 'url'=>array('admin')),
);
?>

<h1>Update Inbox <?php echo $model->inbox_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>