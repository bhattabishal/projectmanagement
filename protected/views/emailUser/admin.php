<?php

?>

<div class="btnalign">
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'emailUser/create',
        'dialogTitle' => 'Create User Email',
        'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New User Email',
        'closeButtonText' => 'Close',
		'id' => 'newClient',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
		//'renderOpenButton' => false,
        'closeOnAction' =>true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'email-user-grid', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
?>
</div>



<?php $this->widget('bootstrap.widgets.TbExtendedGridView',array(
	'id'=>'email-user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => 'striped bordered',
	'columns'=>array(
	
		'user.user_name',
		'email_id',
            /*
		array(
			'name' => 'password',
			  'header' => 'Password',
			  'value' =>function($data,$row) {		  		
						return MyCustomClass::decrypt($data->password);
			  	} ,
			  'filter'=>array(2=>'RPS', 1=>'AO',0=>'Client'),
			  'type' => 'raw',

		),
		
		/*
		'updt_by',
		'updt_dt',
		'updt_cnt',
		*/
			array(
            'class'=>'EJuiDlgsColumn',
			'htmlOptions' => array('style'=>'width:60px'),

			'deleteButtonUrl'=>'Yii::app()->createUrl("/emailUser/delete", array("id" => $data->email_user_id))',
			
           	//'updateButtonImageUrl'=>Yii::app()->baseUrl .'images/viewdetaildialog.png',
            'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
                    ),
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'emailUser/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View User Email',
			 'closeOnAction' => true,
			 'refreshGridId' => 'client-grid',
                         'hideTitleBar' => false, 
              'dialogWidth' => '80%',
        		'dialogHeight' => '300',
        ),
		'updateDialog'=>array(
             'controllerRoute' => 'emailUser/update', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update Email User',
			 'closeOnAction' => true,
			  'refreshGridId' => 'client-grid',
			  'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
             //'dialogWidth' => 800, //use the value from the dialog config
             //'dialogHeight' => 600,
        ),
		
           
        ),
	),
)); ?>
