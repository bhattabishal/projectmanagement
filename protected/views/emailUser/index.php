<?php
/* @var $this EmailUserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Email Users',
);

$this->menu=array(
	array('label'=>'Create EmailUser', 'url'=>array('create')),
	array('label'=>'Manage EmailUser', 'url'=>array('admin')),
);
?>

<h1>Email Users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
