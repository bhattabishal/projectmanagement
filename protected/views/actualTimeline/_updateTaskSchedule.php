

<div class="form wide" >
    <div style="width:80%; float: left;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'actual-timeline-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>
<!-- <?php
 //print_r(schedule);
?>-->
	<div class="row col2">
		<?php echo $form->labelEx($model,'task_id'); ?>
		<?php  
                        $disabled="disabled";
			echo $form->textField($model,'task_id',array('disabled'=>'disabled')); 
		?>
		<?php echo $form->error($model,'task_id'); ?>
	</div>

	
   <div class="row col2">
		<?php
		$records = User::model()->getRpsList();
		?>
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php 
		echo $form->dropDownList($model,'user_id',$records,array('empty' => 'Select user'));  
		
		?>
		
		<?php echo $form->error($model,'user_id'); ?>
	</div>
	
<div class="row col2">
		<?php echo $form->labelEx($model,'task_date'); ?>
		<?php 
		
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $model,
		    'attribute'=>'task_date',
			
		    //'flat'=>true,//remove to hide the datepicker
		    'options'=>array(
		        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
				'dateFormat' => 'yy-mm-dd',
				'showButtonPanel' => true,      // show button panel
		    ),
		    'htmlOptions'=>array(
		        'style'=>''
		    ),
		));
		 ?>
		<?php echo $form->error($model,'task_date'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'percentage_completed'); ?>
		<?php echo $form->textField($model,'percentage_completed'); ?>
		<?php echo $form->error($model,'percentage_completed'); ?>
	</div>

        <div class="row col2">
		<?php echo $form->labelEx($model,'normal_hour'); ?>
		<?php echo $form->textField($model,'normal_hour'); ?>
		<?php echo $form->error($model,'normal_hour'); ?>
	</div>


	<div class="row col2">
		<?php echo $form->labelEx($model,'overtime_hour'); ?>
		<?php echo $form->textField($model,'overtime_hour'); ?>
		<?php echo $form->error($model,'overtime_hour'); ?>
	</div>

        <div class="row col2">
		<?php echo $form->labelEx($model,'error_known'); ?>
		<?php echo $form->textField($model,'error_known'); ?>
		<?php echo $form->error($model,'error_known'); ?>
	</div>
        <div class="row col2">
		<?php echo $form->labelEx($model,'error_unknown'); ?>
		<?php echo $form->textField($model,'error_unknown'); ?>
		<?php echo $form->error($model,'error_unknown'); ?>
	</div>
        <div class="row col2">
		<?php echo $form->labelEx($model,'remarks'); ?>
		<?php echo $form->textArea($model,'remarks',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'remarks'); ?>
	</div>

    
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div>
        
        
<!--
old details
-->
<div style="width:20%; float: left;">
    
    <div>Old Details</div>
  <table style="border: 2px solid #000;">
        <tr>
      <th>Attribute</th> <th>Details</th>
       </tr>
        <tr>
                    <td>Assigned To:</td>
                    <td><?php echo $taskmodel->user->user_name;?></td>
              
         </tr>
         <tr>
                    <td>Total Task Hour</td>
                    <td><?php echo $taskmodel->est_rps_cost;?></td>
              
         </tr>
         <tr>
                    <td>Total % completed</td>
                    <td><?php echo $taskmodel->done_percentage;?></td>
              
         </tr>
    
    </table>
    <div>
        <div>Old Schedules</div>
        <div>
            <table style="border: 2px solid #000; column-span: 2px;">
                <tr>
                    <th>S.N.</th>
                    <th>Date</th>
                    <th>Normal Hours</th>
                    <th>Ot Hours</th>
                </tr>
            <?php
            if(count($oldSchedule) > 0)
            {
                $i=1;
                foreach($oldSchedule as $row)
                {
                    ?>
                   <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['schedule_date']; ?></td>
                    <td><?php echo $row['normal_hour']; ?></td>
                    <td><?php echo $row['overtime_hour'];?></td>
                </tr>
                <?php
                $i++;
                }
            }
            ?>
                </table>
        </div>   
    </div>
    
    
</div>
<!-- end old details -->
       
</div><!-- form -->




<style>
    table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
 th {
    border: 1px solid black;
   color: #009;
}
    
</style>


