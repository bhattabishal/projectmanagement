<?php
/* @var $this ActualTimelineController */
/* @var $data ActualTimeline */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('timeline_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->timeline_id), array('view', 'id'=>$data->timeline_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('task_id')); ?>:</b>
	<?php echo CHtml::encode($data->task_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('normal_hour')); ?>:</b>
	<?php echo CHtml::encode($data->normal_hour); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cusion_hour')); ?>:</b>
	<?php echo CHtml::encode($data->cusion_hour); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('overtime_hour')); ?>:</b>
	<?php echo CHtml::encode($data->overtime_hour); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('task_date')); ?>:</b>
	<?php echo CHtml::encode($data->task_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remarks')); ?>:</b>
	<?php echo CHtml::encode($data->remarks); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_by')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_dt')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_by')); ?>:</b>
	<?php echo CHtml::encode($data->updt_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_dt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_cnt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_cnt); ?>
	<br />

	*/ ?>

</div>