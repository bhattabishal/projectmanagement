<div class="btnalign">
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'actualTimeline/create',
        'dialogTitle' => 'Create Time Line',
        'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New Timeline',
        'closeButtonText' => 'Close',
		'id' => 'newTimeLine',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
		//'renderOpenButton' => false,
        'closeOnAction' =>true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'actual-timeline-grid', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
?>
</div>

<?php 
$uniqid=md5(uniqid());
$this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'actual-timeline-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type'=>'striped bordered',
	'columns'=>array(
		array(
            'header'=>'#',
            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        ),
		'task_date',
		array(
		'name'=>'taskCompositeNo',
		'header'=>'Project No',
		'value'=>'$data->getTaskCompositeNo()',
		),
		'task_id',
		array(
			'header'=>'User',
			'name'=>'nameText',
			'value'=>'$data->user->user_name',
		),
		
		array(
			'header'=>'Assigned Hour',
			'value'=>'$data->task->est_rps_cost',
		),
		'normal_hour',
		//'cusion_hour',
		'overtime_hour',
		'percentage_completed',
		//'task.error_known',
		 array(
			'class' => 'bootstrap.widgets.MyTimelineErrorEditableColumn',
			'name' => 'error_known',
			'value' =>'$data->task->error_known',
			//'type'=>'raw',
			'header'=>'Error Known',
			'headerHtmlOptions' => array('style' => 'width: 110px'),
			'editable' => array(
			 	'type' => 'text',
				'url' => $this->createUrl('actualTimeline/updateErrors'),
				'placement' => 'left',
			)
			),
		//'task.error_unknown',
		array(
			'class' => 'bootstrap.widgets.MyTimelineErrorEditableColumn',
			'name' => 'error_unknown',
			'value' =>'$data->task->error_unknown',
			//'type'=>'raw',
			'header'=>'Error UnKnown',
			'headerHtmlOptions' => array('style' => 'width: 110px'),
			'editable' => array(
			 	'type' => 'text',
				'url' => $this->createUrl('actualTimeline/updateErrors'),
				'placement' => 'left',
			)
			),
		 array(
			'class' => 'bootstrap.widgets.MyTimelineAuditedEditableColumn',
			'name' => 'audited',
			'value' =>'$data->audited',
			'type'=>'raw',
			'header'=>'Audited',
			'filter'=>array('Yes'=>'Yes','No'=>'No'),
			'headerHtmlOptions' => array('style' => 'width:150px'),
		'editable' => array(
		'type' => 'select',
		'source' => $this->createUrl('actualTimeline/getAuditedValues'),
		'url' =>  $this->createUrl('actualTimeline/updateAuditedValues'),
		)
			),
		/*
		'remarks',
		'crtd_by',
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
		*/
		array(
            'class'=>'EJuiDlgsColumn',
            'template'=>'{view}{update}',
			'htmlOptions' => array('style'=>'width:56px'),
			'deleteButtonUrl'=>'Yii::app()->createUrl("/actualTimeline/delete", array("id" => $data->timeline_id))',
			 'buttons'=>array(
			 			
			 			
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
						'options'=>array(
						'class'=>'taskView'.$uniqid,
						)
                    ),
					'update'=>
                             array(
                        'options'=>array(
						'class'=>'taskUpdate'.$uniqid,
						)
                    ),
					'delete'=>
                             array(
                        'options'=>array(
						'class'=>'taskDelete'.$uniqid,
						)
                    ),
					
					
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'actualTimeline/view', //=default
             'actionParams' => array('id' => '$data->timeline_id'), //=default
             'dialogTitle' => 'View Timeline',
			  'closeOnAction' => true,
			 'refreshGridId' => 'actual-timeline-grid',
                         'hideTitleBar' => false, 
             'dialogWidth' => '80%',
        	 'dialogHeight' => '300',
        ),
		'updateDialog'=>array(
		//'controllerRoute' => 'task/updateTaskOnly', //=default
		'controllerRoute' => 'actualTimeline/update', //=default
            'actionParams' => array('id' => '$data->timeline_id'), //=default
             'dialogTitle' => 'Update Timeline',
			  'closeOnAction' => true,
			 'refreshGridId' => 'actual-timeline-grid',
			 'dialogWidth' => '97%',
        		'dialogHeight' => '600',
                         'hideTitleBar' => false,
						  'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
		),
		
		
		
			),
	),
)); ?>
