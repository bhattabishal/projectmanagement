<?php

$this->widget('bootstrap.widgets.TbWizard', array(
    'type' => 'pills', // 'tabs' or 'pills'
            'options' => array(
            ),
            'tabs' => array(
                array('label' => 'Actual Timeline', 'content' => $this->renderPartial('actual_form', array('model' => $actualmodel), true), 'active' => true),
				
				array('label' => 'Fixed Timeline', 'content' => $this->renderPartial('fixed_form', array('model' => $fixedmodel), true)),
		      
            ),
        )
        );
?>

