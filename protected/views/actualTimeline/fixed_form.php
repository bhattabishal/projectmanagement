<div class="btnalign">
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'fixedTaskTimelineBrief/create',
        'dialogTitle' => 'Create Time Line',
        'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New Admin Timeline',
        'closeButtonText' => 'Close',
		'id' => 'fixed-task-timeline-brief-grid',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
		//'renderOpenButton' => false,
        'closeOnAction' =>true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'fixed-task-timeline-brief-grid', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
?>
</div>

<?php 
$uniqid=md5(uniqid());
$this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'fixed-task-timeline-brief-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type'=>'striped bordered',
	'columns'=>array(
		array(
            'header'=>'#',
            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
			
        ),
       
        array(
		'name' =>'project_search',
		'header' =>'Project',
		'value'=>'$data->project->title',
	
		),
		'entry_dt',
		array(
		'name' =>'user_search',
		'header' =>'User',
		'value'=>'$data->user->user_name',
	
		),
		'nt_hours',
		'ot_hours',
		
		/*
		'crtd_by',
		'updt_dt',
		'updt_by',
		'updt_cnt',
		*/
		
		array(
            'class'=>'EJuiDlgsColumn',
            'template'=>'{view}{update}{delete}',
			'htmlOptions' => array('style'=>'width:56px'),
			'deleteButtonUrl'=>'Yii::app()->createUrl("/fixedTaskTimeline/delete", array("id" => $data->fxt_id))',
			 'buttons'=>array(
			 			
			 			
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
						'options'=>array(
						'class'=>'fixedTaskView',
						)
                    ),
					'update'=>
                             array(
                        'options'=>array(
						'class'=>'fixedTaskUpdate',
						)
                    ),
					'delete'=>
                             array(
                        'options'=>array(
						'class'=>'fixedTask'.$uniqid,
						)
                    ),
					
					
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'fixedTaskTimeline/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View Timeline',
			  'closeOnAction' => true,
			 'refreshGridId' => 'fixed-task-timeline-brief-grid',
                         'hideTitleBar' => false, 
             'dialogWidth' => '80%',
        	 'dialogHeight' => '300',
        ),
		'updateDialog'=>array(
		//'controllerRoute' => 'task/updateTaskOnly', //=default
		'controllerRoute' => 'fixedTaskTimeline/update', //=default
            'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update Timeline',
			  'closeOnAction' => true,
			 'refreshGridId' => 'fixed-task-timeline-brief-grid',
			 'dialogWidth' => '97%',
        		'dialogHeight' => '600',
                         'hideTitleBar' => false,
						  'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
		),
		
		
		
			),
		
	),
)); ?>
