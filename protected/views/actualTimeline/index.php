<?php
/* @var $this ActualTimelineController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Actual Timelines',
);

$this->menu=array(
	array('label'=>'Create ActualTimeline', 'url'=>array('create')),
	array('label'=>'Manage ActualTimeline', 'url'=>array('admin')),
);
?>

<h1>Actual Timelines</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
