<?php
/* @var $this ActualTimelineController */
/* @var $model ActualTimeline */
/* @var $form CActiveForm */
//echo $dateDifference=MyCustomClass::GetDateDifference('2014-07-09');
?>

<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'actual-timeline-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row col2">
		<?php echo $form->labelEx($model,'task_date'); ?>
		<?php 
                
                 if($model->isNewRecord==FALSE)
                 {
                     echo $form->textField($model,'task_date',array('readonly'=>'readonly'));
                 }
                 else
                 {
                     $this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $model,
		    'attribute'=>'task_date',
		    //'flat'=>true,//remove to hide the datepicker
		    'options'=>array(
		        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
				'dateFormat' => 'yy-mm-dd',
				'showButtonPanel' => true,      // show button panel
		    ),
		    'htmlOptions'=>array(
		        'style'=>''
		    ),
		));
                 }
		
	  ?>
		<?php echo $form->error($model,'task_date'); ?>
	</div>
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'task_id'); ?>
		<?php
                       
                        if($model->isNewRecord==FALSE)
                        {
                            if(User::model()->checkUserIfCoordinator(Yii::app()->user->id) || Yii::app()->user->checkAccess('viewAllTask'))
                            {
                                 $records = Task::model()->getUserAssignedAllTaskList(0);
                            }
                            else
                            {
                                $records = Task::model()->getUserAssignedAllTaskList(Yii::app()->user->id);
                            }
                          
                            
                            echo $form->dropDownList($model,'task_id',$records,array('empty' => 'Select task',"disabled"=>"disabled"));
                       
                            
                        }
                        else
                        {
                            $records = Task::model()->getUserAssignedActiveTaskList(Yii::app()->user->id);
                            echo $form->dropDownList($model,'task_id',$records,array('empty' => 'Select task'));
                       
                        }
			 
			//$records = Task::model()->getUserAssignedActiveTaskList(0); 
			
		?>
		<?php echo $form->error($model,'task_id'); ?>
	</div>

	<div style="clear:both"></div>
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'normal_hour'); ?>
		<?php echo $form->textField($model,'normal_hour',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'normal_hour'); ?>
	</div>

	<!--<div class="row col2">
		<?php //echo $form->labelEx($model,'cusion_hour'); ?>
		<?php //echo $form->textField($model,'cusion_hour',array('size'=>5,'maxlength'=>5)); ?>
		<?php //echo $form->error($model,'cusion_hour'); ?>
	</div>-->

	
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'overtime_hour'); ?>
		<?php echo $form->textField($model,'overtime_hour',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'overtime_hour'); ?>
	</div>

	<div style="clear:both"></div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'percentage_completed'); ?>
		<?php 
			 if($model->isNewRecord==FALSE)
                         {
                             echo $form->textField($model,'percentage_completed',array('size'=>5,'maxlength'=>5,'readonly'=>'readonly'));
                         }
                         else
                         {
                            echo $form->textField($model,'percentage_completed',array('size'=>5,'maxlength'=>5)); 
                         }
			 
		?>
		<?php echo $form->error($model,'percentage_completed'); ?>
	</div>
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'error_known'); ?>
		<?php 
			$att="disabled";
			if($model->percentage_completed=='100')
			{
				$att="";
			}
			echo $form->textField($model,'error_known',array('size'=>5,'maxlength'=>5,'disabled'=>$att)); 
		?>
		<?php echo $form->error($model,'error_known'); ?>
	</div>
	
	<div style="clear:both"></div>
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'error_unknown'); ?>
		<?php 
		
		echo $form->textField($model,'error_unknown',array('size'=>5,'maxlength'=>5,'disabled'=>$att)); 
		?>
		<?php echo $form->error($model,'error_unknown'); ?>
	</div>
	
	<div  class="row col2">
		<?php echo $form->labelEx($model,'isAlert'); ?>
		<?php 
			$checked=false;
			
				if($model->isAlert=="1")
				{
					$checked=true;
				}
			
			echo $form->checkBox($model,'isAlert',array('checked'=>$checked,'value'=>1)); 
		?>
		<?php echo $form->error($model,'isAlert'); ?>
	</div>

	<div style="clear:both"></div>
	<div class="row col2">
		<?php echo $form->labelEx($model,'remarks'); ?>
		<?php echo $form->textArea($model,'remarks',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'remarks'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	$( "#ActualTimeline_percentage_completed" ).keyup(function() {
		var valuePercentage=$(this).val();
		if(valuePercentage == 100)
		{
			$("#ActualTimeline_error_known").removeAttr('disabled');
			$("#ActualTimeline_error_unknown").removeAttr('disabled');
		}
		else
		{
			$("#ActualTimeline_error_known").attr('disabled','disabled');
			$("#ActualTimeline_error_unknown").attr('disabled','disabled');
		}
	});
</script>