<?php
/* @var $this CodeValueController */
/* @var $model CodeValue */



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#code-value-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<div class="btnalign">
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'codeValue/create',
		'actionParams' => array('coid'=>$coid),
        'dialogTitle' => 'Create Code Value',
        'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New Code Value',
        'closeButtonText' => 'Close',
		'id' => 'newCodeValue',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
		//'renderOpenButton' => false,
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'codeValue-form-grid', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
?>
</div>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'code-value-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => 'striped bordered',
	'columns'=>array(
		array(
            'header'=>'#',
            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		   // 'code_master_id',
		 
			
        ),
		
	//	'code_id',
		'code_lbl',
	//	'code_type',
	//	'order',
	//	'crtd_by',
	//	'crtd_dt',
		/*
		'updt_by',
		'updt_dt',
		'updt_cnt',
		*/
		array(
		
          'class'=>'EJuiDlgsColumn',
			'template'=>'{update}',
			'buttons'=>array(
                        'update'=>
                             array(
                        'label'=>'ajax dialog update',
						'options'=>array(
						'class'=>'codeValueUpdate'.$uniqid,
						)
                    ),
					),
		'updateDialog'=>array(
             'controllerRoute' => 'codeValue/update', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update Code Leble',
			 'closeOnAction' => true,
			  'refreshGridId' => 'codeValueMaster-form-grid',
			  'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
          
        ),
		
           
        ),
	),
)); ?>
