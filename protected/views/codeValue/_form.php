<?php
/* @var $this CodeValueController */
/* @var $model CodeValue */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'code-value-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php echo $form->errorSummary($model); ?>
	
<div class="row">
		<?php echo $form->labelEx($model,'code_type'); ?>
		
		<?php  echo $form->textField($model,'code_type',array('size'=>60,'maxlength'=>255,'disabled'=>'true')); ?>
		<?php  echo $form->error($model,'code_type'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'code_lbl'); ?>
		<?php echo $form->textField($model,'code_lbl',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'code_lbl'); ?>
	</div>
	
	
	<div class="row">
<?php
	$data=true;
	if($model->value_type=="entry")
	{
	$data=false;
	}
	?>
		<?php echo $form->labelEx($model,'value'); ?>
		<?php echo $form->textField($model,'value',array('disabled'=>$data)); ?>
		<?php echo $form->error($model,'value'); ?>
		
	</div>
		
	
<!--
	<div class="row">
		<?php echo $form->labelEx($model,'order'); ?>
		<?php echo $form->textField($model,'order'); ?>
		<?php echo $form->error($model,'order'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'crtd_by'); ?>
		<?php echo $form->textField($model,'crtd_by'); ?>
		<?php echo $form->error($model,'crtd_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'crtd_dt'); ?>
		<?php echo $form->textField($model,'crtd_dt'); ?>
		<?php echo $form->error($model,'crtd_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_by'); ?>
		<?php echo $form->textField($model,'updt_by'); ?>
		<?php echo $form->error($model,'updt_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_dt'); ?>
		<?php echo $form->textField($model,'updt_dt'); ?>
		<?php echo $form->error($model,'updt_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_cnt'); ?>
		<?php echo $form->textField($model,'updt_cnt'); ?>
		<?php echo $form->error($model,'updt_cnt'); ?>
	</div>-->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->