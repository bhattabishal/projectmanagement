<div class="headingalign">
<?php
$uniqid=md5(uniqid());
//echo CHtml::tag('h3',array(),'RELATIONAL DATA EXAMPLE ROW : "'.$id.'"');
echo CHtml::tag('h6',array(),'Code Value Details');
?>
</div>

<div class="btnalign">
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'codeValue/create',
		'actionParams' => array('coid'=>$codetype),
        'dialogTitle' => 'Create Code Value',
        'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New Code Value',
        'closeButtonText' => 'Close',
		'id' => 'newCodeValue',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
		//'renderOpenButton' => false,
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'codeValue-form-grid', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
?>
</div>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'code-value-grid',
	//'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => 'striped bordered',
	'dataProvider' => $gridDataProvider,
     'enableSorting' => false,
     'enablePagination'=>false,
	'columns'=>array(
		array(
            'header'=>'#',
            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		   // 'code_master_id',
		 
			
        ),
		
	//	'code_id',
		'code_lbl',
		'code_type',
	//	'order',
	//	'crtd_by',
	//	'crtd_dt',
		/*
		'updt_by',
		'updt_dt',
		'updt_cnt',
		*/
		array(
		
          'class'=>'EJuiDlgsColumn',
			'template'=>'{update}',
			'buttons'=>array(
                        'update'=>
                             array(
                        'label'=>'ajax dialog update',
						'options'=>array(
						'class'=>'codeValueUpdate'.$uniqid,
						)
                    ),
					),
		'updateDialog'=>array(
             'controllerRoute' => 'codeValue/update', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update Code Label',
			 'closeOnAction' => true,
			  'refreshGridId' => 'codeValueMaster-form-grid',
			  'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
          
        ),
		
           
        ),
	),
)); ?>
