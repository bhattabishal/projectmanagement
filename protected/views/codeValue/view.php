<?php
/* @var $this CodeValueController */
/* @var $model CodeValue */

$this->breadcrumbs=array(
	'Code Values'=>array('index'),
	$model->code_id,
);

$this->menu=array(
	array('label'=>'List CodeValue', 'url'=>array('index')),
	array('label'=>'Create CodeValue', 'url'=>array('create')),
	array('label'=>'Update CodeValue', 'url'=>array('update', 'id'=>$model->code_id)),
	array('label'=>'Delete CodeValue', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->code_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CodeValue', 'url'=>array('admin')),
);
?>

<h1>View CodeValue #<?php echo $model->code_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'code_id',
		'code_lbl',
		'code_type',
		'order',
		'crtd_by',
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
	),
)); ?>
