<?php
/* @var $this CodeValueController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Code Values',
);

$this->menu=array(
	array('label'=>'Create CodeValue', 'url'=>array('create')),
	array('label'=>'Manage CodeValue', 'url'=>array('admin')),
);
?>

<h1>Code Values</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
