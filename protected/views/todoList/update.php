<?php
/* @var $this PmTodoListController */
/* @var $model PmTodoList */

$this->breadcrumbs=array(
	'Pm Todo Lists'=>array('index'),
	$model->to_do_id=>array('view','id'=>$model->to_do_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PmTodoList', 'url'=>array('index')),
	array('label'=>'Create PmTodoList', 'url'=>array('create')),
	array('label'=>'View PmTodoList', 'url'=>array('view', 'id'=>$model->to_do_id)),
	array('label'=>'Manage PmTodoList', 'url'=>array('admin')),
);
?>



<?php $this->renderPartial('_form', array('model'=>$model)); ?>