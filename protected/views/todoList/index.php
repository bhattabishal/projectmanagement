<?php
/* @var $this PmTodoListController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pm Todo Lists',
);

$this->menu=array(
	array('label'=>'Create PmTodoList', 'url'=>array('create')),
	array('label'=>'Manage PmTodoList', 'url'=>array('admin')),
);
?>

<h1>Pm Todo Lists</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
