


<div class="btnalign">
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'todoList/create',
        'dialogTitle' => 'Create ToDoList',
        'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New ToDoList',
        'closeButtonText' => 'Close',
		'id' => 'newTodolist',

		'openButtonHtmlOptions'=>array('class'=>'btn',),
		//'renderOpenButton' => false,
        'closeOnAction' =>true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'todo-list-grid', //the grid with this id will be refreshed after closing
		 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
    )
);
?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'todo-list-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => 'striped bordered',
	
	 'rowHtmlOptionsExpression'=>'array("style"=>"background-color:{$data->setRowColor()}","class"=>"self")',
	'columns'=>array(
			//'user_id',
		array(
            'header'=>'#',
			
            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
			
        ),
	
	array(
				'name'=>'user_id',
				'header' => 'Assigned To',
				'value'=> function($data,$row){
					$id=Yii::app()->user->id;
	               if($data->user_id != $id)
	               {
				   		return $data->pmUsers->user_name;
				   }
				   else
				   {
				   	return "-";
				   }
	            },	
	),
	
	array(
				'name'=>'from_id',
				'header' => 'Assigned From',
				'value'=> function($data,$row){
					$id=Yii::app()->user->id;
	               if($data->from_id != $id)
	               {
				   		return $data->fromUsers->user_name;
				   }
				   else
				   {
				   	return "-";
				   }
	            },	
	),
		
			
	//	'task',
	array(
	'name'=>'task',
	'headerHtmlOptions' => array('style' => 'width: 500px'),
	
	//	'class' => 'bootstrap.widgets.TbEditableColumn',
	),
	//	'date_time',
		array(
		
		'name' =>'date_time',
		'headerHtmlOptions' => array('style' => 'width: 100px'),
   //	'class' => 'bootstrap.widgets.TbEditableColumn',
		
		),
		
		array(
		
		'name' =>'actual_date',
		'headerHtmlOptions' => array('style' => 'width: 100px'),
   //	'class' => 'bootstrap.widgets.TbEditableColumn',
		
		),
	
		
		array(
		'name' => 'status_search',
		'visible'=>5,
		'org_name' =>'status',
		'value'=> '$data->taskStatus->code_lbl',
		'header' => 'Status',
		'filter'=>CodeValue::model()->getTextValuesArryByType('status'),
		'class' => 'bootstrap.widgets.MyStatusTbEditableColumn',
		
		'headerHtmlOptions' => array('style' => 'width:50px'),
		'editable' => array(
		'type' => 'select',
		'source' => $this->createUrl('todoList/getTaskStatusValues'),
		'url' =>  $this->createUrl('todoList/editTaskStatus'),
		)
		
		),
		
		
	//	'crtd_by',
	//	'crtd_dt',
	//	'remarks',
		
		/*
		'updt_by',
		'updt_dt',
		'updt_cnt',
		*/
	array(
            'class'=>'EJuiDlgsColumn',
			'htmlOptions' => array('style'=>'width:66px'),

			'deleteButtonUrl'=>'Yii::app()->createUrl("/todoList/delete", array("id" => $data->to_do_id))',
			
           	//'updateButtonImageUrl'=>Yii::app()->baseUrl .'images/viewdetaildialog.png',
      'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
                    ),
					
					
                            ),
	 
	   'viewDialog'=>array(
             'controllerRoute' => 'todoList/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View TodoList',
			 'closeOnAction' => true,
			 'refreshGridId' => 'todo-list-grid',
                         'hideTitleBar' => false, 
              'dialogWidth' => '80%',
        		'dialogHeight' => '300',
        ),
		   
		'updateDialog'=>array(
             'controllerRoute' => 'todoList/update', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update TodoList',
			 'closeOnAction' => true,
			  'refreshGridId' => 'todo-list-grid',
			  'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
             //'dialogWidth' => 800, //use the value from the dialog config
             //'dialogHeight' => 600,
        ),
		
           
        ),
	),
)); ?>
