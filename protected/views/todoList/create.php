<?php
/* @var $this PmTodoListController */
/* @var $model PmTodoList */

$this->breadcrumbs=array(
	'Pm Todo Lists'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PmTodoList', 'url'=>array('index')),
	array('label'=>'Manage PmTodoList', 'url'=>array('admin')),
);
?>



<?php $this->renderPartial('_form', array('model'=>$model)); ?>