<?php
/* @var $this PmTodoListController */
/* @var $model PmTodoList */
/* @var $form CActiveForm */
?>

<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pm-todo-list-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	

	<?php echo $form->errorSummary($model); ?>

	<div class="row col2">
	<?php
		$records = CHtml::listData(User::model()->findAll(),'user_id','user_name');
		?>
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->dropDownList($model,'user_id',$records,array('empty' => 'Select user')); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>
	
	<div class="row col2">
		
		
		<?php echo $form->labelEx($model,'date_time'); ?>
		<?php 
		
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $model,
		    'attribute'=>'date_time',
		   // 'flat'=>true,//remove to hide the datepicker
		    'options'=>array(
		        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
				'dateFormat' => 'yy-mm-dd',
				'showButtonPanel' => true,      // show button panel
		    ),
		    'htmlOptions'=>array(
		        'style'=>''
		    ),
		));
		
	  ?>
		<?php echo $form->error($model,'date_time'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'task'); ?>
		<?php echo $form->textArea($model,'task',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'task'); ?>
	</div>

	
	<?php
/*
	<div class="row">
		<?php echo $form->labelEx($model,'crtd_by'); ?>
		<?php echo $form->textField($model,'crtd_by'); ?>
		<?php echo $form->error($model,'crtd_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'crtd_dt'); ?>
		<?php echo $form->textField($model,'crtd_dt'); ?>
		<?php echo $form->error($model,'crtd_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_by'); ?>
		<?php echo $form->textField($model,'updt_by'); ?>
		<?php echo $form->error($model,'updt_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_dt'); ?>
		<?php echo $form->textField($model,'updt_dt'); ?>
		<?php echo $form->error($model,'updt_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_cnt'); ?>
		<?php echo $form->textField($model,'updt_cnt'); ?>
		<?php echo $form->error($model,'updt_cnt'); ?>
	</div>
	*/?>
	<div class="row col2">
		<?php echo $form->labelEx($model,'remarks'); ?>
		<?php echo $form->textArea($model,'remarks',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'remarks'); ?>
	</div>
	
		<div class="row col2">
		<?php
		$records = CHtml::listData(CodeValue::model()->findAll("code_type= 'status'"), 'code_id', 'code_lbl');
		?>
		<?php echo $form->labelEx($model,'status'); ?>
	<?php echo $form->dropDownList($model,'status',$records,array('empty' => 'Select status')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>
	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->