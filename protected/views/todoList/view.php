<?php
/* @var $this PmTodoListController */
/* @var $model PmTodoList */

$this->breadcrumbs=array(
	'Pm Todo Lists'=>array('index'),
	$model->to_do_id,
);

$this->menu=array(
	array('label'=>'List PmTodoList', 'url'=>array('index')),
	array('label'=>'Create PmTodoList', 'url'=>array('create')),
	array('label'=>'Update PmTodoList', 'url'=>array('update', 'id'=>$model->to_do_id)),
//	array('label'=>'Delete PmTodoList', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->to_do_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PmTodoList', 'url'=>array('admin')),
);
?>



<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'to_do_id',
		'pmUsers.username',
		'task',
		'date_time',
		'taskStatus.code_lbl',
		'crtd_by',
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
		'remarks',
	
	),
)); ?>
