<?php
/* @var $this ShootingFactorController */
/* @var $model ShootingFactor */
/* @var $form CActiveForm */
?>

<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'shooting-factor-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row col2">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php 
		$records =User::model()->getAoRPSList();
		
		echo $form->dropDownList($model,'user_id',$records,array('empty' => 'Select User'));
		?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'shooting_factor'); ?>
		<?php echo $form->textField($model,'shooting_factor',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'shooting_factor'); ?>
	</div>
	
	
	
	
	<div class="row col2">
		<?php echo $form->labelEx($model,'shooting_year_month'); ?>
		<?php 
		$records =MyCustomClass::getMonthsYearList();
		
		echo $form->dropDownList($model,'shooting_year_month',$records,array('empty' => 'Select Month'));
		?>
		<?php echo $form->error($model,'shooting_year_month'); ?>
	</div>

	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->