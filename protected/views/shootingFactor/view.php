<?php
/* @var $this ShootingFactorController */
/* @var $model ShootingFactor */

$this->breadcrumbs=array(
	'Shooting Factors'=>array('index'),
	$model->shooting_id,
);

$this->menu=array(
	array('label'=>'List ShootingFactor', 'url'=>array('index')),
	array('label'=>'Create ShootingFactor', 'url'=>array('create')),
	array('label'=>'Update ShootingFactor', 'url'=>array('update', 'id'=>$model->shooting_id)),
	array('label'=>'Delete ShootingFactor', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->shooting_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ShootingFactor', 'url'=>array('admin')),
);
?>

<h1>View ShootingFactor #<?php echo $model->shooting_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'shooting_id',
		'user_id',
		'shooting_factor',
		'crtd_by',
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
	),
)); ?>
