<?php
/* @var $this ShootingFactorController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Shooting Factors',
);

$this->menu=array(
	array('label'=>'Create ShootingFactor', 'url'=>array('create')),
	array('label'=>'Manage ShootingFactor', 'url'=>array('admin')),
);
?>

<h1>Shooting Factors</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
