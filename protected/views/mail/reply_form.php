<div id="replyform">
<?php echo $form->errorSummary($replyModel); ?>
	<div class="row" style="margin-left: 0">
		<?php echo $form->hiddenField($replyModel, 'uid'); ?>
		<?php echo $form->hiddenField($replyModel, 'msg_no'); ?>
		<?php echo $form->hiddenField($replyModel, 'fromName'); ?>
		<?php echo $form->hiddenField($replyModel, 'fromAddress'); ?>
		<?php echo $form->hiddenField($replyModel, 'mail_to'); ?>
		<?php echo $form->hiddenField($replyModel, 'mail_cc'); ?>
		<?php echo $form->hiddenField($replyModel, 'mainSubject'); ?>
		<?php echo $form->hiddenField($replyModel, 'mainMessage'); ?>
		<?php echo $form->hiddenField($replyModel, 'mail_date'); ?>
		
		
        <?php echo $form->labelEx($replyModel,'email_from'); ?>
        <?php echo $form->textField($replyModel,'email_from',array('readonly'=>'readonly')); ?>
        <?php echo $form->error($replyModel,'email_from'); ?>
    </div>
	
	<div class="row" style="margin-left: 0">
		<?php echo $form->labelEx($replyModel,'email_to'); ?>
        <?php echo $form->textField($replyModel,'email_to'); ?>
        <?php echo $form->error($replyModel,'email_to'); ?>
	</div>
	
	<div class="row" style="margin-left: 0">
		<?php echo $form->labelEx($replyModel,'email_cc'); ?>
        <?php echo $form->textField($replyModel,'email_cc'); ?>
        <?php echo $form->error($replyModel,'email_cc'); ?>
	</div>
	
	<div class="row" style="margin-left: 0">
		<?php echo $form->labelEx($replyModel,'subject'); ?>
        <?php echo $form->textField($replyModel,'subject'); ?>
        <?php echo $form->error($replyModel,'subject'); ?>
	</div>
	
	<div class="row" style="margin-left: 0">
		<?php echo $form->labelEx($replyModel,'message'); ?>
        <?php echo $form->textArea($replyModel,'message',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($replyModel,'message'); ?>
	</div>
	<div id="replyLoading" style="display: none">
			<img src="<?php echo Yii::app()->request->baseUrl ; ?>/images/ajax-loader.gif" alt="loading" >
		</div>
	<div class="errorReply" style="color:#ff0000;height:26px ">
		
	</div>
	
	<div class="row buttons" style="margin-left: 0;">
        <?php echo CHtml::Button('SUBMIT',array('onclick'=>'send();','class'=>'btn','id'=>'rplyBtn')); ?> 
		
		
    </div>
	
		
		
	<?php $this->endWidget(); ?>	
	</div>