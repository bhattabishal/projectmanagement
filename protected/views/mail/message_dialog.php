<?php
    $this->widget(
    'bootstrap.widgets.TbButtonGroup',
    array(
    'buttons' => array(
    array('label' => 'Reply', 'url' => '#','id'=>'reply'),
    //array('label' => 'Forward', 'url' => '#'),
    array('label' => 'Save as Communication', 'url' => '#','id'=>'saveAsCommunication')
    ),
    )
    );
	?>
	<br />
	
	<?php

   $user_details=EmailUser::model()->getEmailDetailsById(Yii::app()->user->id);
	$user=$user_details->email_id;
	$pass=MyCustomClass::decrypt($user_details->password);
	$imap=new EIMap('{vmcp09.digitalpacific.com.au:993/imap/ssl/novalidate-cert}', $user, $pass);
	if($imap->connect())
	{
		$message=$imap->getMail($emailId,null,false,true);
		//print_r($imap->getBody($emailId));
		//die;
		$mailObject=new Mail();
		$mailObject->messageNo=$emailId;
		$mailObject->attachment=$message->attachments;
		$from=$message->fromName==""?$message->fromAddress:$message->fromName;
		$mailObject->from=$from;
		$mailObject->fromName=$message->fromName;
		$mailObject->uid=$message->UID;
		$mailObject->fromAddress=$message->fromAddress;
		$mailObject->subject=$message->subject;
		$mailObject->date=date('Y-m-d',strtotime($message->date));
		$mailObject->messageOriginal=$message->textHtmlOriginal;
		$mailObject->messagePlain=$message->textPlain;
		$mailObject->message=$message->textHtmlOriginal==""?"<pre>".$message->textPlain."</pre>":$message->textHtmlOriginal;
		$mailObject->to=$message->to;
		$mailObject->toString=$message->toString;
		$mailObject->cc=$message->cc;
		
		
		
		$imap->close();
		$msg;
		$messageObj;
		$attachment=array();
		
		
		$attachment=$mailObject->attachment;
		
		$replyModel=new Reply();
		$comm=new Communication();
		
			$replyModel->msg_no=$mailObject->messageNo;
			$replyModel->uid=$mailObject->uid;
			$replyModel->fromName=$mailObject->fromName;
			$replyModel->fromAddress=$mailObject->fromAddress;
			$replyModel->mail_to=$mailObject->toString;
			if(is_array($mailObject->cc))
			{
				$replyModel->mail_cc=implode(',',$mailObject->cc);
			}
			else
			{
				$replyModel->mail_cc=$mailObject->cc;
			}
			
			$replyModel->mainSubject=$mailObject->subject;
			$replyModel->mainMessage=$mailObject->message;
			
				$msg=$mailObject->message;
				$comm->communication_details=$mailObject->message;
				$comm->isAttachment=1;
				if(empty($attachment))
				{
					$comm->isAttachment=0;
				}
				
			
			
			$replyModel->mail_date=$mailObject->date;
			$user_details=EmailUser::model()->getEmailDetailsById(Yii::app()->user->id);
			$user=$user_details->email_id;
			$replyModel->email_from=$user;
			$replyModel->email_to=$replyModel->fromAddress;
			$replyModel->subject=$replyModel->mainSubject;
			
			$comm->communication_subject=$replyModel->subject;
			$comm->communication_date=date('Y-m-d');
			
			 echo '<div class="messagediv">'; 
		?>
		<!--message start-->
		<div style="display: none;" id="replyform">
	
	<?php
		$form=$this->beginWidget('CActiveForm', array(
    	'id'=>'email-reply-form',
    	'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
                               //'onsubmit'=>"return false;",/* Disable normal form submit */
                               //'onkeypress'=>" if(event.keyCode == 13){ send(); } " /* Do ajax call when user presses enter key */
                             ),
		));
		
		
	?>
	
	<?php echo $form->errorSummary($replyModel); ?>
	<div class="row" style="margin-left: 0">
		<?php echo $form->hiddenField($replyModel, 'uid'); ?>
		<?php echo $form->hiddenField($replyModel, 'msg_no'); ?>
		<?php echo $form->hiddenField($replyModel, 'fromName'); ?>
		<?php echo $form->hiddenField($replyModel, 'fromAddress'); ?>
		<?php echo $form->hiddenField($replyModel, 'mail_to'); ?>
		<?php echo $form->hiddenField($replyModel, 'mail_cc'); ?>
		<?php echo $form->hiddenField($replyModel, 'mainSubject'); ?>
		<?php echo $form->hiddenField($replyModel, 'mainMessage'); ?>
		<?php echo $form->hiddenField($replyModel, 'mail_date'); ?>
		
		
        <?php echo $form->labelEx($replyModel,'email_from'); ?>
        <?php echo $form->textField($replyModel,'email_from',array('readonly'=>'readonly')); ?>
        <?php echo $form->error($replyModel,'email_from'); ?>
    </div>
	
	<div class="row" style="margin-left: 0">
		<?php echo $form->labelEx($replyModel,'email_to'); ?>
        <?php echo $form->textField($replyModel,'email_to'); ?>
        <?php echo $form->error($replyModel,'email_to'); ?>
	</div>
	
	<div class="row" style="margin-left: 0">
		<?php echo $form->labelEx($replyModel,'email_cc'); ?>
        <?php echo $form->textField($replyModel,'email_cc'); ?>
        <?php echo $form->error($replyModel,'email_cc'); ?>
	</div>
	
	<div class="row" style="margin-left: 0">
		<?php echo $form->labelEx($replyModel,'subject'); ?>
        <?php echo $form->textField($replyModel,'subject'); ?>
        <?php echo $form->error($replyModel,'subject'); ?>
	</div>
	
	<div class="row" style="margin-left: 0">
		<?php echo $form->labelEx($replyModel,'message'); ?>
        <?php echo $form->textArea($replyModel,'message',array('rows'=>16, 'cols'=>50)); ?>
        <?php echo $form->error($replyModel,'message'); ?>
	</div>
	<div id="replyLoading" style="display: none">
			<img src="<?php echo Yii::app()->request->baseUrl ; ?>/images/ajax-loader.gif" alt="loading" >
		</div>
	<div class="errorReply" style="color:#ff0000;height:26px ">
		
	</div>
	
	<div class="row buttons" style="margin-left: 0;">
        <?php echo CHtml::Button('SUBMIT',array('onclick'=>'send();','class'=>'btn','id'=>'rplyBtn')); ?> 
		
		
    </div>
	
		
		
	<?php $this->endWidget(); ?>	
	</div>
	
	<!--communication-->
	<div style="display: none;" id="commform">
		<div class="form wide">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'communication-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'), // ADD THIS
	
	//'enableClientValidation' => true,
	//'clientOptions' => array(
                        //    'validateOnSubmit' => true,
                        //    'afterValidate' => 'js:checkIfEmailToClient',
                    //),
 	'stateful' => false,


)); 

?>
	<?php echo $form->errorSummary($comm); ?>
	<div class="row col2" style="width: 97%">
		
		
		
		<?php echo $form->labelEx($comm,'project_id'); ?>
		<?php 
		$records = Project::model()->getAllProjectsNo();
		echo $form->dropDownList($comm,'project_id',$records,array('empty' => 'Select project')); 
		?>
		<?php //echo $form->error($model,'project_id'); ?>
	</div>
	<div class="row col2" style="width: 97%">
		<?php echo $form->labelEx($comm,'communication_subject'); ?>
		<?php echo $form->textField($comm,'communication_subject',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($comm,'communication_subject'); ?>
	</div>
	<div style="clear:both"></div>
	
		<div class="row col2" style="width: 97%">
	<?php echo $form->labelEx($comm,'communication_details'); ?>
		<?php 
		 echo $form->textArea($comm,'communication_details',array('rows'=>16, 'cols'=>50));
		//echo $form->ckEditorRow($comm,'communication_details'); 
		?>
		
		<?php echo $form->error($comm,'communication_details'); ?>
	</div>
	<div style="clear:both"></div>
	<div class="row col2" style="width: 97%">
		<?php echo $form->labelEx($comm,'communication_date'); ?>
		<?php 
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $comm,
		    'attribute'=>'communication_date',
		    //'flat'=>true,//remove to hide the datepicker
		    'options'=>array(
		        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
				'dateFormat' => 'yy-mm-dd',
				'showButtonPanel' => true,      // show button panel
		    ),
		    'htmlOptions'=>array(
		        'style'=>''
		    ),
		));
	  ?>
		<?php echo $form->error($comm,'communication_date'); ?>
	</div>
	
	<div class="row col2" style="width: 97%">
		<?php
		
		
		$list = Branch::model()->getBranchAndClientDropDown();
		
		
		?>
		<?php echo $form->labelEx($comm,'comm_from'); ?>
		<?php echo $form->dropDownList($comm,'comm_from',$list); ?>
		<?php echo $form->error($comm,'comm_from'); ?>
	</div>
	<div style="clear:both"></div>
	<!--<div class="row col2" style="width: 97%">
		<?php
			//$checked=false;
			//if(!$comm->isNewRecord)
			//{
				//if($comm->comm_from <= 0)
				//{
					//$checked=true;
				//}
			//}
		?>
		<?php //echo $form->labelEx($comm,'is_client'); ?>
		<?php //echo $form->checkBox($comm,'is_client',array('checked'=>$checked,'value'=>1)); ?>
		<?php //echo $form->error($comm,'is_client'); ?>
	</div>-->
	<div class="row col2" style="width: 97%">
		<?php
			$checked=false;
			if(!$comm->isNewRecord)
			{
				if($comm->is_internal=="Yes")
				{
					$checked=true;
				}
			}
		?>
		<?php echo $form->labelEx($comm,'is_internal'); ?>
		<?php echo $form->checkBox($comm,'is_internal',array('checked'=>$checked,'value'=>1)); ?>
		<?php echo $form->error($comm,'is_internal'); ?>
	</div>
	<div style="clear:both"></div>
	<!--<div class="row col2" style="width: 97%">-->
		<?php
			//$checked=false;
			//if(!$comm->isNewRecord)
			//{
				//if($comm->is_private=="Yes")
				//{
					//$checked=true;
				//}
			//}
		?>
		<?php //echo $form->labelEx($comm,'is_private'); ?>
		<?php //echo $form->checkBox($comm,'is_private',array('checked'=>$checked,'value'=>1)); ?>
		<?php //echo $form->error($comm,'is_private'); ?>
	<!--</div>
	<div style="clear:both"></div>-->
	
	

	
	<input type="hidden" name="emailtoclient" id="emailtoclient" value="0"/>
	<input  type="hidden" name="emailsendbtn" id="emailsendbtn" value="0"/>
	<!--<input  type="hidden" name="emailId" value="<?php //echo $emailId; ?>"/>-->
	<?php 
	$comm->emailId=$emailId;
	echo $form->hiddenField($comm, 'emailId'); 
	echo $form->hiddenField($comm, 'isAttachment');
	echo $form->hiddenField($comm, 'is_private');
	?>
	
<div id="commLoading" style="display: none">
			<img src="<?php echo Yii::app()->request->baseUrl ; ?>/images/ajax-loader.gif" alt="loading" >
		</div>
	<div class="errorReplyComm" style="color:#ff0000;height:26px ">
		
	</div>

	<div class="row buttons" style="margin-left: 0; padding-left: 0">
		
		<?php echo CHtml::Button('SUBMIT',array('onclick'=>'send2();','class'=>'btn','id'=>'commBtn')); ?> 
	</div>
	
	<!--for tiny mce-->
	<?php $this->widget('application.extensions.tinymce.SladekTinyMce'); ?>
 

 
<script type="text/javascript">
 
    tinymce.init({
    selector: "textarea#Communication_communication_details",
    theme: "modern",
	preformatted:true,
    width: 1084,
    height: 300,
    //plugins: [
       //  "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
        // "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        // "save table contextmenu directionality emoticons template paste textcolor"
  // ],
   content_css: "css/content.css",
   menubar:false,
   
    toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent"

   //toolbar: "bold italic | alignleft aligncenter alignright alignjustify | outdent indent | forecolor backcolor", 
   //style_formats: [
       // {title: 'Bold text', inline: 'b'},
       // {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
       // {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
       // {title: 'Example 1', inline: 'span', classes: 'example1'},
       // {title: 'Example 2', inline: 'span', classes: 'example2'},
       
    //]
 }); 
 </script>
	<!--end tinymce-->

<?php $this->endWidget(); ?>

</div><!-- form -->
	
	</div>
	<!--end communication-->
	<?php
	  
	echo '<pre>'.$msg.'</pre>';
	if(! empty($attachment))
	{
		echo '<p class="MsoNormal">';
		$i=1;
	
		foreach($attachment as $file)
		{
			
			?>
			
			
			<a href="<?php echo $this->createAbsoluteUrl('mail/downloadAttachment',array('msgId'=>$emailId,'pid'=>$file['pid'])); ?>"><img src="<?php echo Yii::app()->request->baseUrl ; ?>/images/attachment.png" alt="attachment"> <?php echo $file['filename']; ?></a> <br />
			<?php
			
			
			$i++;
			
		}
		echo "</p> </div>";
	}
	
	    
	

 
?>
		<!--message end-->
		<?php
	}
	else{
		echo "Connection Lost.Please try again";
	}
	?>
	
	
	
	
	<script type="text/javascript">
	$().ready(function(){
    $('#yw0').click(function(){
		if($("#replyform").is(":visible"))
		{
			$("#replyform").hide();	
		}
		else{
			$("#replyform").removeClass("hidden");
			$('#Reply_message').val('');
			$('#errorReply').val('');
			$("#commform").hide();
			$("#replyform").show();
		}
		
        
    });
});

$().ready(function(){
    $('#yw1').click(function(){
		if($("#commform").is(":visible"))
		{
			$("#commform").hide();	
		}
		else{
			$("#commform").removeClass("hidden");
			$('#Reply_message').val('');
			$('#Communication_project_id').prop('selectedIndex',0);
			$("#replyform").hide();	
			$("#commform").show();
		}
		
        
    });
});

</script>


<script type="text/javascript">
 
function send()
 {
 $('#rplyBtn').attr('disabled','disabled');
 $("#replyLoading").show();
   var data=$("#email-reply-form").serialize();
 
 
  $.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("mail/reply"); ?>',
   data:data,
success:function(data){
                if(data=='yes')
				{
					$("#replyLoading").hide();
					alert('Message Successfully sent');
					
					$('#rplyBtn').removeAttr('disabled');
					$("#replyform").hide();
				} 
				else{
					//alert('Some error occured.Please try again');
					$("#replyLoading").hide();
					$(".errorReply").html(data);
					$('#rplyBtn').removeAttr('disabled');
				}
              },
   error: function(data) { // if error occured
   $("#replyLoading").hide();
         alert("Error occured.please try again");
         alert(data);
		 $('#rplyBtn').removeAttr('disabled');
    },
 
  dataType:'html'
  });
 
}

function send2()
 {
 	//var ed = tinyMCE.get('content');
	tinymce.get('Communication_communication_details').save();
   var data=$("#communication-form").serialize();
 	$('#commBtn').attr('disabled','disabled');
	$("#commLoading").show();
 
  $.ajax({
   type: 'POST',
    url: '<?php echo Yii::app()->createAbsoluteUrl("communication/createFromMail"); ?>',
   data:data,
success:function(data){
                if(data=='yes')
				{
					$("#commLoading").hide();
					alert('Communication Successfully saved');
					$('#commBtn').removeAttr('disabled');
					$("#commform").hide();
				}
				 
				else{
					//alert('Some error occured.Please try again');
					$("#commLoading").hide();
					$(".errorReplyComm").html(data);
					$('#commBtn').removeAttr('disabled');
				}
              },
   error: function(data) { // if error occured
   $("#commLoading").hide();
         alert("Error occured.please try again");
         alert(data);
		$('#commBtn').removeAttr('disabled');
    },
 
  dataType:'html'
  });
 
}
 
</script>

<script type="text/javascript">
function setBtnId()
{
	document.getElementById("emailsendbtn").value="1";
	send2();
}
function setBtnIdNull()
{
	document.getElementById("emailsendbtn").value="0";
	send2();
}
function checkIfEmailToClient(form, data, hasError)
{
	var btnId=document.getElementById("emailsendbtn").value;

	//alert(hasError);
	 if(!hasError && btnId==1)
	 {
	 	var y=confirm("Do you want to send email to client?");
		
		if(y)
		{
			document.getElementById("emailtoclient").value="1";
		}
		else{
			document.getElementById("emailtoclient").value="0";
		}
		
		
	 }
	
	  return true;
	
	
}
</script>
	