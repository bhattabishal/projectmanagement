    <?php Yii::app()->clientScript->registerScript('checkBoxSelect', "
	function callData(){
	fileid=$(\"input[name='checkedIds[]']:checked\").map(function() { 
	return this.value; 
	}).get();
	return fileid;
	}
	");
	
	
	

/*
Yii::app()->clientScript->registerScript('dialogOpen',"
$('#document').ready(function(){
$('.openDlg').live('click', function(){
var dialogId = $(this).attr('class').replace('openDlg', '');
$.ajax({
'type': 'POST',
'url' : $(this).attr('href'),
success: function (data) {
$('#divDialog'+' div.divForForm').html(data);
$( '#divDialog').dialog( 'open' );
},
dataType: 'html'
});
return false; // prevent normal submit
})
});
");
*/




	

	

?>
<?php 
Yii::import('ext.EIMap.EIMap', true);
$user_details=EmailUser::model()->getEmailDetailsById(Yii::app()->user->id);
$user=$user_details->email_id;
$pass=MyCustomClass::decrypt($user_details->password);

$imap=new EIMap('{vmcp09.digitalpacific.com.au:993/imap/ssl/novalidate-cert}', $user, $pass);

//$imap=new EIMap('{vmcp09.digitalpacific.com.au:993/imap/ssl/novalidate-cert}', "sranjit@enravel.com.au", "ashunanu007");
//$imap=new EIMap('{mail.nbhit.com:995/pop3/ssl/novalidate-cert}', $user, $pass);
	
?>
<div class="btnalign">
	<?php
	EQuickDlgs::iframeButton(
		// Yii::app()->baseUrl .'images/view.png',
		array(
			'controllerRoute' => 'reply/compose', //'member/view'
			// 'actionParams' => array('emailId'=>$email_id),
			'dialogTitle' => 'Message',
			'dialogWidth' => '80%',
			'dialogHeight' => '500',
			//'id' => $overview[0]->message_id,
			'id' => 'composeMesssage',
			'openButtonHtmlOptions'=>array('class'=>'btn',),
			'closeOnAction' => true, //important to invoke the close action in the actionCreate
			'openButtonText' => 'Compose',
			'closeButtonText' => 'Close',
			'iframeHtmlOptions' => array(
				'width' => '100%',
				'height' => '100%',
			),
		)
	);
	?>
</div>
<?php
	
$box = $this->beginWidget(
	'bootstrap.widgets.TbBox',
	array(
		'title' => 'Mail Box',
		'headerIcon' => 'icon-inbox',
		'headerButtons' => array(
			array(
				'class' => 'bootstrap.widgets.TbButtonGroup',
				//'type' => 'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
			
				'toggle' => 'radio',
				'buttons'=>array(
					//array('buttonType'=>'link','label'=>'Compose', 'url'=>'mail/compose','htmlOptions'=> array(
					//'icon'=>'envelope',
					//'class'=>'openDlg divDialog',
					//)),
				
					array('buttonType'=>'ajaxSubmit','label'=>'Delete','icon'=>'trash', 'url'=>'mail/bulkDelete','ajaxOptions'=>array(
							'type'=>'POST',
							'data'=> 'js:{"data1":callData()}',                        
							'success'=>'js:function(string){ alert(string);$.fn.yiiGridView.update("my-grid"); }' 	
						)),
					array('buttonType'=>'ajaxSubmit','label'=>'Archive','icon'=>'folder-open', 'url'=>'mail/archive','ajaxOptions'=>array(
							'type'=>'POST',
							'data'=> 'js:{"data1":callData()}',                        
							'success'=>'js:function(string){ alert(string); }' 	
						)),
					//array('label'=>'Save as Communication','icon'=>'arrow-left', 'url'=>'#'),
				),
			),
		),
		'htmlOptions' => array('class' => 'bootstrap-widget-table')
	)
);
	
	
?>
<table class="table mailList">
    
	<tbody>
		<?php
		if($imap->connect()){
			
			 $mailboxCheck = $imap->getCheck();
			$week_emails=$imap->searchMails('SINCE '. date('d-M-Y',strtotime("-3 week")));
			if(count($week_emails) > 0){
				if($week_emails && is_array($week_emails)){
					rsort($week_emails);
					//print_r($week_emails);
					//die;
					$i=1;
					foreach($week_emails as $key=>$email_id){
						$message=$imap->getMail($email_id,null,false,true);
						
						//$overview = $imap->getMailboxOverview2($email_id,0); 
						
						?>
						<tr class="<?php //echo $overview[0]->seen > 0?'read':'unread'; ?>">
							<td><input  type="checkbox" name="checkedIds[]" value="<?php echo $email_id; ?>" class="checkbox"/></td>
							<td>
								<?php
								if(in_array($email_id,$archiveEmailsArray)){
									?>
									<img src="<?php echo Yii::app()->request->baseUrl ; ?>/images/archive.png" alt="archive">
									<?php
								}
								if(in_array($email_id,$commEmailsArray)){
									?>
									<img src="<?php echo Yii::app()->request->baseUrl ; ?>/images/phone.png" alt="comm">
									<?php
								}
								?>
							</td>
							<td><?php echo $i; ?></td>
							<td>
								
								<?php 
									//echo $message->fromName==""?$message->fromAddress:$message->fromName;
									
									?>
								
								<?php
										
										
								EQuickDlgs::iframeButton(
									// Yii::app()->baseUrl .'images/view.png',
									array(
										'controllerRoute' => 'mail/messageDialog', //'member/view'
										'actionParams' => array('emailId'=>$email_id),
										'dialogTitle' => 'Message',
										'dialogWidth' => '1200',
										'dialogHeight' => '600',
										//'id' => $overview[0]->message_id,
										'id' => 'viewMesssage'.$i,
										'openButtonHtmlOptions'=>array('class'=>'btn',),
										//'style'=>'viewMessage',
										'closeOnAction' => true, //important to invoke the close action in the actionCreate
										'openButtonText' => 'view',
										'closeButtonText' => 'Close',
										'iframeHtmlOptions' => array(
											'width' => '100%',
											'height' => '100%',
										),
									)
								);
								?>
								<?php 
								echo $message->fromName==""?$message->fromAddress:$message->fromName;
								//echo $overview[0]->from;
									
								?>
							</td>
							<td><?php 
								echo $message->subject .' - <font color="#aaaaaa">'. MyCustomClass::string_trim($message->textPlain)."</font>"; 
								//echo $overview[0]->subject;
								?></td>
							<td><?php 
								echo date('Y-m-d',strtotime($message->date)); 
								//echo date('Y-m-d',strtotime($overview[0]->date)); 
								?></td>
							<td>
							<?php 
							
							if(empty($message->attachments)) 
							{
											
							}
							else{
							?>
							<img src="<?php echo Yii::app()->request->baseUrl ; ?>/images/attachment.png" alt="attachment">
							<?php
							} 
							?>
									
							</td>
							<td>
								
								<?php
								echo CHtml::link(
									'<img src="'.Yii::app()->request->baseUrl.'/images/delete.gif" alt="delete">',
									array('mail/deleteMail','emailId'=>$email_id),
									array('confirm' => 'Are you sure you want to delete this mail?')
								);
								?>
							</td>
						</tr>
						<?php
						$i++;
					}
					$imap->close();
					// Close our imap stream.
                     //imap_close($stream); 
				}
			}
			else{
				?>
				<tr class="odd">
					<td colspan="7"> No Email found</td>
				</tr>
				<?php
			}
		}
		else{
			?>
			<tr class="odd">
				<td colspan="7"> Connection Failed.Error: <?php echo imap_last_error(); ?></td>
			</tr>
			<?php
		}
		?>
	</tbody>
</table>
	
<?php $this->endWidget(); ?>
	
	
<!--for mail compose dialog-->
	
<?php

/*

$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
'id'=>'composeDialog',
// additional javascript options for the dialog plugin
'options'=>array(
'title'=>'Dialog box 1',
'autoOpen'=>false,
'modal'=>true,
'width'=>600
),
));
*/
?>
<!--<div class="divForForm"></div>-->
<?php
/*$this->endWidget(); */
?>
<!--end mail compose dialog-->
	
	
<style>
	table tr td a {
		display: block;
		height: 100%;
		width: 100%;
	}
	table tr td {
		padding-left: 0;
		padding-right: 0;
	}
</style>
