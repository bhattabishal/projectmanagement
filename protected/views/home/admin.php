<?php
/* @var $this HomeController */
/* @var $model HomeForm */





//Yii::app()->clientScript->registerScript('search', "
//$('.search-button').click(function(){
	//$('.search-form').toggle();
	//return false;
//});
//$('.search-form form').submit(function(){
	//$('#home-form-grid').yiiGridView('update', {
		//data: $(this).serialize()
	//});
	//return false;
//});
//");
?>



<!--for error success messages-->
<div id="statusMsg">
<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
 
<?php if(Yii::app()->user->hasFlash('error')):?>
    <div class="flash-error">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>
</div>
<!--end messages-->


<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<div class="headingalign">
<?php
//$uniqid=md5(uniqid());
//echo CHtml::tag('h3',array(),'RELATIONAL DATA EXAMPLE ROW : "'.$id.'"');
echo CHtml::tag('h6',array(),'Project Details');
?>
</div>
<div class="btnalign">
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'project/create',
        'dialogTitle' => 'Create Project',
        'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New project',
        'closeButtonText' => 'Close',
		'id' => 'newProject',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
		//'renderOpenButton' => false,
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'home-form-grid', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
?>
</div>


<?php
$uniqid=md5(uniqid());
// $this->widget('application.extensions.LiveGridView.RefreshGridView', array(
 $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'home-form-grid',
	//'id'=>'grid-'.$uniqid,
	//'pagerCssClass'=>'pagination pager-'.$uniqid,
	'dataProvider'=>$model->search(),
	//'updatingTime'=>60000, // 6 sec
	//'fixedHeader' => true,
//'headerOffset' => 40,
	//'enableSorting' => false,
//'enablePagination'=>false,
	'filter'=>$model,
	 'type'=>'striped bordered',
	 //'rowCssClass'=>'none',
	 'rowHtmlOptionsExpression'=>'array("style"=>"background-color:{$data->setRowColor()}","class"=>"self")',
	//'rowHtmlOptionsExpression'=>'array("style"=>"background-color:{$data->rowColor}","class"=>"self")',	
	 //'template' => "{items}",
	//'htmlOptions'=>array('style'=>'width:1110px'),
	'columns'=>array(
		array(
			//'class'=>'CButtonColumn',
				'class'=>'bootstrap.widgets.TbRelationalColumn',
				//'htmlOptions' =>array( 'class' => 'tbrelational-column1' ), 
				 'header' => '#',
				'name'=>'projectCompositeNo',
				'cssClass'=>'main tbrelational-column',
				'url' => $this->createUrl('home/communication'),
				'uniqid'=>$uniqid,
				'value'=>'$data->getProjectCompositeNo()',
				//'value'=>array($this,'projectNo'), 
				'afterAjaxUpdate' => 'js:function(tr,rowid,data){
					}'
			     //'template' => '{communication}',
			   //  'buttons'=>array
			     //(
			         //'communication' => array
			         //(
			            // 'label'=>'communication',
			
			            // 'imageUrl'=>Yii::app()->baseUrl."/images/toggle-expand-dark.png",  // make sure you have an image
			             //'url'=>'Yii::app()->createUrl("home/communication", array("id"=>$data->project_id))',
			            // 'options' => array(			
			//'ajax' => array('type' => 'get', 'url'=>'js:$(this).attr("href")', 'success' => 'js:function(data) { $.fn.yiiGridView.update("home-form-grid")}')
			        // ),
			    // ),
			//),
			
			
		),

	array(
	'name'=>'branch_id',
	'value'=>'$data->branch->branch_name',
	'header' => 'Branch',
	'filter'=>Branch::model()->getBranchName('branch_id'),
	'headerHtmlOptions' => array('style' => 'width: 110px'),
	),
		
		'project_title',
		array(
			'name'=>'project_status',
			//'value'=>'$data->projectStatus->code_lbl',
			'value'=>array($this,'projectStatus'),
			'filter'=>CodeValue::model()->getValuesArryByType('project_status'),
			'htmlOptions' => array('style'=>'width:86px'), 
		),
		array(
			'name'=>'project_type',
			'value'=>'$data->projectType->code_lbl',
		),
		array(
			'name'=>'client_id',
			'value'=>'$data->client->client_name',
		),
	
		 array(
			'name' => 'ao_coord',
		    'header'=>'Coord Ao', 
			 'value'=>'$data->aoCoordUser->user_name', 
			
			),
		array(
		    'name'=>'rps_coord',
			'header'=>'Coord Rps',
			 'value'=>'$data->rpsCoordUser->user_name', 
		),
		'project_date',
		//'job_due_date',
		 
		
		array(
			'name'=>'job_due_date',
			'type'=>'raw',
                        'header'=>'Job/Client Due Date',
			//'value'=>'$data->projectDueDate',
                        'value'=>array($this,'projectDueDate'), 
		),
		//array(
			//'name' => 'clientDueDate',
			//'type' => 'raw',
			//'value'=>array($model,'renderClientDueDate'),
		//),
		//'wis',
		//'est_rps_wis',
		//'est_ao_wis',
		//'actual_rps_hrs',
		//'actual_ao_hrs',
		'done_percentage',
		// array(
			//'name' => 'project_comments',
			//'enableSorting' => false,
			//'header' => 'Comments',
			//'class' => 'bootstrap.widgets.TbEditableColumn',
			//'headerHtmlOptions' => array('style' => 'width:80px'),
			//'editable' => array(
				//'type' => 'text',
				//'url' => '/example/editable'
				//)
			//),
            /*
			 array(
			'class' => 'bootstrap.widgets.MyTbEditableColumn',
			'name' => 'project_comments',
			'value' =>'project_comments',
			//'type'=>'raw',
			'header'=>'Comments(RPS)',
			'headerHtmlOptions' => array('style' => 'width: 110px'),
			'editable' => array(
			 	'type' => 'textarea',
				'url' => $this->createUrl('project/updateComments'),
				'placement' => 'left',
			)
			),
			 array(
			'class' => 'bootstrap.widgets.MyTbEditableColumn',
			'name' => 'project_comments_ao',
			'is_ao'=>true,
			'value' =>'project_comments_ao',
			'header'=>'Comments(AO)',
			'headerHtmlOptions' => array('style' => 'width: 110px'),
			'editable' => array(
			 	'type' => 'textarea',
				'url' => $this->createUrl('project/updateComments'),
				'placement' => 'left',
			)
			),*/
		array(
		//'template'=>'{view}{update}{delete}',
            'class'=>'EJuiDlgsColumn',
			//'afterDelete'=>'function(link,success,data){ if(success) $("#statusMsg").show();$("#statusMsg").html(data);$("#statusMsg").fadeOut(1500); }',
			
			'htmlOptions' => array('style'=>'width:56px'),

			'deleteButtonUrl'=>'Yii::app()->createUrl("/project/delete", array("id" => $data->project_id))',
			
           	//'updateButtonImageUrl'=>Yii::app()->baseUrl .'images/viewdetaildialog.png',
            'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
                    ),
					
					
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'project/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View Project',
			 'closeOnAction' => true,
			 'refreshGridId' => 'home-form-grid',
                         'hideTitleBar' => false, 
              'dialogWidth' => '80%',
        		'dialogHeight' => '300',
        ),
		'updateDialog'=>array(
             'controllerRoute' => 'project/update', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update Project',
			 'closeOnAction' => true,
			  'refreshGridId' => 'home-form-grid',
			  'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
             //'dialogWidth' => 800, //use the value from the dialog config
             //'dialogHeight' => 600,
        ),
		
           
        ),
		/*
		'crtd_by',
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
		*/
		
	),
)); 
 ?>
 
 


<!--<script type="text/javascript">
</script>-->