<div class="headingalign">
<?php
//$uniqid=md5(uniqid());
//echo CHtml::tag('h3',array(),'RELATIONAL DATA EXAMPLE ROW : "'.$id.'"');
echo CHtml::tag('h6',array(),'Task Details');
?>
</div>

<div class="btnalign">
<?php
//for new btn
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'task/create',
		'actionParams' => array('cid'=>$cid),
        'dialogTitle' => 'Create Task',
		'id' => 'newTask',
        'dialogWidth' => '97%',
        'dialogHeight' => '600',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
        'openButtonText' => 'New task',
        'closeButtonText' => 'Close',
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'gridTask', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
//end new btn
?>
</div>
<!-- this div are for the popup loading  DO NOT DELETE-->           
        <div id="studentBasicinfo">
        <div id="jobDialog" style="display:none;"></div>
        </div>     
        <!-- this div are for the popup loading -->  

<?php


//$project_communicates=Communication::model()->findAll($criteria); // $params is not needed
$uniqid=md5(uniqid());
$int_unique_id=MyCustomClass::uniqueRandom();
$this->widget('bootstrap.widgets.TbExtendedGridView', array(
'type'=>'striped bordered',
'id' => 'gridTask',
//'id'=>'grid-'.$uniqid,
//'pagerCssClass'=>'pagination pager-'.$uniqid,
'dataProvider' => $gridDataProvider,
'enableSorting' => false,
'enablePagination'=>false,
//'template' => "{items}",
'columns'=>array(
	array(
			//'class'=>'CButtonColumn',
				'class'=>'bootstrap.widgets.TbRelationalColumn',
				'cssClass'=>'main-taskchecklist tbrelational-column',
				'uniqid'=>$uniqid."-".$tno,
				//'htmlOptions' =>array( 'class' => 'tbrelational-column2' ), 
				 'header' => '#',
				'url' => $this->createUrl('home/checklist'),
				'value'=> function($data,$row) use ($tno){
	                return str_replace('_', ' ', $tno)."-".$data->task_id;
	            },
				'afterAjaxUpdate' => 'js:function(tr,rowid,data){console.log(rowid)}'			
		),
		//array(
			
				//'class'=>'bootstrap.widgets.TbRelationalColumn',
				//'cssClass'=>'comm-taskchecklist tbrelational-column',
				//'uniqid'=>$uniqid."aa",
				// 'header' => 'Communication',
				//'url' => $this->createUrl('communication/taskComm',array('sno' => $sno)),
				//'value'=> '"view-communication"',
				//'afterAjaxUpdate' => 'js:function(tr,rowid,data){console.log(rowid)}'			
		//),
	//'job_details',
	array(
			'name'=>'job_details',
			// call the method 'renderCoordAo' from the model
			'type'=>'raw',
            'value'=>'$data->job_details',
		),
	'job_due_date',
	//'done_percentage',
	'assignedTo.user_name',
	//'taskStatus.code_lbl',
	/*
array(
				'name' => 'task_status',
                  'type'=>'raw',
				  'headerHtmlOptions' => array('style' => 'width:80px'),
                  'value'=>"CHtml::ajaxLink(
                            \$data->taskStatus->code_lbl,
                            Yii::app()->createUrl('task/taskStatusProjectPopUp',array('id'=>\$data->task_id)),
							
                            array('update'=>'#jobDialog'),
                            array('id'=>'showJobDialog'.\$data->task_id.'$int_unique_id','class'=>'editable-click')
                            )",
                   
            ),
            */
			
	 array(
		'name' => 'task_status',
		'value'=> '$data->taskStatus->code_lbl',
		'header' => 'Status',
		'class' => 'bootstrap.widgets.MyTaskStatusTbEditableColumn',
		
		'headerHtmlOptions' => array('style' => 'width:80px'),
		'editable' => array(
		'type' => 'select',
		'source' => $this->createUrl('getTaskStatusValues'),
		'url' =>  $this->createUrl('task/editTaskStatus'),
		
		)
		),
		
		array(
			'name'=>'task_variation',
			//'filter'=>array('No'=>'No', 'Yes'=>'Yes'),
			'value' => '$data->task_variation',
        	
		),
	array(
			'template'=>'{view}{update}{delete}',
            'class'=>'EJuiDlgsColumn',
			
			'htmlOptions' => array('style'=>'width:69px'),
			'deleteButtonUrl'=>'Yii::app()->createUrl("/task/delete", array("id" => $data->task_id))',
           	//'updateButtonImageUrl'=>Yii::app()->baseUrl .'images/viewdetaildialog.png',
            'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
						'options'=>array(
						'class'=>'taskView',
						)
                    ),
					'update'=>
                             array(
                        'options'=>array(
						'class'=>'taskUpdate',
						)
                    ),
					'delete'=>
                             array(
                        'options'=>array(
						'class'=>'taskDelete'.$uniqid,
						)
                    ),
				/*	 'creates'=>
                             array(
                        'label'=>'creates',
						'url'=>'Yii::app()->createUrl("task/creates", array("id"=>$data->task_id))',
						'options'=>array(
						'class'=>'taskCreates',
						)
						),
				*/		

//for new btn

//end new btn


					//'comm' => array
			       // (
			           // 'label'=>'communication',
			          //  'imageUrl'=>Yii::app()->request->baseUrl.'/images/comm.png',
			          //  'url'=>'Yii::app()->createUrl("communication/taskComm", array("id"=>$data->task_id,"sno" => $sno))',
						//'options'=>array(
						//'style'=>'padding-left:4px',
						//),
						//'click'=>"function(){
                      //  $('#my-awsome-frame').attr('src',$(this).attr('href')+(($(this).attr('href').indexOf('?') >= 0)?'&':'?')+'qdsClass=EFrameJuiDlg&qdsDialogId=my-awsome-dlg&qdsContentWrapperId=my-awsome-content&qdsIFrameId=my-awsome-frame&qdsCloseDialog=1');$('#my-awsome-content').show();$('#my-awsome-dlg').dialog('open');return false;
                   // }",
						
			       // ),
					
					
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'task/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View Task',
			 'closeOnAction' => true,
			 'refreshGridId' => 'gridTask',
                         'hideTitleBar' => false, 
              'dialogWidth' => '80%',
        		'dialogHeight' => '300',
        ),
		'updateDialog'=>array(
             'controllerRoute' => 'task/update', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update Task',
			 'closeOnAction' => true,
			 'refreshGridId' => 'gridTask',
			  'dialogWidth' => '97%',
        		'dialogHeight' => '600',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ), 
             //'dialogWidth' => 800, //use the value from the dialog config
             //'dialogHeight' => 600,
        ),
		
           
        ),
),
//'columns' => array_merge(array(array('class'=>'bootstrap.widgets.TbImageColumn')),$gridColumns),
));

//EQuickDlgs::iframeButton(//Yii::app()->baseUrl .'/images/comm.png',
              // array(
                    //'id' => 'my-awsome',
           // 'renderOpenButton' => false, // <-----------------
           // 'controllerRoute' => 'awsome',
           // 'dialogTitle' => 'Task Communication',
            //'dialogWidth' => 800,
            //'dialogHeight' => 800,
           // 'closeOnAction' => true, //important to invoke the close action in the actionCreate
                //)
        //);

?><?php

?>
