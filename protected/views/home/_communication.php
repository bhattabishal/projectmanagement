<!--include javascript files for print-->
<?php
$baseUrl = Yii::app()->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($baseUrl.'/js/custom.js');
$cs->registerScriptFile($baseUrl.'/printArea/jquery.PrintArea.js');

$cs->registerCssFile($baseUrl.'/printArea/PrintArea.css');
$cs->registerCssFile($baseUrl.'/printArea/media_all.css');
$cs->registerCssFile($baseUrl.'/printArea/empty.css');
$cs->registerCssFile($baseUrl.'/printArea/noPrint.css');
$cs->registerCssFile($baseUrl.'/printArea/media_none.css');
$cs->registerCssFile($baseUrl.'/printArea/no_rel_no_media.css');
$cs->registerCssFile($baseUrl.'/printArea/no_rel.css');
?>
<div class="headingalign">
<?php
$pno=Project::model()->getProjectNoById($pid);
$uniqid=md5(uniqid());
//echo CHtml::tag('h3',array(),'RELATIONAL DATA EXAMPLE ROW : "'.$id.'"');
echo CHtml::tag('h6',array(),'Communication Details');
?>
</div>

<div class="btnalign">
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'communication/create',
		'actionParams' => array('pid'=>$pid,'tid'=>0),
        'dialogTitle' => 'Create Communication',
       'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New communication',
        'closeButtonText' => 'Close',
		'id' => 'newComm',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'gridComm', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
//end new btn
?>
</div>
<?php
//print_r($data->getAttributes());
$color_code = "#ff0000";
$this->widget('bootstrap.widgets.TbExtendedGridView', array(
'type'=>' bordered',
'id' => 'gridComm',
//'id'=>'grid-'.$uniqid,
//'pagerCssClass'=>'pagination pager-'.$uniqid,
'dataProvider' => $gridDataProvider,
'enableSorting' => false,
'enablePagination'=>false,
//'emptyText' =>'No records found',
'rowCssClass'=>'none',
'rowHtmlOptionsExpression'=>'array("style"=>"background-color:{$data->getColor()}","class"=>"self")',
//'template' => "{items}",
'columns'=>array(
		 array(
'class' => 'bootstrap.widgets.TbToggleColumn',
'toggleAction' => 'communication/toggleBookmark',
'uncheckedIcon' =>'iconbookmark',
'checkedIcon' =>'iconbookmarklight',
'checkedButtonLabel'=>'bookmark',
'uncheckedButtonLabel'=>'UnBookmark',
'name' => 'bookmark',
'header' => ''
),
 array(
'class' => 'bootstrap.widgets.TbToggleColumn',
'toggleAction' => 'communication/toggleStatus',
'checkedButtonLabel'=>'Unhide',
'uncheckedButtonLabel'=>'Hide',

'name' => 'status',
'header' => ''
),
	array(
			//'class'=>'CButtonColumn',
				'class'=>'bootstrap.widgets.TbRelationalColumn',
				'cssClass'=>'main-comm tbrelational-column',
				'uniqid'=>$uniqid."-".$pno,
				//'htmlOptions' =>array( 'class' => 'tbrelational-column2' ), 
				 'header' => '#',
				'url' => $this->createUrl('home/task'),
				//'actionParams' => array('pid'=>$pid,'tid'=>0),
				//'value'=> '$data->communication_id',
				'value'=>function($data,$row) use ($pno){
	                return $pno."-".$data->communication_id;
	            },
				'afterAjaxUpdate' => 'js:function(tr,rowid,data){console.log(rowid)}'			
		),
	'communication_date',
	'communication_subject',
	'user.user_name',
	//'communication_details',
	array(
			'name'=>'communication_details',
			// call the method 'renderCoordAo' from the model
			'type'=>'raw',
            'value'=>function($data,$row)
			  {
			  	return '<div class="comm_details" id="PrintArea_'.$data->communication_id.'">'.$data->communication_details.'</div>'.'<button type="button" class="printbtn" id="'.$data->communication_id.'">Print</button>';
			  	//return '';
			  },
			   'filter'=>'',
			  'type' => 'raw',
			  'htmlOptions' => array('style'=>'width:586px'),
		),
	array(
			'header'=>'Attachments',
			'type' =>'html',
			// call the method 'renderCoordAo' from the model
            'value'=>function($data,$row) {
	                $files = Yii::app()->db->createCommand()
    			->select('attachment')
    			->from('pm_communication_attachment')
			    ->where("communication_id = $data->communication_id")
			    ->queryAll();
				if(count($files) > 0)
			{
				$placesLinks = array();
				foreach($files as $key=>$val)
				{
					$placesLinks[] =CHtml::link($val['attachment'], Yii::app()->createUrl("home/downloadFile", array("file"=>$val['attachment'])));
				}
				
				return implode(', ', $placesLinks);
			}
			else{
				return "..no files..";
			}
	            },
		),
	array(
			'header'=>'Active Tasks',
			// call the method 'renderCoordAo' from the model
            'value'=>'$data->getTaskCount()',
		),
		array(
			'header'=>'Variation',
			// call the method 'renderCoordAo' from the model
            'value'=>'$data->getTaskVariation()',
		),
		'mail_sent',
	//'branch.color_code',
	array(
            'class'=>'EJuiDlgsColumn',
			'htmlOptions' => array('style'=>'width:56px'),
			'deleteButtonUrl'=>'Yii::app()->createUrl("/communication/delete", array("id" => $data->communication_id))',
           	//'updateButtonImageUrl'=>Yii::app()->baseUrl .'images/viewdetaildialog.png',
            'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
						'options'=>array(
						'class'=>'commView',
						)
                    ),
					'update'=>
                             array(
                        'options'=>array(
						'class'=>'commUpdate',
						)
                    ),
					'delete'=>
                             array(
                        'options'=>array(
						'class'=>'commDelete'.$uniqid,
						)
                    ),
					
					
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'communication/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View Communication',
			  'closeOnAction' => true,
			 'refreshGridId' => 'gridComm',
                         'hideTitleBar' => false, 
             'dialogWidth' => '80%',
        	 'dialogHeight' => '300',
        ),
		'updateDialog'=>array(
             'controllerRoute' => 'communication/update', //=default
             'actionParams' => array('id' => '$data->primaryKey','pid'=>$pid,'tid'=>0), //=default
             'dialogTitle' => 'Update Communication',
			  'closeOnAction' => true,
			 'refreshGridId' => 'gridComm',
			 'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
             //'dialogWidth' => 800, //use the value from the dialog config
             //'dialogHeight' => 600,
        ),
		
           
        ),
),
//'columns' => array_merge(array(array('class'=>'bootstrap.widgets.TbImageColumn')),$gridColumns),
));
?>
<script>
    $(document).ready(function(){

       

        $(".printbtn").click(function(){

      	var id = this.id;
		var mode = "popup";//iframe,popup
		var close = true;
		 var print ="div#PrintArea_" + id;
		 var keepAttr = [];
		 var extraCss="";
		 
		 //keepAttr.push("Class='print'");
		 //keepAttr.push("ID");
		 //keepAttr.push("Style");
		 var headElements =  '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>'
		 ;
		 var options = { mode : mode,strict:false, popClose : close, extraCss : extraCss, retainAttr : keepAttr, extraHead : headElements };
		 $(print).printArea(options);
		
        });

        
    });

  </script>
  
  
  
  
  
  
  
  
 
  
