<?php
/* @var $this HomeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Home Forms',
);

$this->menu=array(
	array('label'=>'Create HomeForm', 'url'=>array('create')),
	array('label'=>'Manage HomeForm', 'url'=>array('admin')),
);
?>

<h1>Home Forms</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
