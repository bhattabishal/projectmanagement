<?php
/* @var $this HomeController */
/* @var $model HomeForm */

$this->breadcrumbs=array(
	'Home Forms'=>array('index'),
	$model->project_id=>array('view','id'=>$model->project_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List HomeForm', 'url'=>array('index')),
	array('label'=>'Create HomeForm', 'url'=>array('create')),
	array('label'=>'View HomeForm', 'url'=>array('view', 'id'=>$model->project_id)),
	array('label'=>'Manage HomeForm', 'url'=>array('admin')),
);
?>

<h1>Update HomeForm <?php echo $model->project_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>