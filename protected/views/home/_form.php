<?php
/* @var $this HomeController */
/* @var $model HomeForm */
/* @var $form CActiveForm */
?>

 <fieldset>
 


<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'home-form-form',
	'type' => 'horizontal',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	 <?php echo $form->textFieldRow(
		$model,
		'project_title',
		array('hint' => 'In addition to freeform text, any HTML5 text-based input appears like so.')
	); ?>
	 
		<?php echo $form->dropDownListRow(
		$model,
		'project_status',
		array('Something ...', '1', '2', '3', '4', '5')
		); ?>
	<!--<div class="row">
		<?php echo $form->labelEx($model,'project_title'); ?>
		<?php echo $form->textField($model,'project_title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'project_title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'project_status'); ?>
		<?php echo $form->textField($model,'project_status'); ?>
		<?php echo $form->error($model,'project_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'project_type'); ?>
		<?php echo $form->textField($model,'project_type'); ?>
		<?php echo $form->error($model,'project_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'client_id'); ?>
		<?php echo $form->textField($model,'client_id'); ?>
		<?php echo $form->error($model,'client_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'project_date'); ?>
		<?php echo $form->textField($model,'project_date'); ?>
		<?php echo $form->error($model,'project_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'crtd_by'); ?>
		<?php echo $form->textField($model,'crtd_by'); ?>
		<?php echo $form->error($model,'crtd_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'crtd_dt'); ?>
		<?php echo $form->textField($model,'crtd_dt'); ?>
		<?php echo $form->error($model,'crtd_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_by'); ?>
		<?php echo $form->textField($model,'updt_by'); ?>
		<?php echo $form->error($model,'updt_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_dt'); ?>
		<?php echo $form->textField($model,'updt_dt'); ?>
		<?php echo $form->error($model,'updt_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updt_cnt'); ?>
		<?php echo $form->textField($model,'updt_cnt'); ?>
		<?php echo $form->error($model,'updt_cnt'); ?>
	</div>-->
</fieldset>
	 <div class="form-actions">
<?php $this->widget(
'bootstrap.widgets.TbButton',
array(
'buttonType' => 'submit',
'type' => 'primary',
'label' => 'Submit'
)
); ?>
<?php $this->widget(
'bootstrap.widgets.TbButton',
array('buttonType' => 'reset', 'label' => 'Reset')
); ?>
</div>

<?php $this->endWidget(); 
unset($form);
?>

