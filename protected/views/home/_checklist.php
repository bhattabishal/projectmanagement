<table>
	<tr>
		<td style="border-left:0"><!--checklist open-->
		<div class="headingalign">
<?php
$uniqid=md5(uniqid());
//echo CHtml::tag('h3',array(),'RELATIONAL DATA EXAMPLE ROW : "'.$id.'"');
//echo CHtml::tag('div',array(),'Checklist Details');
?>
</div>

<div class="btnalign">
<?php
//for new btn
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'taskChecklist/create',
		'actionParams' => array('tid'=>$tid),
        'dialogTitle' => 'Create Checklist',
		'id' => 'newTaskChecklist',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
		//'renderOpenButton' => false,
        'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New Checklist',
        'closeButtonText' => 'Close',
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'gridChecklist', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                           'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
//end new btn
?>
</div>

<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', array(
'type'=>'striped bordered',
'id' => 'gridChecklist',
//'id'=>'grid-'.$uniqid,
//'pagerCssClass'=>'pagination pager-'.$uniqid,
'dataProvider' => $gridDataProviderChecklist,
'enableSorting' => false,
'enablePagination'=>false,
//'template' => "{items}",
'columns'=>array(
	array(
			//'class'=>'CButtonColumn',
			'htmlOptions' => array('style'=>'width:10%'),
				 'header' => '#',
				'value'=> function($data,$row) use ($cklno){
	                return str_replace('_', ' ', $cklno)."-".$data->task_checklist_id;
	            },			
		),
	//'checklist_details',
	array(
		'name'=>'checklist_details', 
		'value'=>$data->checklist_details,
		'htmlOptions' => array('style'=>'width:50%'),
		),
		
		//array(
		//'name'=>'status0.code_lbl', 
		//'value'=>$data->$status0->code_lbl,
		//'htmlOptions' => array('style'=>'width:10%'),
		//),
	//'status0.code_lbl',
	 array(
		'name' => 'status',
		'value'=> '$data->status0->code_lbl',
		'header' => 'Status',
		'class' => 'bootstrap.widgets.TbEditableColumn',
		
		'headerHtmlOptions' => array('style' => 'width:80px'),
		'editable' => array(
		'type' => 'select',
		'source' => $this->createUrl('taskChecklist/statusValues'),
		'url' =>  $this->createUrl('taskChecklist/editStatus'),
		
		)
		),
	array(
            'class'=>'EJuiDlgsColumn',
			'htmlOptions' => array('style'=>'width:56px'),
			'deleteButtonUrl'=>'Yii::app()->createUrl("/taskChecklist/delete", array("id" => $data->task_checklist_id))',
           	//'updateButtonImageUrl'=>Yii::app()->baseUrl .'images/viewdetaildialog.png',
            'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
						'options'=>array(
						'class'=>'taskChecklistView',
						)
                    ),
					'update'=>
                             array(
                        'options'=>array(
						'class'=>'taskChecklistUpdate',
						)
                    ),
					'delete'=>
                             array(
                        'options'=>array(
						'class'=>'taskChecklistDelete'.$uniqid,
						)
                    ),
					
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'taskChecklist/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View Checklist',
			 'closeOnAction' => true,
			 'refreshGridId' => 'gridChecklist',
                         'hideTitleBar' => false, 
            'dialogWidth' => '80%',
        		'dialogHeight' => '300',
        ),
		'updateDialog'=>array(
             'controllerRoute' => 'taskChecklist/update', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update Checklist',
			 'closeOnAction' => true,
			 'refreshGridId' => 'gridChecklist',
			  'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
             //'dialogWidth' => 800, //use the value from the dialog config
             //'dialogHeight' => 600,
        ),
		
           
        ),
),
//'columns' => array_merge(array(array('class'=>'bootstrap.widgets.TbImageColumn')),$gridColumns),
));
?>
		</td><!--checklist close-->
		<td><!--task comm open-->
		
<div class="headingalign">
<?php
$uniqid=md5(uniqid());
//echo CHtml::tag('h3',array(),'RELATIONAL DATA EXAMPLE ROW : "'.$id.'"');
//echo CHtml::tag('div',array('class'=>'smallheading'),'Task Communication Details',array('class'=>'smallheading', 
 //'readonly' => true,
       //'style'=>'width:700px;height:340px;'
	  // )
	   //);
?>
</div>

<div class="btnalign">
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'communication/create',
		'actionParams' => array('pid'=>0,'tid'=>$tid),
        'dialogTitle' => 'Create Task Communication',
       'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New task communication',
        'closeButtonText' => 'Close',
		'id' => 'newTaskComm',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'gridTaskComm', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
//end new btn
?>
</div>
<?php
//print_r($data->getAttributes());
//$color_code = "#ff0000";
$this->widget('bootstrap.widgets.TbExtendedGridView', array(
'type'=>' bordered',
'id' => 'gridTaskComm',
//'id'=>'grid-'.$uniqid,
//'pagerCssClass'=>'pagination pager-'.$uniqid,
'dataProvider' => $gridDataProviderTaskComm,
'enableSorting' => false,
'enablePagination'=>false,
//'enableSorting' => false,
//'enablePagination'=>false,
//'emptyText' =>'No records found',
//'rowCssClass'=>'none',
//'rowHtmlOptionsExpression'=>'array("style"=>"background-color:{$data->branch->color_code}","class"=>"self")',
//'template' => "{items}",
'columns'=>array(
	array(
'class' => 'bootstrap.widgets.TbToggleColumn',
'toggleAction' => 'home/toggleInnerBookmark',
'checkedIcon' =>'iconbookmark',
'uncheckedIcon' =>'iconbookmarklight',
'checkedButtonLabel'=>'Unbookmark',
'uncheckedButtonLabel'=>'Bookmark',
'uniqueClassSuffix'=>'innerbookmark',
'name' => 'bookmark',
'header' => ''
),
 array(
'class' => 'bootstrap.widgets.TbToggleColumn',
'toggleAction' => 'home/toggleInnerStatus',
'checkedButtonLabel'=>'Unhide',
'uncheckedButtonLabel'=>'Hide',
'uniqueClassSuffix'=>'innerstatus',
'name' => 'status',
'header' => ''
),
	array(
            'header'=>'#',
			'value'=> function($data,$row) use ($cklno){
	                return str_replace('_', ' ', $cklno)."-".$data->communication_id;
	            },
				'htmlOptions' => array('style'=>'width:10%'),
           // 'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
			//'class'=>'bootstrap.widgets.TbRelationalColumn',
			//'url' => $this->createUrl('clientContact/loadByClient'),
			//'uniqid'=>$uniqid,
			//'afterAjaxUpdate' => 'js:function(tr,rowid,data){
					//}'
        ),
		array(
		'name'=>'communication_date', 
		'value'=>$data->communication_date,
		'htmlOptions' => array('style'=>'width:10%'),
		),
	//'communication_date',
	//'communication_subject',
	array(
		'name'=>'communication_subject', 
		'value'=>$data->communication_subject,
		'htmlOptions' => array('style'=>'width:20%'),
		),
	//'communication_details',
	array(
		'name'=>'communication_details', 
		'value'=>$data->communication_details,
		'htmlOptions' => array('style'=>'width:37%'),
		),
	//'branch.color_code',
		array(
            'class'=>'EJuiDlgsColumn',
			'htmlOptions' => array('style'=>'width:56px'),
			'deleteButtonUrl'=>'Yii::app()->createUrl("/taskCommunication/delete", array("id" => $data->task_communication_id))',
           	//'updateButtonImageUrl'=>Yii::app()->baseUrl .'images/viewdetaildialog.png',
            'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
						'options'=>array(
						'class'=>'taskCommView',
						)
                    ),
					'update'=>
                             array(
                        'options'=>array(
						'class'=>'taskCommUpdate',
						)
                    ),
					'delete'=>
                             array(
                        'options'=>array(
						'class'=>'taskCommDelete'.$uniqid,
						)
                    ),
					
					
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'communication/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View Communication',
			  'closeOnAction' => true,
			 'refreshGridId' => 'gridTaskComm',
                         'hideTitleBar' => false, 
             'dialogWidth' => '80%',
        	 'dialogHeight' => '300',
        ),
		'updateDialog'=>array(
             'controllerRoute' => 'communication/update', //=default
             'actionParams' => array('id' => '$data->primaryKey','pid'=>0,'tid'=>$tid), //=default
             'dialogTitle' => 'Update Communication',
			  'closeOnAction' => true,
			 'refreshGridId' => 'gridTaskComm',
			 'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
             //'dialogWidth' => 800, //use the value from the dialog config
             //'dialogHeight' => 600,
        ),
		
           
        ),
),
//'columns' => array_merge(array(array('class'=>'bootstrap.widgets.TbImageColumn')),$gridColumns),
));
?>
		</td><!--task comm close-->
	</tr>
</table>
