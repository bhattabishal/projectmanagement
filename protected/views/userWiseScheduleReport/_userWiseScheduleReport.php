<?php
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();

//$cs->registerScriptFile($baseUrl.'/fixedHeader/js/jquery.fixedheadertable.js');
//$cs->registerCssFile($baseUrl.'/fixedHeader/css/fixedheadertable.css');
//$cs->registerCssFile($baseUrl.'/fixedHeader/css/custom.css');



$cs->registerScriptFile($baseUrl . '/dataTables/media/js/jquery.dataTables.js');
$cs->registerScriptFile($baseUrl . '/dataTables/extensions/FixedColumns/js/dataTables.fixedColumns.min.js');
$cs->registerCssFile($baseUrl . '/dataTables/media/css/jquery.dataTables.min.css');
$cs->registerCssFile($baseUrl . '/dataTables/extensions/FixedColumns/css/dataTables.fixedColumns.min.css');

/* data tables not used
  //$cs->registerCssFile($baseUrl.'/dataTables/examples/resources/syntax/shCore.css');
  //$cs->registerCssFile($baseUrl.'/dataTables/examples/resources/demo.css');
  //$cs->registerCssFile($baseUrl.'/dataTables/extensions/TableTools/css/dataTables.tableTools.css');
  //$cs->registerScriptFile($baseUrl.'/dataTables/extensions/FixedHeader/js/dataTables.fixedHeader.js');
  //$cs->registerScriptFile($baseUrl.'/dataTables/extensions/KeyTable/js/dataTables.keyTable.js');
  //$cs->registerScriptFile($baseUrl.'/dataTables/media/js/complete.min.js');
  //cs->registerScriptFile($baseUrl.'/dataTables/media/js/jquery.js');
  //$cs->registerScriptFile($baseUrl.'/dataTables/examples/resources/syntax/shCore.js');
  //$cs->registerScriptFile($baseUrl.'/dataTables/examples/resources/demo.js');
  //$cs->registerScriptFile($baseUrl.'/dataTables/extensions/TableTools/js/dataTables.tableTools.js');
 */

$cs->registerScriptFile($baseUrl . '/htmltable_export/tableExport.js');
$cs->registerScriptFile($baseUrl . '/htmltable_export/jquery.base64.js');

//$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/libs/sprintf.js');
//$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/jspdf.js');
//$cs->registerScriptFile($baseUrl.'/htmltable_export/jspdf/libs/base64.js');



$reportModel = new UserWiseScheduleReport;



$days = MyCustomClass::GetDateDifference($model->endDt, $model->startDt);
$days+=1; //for current date the difference is 0 so make it one.we need total days count
?>


<script type="text/javascript" language="javascript" class="init">
//<![CDATA[
    var table;
    $(document).ready(function () {
        var table = $('#myDemoTable').dataTable(
                {
                    "dom": "<'row-fluid'r><'row-fluid't><'row-fluid'<'span6'i><'span6'p>>",
                    "scrollY": 400,
                    "scrollX": true,
                    "ordering": false,
                    "info": false,
                    'bFilter': false,
                    "scrollCollapse": true,
                    "paging": false
                });
        new $.fn.dataTable.FixedColumns(table, {
            leftColumns: 2,
            rightColumns: 6,
            heightMatch: 'auto',
        });
    });
//]]>


</script>


<!--search fields-->
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'search-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
        ));
?>
<table border="1" class="table-bordered">
    <tr>
        <td>User</td>
        <td>

            <?php
            $rec = User::model()->byName()->findAll();
            $list = CHtml::listData($rec, 'user_id', 'user_name');
            echo $form->dropDownList($model,'user_id',$list,array('ajax'=>array(
                                        'type'=>'POST', //request type
									
                                        'url'=>$this->createUrl('userWiseScheduleReport/dynamicRegularProjectByUser'), //url to call.
                                        'update'=>'#UserWiseScheduleReport_projectId',
                                        'data'=>array('user_id'=>'js:this.value') 
                                        ),
										
										'empty' => 'Select User',
									'style' => 'width:110px;'
										)
										); 
            ?>



        </td>
        <td>Project</td>
        <td>
            <?php
                                        $uid=0;
                                        if(isset($model->user_id) && $model->user_id > 0)
                                        {
                                            $uid=$model->user_id;
                                        }
										
										$list=MyCustomClass::getInvolvedProjectsByUserId($uid);
										$records = CHtml::listData($list, 'project_id', 'pno');
					 
					
					echo $form->dropDownList($model,'projectId',$records,array(
					'empty' => 'All',
					'style' => 'width:110px;'
					)); 
					?>

        </td>
        <td>Fixed Task  <?php echo $form->checkBox($model,'adminTasks'); ?></td>
       
        <td>StartDate</td>
        <td>
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'startDt',
                //'flat'=>true,//remove to hide the datepicker
                'options' => array(
                    'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                    'dateFormat' => 'yy-mm-dd',
                    'showButtonPanel' => true, // show button panel
                ),
                'htmlOptions' => array(
                    'style'=>'width:106px'
                ),
            ));
            ?>
        </td>
        <td>End Date</td>
        <td>
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'endDt',
                //'flat'=>true,//remove to hide the datepicker
                'options' => array(
                    'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                    'dateFormat' => 'yy-mm-dd',
                    'showButtonPanel' => true, // show button panel
                ),
               'htmlOptions' => array(
			   'style'=>'width:106px'
                ),
				
            ));
            ?>
        </td>
        <td><input type="submit" value="Search" class="btn"/></td>
    </tr>

</table>
<?php $this->endWidget(); ?>
<!--end search fields-->

<a href="#"  onClick ="
        $('#myDemoTable').tableExport({
            type: 'excel',
            escape: 'false',
            //ignoreColumn: [2],
            htmlContent: 'true',
        });
   ">Export to Excel</a>

<?php
$lastRow="";
?>
<div class="outerbox">
    <div class="innerbox">
        <table id="myDemoTable" class="table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th colspan="2">Project</th>


                    <?php
                    $loopStartDt = $model->startDt;
                    $loopEndDt = $model->endDt;
                    $dates = array();
                    while (strtotime($loopStartDt) <= strtotime($loopEndDt)) {
                        $currentDate = $loopStartDt;
                        $dates[] = $currentDate;
                        //if(date('w', strtotime($currentDate)) != 6)
                        {
                            ?>
                            <th colspan="3"><?php echo date('D-M-j-Y', strtotime($currentDate)); ?></th>	
                            <?php
                        }
                        $loopStartDt = date("Y-m-d", strtotime("+1 day", strtotime($loopStartDt)));
                    }
                    ?>
                    <th colspan="3">Total</th>
                    <th colspan="3">Grand Total</th>
                </tr>
                <tr>
                    <th>No</th>
                    <th>Title</th>
<?php
$lastRow='<tr><td></td><td></td>';
foreach ($dates as $key => $val) {
    echo '<th>Sc</th>';
    echo '<th>Ac</th>';
	echo '<th>Sh</th>';
        
        $lastRow.='<td></td><td></td><td></td>';
}
?>
                    <th>Sc</th>
                    <th>Ac</th>
					<th>Sh</th>
					
                    <th>Sc</th>
                    <th>Ac</th>
					<th>Sh</th>
                                        <?php
                                        $lastRow.='<td></td><td></td><td></td><td></td><td></td><td></td>';
                                        ?>
                </tr>
            </thead>
            <tbody>
<?php
if ($projectRecords != NULL) {

    foreach ($projectRecords as $prj) {
        $totAct = 0;
        $totSche = 0;
		$totSho = 0;
		$tid = $prj['task_id'];
        $pid = $prj['project_id'];
        if ($pid < 0) {
		
                    
                    ?>
                                    <tr>
								
                                     <td><?php echo $prj['project_no']; ?>-<?php echo $prj['code_lbl']; ?>(<?php echo $prj['project_title']; ?>)</td>
                                        <td><?php echo $prj['job_details']; ?></td>
									  
                                    <?php
                                    foreach ($dates as $key => $val) {
                                        $date = $val;
								        $scheduled =0;
									    $actual = $reportModel->getFixedTaskByUser($uid,$date,$tid);
								
										$shooting=0;
										
                                        $totAct+=$actual;
                                        $totSche+=$scheduled;
									//	$totSho+=$shooting;
                                        echo '<td>';
                                        echo $scheduled > 0 ? $scheduled : "";
                                        echo '</td>';
                                        echo '<td>';
                                        echo $actual > 0 ? $actual : "";
                                        echo '</td>';
										echo '<td>';
                                        echo $shooting > 0 ? $shooting : "";
                                        echo '</td>';
                                    }
                                    ?>
                                        <td>
                                        <?php echo $totSche; ?>
                                        </td>
                                        <td>
                                        <?php echo $totAct; ?>
                                        </td>
										 <td>
                                        <?php echo $totSho; ?>
                                        </td>
										
                                    <td><?php echo $she=0;?></td>
                                    <td><?php echo $reportModel->getFixedTotalTaskByUser($uid,$tid); ?></td>
								    <td><?php echo $shooting=0; ?></td>
                                    </tr>
                                        <?php
                               }
							  
							
					else {
                                ?>
                            <tr>
                               <td><?php echo $prj['project_no']; ?>-<?php echo $prj['project_type']; ?></td>
                                <td><?php echo $prj['project_title']; ?></td>
            <?php
            foreach ($dates as $key => $val) {
                $date = $val;
			//	$sDate = "2010-09-07";
				$aDate = explode("-", $date);
				$edate = $aDate[0]."-".$aDate[1];
			//	var_dump($edate);
                $scheduled = $reportModel->getTotAssignedHrsFromProjectDate($pid, $date,$uid);
                $actual = $reportModel->getTotWorkedHrsFromProjectDate($pid, $date,$uid);

				//$shooting1=ShootingFactor::model()->find('shooting_year_month=:sid,user_id=:uid',array(':sid'=>$edate,':uid'=>$uid));
			//	$shooting1=ShootingFactor::model()->find("shooting_year_month=$edate","user_id=$uid");
		    	$shooting1=$reportModel->getShootingFactorByUser($uid,$edate);
				if($shooting1 >0 )
				{
					$shooting=$actual*$shooting1;
				}
				else
				{
				
					$shooting=$actual*1;
				}
			
								
                $totAct+=$actual;
                $totSche+=$scheduled;
				$totSho+=$shooting;
                echo '<td>';
                echo $scheduled > 0 ? $scheduled : "";
                echo '</td>';
                echo '<td>';
                echo $actual > 0 ? $actual : "";
                echo '</td>';
				echo '<td>';
                echo $shooting > 0 ? $shooting : "";
                echo '</td>';
            }
            ?>
			
                                <td>
                                <?php echo $totSche; ?>
                                </td>
                                <td>
                                <?php echo $totAct; ?>
                                </td>
								<td>
                                <?php echo $totSho; ?>
                                </td>
                                <td><?php echo $reportModel->getTotAssignedHrsFromProject($pid,$uid);?></td>
                                <td><?php echo $reportModel->getTotWorkedHrsFromProject($pid,$uid); ?></td>
								<td><?php echo $reportModel->getTotalShootingFactor($uid); ?></td>
                            </tr>
                                <?php
                            }
                        }
                        echo $lastRow;
                    }
                    ?>
            </tbody>
        </table>
    </div>
</div>


<!--<script language="javascript">
        $(document).ready(function() {
              $('#myDemoTable').fixedHeaderTable({
                   altClass : 'odd',
                  footer : false,
                  fixedColumns : 1
               
                });
   
                
            });
</script>-->