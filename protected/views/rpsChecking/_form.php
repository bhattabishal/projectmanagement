<?php
/* @var $this RpsCheckingController */
/* @var $model RpsChecking */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'rps-checking-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	
	<?php// echo $form->errorSummary($model); ?>

    <div class="row">
	<?php 
       $criteria = new CDbCriteria();
       $criteria->condition = 'code_type=:id';
       $criteria->params = array(':id'=>'project_type');
       $criteria->order='code_id';
       $records = CHtml::listData(CodeValue::model()->findAll($criteria), 'code_id', 'code_lbl');	?>
	        <?php echo $form->labelEx($model,'project_type'); ?>
		<?php echo $form->dropDownList($model,'project_type',$records,array('empty' => 'Select Project Type')); ?>
		<?php echo $form->error($model,'project_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'checking_percentage'); ?>
		<?php echo $form->textField($model,'checking_percentage',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'checking_percentage'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->