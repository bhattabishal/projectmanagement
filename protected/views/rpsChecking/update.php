<?php
/* @var $this RpsCheckingController */
/* @var $model RpsChecking */

$this->breadcrumbs=array(
	'Rps Checkings'=>array('index'),
	$model->checking_id=>array('view','id'=>$model->checking_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List RpsChecking', 'url'=>array('index')),
	array('label'=>'Create RpsChecking', 'url'=>array('create')),
	array('label'=>'View RpsChecking', 'url'=>array('view', 'id'=>$model->checking_id)),
	array('label'=>'Manage RpsChecking', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>