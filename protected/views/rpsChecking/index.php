<?php
/* @var $this RpsCheckingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Rps Checkings',
);

$this->menu=array(
	array('label'=>'Create RpsChecking', 'url'=>array('create')),
	array('label'=>'Manage RpsChecking', 'url'=>array('admin')),
);
?>

<h1>Rps Checkings</h1>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
