<div class="btnalign">
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'rpsChecking/create',
        'dialogTitle' => 'Create Rps',
        'dialogWidth' => '80%',
        'dialogHeight' => '500',
        'openButtonText' => 'New',
        'closeButtonText' => 'Close',
		'id' => 'newRpsChecking',
		'openButtonHtmlOptions'=>array('class'=>'btn',),
		//'renderOpenButton' => false,
                'closeOnAction' => true, //important to invoke the close action in the actionCreate
                'refreshGridId' => 'rps-checking-grid', //the grid with this id will be refreshed after closing
		'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '100%',
                                                    ),
    )
);
?>
</div>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'rps-checking-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => 'striped bordered',
	'columns'=>array(
		//'checking_id',
                    array(
                    'header'=>'#',
                    'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                    'headerHtmlOptions' => array('style' => 'width:80px'),
			//'url' => $this->createUrl('clientContact/loadByClient'),
			//'uniqid'=>$uniqid,
			//'afterAjaxUpdate' => 'js:function(tr,rowid,data){
					//}'
     ),
		array(
                    'name'=>'project_type',
                    'value'=>'$data->projectType->code_lbl',
                    'header' => 'Project Type',
                    //'filter'=>CodeValue::model()->getTextValuesArryByType('project_type'),
                    'headerHtmlOptions' => array('style' => 'width:200px'),
                    ),
            array(
		//'checking_percentage',
                    'name'=>'checking_percentage',
                    'value'=>'$data->checking_percentage',
                    'header' => 'Checking Percentage',
                //'headerHtmlOptions' => array('style' => 'width:150px'),
                ),
		
           array(
			'class'=>'EJuiDlgsColumn',
                   // 'header'=>'Actions',
			'htmlOptions' => array('style'=>'width:60px'),
			'deleteButtonUrl'=>'Yii::app()->createUrl("/rpsChecking/delete", array("id" => $data->checking_id))',
			'viewButtonUrl'=>'Yii::app()->createUrl("/rpsChecking/view", array("id" => $data->checking_id))',
			
			'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'view',
                    ),
			
					),
			 'viewDialog'=>array(
                         'controllerRoute' => 'rpsChecking/view', //=default
                         'actionParams' => array('id' => '$data->primaryKey'), //=default
                         'dialogTitle' => 'View Rps',
			 'closeOnAction' => true,
			 'refreshGridId' => 'rpsChecking-grid',
                         'hideTitleBar' => false, 
                         'dialogWidth' => '80%',
        		 'dialogHeight' => '300',
			 'hideTitleBar' => false,
        ),		
                        'updateDialog'=>array(
                        'controllerRoute' => 'rpsChecking/update', //=default
                        'actionParams' => array('id' => '$data->primaryKey'), //=default
                        'dialogTitle' => 'Update Rps',
			'closeButtonText' => 'Close',
			'closeOnAction' => true,
			'refreshGridId' => 'rpsChecking-grid',
			'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                        'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
             //'dialogWidth' => 800, //use the value from the dialog config
             //'dialogHeight' => 600,
        ),			
					
		),
	),
)); ?>
