<?php
/* @var $this RpsCheckingController */
/* @var $data RpsChecking */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('checking_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->checking_id), array('view', 'id'=>$data->checking_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('project_type')); ?>:</b>
	<?php echo CHtml::encode($data->project_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('checking_percentage')); ?>:</b>
	<?php echo CHtml::encode($data->checking_percentage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_by')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_date')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_by')); ?>:</b>
	<?php echo CHtml::encode($data->updt_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_date')); ?>:</b>
	<?php echo CHtml::encode($data->updt_date); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_cnt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_cnt); ?>
	<br />

	*/ ?>

</div>