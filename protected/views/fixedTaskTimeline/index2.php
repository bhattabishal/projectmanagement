<?php
/* @var $this FixedTaskTimelineController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fixed Task Timelines',
);

$this->menu=array(
	array('label'=>'Create FixedTaskTimeline', 'url'=>array('create')),
	array('label'=>'Manage FixedTaskTimeline', 'url'=>array('admin')),
);
?>

<h1>Fixed Task Timelines</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
