<?php
/* @var $this FixedTaskTimelineController */
/* @var $model FixedTaskTimeline */

$this->breadcrumbs=array(
	'Fixed Task Timelines'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FixedTaskTimeline', 'url'=>array('index')),
	array('label'=>'Manage FixedTaskTimeline', 'url'=>array('admin')),
);
?>

<h1>Create FixedTaskTimeline</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>