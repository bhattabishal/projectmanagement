<?php
/* @var $this FixedTaskTimelineController */
/* @var $data FixedTaskTimeline */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('fxt_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->fxt_id), array('view', 'id'=>$data->fxt_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fxt_brief_id')); ?>:</b>
	<?php echo CHtml::encode($data->fxt_brief_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('project_id')); ?>:</b>
	<?php echo CHtml::encode($data->project_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('entry_dt')); ?>:</b>
	<?php echo CHtml::encode($data->entry_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nt_hours')); ?>:</b>
	<?php echo CHtml::encode($data->nt_hours); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ot_hours')); ?>:</b>
	<?php echo CHtml::encode($data->ot_hours); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remarks')); ?>:</b>
	<?php echo CHtml::encode($data->remarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_by')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crtd_dt')); ?>:</b>
	<?php echo CHtml::encode($data->crtd_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_by')); ?>:</b>
	<?php echo CHtml::encode($data->updt_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_dt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_dt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updt_cnt')); ?>:</b>
	<?php echo CHtml::encode($data->updt_cnt); ?>
	<br />

	*/ ?>

</div>