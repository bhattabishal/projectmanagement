<?php
/* @var $this FixedTaskTimelineController */
/* @var $model FixedTaskTimeline */

$this->breadcrumbs=array(
	'Fixed Task Timelines'=>array('index'),
	$model->fxt_id=>array('view','id'=>$model->fxt_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FixedTaskTimeline', 'url'=>array('index')),
	array('label'=>'Create FixedTaskTimeline', 'url'=>array('create')),
	array('label'=>'View FixedTaskTimeline', 'url'=>array('view', 'id'=>$model->fxt_id)),
	array('label'=>'Manage FixedTaskTimeline', 'url'=>array('admin')),
);
?>



<?php $this->renderPartial('_form', array('model'=>$model)); ?>