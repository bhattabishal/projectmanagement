<?php
/* @var $this FixedTaskTimelineController */
/* @var $model FixedTaskTimeline */

$this->breadcrumbs=array(
	'Fixed Task Timelines'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List FixedTaskTimeline', 'url'=>array('index')),
	array('label'=>'Create FixedTaskTimeline', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fixed-task-timeline-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Fixed Task Timelines</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fixed-task-timeline-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'fxt_id',
		'fxt_brief_id',
		'project_id',
		'type',
		'user_id',
		'entry_dt',
		/*
		'nt_hours',
		'ot_hours',
		'remarks',
		'crtd_by',
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
