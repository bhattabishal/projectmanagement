<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', array(
'type'=>'striped bordered',
'id' => 'gridClientContact',
//'id'=>'grid-'.$uniqid,
//'pagerCssClass'=>'pagination pager-'.$uniqid,
'dataProvider' => $gridDataProvider,
'enableSorting' => false,
'enablePagination'=>false,
//'template' => "{items}",
'columns'=>array(
	
	'project.title',
	'user.user_name',
	'entry_dt',
	'nt_hours',
	'ot_hours',
	array(
            'class'=>'EJuiDlgsColumn',
			'htmlOptions' => array('style'=>'width:56px'),
			'deleteButtonUrl'=>'Yii::app()->createUrl("/fixedTaskTimeline/delete", array("id" => $data->fxt_id))',
           	//'updateButtonImageUrl'=>Yii::app()->baseUrl .'images/viewdetaildialog.png',
            'buttons'=>array(
                        'view'=>
                             array(
                        'label'=>'ajax dialog view',
						'options'=>array(
						'class'=>'clientContactView',
						)
                    ),
					'update'=>
                             array(
                        'options'=>array(
						'class'=>'clientContactUpdate',
						)
                    ),
					'delete'=>
                             array(
                        'options'=>array(
						'class'=>'clientContactDelete',
						)
                    ),
					
                            ),
							'viewDialog'=>array(
             'controllerRoute' => 'fixedTaskTimeline/view', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'View Timeline',
			 'closeOnAction' => true,
			 'refreshGridId' => 'gridClientContact',
                         'hideTitleBar' => false, 
            'dialogWidth' => '80%',
        		'dialogHeight' => '300',
        ),
		'updateDialog'=>array(
             'controllerRoute' => 'fixedTaskTimeline/update', //=default
             'actionParams' => array('id' => '$data->primaryKey'), //=default
             'dialogTitle' => 'Update Timeline',
			 'closeOnAction' => true,
			 'refreshGridId' => 'gridClientContact',
			  'dialogWidth' => '80%',
        		'dialogHeight' => '500',
                         'hideTitleBar' => false,
						 'iframeHtmlOptions' => array(
                                                            'width' => '100%',
                                                            'height' => '450',
                                                    ), 
             //'dialogWidth' => 800, //use the value from the dialog config
             //'dialogHeight' => 600,
        ),
		
           
        ),
),
//'columns' => array_merge(array(array('class'=>'bootstrap.widgets.TbImageColumn')),$gridColumns),
));
?>