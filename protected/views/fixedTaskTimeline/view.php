<?php
/* @var $this FixedTaskTimelineController */
/* @var $model FixedTaskTimeline */

$this->breadcrumbs=array(
	'Fixed Task Timelines'=>array('index'),
	$model->fxt_id,
);

$this->menu=array(
	array('label'=>'List FixedTaskTimeline', 'url'=>array('index')),
	array('label'=>'Create FixedTaskTimeline', 'url'=>array('create')),
	array('label'=>'Update FixedTaskTimeline', 'url'=>array('update', 'id'=>$model->fxt_id)),
	array('label'=>'Delete FixedTaskTimeline', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->fxt_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FixedTaskTimeline', 'url'=>array('admin')),
);
?>

<h1>View FixedTaskTimeline #<?php echo $model->fxt_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'fxt_id',
		'fxt_brief_id',
		'project_id',
		'type',
		'user_id',
		'entry_dt',
		'nt_hours',
		'ot_hours',
		'remarks',
		'crtd_by',
		'crtd_dt',
		'updt_by',
		'updt_dt',
		'updt_cnt',
	),
)); ?>
