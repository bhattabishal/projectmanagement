<?php
/* @var $this FixedTaskTimelineController */
/* @var $model FixedTaskTimeline */
/* @var $form CActiveForm */
?>

<div class="form wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fixed-task-timeline-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	

	<div class="row col2">
		<?php
		$list=  FixedTask::model()->listChildTasksForTimeline();
		?>
		<?php echo $form->labelEx($model,'project_id'); ?>
		<?php echo $form->dropDownList($model,'project_id',$list,array('empty' => 'Select project type')); ?>
		<?php echo $form->error($model,'project_id'); ?>
	</div>

	

	<div class="row col2">
		<?php echo $form->labelEx($model,'entry_dt'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model' => $model,
		    'attribute'=>'entry_dt',
			
		    //'flat'=>true,//remove to hide the datepicker
		    'options'=>array(
		        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
				'dateFormat' => 'yy-mm-dd',
				'showButtonPanel' => true,      // show button panel
		    ),
		    'htmlOptions'=>array(
		        'style'=>''
		    ),
		)); ?>
		<?php echo $form->error($model,'entry_dt'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'nt_hours'); ?>
		<?php echo $form->textField($model,'nt_hours',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'nt_hours'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'ot_hours'); ?>
		<?php echo $form->textField($model,'ot_hours',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'ot_hours'); ?>
	</div>

	<div class="row col2">
		<?php echo $form->labelEx($model,'remarks'); ?>
		<?php echo $form->textArea($model,'remarks',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'remarks'); ?>
	</div>

	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->