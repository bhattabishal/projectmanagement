<?php
/* @var $this FixedTaskController */
/* @var $model FixedTask */
/* @var $form CActiveForm */
?>

<div class="form wide">

<?php $form=$this->beginWidget('DynamicTabularForm', array(
	'id'=>'fixed-task-timeline-form',
        'defaultRowView1'=>'_timelineForm',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php //echo CHtml::errorSummary(array($model)); ?>

        <div>
            <div>
                
            </div>
            <div>
                
            </div>
        </div>
        
	<?php
          echo $form->rowForm1($model);
        ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->