<?php
/* @var $this FixedTaskTimelineController */
/* @var $model FixedTaskTimeline */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'fxt_id'); ?>
		<?php echo $form->textField($model,'fxt_id',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fxt_brief_id'); ?>
		<?php echo $form->textField($model,'fxt_brief_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'project_id'); ?>
		<?php echo $form->textField($model,'project_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type'); ?>
		<?php echo $form->textField($model,'type',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'entry_dt'); ?>
		<?php echo $form->textField($model,'entry_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nt_hours'); ?>
		<?php echo $form->textField($model,'nt_hours',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ot_hours'); ?>
		<?php echo $form->textField($model,'ot_hours',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'remarks'); ?>
		<?php echo $form->textField($model,'remarks',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'crtd_by'); ?>
		<?php echo $form->textField($model,'crtd_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'crtd_dt'); ?>
		<?php echo $form->textField($model,'crtd_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updt_by'); ?>
		<?php echo $form->textField($model,'updt_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updt_dt'); ?>
		<?php echo $form->textField($model,'updt_dt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updt_cnt'); ?>
		<?php echo $form->textField($model,'updt_cnt'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->