<?php
class MyCustomClass
{

/**
 * The function is_date() validates the date and returns true or false
 * @param $str sting expected valid date format
 * @return bool returns true if the supplied parameter is a valid date 
 * otherwise false
 */
public static function is_date( $str ) {
    try {
        $dt = new DateTime( trim($str) );
    }
    catch( Exception $e ) {
        return false;
    }
    $month = $dt->format('m');
    $day = $dt->format('d');
    $year = $dt->format('Y');
    if( checkdate($month, $day, $year) ) {
        return true;
    }
    else {
        return false;
    }
}
	
	public static function GetThisDate($format, $daysOffset=0)
	{
		if($daysOffset<>0)
			$today= mktime(date("G")+1, date("i"), date("s"), date("m"), date("d")-$daysOffset, date("Y"));
		else
			$today= mktime(date("G")+1, date("i"), date("s"), date("m"), date("d"), date("Y"));
		return date($format, $today);
	}
	
	/*
	calculate the days different of given two dates
	$dat11=big date
	$date2=small date
	*/
	public static function GetDateDifference($date1, $date2='') 

	{

		if($date2=='')
	
			$date2=self::GetThisDate("Y-m-d");
	
	
	
		$d2Exp=explode("-",$date2);	
	
		$d2=mktime(0, 0, 0, $d2Exp[1], $d2Exp[2], $d2Exp[0]);		
	
		$d1Exp = explode("-",$date1);	
	
		$d1=mktime(0, 0, 0, $d1Exp[1], $d1Exp[2], $d1Exp[0]);	
	
		
	
		$dateDiff = $d1 - $d2;	
	
		$fullDays = floor($dateDiff/(60*60*24));
	
		
	
		return $fullDays;

	}	
	// Returns the date adjusted by the number of hours specified.

  public static function AddHours($date, $hours)

  {

        $newdate = date("Y-m-d G:i:s", mktime($date["hours"] + $hours,

                                                $date["minutes"],

                                                $date["seconds"],

                                                $date["mon"],

                                                $date["mday"],

                                                $date["year"]));

        return $newdate;

  }
  
  public static function check_in_range($start_date, $end_date, $date_from_user)
{
  // Convert to timestamp
  $start_ts = strtotime($start_date);
  $end_ts = strtotime($end_date);
  $user_ts = strtotime($date_from_user);

  // Check that user date is between start & end
  return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
}
  
  public static function addDays($date,$days)
  {
  		

		//$date = "Mar 03, 2011";
		//$date = strtotime($date);
		//$date = strtotime("+7 day", $date);
		//echo date('M d, Y', $date);


  		return date('Y-m-d', strtotime($date. " + $days days"));
  }
  
   public static function subractDays($date,$days)
  {
  		

		//$date = "Mar 03, 2011";
		//$date = strtotime($date);
		//$date = strtotime("+7 day", $date);
		//echo date('M d, Y', $date);


  		return date('Y-m-d', strtotime($date. " - $days days"));
  }
  
  /*
  return comma seperated projects ids associated with the user.i.e from project associates or coordinator
  */
  public static function getInvolvedProjectsByUser()
  {
  	$id=Yii::app()->user->id;
  	$projects=array();
  	
  	//coordinator projects
  	$criteria=new CDbCriteria;
	$criteria->select='project_id';  // only select the 'title' column
	$criteria->condition="user_id=:user_id";
	
	$criteria->params=array(':user_id'=>$id);
	$project_coordinators=ProjectCoordinates::model()->findAll($criteria);
	
	$i=0;
	foreach($project_coordinators as $records)
	{							
			$projects[$i]=$records->project_id;							
		
		$i++;
	}
	
	
	$criteria=new CDbCriteria;
	$criteria->select='project_id';  // only select the 'title' column
	$criteria->condition="user_id=:user_id";
	
	$criteria->params=array(':user_id'=>$id);
	$project_associates=ProjectAssociates::model()->findAll($criteria);
	
	foreach($project_associates as $records)
	{							
			$projects[$i]=$records->project_id;							
		
		$i++;
	}
	
	if(count($projects) <= 0)
	{
		$projects[0]="0";
	}
	else
	{
		$projects=array_unique($projects);
	}

	return implode(",",$projects);
  	
  }
  
  
  /*
  get distinct project from project associates and project coordinators by userid
  */
  
  public static function getInvolvedProjectsByUserId($uid)
  {
  	
	$qry="SELECT DISTINCT
	t1.project_id,
	t1.user_id,
	p.project_no,
	c.code_lbl AS project_type,
	p.project_title,
	CONCAT(
		p.project_no,
		'-',
		c.code_lbl,
		'(',
		p.project_title,
		')'
		
	) AS pno
FROM
	(
		SELECT
			user_id,
			project_id
		FROM
			pm_project_coordinates
		UNION
			SELECT
				user_id,
				project_id
			FROM
				pm_project_associates
	) t1
INNER JOIN pm_project AS p ON t1.project_id = p.project_id
LEFT JOIN pm_code_value AS c ON p.project_type = c.code_id
WHERE
	t1.user_id ='$uid'";
	
	
	$records=Yii::app()->db->createCommand($qry)->queryAll();

	return $records;
	
  }
	
	 public static function getProjectsByUserId($uid,$pid)
  {
  	
	$qry="SELECT DISTINCT
	t1.project_id,
	t1.user_id,
	p.project_no,
	c.code_lbl AS project_type,
	p.project_title,
	CONCAT(
		p.project_no,
		'-',
		c.code_lbl,
		'(',
		p.project_title,
		')'
		
	) AS pno
FROM
	(
		SELECT
			user_id,
			project_id
		FROM
			pm_project_coordinates
		UNION
			SELECT
				user_id,
				project_id
			FROM
				pm_project_associates
	) t1
INNER JOIN pm_project AS p ON t1.project_id = p.project_id
LEFT JOIN pm_code_value AS c ON p.project_type = c.code_id
WHERE
	t1.user_id ='$uid' AND p.project_id ='$pid'";
	
	
	$records=Yii::app()->db->createCommand($qry)->queryAll();

	return $records;
	
  }
	
	 public static function getFixedProjectsByUserId($uid)
  {
  $qry="select t.job_details,cv.code_lbl,p.project_type,p.project_id,p.project_no,p.project_title,t.task_id 
from pm_task as t 
INNER JOIN (select DISTINCT task_id from pm_actual_timeline WHERE user_id='$uid' and task_type='admin')t1 ON t.task_id=t1.task_id
INNER JOIN pm_communication as c ON c.communication_id =t.communication_id
INNER JOIN pm_project as p ON p.project_id = c.project_id
INNER JOIN pm_code_value as cv ON cv.code_id = p.project_type";
  
  	$records=Yii::app()->db->createCommand($qry)->queryAll();

	return $records;
	
  }
	/*
	get associates projects from user id
	*/
	
	public static function getAssociatesProjectsByUser()
	{
		
		$id=Yii::app()->user->id;
		$projects=array();
		$project_associates="";
		
		/*
		if(User::model()->checkUserIfCoordinator($id))
		{
			$criteria=new CDbCriteria;
			$criteria->select='project_id';  // only select the 'title' column
			$criteria->condition="user_id=:user_id";
			
			$criteria->params=array(':user_id'=>$id);
			$project_associates=ProjectCoordinates::model()->findAll($criteria);
		}
		*/
		//else
		//{
			$criteria=new CDbCriteria;
			$criteria->select='project_id';  // only select the 'title' column
			$criteria->condition="user_id=:user_id";
			
			$criteria->params=array(':user_id'=>$id);
			$project_associates=ProjectAssociates::model()->findAll($criteria);
			
			
			
		//}
		
			$i=0;
			foreach($project_associates as $records)
			{							
					$projects[$i]=$records->project_id;							
				
				$i++;
			}
			if(count($projects) <= 0)
			{
				$projects[0]="0";
			}
		
		return implode(",",$projects);
	}
	
	public static function uniqueRandom()
	{
		$length = rand(1,3);
		$chars = array_merge(range(0,9));
		shuffle($chars);
		$password = implode(array_slice($chars, 0,$length));
		return $password;
	}
	
	public static function safeSerialize($arry)
{
	return base64_encode(serialize($arry));
}
public static function safeUnserialize($arry)
{
	$data = unserialize(base64_decode($arry));
	if ($data === false) // could not unserialize
	{
		$data = repairSerializedArray(base64_decode($arry)); // salvage what we can
	}
	return $data;
}
public static function  repairSerializedArray($serialized)
{
	$tmp = preg_replace('/^a:\d+:\{/', '', $serialized);
		return repairSerializedArray_R($tmp); // operates on and whittles down the actual argument
	}
	public static function repairSerializedArray_R(&$broken)
	{
		// array and string length can be ignored
		// sample serialized data
	// a:0:{}
		// s:4:"four";
		// i:1;
		// b:0;
		// N;
		$data       = array();
		$index      = null;
		$len        = strlen($broken);
		$i          = 0;
		
		while(strlen($broken))
		{
			$i++;
			if ($i > $len)
			{
				break;
			}
			
			if (substr($broken, 0, 1) == '}') // end of array
		{
			$broken = substr($broken, 1);
			return $data;
		}
		else
		{
			$bite = substr($broken, 0, 2);
			switch($bite)
				{  
				case 's:': // key or value
				$re = '/^s:\d+:"([^\"]*)";/';
				if (preg_match($re, $broken, $m))
				{
					if ($index === null)
					{
						$index = $m[1];
					}
					else
					{
						$data[$index] = $m[1];
						$index = null;
					}
					$broken = preg_replace($re, '', $broken);
				}
				break;
				
				case 'i:': // key or value
				$re = '/^i:(\d+);/';
				if (preg_match($re, $broken, $m))
				{
					if ($index === null)
					{
						$index = (int) $m[1];
					}
					else
					{
						$data[$index] = (int) $m[1];
						$index = null;
					}
					$broken = preg_replace($re, '', $broken);
				}
				break;
				
				case 'b:': // value only
				$re = '/^b:[01];/';
				if (preg_match($re, $broken, $m))
				{
					$data[$index] = (bool) $m[1];
					$index = null;
					$broken = preg_replace($re, '', $broken);
				}
				break;
				
				case 'a:': // value only
				$re = '/^a:\d+:\{/';
					if (preg_match($re, $broken, $m))
					{
						$broken         = preg_replace('/^a:\d+:\{/', '', $broken);
							$data[$index]   = $this->repairSerializedArray_R($broken);
							$index = null;
						}
						break;
						
						case 'N;': // value only
						$broken = substr($broken, 2);
						$data[$index]   = null;
						$index = null;
						break;
					}
				}
			}
			
			return $data;
		}
		
	public static function string_trim($string, $trimLength = 40) {
    $length = strlen($string);
    if ($length > $trimLength) {
        $count = 0;
        $prevCount = 0;
        $array = explode(" ", $string);
        foreach ($array as $word) {
            $count = $count + strlen($word);
            $count = $count + 1;
            if ($count > ($trimLength - 3)) {
                return substr($string, 0, $prevCount) . "...";
            }
            $prevCount = $count;
        }
    } else {
        return $string;
    }
}


public static function encrypt($string,$key="!@]\enraVel") {
$result = '';
for($i=0; $i<strlen($string); $i++) {
$char = substr($string, $i, 1);
$keychar = substr($key, ($i % strlen($key))-1, 1);
$char = chr(ord($char)+ord($keychar));
$result.=$char;
}

return base64_encode($result); 
}

public static function decrypt($string,$key="!@]\enraVel") {
$result = '';
$string = base64_decode($string);

for($i=0; $i<strlen($string); $i++) {
$char = substr($string, $i, 1);
$keychar = substr($key, ($i % strlen($key))-1, 1);
$char = chr(ord($char)-ord($keychar));
$result.=$char;
}

return $result; 
}


public static function getWeeksList()
{
	$currentdate=MyCustomClass::addDays(date('Y-m-d'),15);
	$start = new DateTime('2014-08-01');
	$end = new DateTime($currentdate);
	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($start, $interval, $end);
	
	$results=array();

	foreach ($period as $dt)
	{
	    if ($dt->format("N") == 7)
	    {
	        //echo $dt->format("d-M-Y") . "<br>\n";
	        $results[$dt->format("Y-m-d")]=$dt->format("d-M-Y");
	    }
	}
	
	return array_reverse($results);
}

public static function getMonthsList()
{
	$str=array();
        for($i = 0; $i < 12; $i++) {
        $time = mktime(0, 0, 0, date('n') - $i, 1);
       // $str .="<option value=" . date('Yn', $time) . ">" . date('M Y', $time) . "</option>"; 
        $str[date('Y-m-d', $time)]=date('M Y', $time);
          
    }
  return $str;
}


public static function getMonthsYearList()
{
	$str=array();
	$date=date('Y-m-d');
	$start = date('Y-m-d', strtotime('-1 years'));
	$start=new DateTime($start);
	$end = date('Y-m-d', strtotime('+1 years'));
	$end=new DateTime($end);
	$interval = DateInterval::createFromDateString('1 month');
	$period   = new DatePeriod($start, $interval, $end);
	foreach ($period as $dt)
	{
		$str[$dt->format("Y-m")]=$dt->format("Y-M");
	}
  	return $str;
}


public static function getYearList($stYr,$enYr)
{
	$year=array();
	for ($i=$stYr;$i<=$enYr;$i++)
	{
		$year[$i]=$i;
	}
	return $year;
}

public static function getWeeks($date)
{
	$stdt=$date;
	$endt=date("Y-m-t", strtotime($date));
		
		$results=array();
		
		while(strtotime($stdt) < strtotime($endt))
		{
			$currentDate=$stdt;
			$wekst = date ("Y-m-d", strtotime("+6 day", strtotime($stdt)));
			$stdt = date ("Y-m-d", strtotime("+7 day", strtotime($stdt)));
			if(strtotime($stdt) > strtotime($endt))
			{
				$results[]=$currentDate."/".$endt;
			}
			else
			{
				$results[]=$currentDate."/".$wekst;
			}
			
		}
		
		

		
		
		return $results;
}

public static function getWeeksCount($i)
{
	if($i==1)
	{
		return "1st Week";
	}
	else if($i==2)
	{
		return '2nd Week';
	}
	else if($i==3)
	{
		return '3rd Week';
	}
	else if($i==4)
	{
		return '4th Week';
	}
	else 
		return '5th Weeek';
}

public static function roundDecimal($val,$places=2)
{
	return number_format((float)$val, $places, '.', '');
}

public static function showOneDecimalPlace($number)
{
	$number = substr($number,0,strpos($number,".") + 2);
	return $number;
}

}
?>