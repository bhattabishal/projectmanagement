<?php
class RequireLogin extends CBehavior
{
	/*
	associate an event handler with the application
	*/
	public function attach($owner)
	{
	    $owner->attachEventHandler('onBeginRequest', array($this, 'handleBeginRequest'));
	}
	
	public function handleBeginRequest($event)
	{
		$app = Yii::app();
		$request=$app->urlManager->parseUrl($app->request);
		
		//if (Yii::app()->user->isGuest && !in_array($_GET['r'],array('site/login'))) {
	  if (Yii::app()->user->isGuest && !in_array($_SERVER['PATH_INFO'], Yii::app()->user->loginUrl )) {
			Yii::app()->user->loginRequired();
		}
		
		//else if(!Yii::app()->user->isGuest && $request=="site/logout")
		//{
			
			
			//echo $app->controller->route;	
		//}
		//else
		//{
			//if($request==$app->defaultController || $request=="")
			//{
				//$app->request->redirect(Yii::app()->user->returnUrl);
			//}
		//}
	}
}
?>