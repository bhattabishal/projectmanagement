<?php
 
class ProgressBar
{
    private $key;
 
    private $running;
    private $total;
    private $done;
 
    public function __construct($key)
    {
        $this->key = $key;
    }
 
    public function start($total)
    {
        $this->running = 1;
        $this->done = 0;
        $this->total = $total;
        $this->put();
    }
 
    public function stop()
    {
        $this->running = 0;
        $this->put();
    }
 
    public function inc($step=1)
    {
        $this->done += $step;
        $this->put();
    }
 
    public function put()
    {
        $ret = Yii::app()->cache->set($this->key, array('running'=>$this->running, 'total'=>$this->total, 'done'=>$this->done));
    }
 
    public static function get($key)
    {
        $test = 1;
        //$test = 0;
        if ($test)
        {
            $data = Yii::app()->cache->get($key);
            if($data === false) 
            {
                $data = array('running'=>1, 'total'=>119, 'done'=>0);
            }
 
            $data['done'] = $data['done'] + 10;
 
            if ($data['done'] > 119) 
            {
                $data['running'] = 0;
            }
 
            Yii::app()->cache->set($key, $data, 1*60);
            return $data;               
        }
 
        $data = Yii::app()->cache->get($key);
        if($data === false)
        {
            $data = array('running' =>1, 'total'=>100, 'done'=>0);
        }
        return $data;       
    }
 
 
}
?>