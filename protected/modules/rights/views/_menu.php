<?php 
$this->widget('zii.widgets.CMenu', array(
	'firstItemCssClass'=>'first',
	'lastItemCssClass'=>'last',
	'htmlOptions'=>array('class'=>'actions'),
	'items'=>array(
		array(
			'label'=>Rights::t('core', 'Assignments'),
			'url'=>array('assignment/view'),
			'linkOptions'=>array('class'=>'btn'),
			'itemOptions'=>array('class'=>'item-assignments'),
			'openButtonHtmlOptions'=>array('class'=>'btn',),
		),
		array(
			'label'=>Rights::t('core', 'Permissions'),
			'url'=>array('authItem/permissions'),
			'linkOptions'=>array('class'=>'btn'),
			'itemOptions'=>array('class'=>'item-permissions'),
		),
		array(
			'label'=>Rights::t('core', 'Roles'),
			'url'=>array('authItem/roles'),
			'linkOptions'=>array('class'=>'btn'),
			'itemOptions'=>array('class'=>'item-roles'),
		),
		array(
			'label'=>Rights::t('core', 'Tasks'),
			'url'=>array('authItem/tasks'),
			'linkOptions'=>array('class'=>'btn'),
			'itemOptions'=>array('class'=>'item-tasks'),
		),
		array(
			'label'=>Rights::t('core', 'Operations'),
			'url'=>array('authItem/operations'),
			'linkOptions'=>array('class'=>'btn'),
			'itemOptions'=>array('class'=>'item-operations'),
		),
	)
));	
?>