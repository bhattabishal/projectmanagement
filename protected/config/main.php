<?php

// uncomment the following to define a path alias
 Yii::setPathOfAlias('local','path/to/local-folder');
 Yii::setPathOfAlias('ecalendarview', dirname(__FILE__) . '/../extensions/ecalendarview');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Project Management',
	
	'defaultController'=>'site/login',
	//'theme'=>'bootstrap',
	//for required login in all pages
		'behaviors' => array(
	    'onBeginRequest' => array(
	        'class' => 'application.components.RequireLogin'
	    )
	),

	// preloading 'log' component
	'preload'=>array('log','bootstrap'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'ext.components.database.*',
		'ext.quickdlgs.*',
		//'ext.quickdlgs1.2.*',
		'ext.emenu.*',
		'ext.dynamictabularform.*',
		'ext.helpers.XHtml',
		'application.modules.rights.*', 
		'application.modules.rights.components.*',
		'ext.yii-mail.YiiMailMessage',
		//'ext.bootstrap-theme.widgets.*',
       // 'ext.bootstrap-theme.helpers.*',
        //'ext.bootstrap-theme.behaviors.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		//'apcinfo',//apc advanced php cache
		'wdcalendar'    => array( 
									//'admin' => 'install' 
									'embed' => true,
									'wd_options' => array(  
                                    	'view' => 'week',
										'weekstartday'=>'1',//1-monday
                                    	//'readonly' => 'JS:true' // execute JS
                                		) ,
								),
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'magnus',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
			'generatorPaths' => array(
                            'bootstrap.gii'
                         ),
		),
		'backup'=> array(
		//'path' => __DIR__.'/../_backup/'  
		),
		'rights'=>array(
			'superuserName'=>'Admin', // Name of the role with super user privileges. 
			'authenticatedName'=>'Authenticated', // Name of the authenticated user role. 
			'userIdColumn'=>'user_id', // Name of the user id column in the database. 
			'userNameColumn'=>'user_name', // Name of the user name column in the database. 
			'enableBizRule'=>true, // Whether to enable authorization item business rules. 
			'enableBizRuleData'=>false, // Whether to enable data for business rules. 
			'displayDescription'=>true, // Whether to use item description instead of name. 
			'flashSuccessKey'=>'RightsSuccess', // Key to use for setting success flash messages. 
			'flashErrorKey'=>'RightsError', // Key to use for setting error flash messages. 
			'install'=>true, // Whether to install rights. 
			'baseUrl'=>'/rights', // Base URL for Rights. Change if module is nested. 
			'layout'=>'rights.views.layouts.main', // Layout to use for displaying Rights. 
			//'appLayout'=>'application.views.layouts.main', // Application layout. 
			'cssFile'=>'/css/customright.css', // Style sheet file to use for Rights. 
			'install'=>false, // Whether to enable installer. 
			'debug'=>false, // Whether to enable debug mode.
		),
		
	),

	// application components
	'components'=>array(
			'background' => array(
	        'class' => 'ext.backjob.EBackJob',
	 
	        // All other configuration options are optional:
	 
	        'checkAndCreateTable' => true,  // creates table if it doesn't exist
	        'useDb' => true,    // Use a database table
	        'useCache' => true, // Use the cache
	        'db' => 'db',    // Database component name to use
	        'ch' => 'cache', // Cache component name to use
	        'tableName' => 'e_background_job', // Name of DB table used
	        'cachePrefix' => 'EBackJobPrefix-',  // Prefix used in the cache
	        'errorTimeout' => 60, // Nr of seconds after which ALL requests time out, measured from the last update.
	        'userAgent' => 'Mozilla/5.0 Firefox/3.6.12' // Useragent used for the background request
	    ),
		
		'bootstrap' => array(
            'class' => 'ext.bootstrap.components.Bootstrap',
			'responsiveCss' => true,
        ),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'class'=>'RWebUser',
		),
		'session' => array(
                   'timeout' => 60000000000,
                   //'cookieMode' =>'only',
                   //'cookieParams' => array('secure' => false, 'httponly' => false),
				   'class' => 'CDbHttpSession',
				  //'class' => 'CCacheHttpSession',
                ),
				//'cache' => array(
           // 'class' => 'CApcCache',
        //),
		'authManager'=>array(
		'class'=>'RDbAuthManager',
		'connectionID'=>'db',
		'itemTable'=>'pm_auth_item',
		'itemChildTable'=>'pm_auth_item_child',
		'assignmentTable'=>'pm_auth_assignment',
		),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			//'class' => 'ext.components.language.XUrlManager',
			'urlFormat'=>'path',
			//'showScriptName'=>true,
			//'appendParams'=>false,
			'rules'=>array(
                //'pattern1'=>'route1',
                //'pattern2'=>'route2',
                //'pattern3'=>'route3',
				//'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				//'<controller:\w+>/<action:\w+>/<id:\-?\d+>' => '<controller>/<action>',
               // '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
               // '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            ),
		),
		
		/*'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			//'class'             => 'CDbConnection',
			'class'=>'application.extensions.PHPPDO.CPdoDbConnection',
            'pdoClass' => 'PHPPDO',
			'connectionString' => 'mysql:host=localhost;dbname=pm_outsource',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
			'enableProfiling'=>true,//log query
        	'enableParamLogging' => true,//log query
			//'schemaCachingDuration' => 180,
			//'connectionString' => 'mysql:host=localhost;dbname=magnusc_pmaus',
			////'emulatePrepare' => true,
			//'username' => 'magnusc_pmaus',
			//'password' => '@G[VaH[gWytw',
			
			//'connectionString' => 'mysql:host=localhost;dbname=enravel_pmaus',
			//'emulatePrepare' => true,
			//'username' => 'enravel_pmaus',
			//'password' => '8h]d?##!x$hV',
			
			//'connectionString' => 'mysql:host=localhost;dbname=pmoutsource',
			//'emulatePrepare' => true,
			//'username' => 'root',
			//'password' => 'admin@mcgprj',
			
			//'connectionString' => 'mysql:host=localhost;dbname=enravel_test_pmaus',
			//'emulatePrepare' => true,
			//'username' => 'enravel_pmaus',
			//'password' => '8h]d?##!x$hV',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'clientScript' => array(
			'class'   =>   'ext.nls.NLSClientScript' , 
			'scriptMap' => array(
				'jquery-ui.css'=> dirname($_SERVER['SCRIPT_NAME']).'/css/jui/custom/jquery-ui.css',
			),
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'trace, info, error, warning',
					'categories' => 'system.db.CDbCommand',
        			'logFile' => 'db.log',
				),
				
				// uncomment the following to show log messages on web pages
				
				array(
					'class'=>'CWebLogRoute',
					'levels'=>'trace, info, error, warning',
					'categories' => 'system.db.CDbCommand',
        			
				),
				
			),
		),
		'mail' => array(
                'class' => 'ext.yii-mail.YiiMail',
                'transportType'=>'smtp',
                'transportOptions'=>array(
                        //'host'=>'mail.nbhit.com',
                        'host'=>'mail.nbhit.com',
                        //'host'=>'smtp.gmail.com',
                        'username'=>'bishalbhatta@magnus.com.np',
                        'password'=>'Bishal123',
                         'encryption'=>'ssl',
                        'port'=>'465',                         
                ),
                'viewPath' => 'application.views.mail',
				'logging' => true,
        'dryRun' => false
             
        ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);