<?php
/**
 *## EditableSaver class file.
 *
 * @author Vitaliy Potapov <noginsk@rambler.ru>
 * @link https://github.com/vitalets/x-editable-yii
 * @copyright Copyright &copy; Vitaliy Potapov 2012
 * @version 1.1.0
 */

/**
 * EditableSaver helps to update model by editable widget submit request.
 *
 * @package booster.widgets.supplementary
*/

class MyCommentsTbEditableSaver extends CComponent
{
	/**
	 * scenarion used in model for update
	 *
	 * @var mixed
	 */
	public $scenario = 'editable';

	/**
	 * name of model
	 *
	 * @var mixed
	 */
	public $modelClass;
	/**
	 * primaryKey value
	 *
	 * @var mixed
	 */
	public $primaryKey;
	/**
	 * name of attribute to be updated
	 *
	 * @var mixed
	 */
	public $attribute;
	/**
	 * model instance
	 *
	 * @var CActiveRecord
	 */
	public $model;

	/**
	 * @var mixed new value of attribute
	 */
	public $value;

	/**
	 * http status code returned in case of error
	 */
	public $errorHttpCode = 400;

	/**
	 * name of changed attributes. Used when saving model
	 *
	 * @var mixed
	 */
	protected $changedAttributes = array();
	
	protected $oldModel;

	/**
	 *### ._construct()
	 *
	 * Constructor
	 *
	 * @param $modelClass
	 *
	 * @throws CException
	 * @internal param mixed $modelName
	 * @return \TbEditableSaver
	 */
	public function __construct($modelClass)
	{
		if (empty($modelClass)) {
			throw new CException(Yii::t(
				'MyCommentsTbEditableSaver.editable',
				'You should provide modelClass in constructor of EditableSaver.'
			));
		}

		$this->modelClass = $modelClass;

		//for non-namespaced models do ucfirst (for backwards compability)
		//see https://github.com/vitalets/x-editable-yii/issues/9
		if (strpos($this->modelClass, '\\') === false) {
			$this->modelClass = ucfirst($this->modelClass);
		}
	}

	/**
	 *### .update()
	 *
	 * main function called to update column in database
	 *
	 */
	public function insert()
	{
		
		
		
		//get params from request
		$this->primaryKey = yii::app()->request->getParam('pk');
		$this->attribute = yii::app()->request->getParam('name');
		//$this->attribute = yii::app()->request->getParam('comments');
		$this->value = yii::app()->request->getParam('value');

		//checking params
		if (empty($this->attribute)) {
			throw new CException(Yii::t('MyCommentsTbEditableSaver.editable', 'Property "attribute" should be defined.'));
		}
		if (empty($this->primaryKey)) {
			throw new CException(Yii::t('MyCommentsTbEditableSaver.editable', 'Property "primaryKey" should be defined.'));
		}

		//loading model
		
		$model=new ProjectComments();
		
		$model->isNewRecord=TRUE;

		//setting new value
		$model->comments=$this->value;
		$model->project_id=$this->primaryKey;
		
		//also set update details,added by bishal
		$model->crtd_by=Yii::app()->user->id;
		$model->crtd_dt = date('Y-m-d');
		
		
		
		if ($model->save()) {
			
		} else {
			$this->error(Yii::t('TbEditableSaver.editable', 'Error while saving record!'));
		}
	}

	public function error($msg)
	{
		throw new CHttpException($this->errorHttpCode, $msg);
	}
}
