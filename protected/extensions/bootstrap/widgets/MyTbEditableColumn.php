<?php
/**
 *## EditableColumn class file.
 *
 * @author Vitaliy Potapov <noginsk@rambler.ru>
 * @link https://github.com/vitalets/x-editable-yii
 * @copyright Copyright &copy; Vitaliy Potapov 2012
 * @version 1.1.0
 */

Yii::import('bootstrap.widgets.TbEditableField');
Yii::import('bootstrap.widgets.TbDataColumn');

/**
 *## EditableColumn widget makes editable one column in CGridView.
 *
 * @package booster.widgets.grids.columns
*/
class MyTbEditableColumn extends TbDataColumn
{
	/**
	 * @var array editable config options.
	 * @see EditableField config
	 */
	public $editable = array();

	//flag to render client script only once for all column cells
	private $_isScriptRendered = false;
	
	public $is_ao=FALSE;

	/**
	 *### .init()
	 *
	 * Widget initialization
	 */
	public function init()
	{
		if (!$this->grid->dataProvider instanceOf CActiveDataProvider) {
			throw new CException('EditableColumn can be applied only to grid based on CActiveDataProvider');
		}
		if (!$this->name) {
			throw new CException('You should provide name for EditableColumn');
		}

		parent::init();

		//need to attach ajaxUpdate handler to refresh editables on pagination and sort
		//should be here, before render of grid js
		$this->attachAjaxUpdateEvent();
	}

	/**
	 *### .renderDataCellContent()
	 */
	protected function renderDataCellContent($row, $data)
	{
		$options = CMap::mergeArray(
			$this->editable,
			array(
				'model' => $data,
				'attribute' => $this->name,
				'parentid' => $this->grid->id,
			)
		);

		//if value defined for column --> use it as element text
		if (strlen($this->value)) {
			ob_start();
			parent::renderDataCellContent($row, $data);
			$text = ob_get_clean();
			$options['text'] = $text;
			$options['encode'] = false;
		}

		/** @var $widget TbEditableField */
		$widget = $this->grid->controller->createWidget('TbEditableField', $options);

		//if editable not applied --> render original text
		if (!$widget->apply) {
			if (isset($text)) {
				echo $text;
			} else {
				parent::renderDataCellContent($row, $data);
			}
			return;
		}

		//manually make selector non unique to match all cells in column
		$selector = str_replace('\\', '_',get_class($widget->model)) . '_' . $widget->attribute;
		$widget->htmlOptions['rel'] = $selector;

		
		//added by bishal
		//to display date and username
		//if($data->project_comments != "")
		//{
			//echo $name=$data->updtUser->user_name."(";
			
			//echo $data->updt_dt==""?$data->crtd_dt:$data->updt_dt;
			//echo ")<br/>";
		//}
		
		$pid=$data->project_id;
		
		$type=18;//rps
		
		if($this->is_ao)
		{
			$type=14;//ao
		}
		
		$project_comments=Yii::app()->db->createCommand()
    			->select('pm_project_comments.comments,pm_user.user_name,pm_project_comments.crtd_dt')
    			->from('pm_project_comments')
			    ->join('pm_user', 'pm_project_comments.crtd_by = pm_user.user_id')
				->join('pm_branch', 'pm_user.branch_id = pm_branch.branch_id')
				->join('pm_code_value', 'pm_branch.branch_type = pm_code_value.code_id')
			    ->where("pm_project_comments.project_id = $pid and pm_code_value.code_id=$type")
				->order('comment_id DESC')
				->limit(1)
			    ->queryRow();
				
			
				if($project_comments['comments'] == "")
				{
					$widget->text="";
				}
				else{
					$widget->text=$project_comments['comments'];
					echo $project_comments['user_name']."(";
			
					echo $project_comments['crtd_dt'];
					echo ")<br/>";
				}
		
		
		//end added by bishal
		
		//can't call run() as it registers clientScript
		
		//added by bishal,link based on ao or rps
		$user_type=User::model()->getAoRpsUser(Yii::app()->user->id);
		if($this->is_ao && $user_type=="AO")
		{
			$widget->renderLink();
		}
		else if(!$this->is_ao && $user_type=="RPS")
		{
			$widget->renderLink();
		}
		else{
			echo $widget->text;
		}
		

		//manually render client script (one for all cells in column)
		if (!$this->_isScriptRendered) {
			$script = $widget->registerClientScript();
			//use parent() as grid is totally replaced by new content
			Yii::app()->getClientScript()->registerScript(
				__CLASS__ . '#' . $this->grid->id . $selector . '-event',
				'
							   $("#' . $this->grid->id . '").parent().on("ajaxUpdate.yiiGridView", "#' . $this->grid->id . '", function() {' . $script . '});
            '
			);
			$this->_isScriptRendered = true;
		}
	}
	
	
	/**
	 *### .attachAjaxUpdateEvent()
	 *
	 * Yii yet does not support custom js events in widgets.
	 * So we need to invoke it manually to ensure update of editables on grid ajax update.
	 *
	 * issue in Yii github: https://github.com/yiisoft/yii/issues/1313
	 *
	 */
	protected function attachAjaxUpdateEvent()
	{
		$trigger = '$("#"+id).trigger("ajaxUpdate.yiiGridView");';

		//check if trigger already inserted by another column
		if (strpos($this->grid->afterAjaxUpdate, $trigger) !== false) {
			return;
		}

		//inserting trigger
		if (strlen($this->grid->afterAjaxUpdate)) {
			$orig = $this->grid->afterAjaxUpdate;
			if (strpos($orig, 'js:') === 0) {
				$orig = substr($orig, 3);
			}
			$orig = "\n($orig).apply(this, arguments);";
		} else {
			$orig = '';
		}
		$this->grid->afterAjaxUpdate = "js: function(id, data) {
            $trigger $orig
        }";
	}
}
