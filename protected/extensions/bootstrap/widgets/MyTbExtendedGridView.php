<?php
Yii :: import ( 'bootstrap.widgets.TbExtendedGridView' );

class MyTbExtendedGridView extends TbExtendedGridView
{
public function init (){
if (( $model = $this -> filter ) instanceof CActiveRecord ){
             //generate unique id
$id =(isset( $_GET [ 'id' ])   && is_string ( $_GET [ 'id' ]))? $_GET [ 'id' ]: '' ;
$unique = get_class ( $model ). $id ;
$this -> id = 'grid' . $unique ;
             //filter - default css class - break design
$this -> filterCssClass = /*'filter */ 'filter' . $unique ;
             //pagination pager - default css class - break design
$this -> pagerCssClass = /*'pagination pager */ 'pager' . $unique ;
$this -> extendedSummaryCssClass = 'extended-summary extended-summary' . $unique ;
         }
parent :: init ();
     }
} 
?>