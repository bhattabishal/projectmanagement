<?php
Yii::import('zii.widgets.jui.CJuiDatePicker');

class EDatePicker extends CJuiDatePicker
{
/**
* Run this widget.
* This method registers necessary javascript and renders the needed HTML code.
*/
public function run()
{
list($name,$id)=$this->resolveNameID();

if(isset($this->htmlOptions['id']))
$id=$this->htmlOptions['id'];
else
$this->htmlOptions['id']=$id;
if(isset($this->htmlOptions['name']))
$name=$this->htmlOptions['name'];
else
$this->htmlOptions['name']=$name;

if($this->hasModel())
echo EHtml::activeTextField($this->model,$this->attribute,$this->htmlOptions);
else
echo EHtml::textField($name,$this->value,$this->htmlOptions);

$options=CJavaScript::encode($this->options);

$js = "jQuery('#{$id}').datepicker($options);";

if (isset($this->language)){
$this->registerScriptFile($this->i18nScriptFile);
$js .= "\njQuery('#{$id}').datepicker('option', jQuery.datepicker.regional['{$this->language}']);";
}
Yii::app()->getClientScript()->registerScript(__CLASS__.'#'.$id, $js);
}
}
?>