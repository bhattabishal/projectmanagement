<?php

/**
 * Allows the user to create tabular forms with a javascript "+" button
 * uses partial views for each row
 *
 * @author Ezekiel Fernandez <ezekiel_p_fernandez@yahoo.com>
 */
class DynamicTabularForm extends CActiveForm {

    const UPDATE_TYPE_CREATE = 'create';
    const UPDATE_TYPE_DELETE = 'delete';
    const UPDATE_TYPE_UPDATE = 'update';

    /**
     *
     * @var string url of the ajax render partial 
     */
    public $rowUrl1;
	public $rowUrl2;
	public $rowUrl3;
	public $rowUrl4;
	public $rowUrl5;
	public $rowUrl6;
	public $rowUrl7;
	public $rowUrl8;
	public $rowUrl9;
	public $rowUrl10;
	public $rowUrl11;
	public $rowUrl12;
	public $rowUrl13;
	public $rowUrl14;
	public $rowUrl15;

    /**
     *
     * @var string view file that is going to be used for initialization
     */
    public $defaultRowView1 = '_rowForm1';
	public $defaultRowView2 = '_rowForm2';
	public $defaultRowView3 = '_rowForm3';
	public $defaultRowView4 = '_rowForm4';
    public $defaultRowView5 = '_rowForm5';
	public $defaultRowView6 = '_rowForm6';
	public $defaultRowView7 = '_rowForm7';
	public $defaultRowView8 = '_rowForm8';
	public $defaultRowView9 = '_rowForm9';
	public $defaultRowView10 = '_rowForm10';
	public $defaultRowView11 = '_rowForm11';
	public $defaultRowView12 = '_rowForm12';
	public $defaultRowView13 = '_rowForm13';
	public $defaultRowView14 = '_rowForm14';
	public $defaultRowView15 = '_rowForm15';
    
    
    public $rowViewCounter1 = 0;
	public $rowViewCounter2 = 0;
	public $rowViewCounter3 = 0;
	public $rowViewCounter4 = 0;
	public $rowViewCounter5 = 0;
	public $rowViewCounter6 = 0;
	public $rowViewCounter7 = 0;
	public $rowViewCounter8 = 0;
	public $rowViewCounter9 = 0;
	public $rowViewCounter10 = 0;
	public $rowViewCounter11 = 0;
	public $rowViewCounter12 = 0;
	public $rowViewCounter13 = 0;
	public $rowViewCounter14 = 0;
	public $rowViewCounter15 = 0;

    public function init() {
        parent::init();
        if ($this->rowUrl1 == null)
            $this->rowUrl1 = $this->controller->createUrl('getRowForm1');
			
		if ($this->rowUrl2 == null)
            $this->rowUrl2 = $this->controller->createUrl('getRowForm2');
			
		if ($this->rowUrl3 == null)
            $this->rowUrl3 = $this->controller->createUrl('getRowForm3');
			
		if ($this->rowUrl4 == null)
            $this->rowUrl4 = $this->controller->createUrl('getRowForm4');
			
		if ($this->rowUrl5 == null)
            $this->rowUrl5 = $this->controller->createUrl('getRowForm5');
			
		if ($this->rowUrl6 == null)
            $this->rowUrl6 = $this->controller->createUrl('getRowForm6');
			
		if ($this->rowUrl7 == null)
            $this->rowUrl7 = $this->controller->createUrl('getRowForm7');
			
		if ($this->rowUrl8 == null)
            $this->rowUrl8 = $this->controller->createUrl('getRowForm8');
			
		if ($this->rowUrl9 == null)
            $this->rowUrl9 = $this->controller->createUrl('getRowForm9');
			
		if ($this->rowUrl10 == null)
            $this->rowUrl10 = $this->controller->createUrl('getRowForm10');
			
		if ($this->rowUrl11 == null)
            $this->rowUrl11 = $this->controller->createUrl('getRowForm11');
		
		if ($this->rowUrl12 == null)
            $this->rowUrl12 = $this->controller->createUrl('getRowForm12');
			
		if ($this->rowUrl13 == null)
            $this->rowUrl13 = $this->controller->createUrl('getRowForm13');
		
		if ($this->rowUrl14 == null)
            $this->rowUrl14 = $this->controller->createUrl('getRowForm14');
		
		if ($this->rowUrl15 == null)
            $this->rowUrl15 = $this->controller->createUrl('getRowForm15');
		
		
    }

    /**
     * generates the Initial row and the "+" button 
     * @param array $models the array of models that will be used
     * @param array $htmlOptions 
     */
    public function updateTypeField($model, $key, $attribute, $htmlOptions = array()) {
        if ($model->isNewRecord)
            $model->{$attribute} = self::UPDATE_TYPE_CREATE;
        else
            $model->{$attribute} = self::UPDATE_TYPE_UPDATE;

        $htmlOptions = array_merge($htmlOptions, array('id' => get_class($model) . '_upateType_' . $htmlOptions['key']));

        return parent::hiddenField($model, "[$key]".$attribute, $htmlOptions);
    }

    /** 
     * @param CModel[] $models 
     * @param array $htmlOptions
     */
   public function rowForm1($models = array(), $rowView=null, $htmlOptions = array()) {
        if($rowView==null)
            $rowView = $this->defaultRowView1;
        
        $htmlOptions = array_merge(array('id' => 'row1-' . $this->rowViewCounter1), $htmlOptions);
        $id = $htmlOptions['id'];

        echo CHtml::openTag('div', $htmlOptions);

        foreach ($models as $key => $model) {
            $this->controller->renderPartial($rowView, array('key' => $key, 'model' => $model, 'form' => $this));
        }
        echo "</div>";

        $buttonId = 'addButton1-' . $this->rowViewCounter1;
        echo CHtml::button('+', array(
            'id' => $buttonId,
        ));
        $cs = Yii::app()->clientScript;
        $cs->registerScript("DynamicForm1", "
            var counter = " . sizeof($models) . ";
            function addRow1(){
                counter = counter + 1;
                $.ajax({
                    url:'" . $this->rowUrl1 . "',
                    data:{
                        key:counter,
                    },
                    success:function(data){appendRow1(data)},
                });
            }
            function appendRow1(html){
               $('#" . $id . "').append(html);
            }
            //for adding rows
            $('#" . $buttonId . "').click(function(e){addRow1()});
            
            //for deleting rows
            $('.delete-row-button1').live('click',function(e){
                var key = $(this).attr('data-key');
                var row_id = $(this).attr('data-delete');
                var updateTypeField = $('#'+ '" . get_class($model) . '_upateType_' . "'+key);
                
                //this indicates that the row is a new entry and therefore can be removed
                //immediately from the HTML body
                if(updateTypeField.val() == '" . self::UPDATE_TYPE_CREATE . "'){
                    $('#'+row_id).remove();
                }
                //this indicates that the row is to be deleted here in the HTML
                // body and also from the database and therefore we will temporarily
                //hide the row and change the update type to delete to determine
                //what items in the controller are to be deleted
                else{
                    updateTypeField.val('" . self::UPDATE_TYPE_DELETE . "');
                    $('#'+row_id).hide();
                }
            });
             
        ");
        $this->rowViewCounter1 = $this->rowViewCounter1 + 1;
    }
	
	
	public function rowForm2($models = array(), $rowView=null, $htmlOptions = array()) {
        if($rowView==null)
            $rowView = $this->defaultRowView2;
        
        $htmlOptions = array_merge(array('id' => 'row2-' . $this->rowViewCounter2), $htmlOptions);
        $id = $htmlOptions['id'];

        echo CHtml::openTag('div', $htmlOptions);

        foreach ($models as $key => $model) {
            $this->controller->renderPartial($rowView, array('key' => $key, 'model' => $model, 'form' => $this));
        }
        echo "</div>";

        $buttonId = 'addButton2-' . $this->rowViewCounter2;
        echo CHtml::button('+', array(
            'id' => $buttonId,
        ));
        $cs = Yii::app()->clientScript;
        $cs->registerScript("DynamicForm2", "
            var counter = " . sizeof($models) . ";
            function addRow2(){
                counter = counter + 1;
                $.ajax({
                    url:'" . $this->rowUrl2 . "',
                    data:{
                        key:counter,
                    },
                    success:function(data){appendRow2(data)},
                });
            }
            function appendRow2(html){
               $('#" . $id . "').append(html);
            }
            //for adding rows
            $('#" . $buttonId . "').click(function(e){addRow2()});
            
            //for deleting rows
            $('.delete-row-button2').live('click',function(e){
                var key = $(this).attr('data-key');
                var row_id = $(this).attr('data-delete');
                var updateTypeField = $('#'+ '" . get_class($model) . '_upateType_' . "'+key);
                
                //this indicates that the row is a new entry and therefore can be removed
                //immediately from the HTML body
                if(updateTypeField.val() == '" . self::UPDATE_TYPE_CREATE . "'){
                    $('#'+row_id).remove();
                }
                //this indicates that the row is to be deleted here in the HTML
                // body and also from the database and therefore we will temporarily
                //hide the row and change the update type to delete to determine
                //what items in the controller are to be deleted
                else{
                    updateTypeField.val('" . self::UPDATE_TYPE_DELETE . "');
                    $('#'+row_id).hide();
                }
            });
             
        ");
        $this->rowViewCounter2 = $this->rowViewCounter2 + 1;
    }
	
	
	
	public function rowForm3($models = array(), $rowView=null, $htmlOptions = array()) {
        if($rowView==null)
            $rowView = $this->defaultRowView3;
        
        $htmlOptions = array_merge(array('id' => 'row3-' . $this->rowViewCounter3), $htmlOptions);
        $id = $htmlOptions['id'];

        echo CHtml::openTag('div', $htmlOptions);

        foreach ($models as $key => $model) {
            $this->controller->renderPartial($rowView, array('key' => $key, 'model' => $model, 'form' => $this));
        }
        echo "</div>";

        $buttonId = 'addButton3-' . $this->rowViewCounter3;
        echo CHtml::button('+', array(
            'id' => $buttonId,
        ));
        $cs = Yii::app()->clientScript;
        $cs->registerScript("DynamicForm3", "
            var counter = " . sizeof($models) . ";
            function addRow3(){
                counter = counter + 1;
                $.ajax({
                    url:'" . $this->rowUrl3 . "',
                    data:{
                        key:counter,
                    },
                    success:function(data){appendRow3(data)},
                });
            }
            function appendRow3(html){
               $('#" . $id . "').append(html);
            }
            //for adding rows
            $('#" . $buttonId . "').click(function(e){addRow3()});
            
            //for deleting rows
            $('.delete-row-button3').live('click',function(e){
                var key = $(this).attr('data-key');
                var row_id = $(this).attr('data-delete');
                var updateTypeField = $('#'+ '" . get_class($model) . '_upateType_' . "'+key);
                
                //this indicates that the row is a new entry and therefore can be removed
                //immediately from the HTML body
                if(updateTypeField.val() == '" . self::UPDATE_TYPE_CREATE . "'){
                    $('#'+row_id).remove();
                }
                //this indicates that the row is to be deleted here in the HTML
                // body and also from the database and therefore we will temporarily
                //hide the row and change the update type to delete to determine
                //what items in the controller are to be deleted
                else{
                    updateTypeField.val('" . self::UPDATE_TYPE_DELETE . "');
                    $('#'+row_id).hide();
                }
            });
             
        ");
        $this->rowViewCounter3 = $this->rowViewCounter3 + 1;
    }
	
	
	public function rowForm4($models = array(), $rowView=null, $htmlOptions = array()) {
        if($rowView==null)
            $rowView = $this->defaultRowView4;
        
        $htmlOptions = array_merge(array('id' => 'row4-' . $this->rowViewCounter4), $htmlOptions);
        $id = $htmlOptions['id'];

        echo CHtml::openTag('div', $htmlOptions);

        foreach ($models as $key => $model) {
            $this->controller->renderPartial($rowView, array('key' => $key, 'model' => $model, 'form' => $this));
        }
        echo "</div>";

        $buttonId = 'addButton4-' . $this->rowViewCounter4;
        echo CHtml::button('+', array(
            'id' => $buttonId,
        ));
        $cs = Yii::app()->clientScript;
        $cs->registerScript("DynamicForm4", "
            var counter = " . sizeof($models) . ";
            function addRow4(){
                counter = counter + 1;
                $.ajax({
                    url:'" . $this->rowUrl4 . "',
                    data:{
                        key:counter,
                    },
                    success:function(data){appendRow4(data)},
                });
            }
            function appendRow4(html){
               $('#" . $id . "').append(html);
            }
            //for adding rows
            $('#" . $buttonId . "').click(function(e){addRow4()});
            
            //for deleting rows
            $('.delete-row-button4').live('click',function(e){
                var key = $(this).attr('data-key');
                var row_id = $(this).attr('data-delete');
                var updateTypeField = $('#'+ '" . get_class($model) . '_upateType_' . "'+key);
                
                //this indicates that the row is a new entry and therefore can be removed
                //immediately from the HTML body
                if(updateTypeField.val() == '" . self::UPDATE_TYPE_CREATE . "'){
                    $('#'+row_id).remove();
                }
                //this indicates that the row is to be deleted here in the HTML
                // body and also from the database and therefore we will temporarily
                //hide the row and change the update type to delete to determine
                //what items in the controller are to be deleted
                else{
                    updateTypeField.val('" . self::UPDATE_TYPE_DELETE . "');
                    $('#'+row_id).hide();
                }
            });
             
        ");
        $this->rowViewCounter4 = $this->rowViewCounter4 + 1;
    }
	
	public function rowForm5($models = array(), $rowView=null, $htmlOptions = array()) {
        if($rowView==null)
            $rowView = $this->defaultRowView5;
        
        $htmlOptions = array_merge(array('id' => 'row5-' . $this->rowViewCounter5), $htmlOptions);
        $id = $htmlOptions['id'];

        echo CHtml::openTag('div', $htmlOptions);

        foreach ($models as $key => $model) {
            $this->controller->renderPartial($rowView, array('key' => $key, 'model' => $model, 'form' => $this));
        }
        echo "</div>";

        $buttonId = 'addButton5-' . $this->rowViewCounter5;
        echo CHtml::button('+', array(
            'id' => $buttonId,
        ));
        $cs = Yii::app()->clientScript;
        $cs->registerScript("DynamicForm5", "
            var counter = " . sizeof($models) . ";
            function addRow5(){
                counter = counter + 1;
                $.ajax({
                    url:'" . $this->rowUrl5 . "',
                    data:{
                        key:counter,
                    },
                    success:function(data){appendRow5(data)},
                });
            }
            function appendRow5(html){
               $('#" . $id . "').append(html);
            }
            //for adding rows
            $('#" . $buttonId . "').click(function(e){addRow5()});
            
            //for deleting rows
            $('.delete-row-button5').live('click',function(e){
                var key = $(this).attr('data-key');
                var row_id = $(this).attr('data-delete');
                var updateTypeField = $('#'+ '" . get_class($model) . '_upateType_' . "'+key);
                
                //this indicates that the row is a new entry and therefore can be removed
                //immediately from the HTML body
                if(updateTypeField.val() == '" . self::UPDATE_TYPE_CREATE . "'){
                    $('#'+row_id).remove();
                }
                //this indicates that the row is to be deleted here in the HTML
                // body and also from the database and therefore we will temporarily
                //hide the row and change the update type to delete to determine
                //what items in the controller are to be deleted
                else{
                    updateTypeField.val('" . self::UPDATE_TYPE_DELETE . "');
                    $('#'+row_id).hide();
                }
            });
             
        ");
        $this->rowViewCounter5 = $this->rowViewCounter5 + 1;
    }
	
	
	//6
	public function rowForm6($models = array(), $rowView=null, $htmlOptions = array()) {
        if($rowView==null)
            $rowView = $this->defaultRowView6;
        
        $htmlOptions = array_merge(array('id' => 'row6-' . $this->rowViewCounter5), $htmlOptions);
        $id = $htmlOptions['id'];

        echo CHtml::openTag('div', $htmlOptions);

        foreach ($models as $key => $model) {
            $this->controller->renderPartial($rowView, array('key' => $key, 'model' => $model, 'form' => $this));
        }
        echo "</div>";

        $buttonId = 'addButton6-' . $this->rowViewCounter6;
        echo CHtml::button('+', array(
            'id' => $buttonId,
        ));
        $cs = Yii::app()->clientScript;
        $cs->registerScript("DynamicForm6", "
            var counter = " . sizeof($models) . ";
            function addRow6(){
                counter = counter + 1;
                $.ajax({
                    url:'" . $this->rowUrl6 . "',
                    data:{
                        key:counter,
                    },
                    success:function(data){appendRow6(data)},
                });
            }
            function appendRow6(html){
               $('#" . $id . "').append(html);
            }
            //for adding rows
            $('#" . $buttonId . "').click(function(e){addRow6()});
            
            //for deleting rows
            $('.delete-row-button6').live('click',function(e){
                var key = $(this).attr('data-key');
                var row_id = $(this).attr('data-delete');
                var updateTypeField = $('#'+ '" . get_class($model) . '_upateType_' . "'+key);
                
                //this indicates that the row is a new entry and therefore can be removed
                //immediately from the HTML body
                if(updateTypeField.val() == '" . self::UPDATE_TYPE_CREATE . "'){
                    $('#'+row_id).remove();
                }
                //this indicates that the row is to be deleted here in the HTML
                // body and also from the database and therefore we will temporarily
                //hide the row and change the update type to delete to determine
                //what items in the controller are to be deleted
                else{
                    updateTypeField.val('" . self::UPDATE_TYPE_DELETE . "');
                    $('#'+row_id).hide();
                }
            });
             
        ");
        $this->rowViewCounter6 = $this->rowViewCounter6 + 1;
    }
	
	
	//7
	public function rowForm7($models = array(), $rowView=null, $htmlOptions = array()) {
        if($rowView==null)
            $rowView = $this->defaultRowView7;
        
        $htmlOptions = array_merge(array('id' => 'row7-' . $this->rowViewCounter7), $htmlOptions);
        $id = $htmlOptions['id'];

        echo CHtml::openTag('div', $htmlOptions);

        foreach ($models as $key => $model) {
            $this->controller->renderPartial($rowView, array('key' => $key, 'model' => $model, 'form' => $this));
        }
        echo "</div>";

        $buttonId = 'addButton7-' . $this->rowViewCounter7;
        echo CHtml::button('+', array(
            'id' => $buttonId,
        ));
        $cs = Yii::app()->clientScript;
        $cs->registerScript("DynamicForm7", "
            var counter = " . sizeof($models) . ";
            function addRow7(){
                counter = counter + 1;
                $.ajax({
                    url:'" . $this->rowUrl7 . "',
                    data:{
                        key:counter,
                    },
                    success:function(data){appendRow7(data)},
                });
            }
            function appendRow7(html){
               $('#" . $id . "').append(html);
            }
            //for adding rows
            $('#" . $buttonId . "').click(function(e){addRow7()});
            
            //for deleting rows
            $('.delete-row-button7').live('click',function(e){
                var key = $(this).attr('data-key');
                var row_id = $(this).attr('data-delete');
                var updateTypeField = $('#'+ '" . get_class($model) . '_upateType_' . "'+key);
                
                //this indicates that the row is a new entry and therefore can be removed
                //immediately from the HTML body
                if(updateTypeField.val() == '" . self::UPDATE_TYPE_CREATE . "'){
                    $('#'+row_id).remove();
                }
                //this indicates that the row is to be deleted here in the HTML
                // body and also from the database and therefore we will temporarily
                //hide the row and change the update type to delete to determine
                //what items in the controller are to be deleted
                else{
                    updateTypeField.val('" . self::UPDATE_TYPE_DELETE . "');
                    $('#'+row_id).hide();
                }
            });
             
        ");
        $this->rowViewCounter7 = $this->rowViewCounter7 + 1;
    }
	
	//8
	public function rowForm8($models = array(), $rowView=null, $htmlOptions = array()) {
        if($rowView==null)
            $rowView = $this->defaultRowView8;
        
        $htmlOptions = array_merge(array('id' => 'row8-' . $this->rowViewCounter8), $htmlOptions);
        $id = $htmlOptions['id'];

        echo CHtml::openTag('div', $htmlOptions);

        foreach ($models as $key => $model) {
            $this->controller->renderPartial($rowView, array('key' => $key, 'model' => $model, 'form' => $this));
        }
        echo "</div>";

        $buttonId = 'addButton8-' . $this->rowViewCounter8;
        echo CHtml::button('+', array(
            'id' => $buttonId,
        ));
        $cs = Yii::app()->clientScript;
        $cs->registerScript("DynamicForm8", "
            var counter = " . sizeof($models) . ";
            function addRow8(){
                counter = counter + 1;
                $.ajax({
                    url:'" . $this->rowUrl8 . "',
                    data:{
                        key:counter,
                    },
                    success:function(data){appendRow8(data)},
                });
            }
            function appendRow8(html){
               $('#" . $id . "').append(html);
            }
            //for adding rows
            $('#" . $buttonId . "').click(function(e){addRow8()});
            
            //for deleting rows
            $('.delete-row-button8').live('click',function(e){
                var key = $(this).attr('data-key');
                var row_id = $(this).attr('data-delete');
                var updateTypeField = $('#'+ '" . get_class($model) . '_upateType_' . "'+key);
                
                //this indicates that the row is a new entry and therefore can be removed
                //immediately from the HTML body
                if(updateTypeField.val() == '" . self::UPDATE_TYPE_CREATE . "'){
                    $('#'+row_id).remove();
                }
                //this indicates that the row is to be deleted here in the HTML
                // body and also from the database and therefore we will temporarily
                //hide the row and change the update type to delete to determine
                //what items in the controller are to be deleted
                else{
                    updateTypeField.val('" . self::UPDATE_TYPE_DELETE . "');
                    $('#'+row_id).hide();
                }
            });
             
        ");
        $this->rowViewCounter8 = $this->rowViewCounter8 + 1;
    }
	
	
	//9
	public function rowForm9($models = array(), $rowView=null, $htmlOptions = array()) {
        if($rowView==null)
            $rowView = $this->defaultRowView9;
        
        $htmlOptions = array_merge(array('id' => 'row9-' . $this->rowViewCounter9), $htmlOptions);
        $id = $htmlOptions['id'];

        echo CHtml::openTag('div', $htmlOptions);

        foreach ($models as $key => $model) {
            $this->controller->renderPartial($rowView, array('key' => $key, 'model' => $model, 'form' => $this));
        }
        echo "</div>";

        $buttonId = 'addButton9-' . $this->rowViewCounter9;
        echo CHtml::button('+', array(
            'id' => $buttonId,
        ));
        $cs = Yii::app()->clientScript;
        $cs->registerScript("DynamicForm9", "
            var counter = " . sizeof($models) . ";
            function addRow9(){
                counter = counter + 1;
                $.ajax({
                    url:'" . $this->rowUrl9 . "',
                    data:{
                        key:counter,
                    },
                    success:function(data){appendRow9(data)},
                });
            }
            function appendRow9(html){
               $('#" . $id . "').append(html);
            }
            //for adding rows
            $('#" . $buttonId . "').click(function(e){addRow9()});
            
            //for deleting rows
            $('.delete-row-button9').live('click',function(e){
                var key = $(this).attr('data-key');
                var row_id = $(this).attr('data-delete');
                var updateTypeField = $('#'+ '" . get_class($model) . '_upateType_' . "'+key);
                
                //this indicates that the row is a new entry and therefore can be removed
                //immediately from the HTML body
                if(updateTypeField.val() == '" . self::UPDATE_TYPE_CREATE . "'){
                    $('#'+row_id).remove();
                }
                //this indicates that the row is to be deleted here in the HTML
                // body and also from the database and therefore we will temporarily
                //hide the row and change the update type to delete to determine
                //what items in the controller are to be deleted
                else{
                    updateTypeField.val('" . self::UPDATE_TYPE_DELETE . "');
                    $('#'+row_id).hide();
                }
            });
             
        ");
        $this->rowViewCounter9 = $this->rowViewCounter9 + 1;
    }
	
	//10
	public function rowForm10($models = array(), $rowView=null, $htmlOptions = array()) {
        if($rowView==null)
            $rowView = $this->defaultRowView10;
        
        $htmlOptions = array_merge(array('id' => 'row10-' . $this->rowViewCounter10), $htmlOptions);
        $id = $htmlOptions['id'];

        echo CHtml::openTag('div', $htmlOptions);

        foreach ($models as $key => $model) {
            $this->controller->renderPartial($rowView, array('key' => $key, 'model' => $model, 'form' => $this));
        }
        echo "</div>";

        $buttonId = 'addButton10-' . $this->rowViewCounter9;
        echo CHtml::button('+', array(
            'id' => $buttonId,
        ));
        $cs = Yii::app()->clientScript;
        $cs->registerScript("DynamicForm10", "
            var counter = " . sizeof($models) . ";
            function addRow10(){
                counter = counter + 1;
                $.ajax({
                    url:'" . $this->rowUrl10 . "',
                    data:{
                        key:counter,
                    },
                    success:function(data){appendRow10(data)},
                });
            }
            function appendRow10(html){
               $('#" . $id . "').append(html);
            }
            //for adding rows
            $('#" . $buttonId . "').click(function(e){addRow10()});
            
            //for deleting rows
            $('.delete-row-button10').live('click',function(e){
                var key = $(this).attr('data-key');
                var row_id = $(this).attr('data-delete');
                var updateTypeField = $('#'+ '" . get_class($model) . '_upateType_' . "'+key);
                
                //this indicates that the row is a new entry and therefore can be removed
                //immediately from the HTML body
                if(updateTypeField.val() == '" . self::UPDATE_TYPE_CREATE . "'){
                    $('#'+row_id).remove();
                }
                //this indicates that the row is to be deleted here in the HTML
                // body and also from the database and therefore we will temporarily
                //hide the row and change the update type to delete to determine
                //what items in the controller are to be deleted
                else{
                    updateTypeField.val('" . self::UPDATE_TYPE_DELETE . "');
                    $('#'+row_id).hide();
                }
            });
             
        ");
        $this->rowViewCounter10 = $this->rowViewCounter10 + 1;
    }
	
	//11
	public function rowForm11($models = array(), $rowView=null, $htmlOptions = array()) {
        if($rowView==null)
            $rowView = $this->defaultRowView11;
        
        $htmlOptions = array_merge(array('id' => 'row11-' . $this->rowViewCounter11), $htmlOptions);
        $id = $htmlOptions['id'];

        echo CHtml::openTag('div', $htmlOptions);

        foreach ($models as $key => $model) {
            $this->controller->renderPartial($rowView, array('key' => $key, 'model' => $model, 'form' => $this));
        }
        echo "</div>";

        $buttonId = 'addButton11-' . $this->rowViewCounter9;
        echo CHtml::button('+', array(
            'id' => $buttonId,
        ));
        $cs = Yii::app()->clientScript;
        $cs->registerScript("DynamicForm11", "
            var counter = " . sizeof($models) . ";
            function addRow11(){
                counter = counter + 1;
                $.ajax({
                    url:'" . $this->rowUrl11 . "',
                    data:{
                        key:counter,
                    },
                    success:function(data){appendRow11(data)},
                });
            }
            function appendRow11(html){
               $('#" . $id . "').append(html);
            }
            //for adding rows
            $('#" . $buttonId . "').click(function(e){addRow11()});
            
            //for deleting rows
            $('.delete-row-button11').live('click',function(e){
                var key = $(this).attr('data-key');
                var row_id = $(this).attr('data-delete');
                var updateTypeField = $('#'+ '" . get_class($model) . '_upateType_' . "'+key);
                
                //this indicates that the row is a new entry and therefore can be removed
                //immediately from the HTML body
                if(updateTypeField.val() == '" . self::UPDATE_TYPE_CREATE . "'){
                    $('#'+row_id).remove();
                }
                //this indicates that the row is to be deleted here in the HTML
                // body and also from the database and therefore we will temporarily
                //hide the row and change the update type to delete to determine
                //what items in the controller are to be deleted
                else{
                    updateTypeField.val('" . self::UPDATE_TYPE_DELETE . "');
                    $('#'+row_id).hide();
                }
            });
             
        ");
        $this->rowViewCounter11 = $this->rowViewCounter11 + 1;
    }
	
	//12
	public function rowForm12($models = array(), $rowView=null, $htmlOptions = array()) {
        if($rowView==null)
            $rowView = $this->defaultRowView12;
        
        $htmlOptions = array_merge(array('id' => 'row12-' . $this->rowViewCounter12), $htmlOptions);
        $id = $htmlOptions['id'];

        echo CHtml::openTag('div', $htmlOptions);

        foreach ($models as $key => $model) {
            $this->controller->renderPartial($rowView, array('key' => $key, 'model' => $model, 'form' => $this));
        }
        echo "</div>";

        $buttonId = 'addButton12-' . $this->rowViewCounter12;
        echo CHtml::button('+', array(
            'id' => $buttonId,
        ));
        $cs = Yii::app()->clientScript;
        $cs->registerScript("DynamicForm12", "
            var counter = " . sizeof($models) . ";
            function addRow12(){
                counter = counter + 1;
                $.ajax({
                    url:'" . $this->rowUrl12 . "',
                    data:{
                        key:counter,
                    },
                    success:function(data){appendRow12(data)},
                });
            }
            function appendRow12(html){
               $('#" . $id . "').append(html);
            }
            //for adding rows
            $('#" . $buttonId . "').click(function(e){addRow12()});
            
            //for deleting rows
            $('.delete-row-button12').live('click',function(e){
                var key = $(this).attr('data-key');
                var row_id = $(this).attr('data-delete');
                var updateTypeField = $('#'+ '" . get_class($model) . '_upateType_' . "'+key);
                
                //this indicates that the row is a new entry and therefore can be removed
                //immediately from the HTML body
                if(updateTypeField.val() == '" . self::UPDATE_TYPE_CREATE . "'){
                    $('#'+row_id).remove();
                }
                //this indicates that the row is to be deleted here in the HTML
                // body and also from the database and therefore we will temporarily
                //hide the row and change the update type to delete to determine
                //what items in the controller are to be deleted
                else{
                    updateTypeField.val('" . self::UPDATE_TYPE_DELETE . "');
                    $('#'+row_id).hide();
                }
            });
             
        ");
        $this->rowViewCounter12 = $this->rowViewCounter12 + 1;
    }
	
	
	//13
	public function rowForm13($models = array(), $rowView=null, $htmlOptions = array()) {
        if($rowView==null)
            $rowView = $this->defaultRowView13;
        
        $htmlOptions = array_merge(array('id' => 'row13-' . $this->rowViewCounter13), $htmlOptions);
        $id = $htmlOptions['id'];

        echo CHtml::openTag('div', $htmlOptions);

        foreach ($models as $key => $model) {
            $this->controller->renderPartial($rowView, array('key' => $key, 'model' => $model, 'form' => $this));
        }
        echo "</div>";

        $buttonId = 'addButton13-' . $this->rowViewCounter13;
        echo CHtml::button('+', array(
            'id' => $buttonId,
        ));
        $cs = Yii::app()->clientScript;
        $cs->registerScript("DynamicForm13", "
            var counter = " . sizeof($models) . ";
            function addRow13(){
                counter = counter + 1;
                $.ajax({
                    url:'" . $this->rowUrl13 . "',
                    data:{
                        key:counter,
                    },
                    success:function(data){appendRow13(data)},
                });
            }
            function appendRow13(html){
               $('#" . $id . "').append(html);
            }
            //for adding rows
            $('#" . $buttonId . "').click(function(e){addRow13()});
            
            //for deleting rows
            $('.delete-row-button13').live('click',function(e){
                var key = $(this).attr('data-key');
                var row_id = $(this).attr('data-delete');
                var updateTypeField = $('#'+ '" . get_class($model) . '_upateType_' . "'+key);
                
                //this indicates that the row is a new entry and therefore can be removed
                //immediately from the HTML body
                if(updateTypeField.val() == '" . self::UPDATE_TYPE_CREATE . "'){
                    $('#'+row_id).remove();
                }
                //this indicates that the row is to be deleted here in the HTML
                // body and also from the database and therefore we will temporarily
                //hide the row and change the update type to delete to determine
                //what items in the controller are to be deleted
                else{
                    updateTypeField.val('" . self::UPDATE_TYPE_DELETE . "');
                    $('#'+row_id).hide();
                }
            });
             
        ");
        $this->rowViewCounter13 = $this->rowViewCounter13 + 1;
    }
	
	
	//14
	public function rowForm14($models = array(), $rowView=null, $htmlOptions = array()) {
        if($rowView==null)
            $rowView = $this->defaultRowView14;
        
        $htmlOptions = array_merge(array('id' => 'row14-' . $this->rowViewCounter14), $htmlOptions);
        $id = $htmlOptions['id'];

        echo CHtml::openTag('div', $htmlOptions);

        foreach ($models as $key => $model) {
            $this->controller->renderPartial($rowView, array('key' => $key, 'model' => $model, 'form' => $this));
        }
        echo "</div>";

        $buttonId = 'addButton14-' . $this->rowViewCounter14;
        echo CHtml::button('+', array(
            'id' => $buttonId,
        ));
        $cs = Yii::app()->clientScript;
        $cs->registerScript("DynamicForm14", "
            var counter = " . sizeof($models) . ";
            function addRow14(){
                counter = counter + 1;
                $.ajax({
                    url:'" . $this->rowUrl14 . "',
                    data:{
                        key:counter,
                    },
                    success:function(data){appendRow14(data)},
                });
            }
            function appendRow14(html){
               $('#" . $id . "').append(html);
            }
            //for adding rows
            $('#" . $buttonId . "').click(function(e){addRow14()});
            
            //for deleting rows
            $('.delete-row-button14').live('click',function(e){
                var key = $(this).attr('data-key');
                var row_id = $(this).attr('data-delete');
                var updateTypeField = $('#'+ '" . get_class($model) . '_upateType_' . "'+key);
                
                //this indicates that the row is a new entry and therefore can be removed
                //immediately from the HTML body
                if(updateTypeField.val() == '" . self::UPDATE_TYPE_CREATE . "'){
                    $('#'+row_id).remove();
                }
                //this indicates that the row is to be deleted here in the HTML
                // body and also from the database and therefore we will temporarily
                //hide the row and change the update type to delete to determine
                //what items in the controller are to be deleted
                else{
                    updateTypeField.val('" . self::UPDATE_TYPE_DELETE . "');
                    $('#'+row_id).hide();
                }
            });
             
        ");
        $this->rowViewCounter14 = $this->rowViewCounter14 + 1;
    }
	
	
	//15
	public function rowForm15($models = array(), $rowView=null, $htmlOptions = array()) {
        if($rowView==null)
            $rowView = $this->defaultRowView15;
        
        $htmlOptions = array_merge(array('id' => 'row15-' . $this->rowViewCounter15), $htmlOptions);
        $id = $htmlOptions['id'];

        echo CHtml::openTag('div', $htmlOptions);

        foreach ($models as $key => $model) {
            $this->controller->renderPartial($rowView, array('key' => $key, 'model' => $model, 'form' => $this));
        }
        echo "</div>";

        $buttonId = 'addButton15-' . $this->rowViewCounte15;
        echo CHtml::button('+', array(
            'id' => $buttonId,
        ));
        $cs = Yii::app()->clientScript;
        $cs->registerScript("DynamicForm15", "
            var counter = " . sizeof($models) . ";
            function addRow15(){
                counter = counter + 1;
                $.ajax({
                    url:'" . $this->rowUrl15 . "',
                    data:{
                        key:counter,
                    },
                    success:function(data){appendRow15(data)},
                });
            }
            function appendRow15(html){
               $('#" . $id . "').append(html);
            }
            //for adding rows
            $('#" . $buttonId . "').click(function(e){addRow15()});
            
            //for deleting rows
            $('.delete-row-button15').live('click',function(e){
                var key = $(this).attr('data-key');
                var row_id = $(this).attr('data-delete');
                var updateTypeField = $('#'+ '" . get_class($model) . '_upateType_' . "'+key);
                
                //this indicates that the row is a new entry and therefore can be removed
                //immediately from the HTML body
                if(updateTypeField.val() == '" . self::UPDATE_TYPE_CREATE . "'){
                    $('#'+row_id).remove();
                }
                //this indicates that the row is to be deleted here in the HTML
                // body and also from the database and therefore we will temporarily
                //hide the row and change the update type to delete to determine
                //what items in the controller are to be deleted
                else{
                    updateTypeField.val('" . self::UPDATE_TYPE_DELETE . "');
                    $('#'+row_id).hide();
                }
            });
             
        ");
        $this->rowViewCounter15 = $this->rowViewCounter15 + 1;
    }

    public function deleteRowButton1($row_id, $key, $label='X', $htmlOptions = array()) {
        if(array_key_exists('class', $htmlOptions))
            $htmlOptions['class'] = $htmlOptions['class'] . ' ' . 'delete-row-button';
        else
            $htmlOptions = array_merge($htmlOptions,array('class'=>'delete-row-button1'));
        
        $htmlOptions = array_merge($htmlOptions, array('data-delete' => $row_id, 'data-key' => $key));
        
        echo CHtml::button($label, $htmlOptions);
    }
	
	public function deleteRowButton2($row_id, $key, $label='X', $htmlOptions = array()) {
        if(array_key_exists('class', $htmlOptions))
            $htmlOptions['class'] = $htmlOptions['class'] . ' ' . 'delete-row-button';
        else
            $htmlOptions = array_merge($htmlOptions,array('class'=>'delete-row-button2'));
        
        $htmlOptions = array_merge($htmlOptions, array('data-delete' => $row_id, 'data-key' => $key));
        
        echo CHtml::button($label, $htmlOptions);
    }
	
	
	public function deleteRowButton3($row_id, $key, $label='X', $htmlOptions = array()) {
        if(array_key_exists('class', $htmlOptions))
            $htmlOptions['class'] = $htmlOptions['class'] . ' ' . 'delete-row-button';
        else
            $htmlOptions = array_merge($htmlOptions,array('class'=>'delete-row-button3'));
        
        $htmlOptions = array_merge($htmlOptions, array('data-delete' => $row_id, 'data-key' => $key));
        
        echo CHtml::button($label, $htmlOptions);
    }
	
	
	public function deleteRowButton4($row_id, $key, $label='X', $htmlOptions = array()) {
        if(array_key_exists('class', $htmlOptions))
            $htmlOptions['class'] = $htmlOptions['class'] . ' ' . 'delete-row-button';
        else
            $htmlOptions = array_merge($htmlOptions,array('class'=>'delete-row-button4'));
        
        $htmlOptions = array_merge($htmlOptions, array('data-delete' => $row_id, 'data-key' => $key));
        
        echo CHtml::button($label, $htmlOptions);
    }
	
	
	public function deleteRowButton5($row_id, $key, $label='X', $htmlOptions = array()) {
        if(array_key_exists('class', $htmlOptions))
            $htmlOptions['class'] = $htmlOptions['class'] . ' ' . 'delete-row-button';
        else
            $htmlOptions = array_merge($htmlOptions,array('class'=>'delete-row-button5'));
        
        $htmlOptions = array_merge($htmlOptions, array('data-delete' => $row_id, 'data-key' => $key));
        
        echo CHtml::button($label, $htmlOptions);
    }
	
	public function deleteRowButton6($row_id, $key, $label='X', $htmlOptions = array()) {
        if(array_key_exists('class', $htmlOptions))
            $htmlOptions['class'] = $htmlOptions['class'] . ' ' . 'delete-row-button';
        else
            $htmlOptions = array_merge($htmlOptions,array('class'=>'delete-row-button6'));
        
        $htmlOptions = array_merge($htmlOptions, array('data-delete' => $row_id, 'data-key' => $key));
        
        echo CHtml::button($label, $htmlOptions);
    }
	
	
	public function deleteRowButton7($row_id, $key, $label='X', $htmlOptions = array()) {
        if(array_key_exists('class', $htmlOptions))
            $htmlOptions['class'] = $htmlOptions['class'] . ' ' . 'delete-row-button';
        else
            $htmlOptions = array_merge($htmlOptions,array('class'=>'delete-row-button7'));
        
        $htmlOptions = array_merge($htmlOptions, array('data-delete' => $row_id, 'data-key' => $key));
        
        echo CHtml::button($label, $htmlOptions);
    }
	
	public function deleteRowButton8($row_id, $key, $label='X', $htmlOptions = array()) {
        if(array_key_exists('class', $htmlOptions))
            $htmlOptions['class'] = $htmlOptions['class'] . ' ' . 'delete-row-button';
        else
            $htmlOptions = array_merge($htmlOptions,array('class'=>'delete-row-button8'));
        
        $htmlOptions = array_merge($htmlOptions, array('data-delete' => $row_id, 'data-key' => $key));
        
        echo CHtml::button($label, $htmlOptions);
    }
	
	
	public function deleteRowButton9($row_id, $key, $label='X', $htmlOptions = array()) {
        if(array_key_exists('class', $htmlOptions))
            $htmlOptions['class'] = $htmlOptions['class'] . ' ' . 'delete-row-button';
        else
            $htmlOptions = array_merge($htmlOptions,array('class'=>'delete-row-button9'));
        
        $htmlOptions = array_merge($htmlOptions, array('data-delete' => $row_id, 'data-key' => $key));
        
        echo CHtml::button($label, $htmlOptions);
    }
	
	public function deleteRowButton10($row_id, $key, $label='X', $htmlOptions = array()) {
        if(array_key_exists('class', $htmlOptions))
            $htmlOptions['class'] = $htmlOptions['class'] . ' ' . 'delete-row-button';
        else
            $htmlOptions = array_merge($htmlOptions,array('class'=>'delete-row-button10'));
        
        $htmlOptions = array_merge($htmlOptions, array('data-delete' => $row_id, 'data-key' => $key));
        
        echo CHtml::button($label, $htmlOptions);
    }
	
	
	public function deleteRowButton11($row_id, $key, $label='X', $htmlOptions = array()) {
        if(array_key_exists('class', $htmlOptions))
            $htmlOptions['class'] = $htmlOptions['class'] . ' ' . 'delete-row-button';
        else
            $htmlOptions = array_merge($htmlOptions,array('class'=>'delete-row-button11'));
        
        $htmlOptions = array_merge($htmlOptions, array('data-delete' => $row_id, 'data-key' => $key));
        
        echo CHtml::button($label, $htmlOptions);
    }
	
	public function deleteRowButton12($row_id, $key, $label='X', $htmlOptions = array()) {
        if(array_key_exists('class', $htmlOptions))
            $htmlOptions['class'] = $htmlOptions['class'] . ' ' . 'delete-row-button';
        else
            $htmlOptions = array_merge($htmlOptions,array('class'=>'delete-row-button12'));
        
        $htmlOptions = array_merge($htmlOptions, array('data-delete' => $row_id, 'data-key' => $key));
        
        echo CHtml::button($label, $htmlOptions);
    }
	
	public function deleteRowButton13($row_id, $key, $label='X', $htmlOptions = array()) {
        if(array_key_exists('class', $htmlOptions))
            $htmlOptions['class'] = $htmlOptions['class'] . ' ' . 'delete-row-button';
        else
            $htmlOptions = array_merge($htmlOptions,array('class'=>'delete-row-button13'));
        
        $htmlOptions = array_merge($htmlOptions, array('data-delete' => $row_id, 'data-key' => $key));
        
        echo CHtml::button($label, $htmlOptions);
    }
	
	public function deleteRowButton14($row_id, $key, $label='X', $htmlOptions = array()) {
        if(array_key_exists('class', $htmlOptions))
            $htmlOptions['class'] = $htmlOptions['class'] . ' ' . 'delete-row-button';
        else
            $htmlOptions = array_merge($htmlOptions,array('class'=>'delete-row-button14'));
        
        $htmlOptions = array_merge($htmlOptions, array('data-delete' => $row_id, 'data-key' => $key));
        
        echo CHtml::button($label, $htmlOptions);
    }
	
	
	public function deleteRowButton15($row_id, $key, $label='X', $htmlOptions = array()) {
        if(array_key_exists('class', $htmlOptions))
            $htmlOptions['class'] = $htmlOptions['class'] . ' ' . 'delete-row-button';
        else
            $htmlOptions = array_merge($htmlOptions,array('class'=>'delete-row-button15'));
        
        $htmlOptions = array_merge($htmlOptions, array('data-delete' => $row_id, 'data-key' => $key));
        
        echo CHtml::button($label, $htmlOptions);
    }

}

?>
