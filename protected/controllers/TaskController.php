<?php

class TaskController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    public function actions() {
        return array
            (
            'toggle' => array(
                'class' => 'bootstrap.actions.TbToggleAction',
                'modelName' => 'Task'
            ),
            'getRowForm1' => array(
                'class' => 'ext.dynamictabularform.actions.GetRowForm1',
                'view' => '_taskScheduleTabularForm',
                'modelClass' => 'TaskSchedule'
            ),
        );
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('admin', 'create', 'update', 'comm', 'editTaskStatus', 'updateTaskOnly', 'toggle', 'taskStatusProjectPopUp', 'getRowForm1', 'updateExtend', 'taskPlannedPopUp', 'getAlertValues', 'updateAlert'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        //$this->render('view',array(
        //'model'=>$this->loadModel($id),
        //));
        EQuickDlgs::render('view', array('model' => $this->loadModel($id)));
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($cid) {
        $model = new Task;
        $rpsTask = new Task;
     
        $variation = new Variation;
        $variation->scenario = 'batchSave';
        $model->communication_id = $cid;
        $rpsTask->communication_id=$cid;
       
        $model->job_start_date = date('Y-m-d');
        $model->job_due_date = date('Y-m-d');
        $model->extendedTask = 0;
        $model->job_client_due_date=date('Y-m-d');
        $model->task_type='normal';
        
        $rpsTask->job_start_date = date('Y-m-d');
        $rpsTask->job_due_date = date('Y-m-d');
        $rpsTask->job_client_due_date=date('Y-m-d');
        $rpsTask->extendedTask = 0;
        $rpsTask->task_type='checking';
        
        

        $model->sharable = 1;
        $model->is_hidden = 0; //means not hidden
        $model->actual_ao_cost = 0;
        $model->actual_rps_cost = 0;
        
        $rpsTask->sharable = 1;
        $rpsTask->is_hidden = 0; //means not hidden
        $rpsTask->actual_ao_cost = 0;
        $rpsTask->actual_rps_cost = 0;
        
        
        //$model->est_ao_cost=0;
        //$model->est_rps_cost=0;
        $model->done_percentage = 0;
        $model->task_status = 11;
        $model->piority = "Normal";
        
        $rpsTask->done_percentage = 0;
        $rpsTask->task_status = 11;
        $rpsTask->piority = "Normal";
        
        
        
        // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($model);

        if (isset($_POST['Task'])) {
            $model->attributes = $_POST['Task'];
            $rpsTask->attributes=$_POST['Task'];
            $rpsTask->job_details='Checking -:'.$rpsTask->job_details;
            
            

            if ($model->task_variation > 0) {
                $model->task_variation = "Yes";
                $rpsTask->task_variation = "Yes";
               
            } else {
                $model->task_variation = "No";
                $rpsTask->task_variation = "No";
                
            }

            //for task schedule new concept


            $taskScheduleModel = array();

            $taskScheduleModel = $model->generateSchedule($model, 'val');

            //end 
            //validate
           

            if (!$model->generateSchedule($model, 'boolean')) {
                $model->addError('validation', "Invalid work hour specified.Check the schedule if it covers all the work hours.");
            }
            
             $valid = $model->validate();

            foreach ($taskScheduleModel as $scheduledetail) {
                $valid = $scheduledetail->validate() && $valid;
            }

            //if variation
            $saveVariation=FALSE;
            if (isset($_POST['Variation']) && $model->task_variation == "Yes") {
                $variation->attributes = $_POST['Variation'];
                $valid = $variation->validate() && $valid;
                $saveVariation=TRUE;
            }


            if ($valid) {
                $transaction = $model->getDbConnection()->beginTransaction();
                try {
                    if ($model->save()) {

                        $model->refresh();

                        //save variation
                        if($saveVariation==TRUE)
                        {
				$variation->task_id = $model->task_id;
	                        $variation->variation_no = Variation::model()->getUniqueVariationByProject($model->communication_id);
	                        $variation->variation_dt = $model->job_start_date;
	                        $variation->status = 0;
	                        $variation->isNewRecord = TRUE;
	                        $variation->save(FALSE);
			}
                       
                        


                        //save checklists defined for project type

                        $proj = Yii::app()->db->createCommand()
                                ->select('pm_project.project_id,pm_project.project_type,pm_project.est_ao_wis')
                                ->from('pm_project')
                                ->join('pm_communication', 'pm_project.project_id = pm_communication.project_id')
                                ->where("pm_communication.communication_id = $cid")
                                ->queryRow();

                        $proj_type = $proj['project_type'];
                        //get task list from project type for before starting
                        $checklists = ChecklistScheme::model()->findAll("job_type_id=$proj_type and checklist_stages='24'");
                        foreach ($checklists as $values) {
                            $chklist = Checklist::model()->find(array(
                                'select' => 'checklist_details',
                                'condition' => 'checklist_id=:checklist_id',
                                'params' => array(':checklist_id' => $values->chklist_id),
                            ));

                            $checklistModel = new TaskChecklist();
                            $checklistModel->isNewRecord = TRUE;
                            $checklistModel->task_id = $model->task_id;
                            $checklistModel->checklist_details = $chklist->checklist_details;
                            $checklistModel->status = 13; //undone
                            $checklistModel->save();
                        }

                        //save in task schedule table
						
                        foreach ($taskScheduleModel as $taskScheduleDetail) {
                            $taskScheduleDetail->task_id = $model->task_id;
							
                            //
                            $taskScheduleDetail->save();
                        }
             //schedule date for first date
                        $date=$taskScheduleDetail->schedule_date;
							//$minDate=min($date);
						  //  echo($date);
						//	die;
                        //create automatic task for rps coordinator
                        $pid=$proj['project_id'];
                        
                        $rpsCoordinator=ProjectCoordinates::model()->find('project_id=:pid and type=:type',array(':pid'=>$pid,':type'=>'rps'));
                        $aoCoordinator=ProjectCoordinates::model()->find('project_id=:pid and type=:type',array(':pid'=>$pid,':type'=>'ao'));
                        
                        
                        
                        $rpsTask->est_rps_cost = $rpsTask->getRpsCoordinatorWorkingHour($proj_type, $rpsTask->est_rps_cost);
                        $rpsTask->assigned_to=$rpsCoordinator->user_id;
                        $rpsTask->parent_task_id=$model->task_id;
                        
                      
                        $rpsTask->save(FALSE);
                        $rpsTask->refresh();
                        
                     

                        $rpsScheduleModel = $rpsTask->generateRpsCoordinatorSchedule($rpsTask);
                        $rpsScheduleModel->save();
                        //end task coordinator
                      
                     //also save admin tasks
					 
					//  $schDate=TaskSchedule::model()->findAll(array(
					 //  'select'=>'schedule_date',
					 //   'condition'=>'task_id=:task_id',
					 //   'params'=>array(':task_id'=>$model->task_id),
					//	 'params'=>array(':task_id'=>$this->task_id),
			       //  	));
					
                          $associates=array();
                     //    $associates=$model->getAllProjectAssociatesFromTask($model->task_id);
					   //   $associates=$associates['associates'];
						$associates=$model->associates;
						
                         $adminTasksList=  CodeValue::model()->findAll('code_type=:type',array(':type'=>'admin_task'));
                         if($model->associates > 0)
						 {
						
                             foreach($associates as $key=>$val)
                             {
                                 $user_id=$val;
                                 foreach($adminTasksList as $taskrow)
                                 {
                                    $adminTask=new Task;
                                    $adminTask->job_details=$taskrow->code_lbl;
                                    $adminTask->piority = "Normal";
                                    $adminTask->sharable = 1;
                                    $adminTask->job_start_date=$date;
                                    $adminTask->job_due_date=$model->job_due_date;
                                    $adminTask->job_client_due_date=$model->job_client_due_date;
                                    $adminTask->est_rps_cost = 1;
                                    $adminTask->est_ao_cost = 1;
                                    $adminTask->actual_rps_cost = 0;
                                    $adminTask->actual_ao_cost = 0;
                                    $adminTask->done_percentage=0;
                                    $adminTask->assigned_to=$user_id;
                                    $adminTask->task_status = 11;
                                    $adminTask->error_known=0;
                                    $adminTask->error_unknown=0;
                                    
                                    $adminTask->task_cat=$model->task_cat;
                                    $adminTask->communication_id=$model->communication_id;
                                    $adminTask->job_type=$model->job_type;
                                    $adminTask->is_hidden = 0; //means not hidden
                                    $adminTask->task_variation = $model->task_variation;
                                    $adminTask->is_planned=0;
                                    $adminTask->parent_task_id=$model->task_id;
                                    $adminTask->task_type='admin';
                                  
                                    $adminTask->admin_task_id=0;
                                    
                                  
                                    $adminTask->extendedTask = 0;
                                   
                                   
                                 
                                    $adminTask->save(FALSE);
                                    $adminTask->refresh();
                                    $rpsScheduleModel = $adminTask->generateRpsCoordinatorSchedule($adminTask,1);
                                    $rpsScheduleModel->save();
                                 }
                                 
                             }
                          	
						 }
                        //end admin tasks


                        $transaction->commit();
                        

                        EQuickDlgs::checkDialogJsScript();
                        $this->redirect(array('home/admin', 'id' => $model->task_id));
                    }
                } 
                catch (Exception $e) {
                    $transaction->rollback();
                    throw new CHttpException(404, 'Some error occured.Please try again');
                }
            } else {
                echo "internal error occured.";
                //die;
            }
        }

        EQuickDlgs::render('create', array(
            'model' => $model,
            'variationModel' => $variation,
        ));
    }

    private function generateScheduleOld($model, $type = 'val') {
        //for task schedule new concept
        $date = date('Y-m-d');

        if ($model->extendedTask == 1) {
            $task = Task::model()->findByPk($model->task_id);
            $date = $task->job_due_date;
        }
        $ot_hour = $model->totalOverTimeHours;
        $rps_hour = $model->est_rps_cost;
        $totHour = $rps_hour;
        $totNormalHour = $rps_hour - $ot_hour;
        $due_date = $model->job_due_date;

        $taskScheduleModel = array();

        $totOTHours = $ot_hour;

        $definedNH = CodeValue::model()->getCodeTypeValue('work_hour', 28);
        $definedCH = CodeValue::model()->getCodeTypeValue('work_hour', 29);
        $definedOH = CodeValue::model()->getCodeTypeValue('work_hour', 30);
        while (strtotime($due_date) >= strtotime($date)) {
            $currentDate = $due_date;

            if ($totHour <= 0) {
                break;
            }
            $record = Yii::app()->db->createCommand()
                    ->select('IFNULL(sum(normal_hour),0) as nh,IFNULL(sum(cusion_hour),0) as ch,IFNULL(sum(overtime_hour),0) as oh')
                    ->from('pm_task_schedule')
                    ->where('pm_task_schedule.user_id=:userid', array(':userid' => $model->assigned_to))
                    ->andWhere('pm_task_schedule.completed_status=0')
                    ->andWhere('pm_task_schedule.schedule_date=:schedule_date', array(':schedule_date' => $currentDate))
                    ->group('schedule_date')
                    ->queryRow();

            $remainingNormal = floatval($definedNH) - floatval($record['nh']);

            $remainingOT = floatval($definedOH) - floatval($record['oh']);
            $remainingCusion = floatval($definedCH) - floatval($record['ch']);

            if ($ot_hour > 0) {
                $remaining = $remainingNormal + $remainingOT;
            } else {
                $remaining = $remainingNormal;
            }

            if ($remaining > 0 && $totHour > 0) {
                $j = 0;
                while ($remainingNormal > 0 && $totNormalHour > 0) {
                    $remainingNormal = $remainingNormal - 1;
                    $totNormalHour = $totNormalHour - 1;
                    $totHour = $totHour - 1;
                    $j = $j + 1;
                }

                $k = 0;
                if ($ot_hour > 0) {
                    while ($remainingOT > 0 && $totOTHours > 0) {
                        $remainingOT = $remainingOT - 1;
                        $totHour = $totHour - 1;
                        $totOTHours = $totOTHours - 1;
                        $k = $k + 1;
                    }
                }

                $schedule = new TaskSchedule('batchSave');
                $schedule->isNewRecord = TRUE;
                $schedule->user_id = $model->assigned_to;
                $schedule->schedule_date = $currentDate;
                $schedule->normal_hour = $j;
                $schedule->task_id = $model->task_id;
                $schedule->cusion_hour = $remainingCusion;
                $schedule->overtime_hour = $k;
                $schedule->remarks = "";
                $schedule->completed_status = 0;
                $schedule->extended_status = $model->extendedTask;
                $taskScheduleModel[] = $schedule;
            }


            $due_date = date("Y-m-d", strtotime("-1 day", strtotime($due_date)));
        }

        //end 

        if ($type == 'val') {
            return $taskScheduleModel;
        } else {
            if (($rps_hour - $totHour) != $rps_hour) {
                return FALSE;
            }
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $model->extendedTask = 0;

        //$taskScheduleModel=TaskSchedule::model()->findAll("task_id=$model->task_id and user_id=$model->assigned_to");
        //if(count($taskScheduleModel) <= 0)
        //{
        //$taskScheduleModel=array(new TaskSchedule);
        //}

        $ot = TaskSchedule::model()->checkWorkingHour($model->task_id, $model->assigned_to, 'ot');
        $model->totalOverTimeHours = $ot;

        $variation = Variation::model()->find("task_id=$model->task_id");
        if ($variation == "") {
            $variation = new Variation;
        }

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Task'])) {
            $model->attributes = $_POST['Task'];

            if ($model->task_variation > 0) {
                $model->task_variation = "Yes";
            } else {
                $model->task_variation = "No";
            }

            /*
              //for task schedule update
              if (isset($_POST['TaskSchedule']))
              {
              $taskScheduleModel = array();
              $i=1;
              $estimatedRpsHrs=0;
              foreach ($_POST['TaskSchedule'] as $key => $value)
              {



              if ($value['updateType'] == DynamicTabularForm::UPDATE_TYPE_CREATE)
              $taskScheduleModel[$key] = new TaskSchedule();
              else if ($value['updateType'] == DynamicTabularForm::UPDATE_TYPE_UPDATE)
              $taskScheduleModel[$key] = TaskSchedule::model()->findByPk($value['schedule_id']);
              else if ($value['updateType'] == DynamicTabularForm::UPDATE_TYPE_DELETE)
              {
              $delete = TaskSchedule::model()->findByPk($value['schedule_id']);
              if ($delete->delete())
              {
              unset($taskScheduleModel[$key]);
              continue;
              }
              }
              $taskScheduleModel[$key]->attributes = $value;
              $taskScheduleModel[$key]->user_id=$model->assigned_to;
              if($i==1)
              {
              $model->job_start_date=$taskScheduleModel[$key]->schedule_date;
              }
              $model->job_due_date=$taskScheduleModel[$key]->schedule_date;
              $estimatedRpsHrs+=$taskScheduleModel[$key]->normal_hour+$taskScheduleModel[$key]->cusion_hour+$taskScheduleModel[$key]->overtime_hour;
              $i++;
              }
              $model->est_rps_cost=$estimatedRpsHrs;
              }
              //end task schedule update
             */

            //for task schedule new concept
            $taskScheduleModel = array();

            $taskScheduleModel = $model->generateSchedule($model, 'val');

            //end 



            $valid = $model->validate();

            if (!Task::model()->generateSchedule($model, 'boolean')) {
                $model->addError('validation', "Invalid work hour specified.Check the schedule if it covers all the work hours.");
            }

            foreach ($taskScheduleModel as $scheduledetail) {
                $valid = $scheduledetail->validate() && $valid;
            }

            //if variation
            if (isset($_POST['Variation']) && $model->task_variation == "Yes") {
                $variation->attributes = $_POST['Variation'];
                $valid = $variation->validate() && $valid;
            }

            if ($valid) {

                $transaction = $model->getDbConnection()->beginTransaction();
                try {
                    if ($model->save()) {

                        $model->refresh();

                        //delete old schedule
                        TaskSchedule::model()->deleteAll("task_id=$model->task_id");

                        //save variation
                        $variation->task_id = $model->task_id;
                        //$variation->variation_no=Variation::model()->getUniqueVariationByProject($model->communication_id);
                        $variation->variation_dt = $model->job_start_date;
                        //$variation->status=0;
                        //$variation->isNewRecord=TRUE;
                        $variation->save();




                        //save in task schedule table

                        foreach ($taskScheduleModel as $taskScheduleDetail) {
                            $taskScheduleDetail->task_id = $model->task_id;
                            $taskScheduleDetail->save();
                        }




                        $transaction->commit();

                        EQuickDlgs::checkDialogJsScript();
                        $this->redirect(array('home/admin', 'id' => $model->task_id));
                    } else {
                        throw new CHttpException(400, "Cannot update.The task is in use");
                    }
                } catch (Exception $e) {
                    $transaction->rollback();
                    throw new CHttpException(400, 'Some error occured.Please try again');
                }
            }
        }

        EQuickDlgs::render('update', array(
            'model' => $model,
            'variationModel' => $variation,
            'taskScheduleModel' => $taskScheduleModel,
        ));
    }

    public function actionUpdateExtend($id) {
        $model = $this->loadModel($id);
        $task_old_model = $this->loadModel($id);
        $model->extendedTask = 1;


        $ot = TaskSchedule::model()->checkWorkingHour($model->task_id, $model->assigned_to, 'ot');
        $model->totalOverTimeHours = $ot;



        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Task'])) {
            $model->attributes = $_POST['Task'];




            //for task schedule new concept
            $taskScheduleModel = array();

            $taskScheduleModel = $this->generateSchedule($model, 'val');

            //end 



            $valid = $model->validate();

            if (!$this->generateSchedule($model, 'boolean')) {
                $model->addError('validation', "Invalid work hour specified.Check the schedule if it covers all the work hours.");
            }

            if (MyCustomClass::GetDateDifference($model->job_due_date, $task_old_model->job_due_date) < 0) {
                $model->addError('job_due_date', "The new job due date should not be smaller than previous job due date.");
            }

            foreach ($taskScheduleModel as $scheduledetail) {
                $valid = $scheduledetail->validate() && $valid;
            }



            if ($valid) {

                $transaction = $model->getDbConnection()->beginTransaction();
                try {
                    $model->est_rps_cost = $model->est_rps_cost + $task_old_model->est_rps_cost;
                    $model->assigned_to = $task_old_model->assigned_to;
                    if ($model->save()) {

                        $model->refresh();


                        //save in task schedule table

                        foreach ($taskScheduleModel as $taskScheduleDetail) {
                            $taskScheduleDetail->task_id = $model->task_id;
                            $taskScheduleDetail->save();
                        }




                        $transaction->commit();

                        EQuickDlgs::checkDialogJsScript();
                        $this->redirect(array('home/admin', 'id' => $model->task_id));
                    } else {
                        throw new CHttpException(400, "Cannot update.The task is in use");
                    }
                } catch (Exception $e) {
                    $transaction->rollback();
                    throw new CHttpException(404, 'Some error occured.Please try again');
                }
            }
        }

        EQuickDlgs::render('updateExtend', array(
            'model' => $model
        ));
    }

    public function actionUpdateTaskOnly($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Task'])) {
            $model->attributes = $_POST['Task'];
            if ($model->task_variation > 0) {
                $model->task_variation = "Yes";
            } else {
                $model->task_variation = "No";
            }
            if ($model->save()) {


                EQuickDlgs::checkDialogJsScript();
                $this->redirect(array('task/admin', 'id' => $model->task_id));
            }
        }

        EQuickDlgs::render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {


        try {
            if ($this->loadModel($id)->delete()) {
                
            } else {
                throw new CHttpException(400, "Cannot delete.The task is in use");
            }
        } catch (CDbException $e) {
            throw new CHttpException(400, "Some error occured while deleting.Please try again");
        }


        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        //$dataProvider=new CActiveDataProvider('Task');
        //$this->render('index',array(
        //'dataProvider'=>$dataProvider,
        //));
        $this->actionAdmin();
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Task('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Task']))
            $model->attributes = $_GET['Task'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Task the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Task::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Task $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'task-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionComm($tid) {
        $model = new Task('search');
        EQuickDlgs::render('_communication', array(
            'model' => $model,
        ));
    }

    public function actionTest() {
        EQuickDlgs::render('_updateproject', array('model' => $model));
    }

    public function actionTaskStatusProjectPopUp($id) {
        //$this->renderPartial('_updateproject', array('model'=>$model), FALSE);
        $model = $this->loadModel($id);
        $this->renderPartial('_updateproject', array('model' => $model,), false, true);
    }

    public function actionTaskPlannedPopUp($id) {
        $model = $this->loadModel($id);
        $this->renderPartial('_updatePlanned', array('model' => $model,), false, true);
    }

    public function actionEditTaskStatus() {

        if (Yii::app()->request->isAjaxRequest) {
            Yii::import('bootstrap.widgets.MyTaskStatusTbEditableSaver');
            $es = new MyTaskStatusTbEditableSaver('Task');

            //$additionalAttr=array();
            //$attribute = yii::app()->request->getParam('name');



            $es->update();
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function getTaskNo($tid) {
        return $tid . "5";
    }

    public function actionGetAlertValues() {


        $data = array(
            array("code_id" => 0, "code_lbl" => "No"),
            array("code_id" => 1, "code_lbl" => "Yes"),
        );
        /* Replace the keys in the new array with 'value' and 'text' */
        foreach ($data as $k => $v) {
            $data[$k]['value'] = $data[$k]['code_id'];
            unset($data[$k]['code_id']);

            $data[$k]['text'] = $data[$k]['code_lbl'];
            unset($data[$k]['code_lbl']);
        }

        echo json_encode($data);
    }

    public function actionUpdateAlert() {

        if (Yii::app()->request->isAjaxRequest) {
            Yii::import('bootstrap.widgets.TbEditableSaver');
            $es = new TbEditableSaver('TaskAlert');
            //$id=Yii::app()->user->id;
            //$this->updt_cnt=$this->updt_cnt+1;
            //$this->updt_by=$id;
            $es->update(array('fixed_by' => Yii::app()->user->id));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

}
