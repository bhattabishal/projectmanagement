 <?php

   class ClientInstructionSheetController extends Controller
    {
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','excel','getTaskStatusValues','report','editTaskStatus'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionView($id)
	{
	EQuickDlgs::render('view',array(
	        'model'=>$this->loadModel($id)
		));
	}
	
	public function actionIndex()
	{

	$model=new ClientInstructionSheet;
		$model->unsetAttributes();  // clear any default values
	
	
    	$max = Yii::app()->db->createCommand('SELECT max(project_id) as project_id FROM pm_project');
		$a=$max->queryRow();
	//	print_r($a);
	//	die;
		
		$model->projectId=$a['project_id'];
		if(isset($_POST['ClientInstructionSheet']))
		{
			$model->attributes=$_POST['ClientInstructionSheet'];
			
		//	print_r($model->attributes);
		//	die;
		}
		$this->render('_clientInstruction',array('model'=>$model));
	}

   /*
	export data to excel
	*/
	 public function actionExcel() {

        $d = $_SESSION['variation-excel'];

        $data = array();

       // $data[]=array_keys($d->data[0]->attributes);//headers: cols name
       Yii::import('ext.phpexcel.XPHPExcel');
        $objPHPExcel= XPHPExcel::createPHPExcel();
        // set default font
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');
		// set default font size
       $objPHPExcel->getDefaultStyle()->getFont()->setSize(8);

       $objPHPExcel->getProperties()->setCreator("Enravel")
                             ->setLastModifiedBy("Enravel")
                             ->setTitle("Office 2007 XLSX Test Document")
                             ->setSubject("Office 2007 XLSX Test Document")
                             ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Test result file");
            // Add some data
            $i=1;
			$sheet = $objPHPExcel->getActiveSheet();
			$sheet->mergeCells("A1:G1");
			$sheet->setCellValue("A1", "Client Instruction Sheet");
            $sheet->getStyle("A$i")->getFont()->setBold(true)->setSize(15);
           
			$oldProjectId=0;
        foreach ($d->data as $item) {
        	if($oldProjectId != $item->project->project_id)
        	{
        		$i=$i+2;
        		$sheet->mergeCells("A$i:G$i");
        		$projNo=$item->project->project_no==""?$item->project->project_id:$item->project->project_no;
				$sheet->getCell("A$i")->setValue($item->project->project_title."(".$projNo."-".$item->codeValueProjectType->code_lbl.")");
				
				$sheet->getStyle("A$i")->getFont()->setBold(true)->setSize(14);
			}
			$i=$i+2;
            $attribues=$item->attributes;
            
            $sheet->getStyle("A$i:G$i")->getFont()->setBold(true)->setSize(12);
            
			$sheet->getCell("A$i")->setValue('Date');
			$sheet->getCell("B$i")->setValue('Subject');
			$sheet->getCell("C$i")->setValue('Communication Id');
			$sheet->getCell("D$i")->setValue('Task Id');
			$sheet->getCell("E$i")->setValue('Task Description');
			$sheet->getCell("F$i")->setValue('Allocated Hours');
			$sheet->getCell("G$i")->setValue('Comments');
			
			$sheet->getStyle("A$i:G$i")->getBorders()->
getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
			
			$i++;
			
			$sheet->getCell("A$i")->setValue($attribues['communication_date']);
			$sheet->getCell("B$i")->setValue($attribues['communication_subject']);
			$sheet->getCell("C$i")->setValue($attribues['communication_id']);
			$sheet->getCell("D$i")->setValue($attribues['task_id']);
			$sheet->getCell("E$i")->setValue($attribues['job_details']);
			$sheet->getCell("F$i")->setValue($attribues['est_rps_cost']);
			$sheet->getCell("G$i")->setValue($attribues['comments']);
			
			$sheet->getStyle("A$i:G$i")->getBorders()->
		getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        }
		            
		            // autosize the columns
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		
		            // Redirect output to a clientâ€™s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Variation Tracking Sheet.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		      Yii::app()->end();
		    }
		    
		    
		 
		}
