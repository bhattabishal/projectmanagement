<?php
class ActionGetProgressBarData extends CAction 
{    
    public function run($key) 
    {
        $response = ProgressBar::get($key);
        echo json_encode($response);            
    }
 
}
 
?>