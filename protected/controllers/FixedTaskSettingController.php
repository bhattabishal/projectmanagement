<?php

class FixedTaskSettingController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout = '//layouts/column1';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl',// perform access control for CRUD operations
			'postOnly + delete',// we only allow deletion via POST request
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow'  ,// allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'  =>array('@'),
			),
			array('allow'  ,// allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'  =>array('@'),
			),
			array('allow'  ,// allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'  =>array('@'),
			),
			array('deny' ,// deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
				'model'=>$this->loadModel($id),
			));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model = new FixedTaskSetting;
		$model->start_date = date('Y-m-d',strtotime('sunday'));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['FixedTaskSetting'])){
			$model->attributes = $_POST['FixedTaskSetting'];
			$model->end_date = MyCustomClass::addDays($model->start_date,6);

			$transaction = $model->getDbConnection()->beginTransaction();

			try
			{
				if($model->validate()){
					$model->save();
					$model->refresh();

					//now save in project as task.find if task already saved.if so update the time else insert new record
					$record = Yii::app()->db->createCommand()
					->select('task_id')
					->from('pm_task')
					->where('task_type=:task_type and admin_task_id=:id',array(':id'       =>$model->project_id,':task_type'=>'admin'))
					->queryRow();

					$taskModel;
					if($record['task_id'] > 0){

						$taskModel = Task::model()->findByPk($record['task_id']);

						Task::model()->updateByPk($taskModel->task_id, array(
								'est_rps_cost'=> $taskModel->est_rps_cost + $model->budget//ready
							));

					}
					else
					{
						$taskModel = new Task;
						$taskModel->job_details = $model->task->title;
						$taskModel->piority = 'Normal';
						$taskModel->sharable = 1;
						$taskModel->job_start_date = $model->start_date;
						$taskModel->job_due_date = $model->end_date;
						$taskModel->job_client_due_date = $model->end_date;
						$taskModel->est_rps_cost = $model->budget;
						$taskModel->est_ao_cost = 0;
						$taskModel->actual_rps_cost = 0;
						$taskModel->actual_ao_cost = 0;
						$taskModel->done_percentage = 0;
						$taskModel->assigned_to = 0;
						$taskModel->task_status = 11;
						$taskModel->error_known = 0;
						$taskModel->error_unknown = 0;
						$taskModel->task_cat = 6;
						$taskModel->communication_id=-1;
						$taskModel->job_type = 10;
						$taskModel->is_hidden = 0;
						$taskModel->task_variation = 'No';
						$taskModel->is_planned = 0;
						$taskModel->parent_task_id = 0;
						$taskModel->task_type = 'admin';
						$taskModel->admin_task_id = $model->project_id;
						$taskModel->save(false);


					}

					//generate schedule
					$days = MyCustomClass::GetDateDifference($model->end_date,$model->start_date);
					$days += 1;
					$totHours = $model->budget;
					$dailyhour= round($model->budget / $days,1);
					$tot      = 0;
					for($i = 1;$i <= $days; $i++){
						$schedule = new TaskSchedule;
						$schedule->task_id = $taskModel->task_id;
						$schedule->user_id = 0;
						$schedule->schedule_date = MyCustomClass::addDays($model->start_date,$i);
						$schedule->normal_hour = $dailyhour;
						$schedule->cusion_hour = 2;
						$schedule->overtime_hour = 0;
						$schedule->worked_normal_hr = 0;
						$schedule->worked_ot_hr = 0;

						if($i == $days){
							$schedule->normal_hour = $totHours - $tot;

						}

						$schedule->schedule_nr_hr = $schedule->normal_hour;

						$tot += $dailyhour;
						$schedule->schedule_ot_hr = 0;
						$schedule->remarks = 'fixed tasks';
						$schedule->completed_status = 0;
						$schedule->extended_status = 0;

						$schedule->save(FALSE);

					}
					$transaction->commit();
					EQuickDlgs::checkDialogJsScript();
					$this->redirect(array('admin'));

				}

			}
			catch(Exception $e){
				$transaction->rollback();
				throw new CHttpException(404, 'Some error occured.Please try again');
			}



		}

		EQuickDlgs::render('create',array(
				'model'=>$model,
			));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model    = $this->loadModel($id);
		$oldModel = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['FixedTaskSetting'])){
			$model->attributes = $_POST['FixedTaskSetting'];
			$model->end_date = MyCustomClass::addDays($model->start_date,6);


			$transaction = $model->getDbConnection()->beginTransaction();

			try
			{
				if($model->validate()){
					$model->save();
					$model->refresh();

					//now save in project as task.find if task already saved.if so update the time else insert new record
					$record = Yii::app()->db->createCommand()
					->select('task_id')
					->from('pm_task')
					->where('task_type=:task_type and admin_task_id=:id',array(':id'       =>$model->project_id,':task_type'=>'admin'))
					->queryRow();

					$taskModel;
					if($record['task_id'] > 0){

						$taskModel = Task::model()->findByPk($record['task_id']);

						Task::model()->updateByPk($taskModel->task_id, array(
								'est_rps_cost'=> $taskModel->est_rps_cost + $model->budget//ready
							));

					}
					else
					{
						$taskModel = new Task;
						$taskModel->job_details = $model->task->title;
						$taskModel->piority = 'Normal';
						$taskModel->sharable = 1;
						$taskModel->job_start_date = $model->start_date;
						$taskModel->job_due_date = $model->end_date;
						$taskModel->job_client_due_date = $model->end_date;
						$taskModel->est_rps_cost = $model->budget;
						$taskModel->est_ao_cost = 0;
						$taskModel->actual_rps_cost = 0;
						$taskModel->actual_ao_cost = 0;
						$taskModel->done_percentage = 0;
						$taskModel->assigned_to = 0;
						$taskModel->task_status = 11;
						$taskModel->error_known = 0;
						$taskModel->error_unknown = 0;
						$taskModel->task_cat = 6;
						$taskModel->communication_id=-1;
						$taskModel->job_type = 10;
						$taskModel->is_hidden = 0;
						$taskModel->task_variation = 'No';
						$taskModel->is_planned = 0;
						$taskModel->parent_task_id = 0;
						$taskModel->task_type = 'admin';
						$taskModel->admin_task_id = $model->project_id;
						$taskModel->save(false);


					}

					//generate schedule
					$days = MyCustomClass::GetDateDifference($model->end_date,$model->start_date);
					$days += 1;
					$totHours = $model->budget;
					$dailyhour= round($model->budget / $days,1);
					$tot      = 0;
					for($i = 1;$i <= $days; $i++){
						$schedule = new TaskSchedule;
						$schedule->task_id = $taskModel->task_id;
						$schedule->user_id = 0;
						$schedule->schedule_date = MyCustomClass::addDays($model->start_date,$i);
						$schedule->normal_hour = $dailyhour;
						$schedule->cusion_hour = 2;
						$schedule->overtime_hour = 0;
						$schedule->worked_normal_hr = 0;
						$schedule->worked_ot_hr = 0;

						if($i == $days){
							$schedule->normal_hour = $totHours - $tot;

						}

						$schedule->schedule_nr_hr = $schedule->normal_hour;

						$tot += $dailyhour;
						$schedule->schedule_ot_hr = 0;
						$schedule->remarks = 'fixed tasks';
						$schedule->completed_status = 0;
						$schedule->extended_status = 0;

						$schedule->save(FALSE);

					}
					$transaction->commit();
					EQuickDlgs::checkDialogJsScript();
					$this->redirect(array('admin'));

				}

			}
			catch(Exception $e){
				$transaction->rollback();
				throw new CHttpException(404, 'Some error occured.Please try again');
			}

		}

		EQuickDlgs::render('update',array(
				'model'=>$model,
			));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		//$dataProvider = new CActiveDataProvider('FixedTaskSetting');
		//$this->render('index',array(
		//'dataProvider'=>$dataProvider,
		//));
		$this->actionAdmin();
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model = new FixedTaskSetting('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['FixedTaskSetting']))
		$model->attributes = $_GET['FixedTaskSetting'];

		$this->render('admin',array(
				'model'=>$model,
			));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return FixedTaskSetting the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
		$model = FixedTaskSetting::model()->findByPk($id);
		if($model === null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param FixedTaskSetting $model the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax'] === 'fixed-task-setting-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
