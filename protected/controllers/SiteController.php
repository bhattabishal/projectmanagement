<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		
		if($error=Yii::app()->errorHandler->error)
		{
                  
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				//$this->render('//system/error'.$error['code'], array('data'=>$error));
                               switch($error['code'])
                               {
                                       case 400:
                                            $user = Yii::app()->getComponent('user');
                                            $user->setFlash(
                                            'error',
                                            '<strong>Oh snap!</strong> Bad request on HTTP.'
                                            );
                                               $this->render('//system/error400', array('data'=>$error));
                                               break;

                                       case 403:
                                            $user = Yii::app()->getComponent('user');
                                            $user->setFlash(
                                            'error',
                                            $error['message']
                                            );
                                              $this->render('//system/error403', array('data'=>$error));
                                               break;

                                       case 404:
                                            $user = Yii::app()->getComponent('user');
                                            $user->setFlash(
                                            'error',
                                            '<strong>Warning!</strong>The requested URL [URL] was not found on this server.'
                                            );
                                               $this->render('//system/error404', array('data'=>$error));
                                               break;

                               }
                            
                }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$this->layout = '//layouts/customlogin';
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				//$this->redirect(Yii::app()->user->returnUrl);
				$this->redirect(array('home/admin'));
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->setState('branchId',null);
		Yii::app()->session->clear(); // removes all session variables
        Yii::app()->session->destroy() ; // removes actual data from server
		Yii::app()->user->logout();
		unset(Yii::app()->session['mailObj']);
		$this->redirect(Yii::app()->homeUrl);
	}
}