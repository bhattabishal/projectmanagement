<?php

class TaskChecklistController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','create','update','editStatus','statusValues'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		//$this->render('view',array(
		//'model'=>$this->loadModel($id),
		//));
		 EQuickDlgs::render('view',array('model'=>$this->loadModel($id)));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($tid)
	{
		$model=new TaskChecklist;
		$model->task_id=$tid;
		$model->status=11;
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		
		if(isset($_POST['TaskChecklist']))
		{
		$model->attributes=$_POST['TaskChecklist'];
		if($model->save())
		{
			//$this->redirect(array('view','id'=>$model->task_checklist_id));
			EQuickDlgs::checkDialogJsScript();
			$this->redirect(array('home/admin','id'=>$model->task_checklist_id));
		}
		
		}
		
		EQuickDlgs::render('create',array(
		'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		
		if(isset($_POST['TaskChecklist']))
		{
		$model->attributes=$_POST['TaskChecklist'];
		if($model->save())
		{
			//$this->redirect(array('view','id'=>$model->task_checklist_id));
			EQuickDlgs::checkDialogJsScript();
			$this->redirect(array('home/admin','id'=>$model->task_checklist_id));
		}
		
		}
		
		EQuickDlgs::render('update',array(
		'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('TaskChecklist');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new TaskChecklist('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TaskChecklist']))
			$model->attributes=$_GET['TaskChecklist'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return TaskChecklist the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=TaskChecklist::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param TaskChecklist $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='task-checklist-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	/*
	to get values for drop down editable field
	*/
	public function actionStatusValues()
	{
		$criteria = new CDbCriteria;
		$criteria->select='*';  // only select the 'title' column
		$criteria->condition="code_type='job_status' and code_id not in (26)";
		$models=CodeValue::model()->findAll($criteria);
		
		/* Extract only the attributes that you need from the models
       into a new array */
    	$data = array_map(
        create_function('$m', 
            'return $m->getAttributes(array(\'code_id\',\'code_lbl\'));'), 
            $models);
			
		/* Replace the keys in the new array with 'value' and 'text' */
	    foreach($data as $k => $v )
	    {
	        $data[$k]['value'] = $data[$k]['code_id'];
	        unset($data[$k]['code_id']);
	 
	        $data[$k]['text'] = $data[$k]['code_lbl'];
	        unset($data[$k]['code_lbl']);
	    }
		echo json_encode($data);
	}
	public function actionEditStatus()
	{
		
		if(Yii::app()->request->isAjaxRequest)
	    {
	        Yii::import('bootstrap.widgets.TbEditableSaver');
	        $es=new TbEditableSaver('TaskChecklist');
			
			$additionalAttr=array();
			//$attribute = yii::app()->request->getParam('name');
			//$value = yii::app()->request->getParam('value');
			//if($value==12)
			//{
				//$additionalAttr['is_hidden']="1";
			//}
			//else
			//{
				//$additionalAttr['is_hidden']="0";
			//}
			
			
	        $es->update($additionalAttr);
	    }
	    else
	        throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
}
