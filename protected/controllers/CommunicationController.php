<?php
Yii::import('ext.EIMap.EIMap', true);
class CommunicationController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
	
	private $fileattachment;
	private $filearray;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','create','update','excel','taskComm','updateCommOnly','toggleBookmark','toggleStatus','toggle','deleteFile','createFromMail','messageDialog'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		//$this->render('view',array(
			//'model'=>$this->loadModel($id),
		//));
		 EQuickDlgs::render('view',array('model'=>$this->loadModel($id)));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($pid,$tid)
	{
		if($pid <= 0)
		{
			$task=Task::model()->find(array(
				    'select'=>'communication_id',
				    'condition'=>'task_id=:task_id',
				    'params'=>array(':task_id'=>$tid),
				));
				
				$comm_id=$task->communication_id;
				
				$comm=Communication::model()->find(array(
				    'select'=>'project_id',
				    'condition'=>'communication_id=:communication_id',
				    'params'=>array(':communication_id'=>$comm_id),
				));
				
				$pid=$comm->project_id;
		}
		
		$id=Yii::app()->user->id;
			
			$user=User::model()->find(array(
				    'select'=>'user_id,user_name,branch_id',
				    'condition'=>'user_id=:userID',
				    'params'=>array(':userID'=>$id),
				));
		
		$model=new Communication;
		$model->communication_date=date('Y-m-d');
		$model->project_id=$pid;
		$model->comm_from=$user->branch_id;
		// Uncomment the following line if AJAX validation is needed
		//$this->performAjaxValidation($model);
		
		$emailSetting=array();
		
		if(isset($_POST['Communication']))
		{
			$emailSetting=$_POST['Communication'];
			
			$model->attributes=$_POST['Communication'];
			$model->status=0;//unhidden
			$model->user_id=Yii::app()->user->id;
			$model->bookmark=0;//not bookmarked
			
			if($model->is_internal > 0)
			{
				$model->is_internal="Yes";
			}
			else{
				$model->is_internal="No";
			}
			
				
			$model->emailId=0;
			
			$valid=$model->validate();
			
			
			//print_r($emailSetting);
			//die;
			
			
			$message= new YiiMailMessage;
			$message->view = "test";
			$params= array('myMail'=>$model);
			$message->subject    = $model->communication_subject;
			$message->setBody($params, 'text/html');
			
			
			
			if($valid)
			{
				 $transaction = $model->getDbConnection()->beginTransaction();
				try
				{
					
					if ($model->save())
					{
					
						$attchment=array();
						$files_arry=array();
						$files = CUploadedFile::getInstancesByName('files');
						
						if (isset($files) && count($files) > 0)
						{
							$i=0;
							foreach ($files as $image => $pic)
							{
								$uniqid=MyCustomClass::uniqueRandom();
								if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/images/uploads/commfiles/'. $uniqid.$pic->name))
								{
									$files_arry[]=$uniqid.$pic->name;
									$path=realpath(Yii::app()->basePath . '/../images/uploads/commfiles/'. $uniqid.$pic->name);
									$swiftAttachment = Swift_Attachment::fromPath($path,$pic->type)->setFilename($pic->name); 
									$attchment[$i]=$swiftAttachment;
									
									$img_add = new CommunicationAttachment();
		                        	$img_add->attachment = $uniqid.$pic->name; //it might be $img_add->name for you, filename is just what I chose to call it in my model
		                        	$img_add->communication_id = $model->communication_id; // this links your picture model to the main model (like your user, or profile model)
		 
		                        	$img_add->save(); // DONE
								}
								else
								{
	                        		echo 'Cannot upload!';
	                    		}
	                    		$i++;
							}
						}
						
						//also save if commu is of task
						if($tid > 0)
						{
							$taskCommModel=new TaskCommunication();
							$taskCommModel->task_id=$tid;
							$taskCommModel->communication_id=$model->communication_id;
							
							$taskCommModel->save();
						}
						
						
						//now configure the email sending
							//Email send
						
							$sendToClient=$emailSetting['sendToClient'];
							$sendAttachment=$emailSetting['sendAttach'];
							$associates=$emailSetting['associates'];//array
							
							
							
							$emailSend=FALSE;
							
							if($sendToClient == "1")
							{
								$emailSend=TRUE;
								$message->from = array('enravel@enravel.com.au'=>'Enravel');
								
								
								
								
								$project_contact=ProjectContact::model()->getProjectContacts($pid); // $params is not needed
								foreach($project_contact as $contactval)
								{
									if($contactval->contact_email != "")
									{
										$email=$contactval->contact_email;					                        
                                         $message->addTo($email);
									}
								}
								
							}
							else
							{
								$emailUser=EmailUser::model()->getEmailDetailsById($id);
								if($emailUser->email_id != "")
								{
									$message->from=array($emailUser->email_id=>$user->user_name);
								}
								else
								{
									$message->from = array('enravel@enravel.com.au'=>'Enravel');
								}
							}
							
							
							
							if(is_array($associates) &&  count($associates) > 0)
							{
								$emailSend=TRUE;
								foreach($associates as $key=>$val)
								{
									$uid=$val;
									///$criteria=new CDbCriteria;
									//$criteria->select='*'; 
									//$criteria->condition='user_id=:user_id';
									//$criteria->params=array(':user_id'=>$uid);
									//$passoc=User::model()->find($criteria);
									$emailUser=EmailUser::model()->getEmailDetailsById($uid);
									
									$email=$emailUser->email_id;
									
									if($email != "")
									{
										$message->addTo($email);
									}
														                        
							        
								}
							}
							
							if($sendAttachment == "1")
							{
								if(sizeof($attchment) > 0)
								{
									foreach($attchment as $key=>$val)
									{
										$message->attach($val);
									}
								}
							}
							
						//	$emailSend=FALSE;
							if($emailSend)
							{
								if(Yii::app()->mail->send($message))
								 {
								 	Communication::model()->updateByPk($model->communication_id, array(
							    	'mail_sent' => 'Yes'
									));
								 } 
								 else
								 {
								 	Communication::model()->updateByPk($model->communication_id, array(
							    	'mail_sent' => 'No'
									));
								 }
							}
							else
							{
								Communication::model()->updateByPk($model->communication_id, array(
						    	'mail_sent' => 'No'
								));
							}
							
							
						
						//end email sending
						 
					}
					
					 $transaction->commit();
					 EQuickDlgs::checkDialogJsScript();
					$this->redirect(array('home/admin','id'=>$model->communication_id));
				}
				catch(Exception $e)
				{
					 $transaction->rollback();
                    throw new CHttpException(404, 'Some error occured.Please try again');
				}
				
			}	
		}
		
		

		
		EQuickDlgs::render('_combineForm',array(
			'model'=>$model,
		));
	}
	
	
	
	
	
	public function saveEmailAttachments($msgId,$cid,$project_no)
	{
		$user_details=EmailUser::model()->getEmailDetailsById(Yii::app()->user->id);
		$user=$user_details->email_id;
		$pass=MyCustomClass::decrypt($user_details->password);
		$imap=new EIMap('{vmcp09.digitalpacific.com.au:993/imap/ssl/novalidate-cert}', $user, $pass);
		$uniqid=MyCustomClass::uniqueRandom();
		
		$attachment=array();
		if($imap->connect())
		{
			$message=$imap->getMail($msgId,null,false,true);
			$attachment=$message->attachments;
			$imap->close();
		}
		
		$fileDir= Yii::getPathOfAlias('webroot').'/images/uploads/commfiles/';
		ob_start(); 
		$zip = new ZipClass();
		$zip->setComment($project_no);
	
		foreach($attachment as $file)
		{
			
				//print_r($file);
				//die;
				//$filename = Yii::getPathOfAlias('webroot').'/images/uploads/commfiles/'.$uniqid.$file['filename'];
				$fileName =$file['filename'];
				$fileData=$file['data'];
				
				$zip->addFile($fileData, $fileName);
	
	/*
				$fp = fopen($filename, 'w');
				fputs($fp, $file['data']);
				fclose($fp);
				
				$img_add = new CommunicationAttachment();
				$img_add->attachment = $uniqid.$file['filename'];
				$img_add->communication_id=$cid;
				$img_add->save();
				*/
				
				
				
		}
		$date=date('Y-m-d');
		$zipfile=$date.$project_no.".zip";
		$zipName= Yii::getPathOfAlias('webroot').'/images/uploads/commfiles/'.$zipfile;
		
		$fp = fopen($zipName, 'w');
		fputs($fp, $zip->getZipData());
		fclose($fp);
		
		ob_end_flush();
		
		$zip->__destruct();
		
		$img_add = new CommunicationAttachment();
		$img_add->attachment = $zipfile;
		$img_add->communication_id=$cid;
		$img_add->save();
	}
	
	public function actionCreateFromMail()
	{
		
		$model=new Communication;
		

		if(isset($_POST['Communication']))
		{
			$errors=array();
			$commDatas=$_POST['Communication'];
			//print_r($commDatas);
			//die;
			if($commDatas['project_id'] =="" || $commDatas['project_id'] <= 0)
			{
				$errors[]="Please select project number.";
			}
			if($commDatas['communication_subject'] =="")
			{
				$errors[]="Please enter subject.";
			}
			if($commDatas['communication_details'] =="")
			{
				$errors[]="Please enter communication details.";
			}
			
			if($commDatas['communication_date'] == "")
			{
				$errors[]="Please select date.";
			}
			
			if($commDatas['comm_from'] == "")
			{
				$errors[]="Please select Communication from.";
			}
			
			$cMailid=$commDatas['emailId'];
			$userId=Yii::app()->user->id;
			$commByEmailId=Communication::model()->find("emailId=$cMailid and user_id=$userId");
			//print_r($commByEmailId);
			
			if(!($commByEmailId==null || $commByEmailId->communication_id <= 0))
			{
				$errors[]="Selected email has already been saved as communication.";
			}
			
			$archiveByEmailId=Inbox::model()->find("msg_no=$cMailid and crtd_by=$userId");
			if(!($archiveByEmailId==null || $archiveByEmailId->inbox_id <= 0))
			{
				$errors[]="Selected email has already been saved as archive.";
			}
			
			
			if(count($errors) <= 0)
			{
				$model->status=0;//unhidden
				$model->user_id=Yii::app()->user->id;
				$model->bookmark=0;//not bookmarked
				$model->project_id=$commDatas['project_id'];
				$model->communication_details=$commDatas['communication_details'];
				$model->communication_date=$commDatas['communication_date'];
				$model->communication_subject=$commDatas['communication_subject'];
				$model->mail_sent="No";
				$model->emailId=$commDatas['emailId'];
				$model->comm_from=$commDatas['comm_from'];
				
				if($model->is_internal > 0)
				{
					$model->is_internal="Yes";
				}
				else{
					$model->is_internal="No";
				}
				
				if($model->is_private > 0)
				{
					$model->is_private="Yes";
				}
				else{
					$model->is_private="No";
				}
				
				
				//else{
					//$id=Yii::app()->user->id;
					//$user=User::model()->find(array(
					  //  'select'=>'user_id,user_name,branch_id',
					   // 'condition'=>'user_id=:userID',
					   // 'params'=>array(':userID'=>$id),
					//));
					
					//$model->comm_from=$user->branch_id;
				//}
				
				$model->setIsNewRecord(true);
				if($model->save())
				{
					$model->refresh();
					//save attachments
					// make the directory to store the pic:
					
					$project=Project::model()->findByPk($model->project_id);
					$project_no=$project->project_no==""||$project->project_no <= 0 ? $project->project_id:$project->project_no;
					$project_no=$project_no."-".$project->projectType->code_lbl."-".$model->communication_id;

					$this->saveEmailAttachments($model->emailId,$model->communication_id,$project_no);
					
					
					
					//email ends
					//EQuickDlgs::checkDialogJsScript();
					//$this->redirect(array('home/admin','id'=>$model->communication_id));
					echo "yes";
				}
				else
				{
					echo "Some error occured.Please try again";
				}
				
			}
			else
			{
				foreach($errors as $val)
				{
					echo $val;
				}
			}
			
				
		}

		
		//EQuickDlgs::render('mail_form',array(
			//'model'=>$model,
		//));
	}
	
	public function actionUpdateCommOnly($id,$pid)
	{
		if(isset($_POST['Communication']))
		{
			$this->actionCreate($pid,0);
		}
		else
		{
			$model=$this->loadModel($id);
		}
		
		
		EQuickDlgs::render('_combineForm',array(
			'model'=>$model,
		));
	}
	
	/*
	
	/*
	

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id,$pid,$tid)
	{
		
		
		if(isset($_POST['Communication']))
		{
			$this->actionCreate($pid,$tid);
		}
		else
		{
			$model=$this->loadModel($id);
		}
		

		EQuickDlgs::render('_combineForm',array(
			'model'=>$model,
		));
		
		
	}
	
	
	/*
	public function actionUpdate($id,$pid,$tid)
	{
		if(isset($_POST['Communication']) && (isset($_POST['yt2']) || isset($_POST['yt3'])))
		{
			$this->actionCreate($pid,$tid);
		}
		else{
			$model=$this->loadModel($id);
		
		

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		
		
		if($pid <= 0)
		{
			$task=Task::model()->find(array(
				    'select'=>'communication_id',
				    'condition'=>'task_id=:task_id',
				    'params'=>array(':task_id'=>$tid),
				));
				
				$comm_id=$task->communication_id;
				
				$comm=Communication::model()->find(array(
				    'select'=>'project_id',
				    'condition'=>'communication_id=:communication_id',
				    'params'=>array(':communication_id'=>$comm_id),
				));
				
				$pid=$comm->project_id;
		}
		

		if(isset($_POST['Communication']))
		{
			$model->attributes=$_POST['Communication'];
			
			$id=Yii::app()->user->id;
				$user=User::model()->find(array(
				    'select'=>'user_id,user_name,branch_id',
				    'condition'=>'user_id=:userID',
				    'params'=>array(':userID'=>$id),
				));
				
			if($model->is_internal > 0)
			{
				$model->is_internal="Yes";
			}
			else{
				$model->is_internal="No";
			}
			if($model->is_client > 0)
			{
				$model->comm_from=0;
			}
			else{
				
				
				$model->comm_from=$user->branch_id;
			}
			if($model->save())
			{
					//save attachments
					// make the directory to store the pic:
						$message= new YiiMailMessage;
					
					$files = CUploadedFile::getInstancesByName('files');
					if (isset($files) && count($files) > 0) {
						foreach ($files as $image => $pic) {
						$uniqid=MyCustomClass::uniqueRandom();
						echo $pic->name.'<br />';
						//die;
							if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/images/uploads/commfiles/'. $uniqid.$pic->name)) {
							$path=realpath(Yii::app()->basePath . '/../images/uploads/commfiles/'. $uniqid.$pic->name);
							$swiftAttachment = Swift_Attachment::fromPath($path,$pic->type)->setFilename($pic->name); 
							$message->attach($swiftAttachment);	
								
							$img_add = new CommunicationAttachment();
	                        $img_add->attachment = $uniqid.$pic->name; //it might be $img_add->name for you, filename is just what I chose to call it in my model
	                        $img_add->communication_id = $model->communication_id; // this links your picture model to the main model (like your user, or profile model)
	
	                        $img_add->save(); // DONE
							
	 
							}
							else
							{
                        		echo 'Cannot upload!';
                    		}
						}
					}
					//end save attachments
					
					//email
					if(isset($_POST['yt1']))
					{
						$pid=$model->project_id;
		 
						$criteria=new CDbCriteria;
						$criteria->select='*'; 
						$criteria->condition='project_id=:project_id';
						$criteria->params=array(':project_id'=>$pid);
						$passoc=ProjectAssociates::model()->findAll($criteria);
						
					
						$message->view = "test";
						$params= array('myMail'=>$model);
						$message->subject    = $model->communication_subject;
						$message->setBody($params, 'text/html');
						//$message->from = 'enravel@enravel.com.au';
						if($_POST['emailtoclient'] ==1)
						{
							$message->from = array('enravel@enravel.com.au'=>'Enravel');
						}
						else
						{
							$emailUser=EmailUser::model()->getEmailDetailsById($id);
							if($emailUser->email_id != "")
							{
								$message->from=array($emailUser->email_id=>$user->user_name);
							}
							else
							{
								$message->from = array('enravel@enravel.com.au'=>'Enravel');
							}
						}
						foreach($passoc as $values)
						{
							$uid=$values->user_id;
					
							$criteria=new CDbCriteria;
							$criteria->select='*'; 
							$criteria->condition='user_id=:user_id';
							$criteria->params=array(':user_id'=>$uid);
							$passoc=User::model()->find($criteria);
							
							$email=$passoc->login_name;					                        
					        $message->addTo($email);					          					      
						}
						
						//also sent to client,but conformation after wards
						if($_POST['emailtoclient'] ==1)
						{
							$criteria2=new CDbCriteria;
							$criteria2->select='t.*,tu.contact_email';  // only select the 'title' column
							$criteria2->join='LEFT JOIN `pm_client_contact` as `tu` ON t.contact_id=tu.client_contact_id';
							$criteria2->condition="t.project_id=$pid";
							
							//$criteria->params=array(':task_id'=>$tid);
							$project_contact=ProjectContact::model()->findAll($criteria2); // $params is not needed
							
							foreach($project_contact as $contactval)
							{
								if($contactval->contact_email != "")
								{
									$email=$contactval->contact_email;					                        
						        	$message->addTo($email);
								}
							}
						}
						
						
						
						 if(Yii::app()->mail->send($message))
						 {
						 	Communication::model()->updateByPk($model->communication_id, array(
					    	'mail_sent' => 'Yes'
							));
						 } 
						 else
						 {
						 	Communication::model()->updateByPk($model->communication_id, array(
					    	'mail_sent' => 'No'
							));
						 }
					}
					else{
						Communication::model()->updateByPk($model->communication_id, array(
					    	'mail_sent' => 'No'
							));
					}
					//email ends
				EQuickDlgs::checkDialogJsScript();
				$this->redirect(array('home/admin','id'=>$model->communication_id));
			}
				
		}

		EQuickDlgs::render('update',array(
			'model'=>$model,
		));
		}
		
	}
	*/

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		try
		{
			if($this->loadModel($id)->delete())
					{
						//delete communication attachments;
						$attachments=CommunicationAttachment::model()->findAll('communication_id=:communication_id',array(':communication_id'=>$id));
						
						if(count($attachments) > 0)
						{
							foreach($attachments as $attach)
							{
								$this->actionDeleteFile($attach->communication_attachment_id,$attach->attachment);
							}
						}
					}
					else{
						throw new CHttpException(400, "Cannot delete.The communication is in use");
					}
		}
		catch(CDbException $e)
		{
			throw new CHttpException(400, "Some error occured while deleting.Please try again");
		}
		

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		//$dataProvider=new CActiveDataProvider('Communication');
		//$this->render('index',array(
			//'dataProvider'=>$dataProvider,
		//));
		$this->actionAdmin();
	}
	
	
	

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Communication('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Communication']))
			$model->attributes=$_GET['Communication'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionManage()
	{
		$model=new Communication('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Communication']))
			$model->attributes=$_GET['Communication'];

		$this->render('manage',array(
			'model'=>$model,
		));
	}
	
	public function actionExcel(){
		$model=new Communication('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Communication']))
			$model->attributes=$_GET['Communication'];
		$this->widget('application.extensions.eexcel.EExcelView', array(
		'dataProvider'=> $model->search(),
		'grid_mode'=>'export',
		'exportType'=>'Excel5',
		'filename'=>'report',
		));
	}
	
	
	/*
	to hide unhide communication,change status
	*/
	
	
	public function actions()
	{
		return array(
                'toggleStatus' => array(
                        'class'=>'bootstrap.actions.TbToggleAction',
                        'modelName' => 'Communication',
						'scenario'=>'toggle',
                        ),
						'toggleBookmark' => array(
                        'class'=>'bootstrap.actions.TbToggleAction',
                        'modelName' => 'Communication',
                        )
                );

	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Communication the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Communication::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	
	public function actionDeleteFile($cfid,$file)
	{
		$upload_path = Yii::getPathOfAlias('webroot').'/images/uploads/commfiles/';
		CommunicationAttachment::model()->deleteByPk($cfid);
		if(file_exists($upload_path.$file))
		{
			unlink($upload_path.$file);
		}
		
	}

	/**
	 * Performs the AJAX validation.
	 * @param Communication $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='communication-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

public function actionMessageDialog()
{
	$model=new Communication;
	$reply=new Reply;
}


//################# commented code previously saved as new,update  ####################//
	
	/*
	
	public function actionCreate($pid,$tid)
	{
		
		if($pid <= 0)
		{
			$task=Task::model()->find(array(
				    'select'=>'communication_id',
				    'condition'=>'task_id=:task_id',
				    'params'=>array(':task_id'=>$tid),
				));
				
				$comm_id=$task->communication_id;
				
				$comm=Communication::model()->find(array(
				    'select'=>'project_id',
				    'condition'=>'communication_id=:communication_id',
				    'params'=>array(':communication_id'=>$comm_id),
				));
				
				$pid=$comm->project_id;
		}
		$model=new Communication;
		$model->communication_date=date('Y-m-d');
		$model->project_id=$pid;
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		
		

		if(isset($_POST['Communication']))
		{
			
			
			$id=Yii::app()->user->id;
			$user=User::model()->find(array(
				    'select'=>'user_id,user_name,branch_id',
				    'condition'=>'user_id=:userID',
				    'params'=>array(':userID'=>$id),
				));
				
			$model->attributes=$_POST['Communication'];
			$model->status=0;//unhidden
			$model->user_id=Yii::app()->user->id;
			$model->bookmark=0;//not bookmarked
			
			if($model->is_internal > 0)
			{
				$model->is_internal="Yes";
			}
			else{
				$model->is_internal="No";
			}
			//if($model->is_client > 0)
			//{
				//$model->comm_from=0;
			//}
			//else{
				
				//$model->comm_from=$user->branch_id;
			//}
				
			$model->emailId=0;	
				
			if($model->save())
				{
					//save attachments
					// make the directory to store the pic:
					$message= new YiiMailMessage;

						$files_arry=array();
					$files = CUploadedFile::getInstancesByName('files');
					if (isset($files) && count($files) > 0) {
						foreach ($files as $image => $pic) {
						$uniqid=MyCustomClass::uniqueRandom();
						echo $pic->name.'<br />';
						//die;
							if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/images/uploads/commfiles/'. $uniqid.$pic->name)) {
							$files_arry[]=$uniqid.$pic->name;
							$path=realpath(Yii::app()->basePath . '/../images/uploads/commfiles/'. $uniqid.$pic->name);
							$swiftAttachment = Swift_Attachment::fromPath($path,$pic->type)->setFilename($pic->name); 
							$message->attach($swiftAttachment);
							$img_add = new CommunicationAttachment();
	                        $img_add->attachment = $uniqid.$pic->name; //it might be $img_add->name for you, filename is just what I chose to call it in my model
	                        $img_add->communication_id = $model->communication_id; // this links your picture model to the main model (like your user, or profile model)
	 
	                        $img_add->save(); // DONE
							}
							else
							{
                        		echo 'Cannot upload!';
                    		}
						}
					}
					//end save attachments
					
					//also save if commu is of task
					if($tid > 0)
					{
						$taskCommModel=new TaskCommunication();
						$taskCommModel->task_id=$tid;
						$taskCommModel->communication_id=$model->communication_id;
						
						$taskCommModel->save();
					}
				
					//email
					if(isset($_POST['yt1']) || isset($_POST['yt3']))
					{
						$pid=$model->project_id;
		 
						$criteria=new CDbCriteria;
						$criteria->select='*'; 
						$criteria->condition='project_id=:project_id';
						$criteria->params=array(':project_id'=>$pid);
						$passoc=ProjectAssociates::model()->findAll($criteria);
						
						
						$message->view = "test";
						$params= array('myMail'=>$model);
						$message->subject    = $model->communication_subject;
						$message->setBody($params, 'text/html');
						
						if($_POST['emailtoclient'] ==1)
						{
							$message->from = array('enravel@enravel.com.au'=>'Enravel');
						}
						else
						{
							$emailUser=EmailUser::model()->getEmailDetailsById($id);
							if($emailUser->email_id != "")
							{
								$message->from=array($emailUser->email_id=>$user->user_name);
							}
							else
							{
								$message->from = array('enravel@enravel.com.au'=>'Enravel');
							}
						}
						
						
						
						
						
						
						
						foreach($passoc as $values)
						{
							$uid=$values->user_id;
					
							$criteria=new CDbCriteria;
							$criteria->select='*'; 
							$criteria->condition='user_id=:user_id';
							$criteria->params=array(':user_id'=>$uid);
							$passoc=User::model()->find($criteria);
							
							$email=$passoc->login_name;					                        
					        $message->addTo($email);					          					      
						}
						
						//also sent to client,but conformation after wards
						if($_POST['emailtoclient'] ==1)
						{
							
							$criteria2=new CDbCriteria;
							$criteria2->select='t.*,tu.contact_email';  // only select the 'title' column
							$criteria2->join='LEFT JOIN `pm_client_contact` as `tu` ON t.contact_id=tu.client_contact_id';
							$criteria2->condition="t.project_id=$pid";
							
							//$criteria->params=array(':task_id'=>$tid);
							$project_contact=ProjectContact::model()->findAll($criteria2); // $params is not needed
							
							foreach($project_contact as $contactval)
							{
								if($contactval->contact_email != "")
								{
									$email=$contactval->contact_email;					                        
						        	$message->addTo($email);
								}
							}
						}
						
		
		
						
						 if(Yii::app()->mail->send($message))
						 {
						 	Communication::model()->updateByPk($model->communication_id, array(
					    	'mail_sent' => 'Yes'
							));
						 } 
						 else
						 {
						 	Communication::model()->updateByPk($model->communication_id, array(
					    	'mail_sent' => 'No'
							));
						 }
					}
					else{
						Communication::model()->updateByPk($model->communication_id, array(
					    	'mail_sent' => 'No'
							));
					}
					//email ends
					EQuickDlgs::checkDialogJsScript();
					$this->redirect(array('home/admin','id'=>$model->communication_id));
				}
				
			
				
		}

		
		EQuickDlgs::render('create',array(
			'model'=>$model,
		));
	}
	
	public function actionUpdateCommOnly($id,$pid)
	{
		if(isset($_POST['Communication']) && (isset($_POST['yt2']) || isset($_POST['yt3'])))
		{
			$this->actionCreate($pid,0);
		}
		else{
			$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		

		if(isset($_POST['Communication']))
		{
			$model->attributes=$_POST['Communication'];
			
			$user_id=Yii::app()->user->id;
				$user=User::model()->find(array(
				    'select'=>'user_id,user_name,branch_id',
				    'condition'=>'user_id=:userID',
				    'params'=>array(':userID'=>$user_id),
				));
				
				
			if($model->is_internal > 0)
			{
				$model->is_internal="Yes";
			}
			else{
				$model->is_internal="No";
			}
			if($model->is_client > 0)
			{
				$model->comm_from=0;
			}
			else{
				
				
				$model->comm_from=$user->branch_id;
			}
			if($model->save())
			{
						//save attachments
					// make the directory to store the pic:
						$message= new YiiMailMessage;
					
					$files = CUploadedFile::getInstancesByName('files');
					if (isset($files) && count($files) > 0) {
						foreach ($files as $image => $pic) {
						$uniqid=MyCustomClass::uniqueRandom();
						echo $pic->name.'<br />';
						
						
						
						//die;
							if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/images/uploads/commfiles/'. $uniqid.$pic->name)) {
								
							$path=realpath(Yii::app()->basePath . '/../images/uploads/commfiles/'. $uniqid.$pic->name);
							$swiftAttachment = Swift_Attachment::fromPath($path,$pic->type)->setFilename($pic->name); 
							$message->attach($swiftAttachment);
								
							$img_add = new CommunicationAttachment();
	                        $img_add->attachment = $uniqid.$pic->name; //it might be $img_add->name for you, filename is just what I chose to call it in my model
	                        $img_add->communication_id = $model->communication_id; // this links your picture model to the main model (like your user, or profile model)
	 
	                        $img_add->save(); // DONE
							}
							else
							{
                        		echo 'Cannot upload!';
                    		}
						}
					}
					//end save attachments
					
					//email
					if(isset($_POST['yt1']))
					{
						$pid=$model->project_id;
		 
						$criteria=new CDbCriteria;
						$criteria->select='*'; 
						$criteria->condition='project_id=:project_id';
						$criteria->params=array(':project_id'=>$pid);
						$passoc=ProjectAssociates::model()->findAll($criteria);
						
					
						$message->view = "test";
						$params= array('myMail'=>$model);
						$message->subject    = $model->communication_subject;
						$message->setBody($params, 'text/html');
						
						
						if($_POST['emailtoclient'] ==1)
						{
							$message->from = array('enravel@enravel.com.au'=>'Enravel');
						}
						else
						{
							$emailUser=EmailUser::model()->getEmailDetailsById($user_id);
							if($emailUser->email_id != "")
							{
								$message->from=array($emailUser->email_id=>$user->user_name);
							}
							else
							{
								$message->from = array('enravel@enravel.com.au'=>'Enravel');
							}
						}
						
						foreach($passoc as $values)
						{
							$uid=$values->user_id;
					
							$criteria=new CDbCriteria;
							$criteria->select='*'; 
							$criteria->condition='user_id=:user_id';
							$criteria->params=array(':user_id'=>$uid);
							$passoc=User::model()->find($criteria);
							
							$email=$passoc->login_name;					                        
					        $message->addTo($email);					          					      
						}
						
						//also sent to client,but conformation after wards
						if($_POST['emailtoclient'] ==1)
						{
							$criteria2=new CDbCriteria;
							$criteria2->select='t.*,tu.contact_email';  // only select the 'title' column
							$criteria2->join='LEFT JOIN `pm_client_contact` as `tu` ON t.contact_id=tu.client_contact_id';
							$criteria2->condition="t.project_id=$pid";
							
							//$criteria->params=array(':task_id'=>$tid);
							$project_contact=ProjectContact::model()->findAll($criteria2); // $params is not needed
							
							foreach($project_contact as $contactval)
							{
								if($contactval->contact_email != "")
								{
									$email=$contactval->contact_email;					                        
						        	$message->addTo($email);
								}
							}
						}
						
						
						
						 if(Yii::app()->mail->send($message))
						 {
						 	Communication::model()->updateByPk($model->communication_id, array(
					    	'mail_sent' => 'Yes'
							));
						 } 
						 else
						 {
						 	Communication::model()->updateByPk($model->communication_id, array(
					    	'mail_sent' => 'No'
							));
						 }
					}
					else{
						Communication::model()->updateByPk($model->communication_id, array(
					    	'mail_sent' => 'No'
							));
					}
					//email ends
				EQuickDlgs::checkDialogJsScript();
				$this->redirect(array('communication/admin','id'=>$model->communication_id));
			}
				
		}

		EQuickDlgs::render('update',array(
			'model'=>$model,
		));
		}
		
	}
	
	
	public function actionUpdate($id,$pid,$tid)
	{
		if(isset($_POST['Communication']) && (isset($_POST['yt2']) || isset($_POST['yt3'])))
		{
			$this->actionCreate($pid,$tid);
		}
		else{
			$model=$this->loadModel($id);
		
		

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		
		
		if($pid <= 0)
		{
			$task=Task::model()->find(array(
				    'select'=>'communication_id',
				    'condition'=>'task_id=:task_id',
				    'params'=>array(':task_id'=>$tid),
				));
				
				$comm_id=$task->communication_id;
				
				$comm=Communication::model()->find(array(
				    'select'=>'project_id',
				    'condition'=>'communication_id=:communication_id',
				    'params'=>array(':communication_id'=>$comm_id),
				));
				
				$pid=$comm->project_id;
		}
		

		if(isset($_POST['Communication']))
		{
			$model->attributes=$_POST['Communication'];
			
			$id=Yii::app()->user->id;
				$user=User::model()->find(array(
				    'select'=>'user_id,user_name,branch_id',
				    'condition'=>'user_id=:userID',
				    'params'=>array(':userID'=>$id),
				));
				
			if($model->is_internal > 0)
			{
				$model->is_internal="Yes";
			}
			else{
				$model->is_internal="No";
			}
			//if($model->is_client > 0)
			//{
				//$model->comm_from=0;
			//}
			//else{
				
				
				//$model->comm_from=$user->branch_id;
			//}
			if($model->save())
			{
					//save attachments
					// make the directory to store the pic:
						$message= new YiiMailMessage;
					
					$files = CUploadedFile::getInstancesByName('files');
					if (isset($files) && count($files) > 0) {
						foreach ($files as $image => $pic) {
						$uniqid=MyCustomClass::uniqueRandom();
						echo $pic->name.'<br />';
						//die;
							if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/images/uploads/commfiles/'. $uniqid.$pic->name)) {
							$path=realpath(Yii::app()->basePath . '/../images/uploads/commfiles/'. $uniqid.$pic->name);
							$swiftAttachment = Swift_Attachment::fromPath($path,$pic->type)->setFilename($pic->name); 
							$message->attach($swiftAttachment);	
								
							$img_add = new CommunicationAttachment();
	                        $img_add->attachment = $uniqid.$pic->name; //it might be $img_add->name for you, filename is just what I chose to call it in my model
	                        $img_add->communication_id = $model->communication_id; // this links your picture model to the main model (like your user, or profile model)
	
	                        $img_add->save(); // DONE
							
	 
							}
							else
							{
                        		echo 'Cannot upload!';
                    		}
						}
					}
					//end save attachments
					
					//email
					if(isset($_POST['yt1']))
					{
						$pid=$model->project_id;
		 
						$criteria=new CDbCriteria;
						$criteria->select='*'; 
						$criteria->condition='project_id=:project_id';
						$criteria->params=array(':project_id'=>$pid);
						$passoc=ProjectAssociates::model()->findAll($criteria);
						
					
						$message->view = "test";
						$params= array('myMail'=>$model);
						$message->subject    = $model->communication_subject;
						$message->setBody($params, 'text/html');
						//$message->from = 'enravel@enravel.com.au';
						if($_POST['emailtoclient'] ==1)
						{
							$message->from = array('enravel@enravel.com.au'=>'Enravel');
						}
						else
						{
							$emailUser=EmailUser::model()->getEmailDetailsById($id);
							if($emailUser->email_id != "")
							{
								$message->from=array($emailUser->email_id=>$user->user_name);
							}
							else
							{
								$message->from = array('enravel@enravel.com.au'=>'Enravel');
							}
						}
						foreach($passoc as $values)
						{
							$uid=$values->user_id;
					
							$criteria=new CDbCriteria;
							$criteria->select='*'; 
							$criteria->condition='user_id=:user_id';
							$criteria->params=array(':user_id'=>$uid);
							$passoc=User::model()->find($criteria);
							
							$email=$passoc->login_name;					                        
					        $message->addTo($email);					          					      
						}
						
						//also sent to client,but conformation after wards
						if($_POST['emailtoclient'] ==1)
						{
							$criteria2=new CDbCriteria;
							$criteria2->select='t.*,tu.contact_email';  // only select the 'title' column
							$criteria2->join='LEFT JOIN `pm_client_contact` as `tu` ON t.contact_id=tu.client_contact_id';
							$criteria2->condition="t.project_id=$pid";
							
							//$criteria->params=array(':task_id'=>$tid);
							$project_contact=ProjectContact::model()->findAll($criteria2); // $params is not needed
							
							foreach($project_contact as $contactval)
							{
								if($contactval->contact_email != "")
								{
									$email=$contactval->contact_email;					                        
						        	$message->addTo($email);
								}
							}
						}
						
						
						
						 if(Yii::app()->mail->send($message))
						 {
						 	Communication::model()->updateByPk($model->communication_id, array(
					    	'mail_sent' => 'Yes'
							));
						 } 
						 else
						 {
						 	Communication::model()->updateByPk($model->communication_id, array(
					    	'mail_sent' => 'No'
							));
						 }
					}
					else{
						Communication::model()->updateByPk($model->communication_id, array(
					    	'mail_sent' => 'No'
							));
					}
					//email ends
				EQuickDlgs::checkDialogJsScript();
				$this->redirect(array('home/admin','id'=>$model->communication_id));
			}
				
		}

		EQuickDlgs::render('update',array(
			'model'=>$model,
		));
		}
		
	}
	
	
	*/
}
