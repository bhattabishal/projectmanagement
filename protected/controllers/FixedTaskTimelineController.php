<?php

class FixedTaskTimelineController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	 public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('@'),
            ),
            
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('delete','loadByBrief','update'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionLoadByBrief()
	{
		$id=explode("-",Yii::app()->getRequest()->getParam('id'));
		$id=$id[0];
		
		$criteria=new CDbCriteria;
		$criteria->select='*';  // only select the 'title' column
		$criteria->condition='fxt_brief_id=:id';
		$criteria->params=array(':id'=>$id);
		$records=FixedTaskTimeline::model()->findAll($criteria); // $params is not needed
		
		
		$dataProvider=new CActiveDataProvider(FixedTaskTimeline, array(
    	'data'=>$records,));
		
		
		
		$this->renderPartial('_fixed_form_details', array(
			
			'gridDataProvider' => $dataProvider,
			//'gridColumns' => $this->getGridColumns()
			'bid' => $id,
			),false,true);
	}


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new FixedTaskTimeline;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['FixedTaskTimeline']))
		{
			$model->attributes=$_POST['FixedTaskTimeline'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->fxt_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['FixedTaskTimeline']))
		{
			$model->attributes=$_POST['FixedTaskTimeline'];
			$transaction = $model->getDbConnection()->beginTransaction();
			try{
				
				if($model->save())
				{
					 $model->refresh();
					 
					 $actual=ActualTimeline::model()->find('admin_timeline_id=:id and task_type=:type',array(':id'=>$model->fxt_id,':type'=>'admin'));
					 
					 $aid=$actual->timeline_id;
					 
					 $pid=$model->project_id;
					 $adminProjectTask=Task::model()->find('admin_task_id=:id',array(':id'=>$pid));
					 
					 $actualModel=ActualTimeline::model()->findByPk($aid);
                                         
					 $actualModel->task_id=$adminProjectTask->task_id;
				   $actualModel->user_id=$model->user_id;
				   $actualModel->normal_hour=$model->nt_hours;
				   $actualModel->cusion_hour=0;
				   $actualModel->overtime_hour=$model->ot_hours;
				   $actualModel->task_date=$model->entry_dt;
				   $actualModel->percentage_completed=0;
				   $actualModel->schedule_id_completed=0;
				   $actualModel->remarks=$model->remarks;
				   $actualModel->task_type='admin';
				   $actualModel->admin_timeline_id=$model->fxt_id;
				 
				   $actualModel->save(FALSE);
					 
				}
				
				 $transaction->commit();
				 
				EQuickDlgs::checkDialogJsScript();
				$this->redirect(array('view','id'=>$model->fxt_id));
			}
			catch(Exception $e)
			{
				$transaction->rollback();
                throw new CHttpException(404, 'Some error occured while saving.Please try again'); 
			}
			
			
				
		}

		EQuickDlgs::render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		 $actual=ActualTimeline::model()->find('admin_timeline_id=:id and task_type=:type',array(':id'=>$id,':type'=>'admin'));
					 
		$aid=$actual->timeline_id;
		
		ActualTimeline::model()->deleteByPk($aid);
		
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('FixedTaskTimeline');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new FixedTaskTimeline('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['FixedTaskTimeline']))
			$model->attributes=$_GET['FixedTaskTimeline'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return FixedTaskTimeline the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=FixedTaskTimeline::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param FixedTaskTimeline $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='fixed-task-timeline-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
