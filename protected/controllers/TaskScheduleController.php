<?php

class TaskScheduleController extends Controller
{

	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout = '//layouts/column1';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl',// perform access control for CRUD operations
			'postOnly + delete',// we only allow deletion via POST request
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow'  ,// allow all users to perform 'index' and 'view' actions
				'actions' => array('index','view'),
				'users'   => array('@'),
			),
			array('allow'  ,// allow authenticated user to perform 'create' and 'update' actions
				'actions' => array('create','update','dynamicTaskAssignment','dynamicTaskSchedule','report','scheduleReport','updatePlanned','createReportDialog','taskSchedulePopUpUpdate','taskSchedulePopUpShift','checkLink','getPlannedValues'),
				'users'   => array('@'),
			),
			array('allow'  ,// allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('admin','delete'),
				'users'   => array('@'),
			),
			array('deny' ,// deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		EQuickDlgs::render('view', array(
				'model'=> $this->loadModel($id),
			));
	}

	/*
	same as update only difference is the form after save
	some conditions for update

	1. if work not done than update the exsisting record
	2. if work done than give option to change the date only or change the record

	the above two is divided into two methods
	1. actionTaskSchedulePopUpUpdate

	*/

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionTaskSchedulePopUpUpdate($id)
	{
		$model    = $this->loadModel($id);
		//$oldModel = $model;
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['TaskSchedule']))
		{
			$model->attributes = $_POST['TaskSchedule'];

			if($model->validate())
			{
					$model->save();
				

				EQuickDlgs::checkDialogJsScript();
				$this->redirect(array('_report','id'=> $model->schedule_id));
			}
		}

		EQuickDlgs::render('_updateSchedulePopUp', array(
				'model'=> $model,
			));
	}

	public function actionTaskSchedulePopUpShift($id)
	{


		$model    = $this->loadModel($id);
	


		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['TaskSchedule']))
		{

			$model->attributes = $_POST['TaskSchedule'];
                     

			if($model->validate())
			{
				$a=$model->changeHour;
				$b=$model->changeDate;
				
				if($model->changeHour > 0)
				{
					$model->save();
				}
				else if($model->changeDate > 0)
				{
					//if date changed than leave the record as it is  marked completed.changed normal hour and cusion hour as worked hours.insert new record for the selected date
					
					//$oldRecord= TaskSchedule::model()->findByPk($model->schedule_id);
                                       $oldModel = $this->loadModel($id);
					$transaction = $oldModel->getDbConnection()->beginTransaction();
					
					try
					{
						
						
						$oldModel->normal_hour=$oldModel->worked_normal_hr;
						$oldModel->overtime_hour=$oldModel->worked_ot_hr;
						
						$c=$oldModel->normal_hour;
						$d=$oldModel->overtime_hour;
						
						$oldModel->completed_status=1;
						$oldModel->save(false);
						$oldModel->refresh();
						
						//check if for new record already task scheduled
						$existingRecord=TaskSchedule::model()->find('task_id=:task_id and user_id = :user_id and schedule_date = :schedule_date', array(':task_id'=>$model->task_id,':user_id'=>$model->user_id,':schedule_date'=>$model->schedule_date));
						
						$newRecord=new TaskSchedule;
						
						if($existingRecord->schedule_id > 0)//already task in shifting date
						{
							$newRecord->isNewRecord=FALSE;
							$newRecord->schedule_id=$existingRecord->schedule_id;
							$newRecord->task_id=$model->task_id;
							$newRecord->user_id=$model->user_id;
							$newRecord->schedule_date=$model->schedule_date;
							$newRecord->normal_hour=$existingRecord->normal_hour+($model->normal_hour-$model->worked_normal_hr);
							$e=$newRecord->normal_hour;
							$f=$newRecord->overtime_hour;
							
							$newRecord->overtime_hour=$existingRecord->overtime_hour+($model->overtime_hour-$model->worked_ot_hr);
							$newRecord->cusion_hour=CodeValue::model()->getCodeTypeValue('work_hour',29);
							$newRecord->worked_normal_hr=$existingRecord->worked_normal_hr;
							$newRecord->worked_ot_hr=$existingRecord->worked_ot_hr;
							$newRecord->schedule_nr_hr=$existingRecord->schedule_nr_hr;
							$newRecord->schedule_ot_hr=$existingRecord->schedule_ot_hr;
							$newRecord->remarks="";
							$newRecord->completed_status=0;
							$newRecord->extended_status=$existingRecord->extended_status;
							
							$newRecord->save(false);
							
						}
						else
						{
							
							$newRecord->isNewRecord=true;
							$newRecord->task_id=$model->task_id;
							$newRecord->user_id=$model->user_id;
							$newRecord->schedule_date=$model->schedule_date;
							$newRecord->normal_hour=$model->normal_hour-$model->worked_normal_hr;
							$newRecord->overtime_hour=$model->overtime_hour-$model->worked_ot_hr;
							$newRecord->cusion_hour=CodeValue::model()->getCodeTypeValue('work_hour',29);
							
							
							
							$e=$newRecord->normal_hour;
							$f=$newRecord->overtime_hour;
							
							$newRecord->worked_normal_hr=0;
							$newRecord->schedule_nr_hr=$newRecord->normal_hour;
							$newRecord->schedule_ot_hr=$newRecord->overtime_hour;
							$newRecord->worked_ot_hr=0;
							$newRecord->remarks="";
							$newRecord->completed_status=0;
							$newRecord->extended_status=0;
							
							$newRecord->save(false);
						}
						$transaction->commit();
					}
					catch(Exception $e)
					{
						$transaction->rollback();
						throw new CHttpException(404, 'Some error occured.Please try again');
					}
				}
				else
				{
					$model->save();
				}
				
				

				EQuickDlgs::checkDialogJsScript();
				$this->redirect(array('scheduleReport'));
			}
			
			
		}

		EQuickDlgs::render('_updateSchedulePopUpShift', array(
				'model'=> $model,
			));
	}

	/*
	* create new task schedule creating new task if user different
	*/

	public function actionCreateReportDialog($user, $task)
	{
		$model = new TaskSchedule;
		$model->scenario = "insert";
		$model->schedule_date = date('Y-m-d');
		$model->user_id = $user;
		$model->task_id = $task;
		$model->completed_status = 0;
		$model->extended_status = 0;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['TaskSchedule']))
		{
			$model->attributes = $_POST['TaskSchedule'];

			$newTask = FALSE;

			if($model->user_id != $user)
			{
				$newTask = true;
				//create new task and assign schedule
				$oldTask = Task::model()->findByPk($task);
				$task    = new Task();
				$task->isNewRecord = true;
				$task->job_details = $oldTask->job_details;
				$task->piority = $oldTask->piority;
				$task->sharable = $oldTask->sharable;
				$task->job_start_date = $oldTask->job_start_date;
				$task->job_due_date = $oldTask->job_due_date;
				$task->est_rps_cost = $model->normal_hour + $model->overtime_hour;
				$task->est_ao_cost = $task->est_rps_cost;
				$task->actual_rps_cost = 0;
				$task->actual_ao_cost = 0;
				$task->done_percentage = 0;
				$task->assigned_to = $model->user_id;
				$task->task_status = 11;
				$task->error_known = 0;
				$task->error_unknown = 0;
				$task->task_cat = $oldTask->task_cat;
				$task->communication_id = $oldTask->communication_id;
				$task->job_type = $oldTask->job_type;
				$task->is_hidden = $oldTask->is_hidden;
				$task->variation = $oldTask->variation;
				$task->is_planned = $oldTask->is_planned;


				//task schedule
			}

			$valid = $model->validate();
			if($newTask)
			{
				$valid = $task->validate() && $valid;
			}


			if($valid)
			{
				$transaction = $model->getDbConnection()->beginTransaction();
				try
				{

					if($newTask)
					{
						$task->save();
						$task->refresh();
						$model->task_id = $task->task_id;
					}
					$model->save();
					$transaction->commit();
					EQuickDlgs::checkDialogJsScript();
					$this->redirect(array('scheduleReport'));
				} catch(Exception $ex)
				{
					$transaction->rollback();
					throw new CHttpException(404, 'Some error occured.Please try again');
				}
			}


		}

		EQuickDlgs::render('_updateSchedulePopUp', array(
				'model'=> $model,
			));
	}

	public function actionDynamicTaskAssignment()
	{

		if(isset($_POST['user_id']) && $_POST['user_id'] > 0)
		{
			$user_id = $_POST['user_id'];
			$model   = new TaskSchedule;
			$results = $model->getActiveTaskScheduleSummary($user_id);
			$this->renderPartial('_summary', array(
					'results'=> $results,
				), false, true);
		}
	}

	public function actionReport()
	{
		EQuickDlgs::render('_report');
	}
	
	

	public function actionScheduleReport()
	{
		//$baseUrl = Yii::app()->baseUrl;
		//Yii::app()->clientScript->scriptMap = array(
		/// 'screen.css' => false
		///);
		//Yii::app()->clientScript->reset();
		//Yii::app()->clientScript->scriptMap['jquery.js'] = false;
		//Yii::app()->clientScript->registerCoreScript($baseUrl.' / fixedHeader / js / jquery - 1.7.2.js');
            $model=new TaskSchedule();
            $date_range=Yii::app()->db->createCommand("SELECT
            min(pm_task_schedule.schedule_date) as mindate,
            max(pm_task_schedule.schedule_date) as maxdate

            FROM
            pm_task_schedule
            INNER JOIN pm_task ON pm_task_schedule.task_id = pm_task.task_id
            WHERE
            pm_task.task_status <> 26")->queryRow();
            
            
              
                if($date_range['mindate'] != "")
                {
                    $model->stDtSearch=$date_range['mindate'];
                    $model->enDtSearch=$date_range['maxdate'];

                    $days=MyCustomClass::GetDateDifference( $model->enDtSearch,$model->stDtSearch);
                    $days+=1;//for current date the difference is 0 so make it one.we need total days count
                    if($days < 30)
                    {
                                $diff=30-$days;
                                 $model->enDtSearch=MyCustomClass::addDays( $model->enDtSearch,$diff);
                        }
                        $model->days=$days;
                }
                else
                {
                        $model->stDtSearch=MyCustomClass::subractDays(date('Y-m-d'),2);
                        $model->enDtSearch=MyCustomClass::addDays(date('Y-m-d'),28);
                        $days=MyCustomClass::GetDateDifference($model->enDtSearch,$model->stDtSearch);
                        $days+=1;//for current date the difference is 0 so make it one.we need total days count
                        
                        $model->days=$days;
                }
                
                $model->branch_id=Yii::app()->user->getState('branchId');
                $model->ispost=0;

            if(isset($_POST['TaskSchedule']))
            {
                $model->attributes=$_POST['TaskSchedule'];
                $model->ispost=1;
            }
		$this->render('_report', array('model'=>$model), false, true);
	}

	public function actionDynamicTaskSchedule()
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$user_id = $_POST['user_id'];
			$due_date= $_POST['due_date'];
			$rps_hour= floatval($_POST['rps_hour']);
			$ot_hour = floatval($_POST['ot_hour']);
			$task_id = intval($_POST['task_id']);
			$date    = date('Y-m-d');
			$extended= intval($_POST['extended']);

			if($extended == 1)
			{
				$task    = Task::model()->findByPk($task_id);
				$date    = $task->job_due_date;
				$task_id = 0;
			}

			$task = new Task;

			if($user_id == "" || $due_date == "" || !MyCustomClass::is_date($due_date) || $rps_hour <= 0)
			{
				echo '<font color="red">User id, due date or rps hour is invalid.</font>';
			}
			else
			if($ot_hour > $rps_hour)
			{
				echo '<font color="red">OT hours cannot be greater than total rps work hours.</font>';
			}
			//	else if(!$task->normalWorkHourAvailable($date,$due_date,$user_id,$rps_hour,$ot_hour,$task_id))
			//{
			//echo ' < font color = "red" > The work hour is not available.</font > ';
			//}
			else
			{
				$taskModel = new Task;
				$taskModel->assigned_to = $user_id;
				$taskModel->job_due_date = $due_date;
				$taskModel->est_rps_cost = $rps_hour;
				$taskModel->totalOverTimeHours = $ot_hour;
				$taskModel->task_id = $task_id;
				$taskModel->job_start_date = $date;
				$taskModel->extendedTask = $extended;

				$taskSchedule = Task::model()->generateSchedule($taskModel);
				$this->renderPartial('_schedule', array(
						//'user_id'=>$user_id,
						//'due_date'=>$due_date,
						'rps_hour'=> $rps_hour,
						//'ot_hour'=>$ot_hour,
						//'task_id'=>$task_id,
						//'date'=>$date,
						'taskSchedule'=> $taskSchedule,
					), false, true);
			}
		}
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model = new TaskSchedule;
		$model->schedule_date = date('Y-m-d');
		$model->completed_status = 0;
		$model->extended_status = 0;
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['TaskSchedule']))
		{
			$model->attributes = $_POST['TaskSchedule'];
			$model->schedule_nr_hr = $model->normal_hour;
			$model->schedule_ot_hr = $model->overtime_hour;
			if($model->save())
			{
				EQuickDlgs::checkDialogJsScript();
				$this->redirect(array('view','id'=> $model->schedule_id));
			}
		}

		EQuickDlgs::render('create', array(
				'model'=> $model,
			));
	}

public function actionUpdate($id)
	{
		$model    = $this->loadModel($id);
		//$oldModel = $model;
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['TaskSchedule']))
		{
			$model->attributes = $_POST['TaskSchedule'];

			if($model->validate())
			{

				//if(MyCustomClass::GetDateDifference($oldModel->schedule_date, $model->schedule_date) != 0)
				//{
					//TaskSchedule::model()->shiftScheduleWorkNextDate($id, $model->schedule_date);
				//}
				//else
				//{
					$model->save();
				//}

				EQuickDlgs::checkDialogJsScript();
				$this->redirect(array('view','id'=> $model->schedule_id));
			}
		}

		EQuickDlgs::render('update', array(
				'model'=> $model,
			));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		//$dataProvider = new CActiveDataProvider('TaskSchedule');
		//$this->render('index',array(
		//'dataProvider'=>$dataProvider,
		//));
		$this->actionAdmin();
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model = new TaskSchedule('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TaskSchedule']))
		$model->attributes = $_GET['TaskSchedule'];

		$this->render('admin', array(
				'model'=> $model,
			));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return TaskSchedule the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
		$model = TaskSchedule::model()->findByPk($id);
		if($model === null)
		throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	public function actionGetPlannedValues()
	{


		$data = array(
			array("code_id" => 0,"code_lbl"=> "Scheduled"),
			array("code_id" => 1,"code_lbl"=> "Planned"),
		);
		/* Replace the keys in the new array with 'value' and 'text' */
		foreach($data as $k => $v)
		{
			$data[$k]['value'] = $data[$k]['code_id'];
			unset($data[$k]['code_id']);

			$data[$k]['text'] = $data[$k]['code_lbl'];
			unset($data[$k]['code_lbl']);
		}

		echo json_encode($data);
	}

	public function actionUpdatePlanned()
	{

		if(Yii::app()->request->isAjaxRequest)
		{
			Yii::import('bootstrap.widgets.TbEditableSaver');
			$es = new TbEditableSaver('Task');
			//$id = Yii::app()->user->id;
			//$this->updt_cnt = $this->updt_cnt + 1;
			//$this->updt_by = $id;
			$es->update();
		}
		else
		throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Performs the AJAX validation.
	* @param TaskSchedule $model the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax'] === 'task-schedule-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
