<?php

class HomeController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
      //  public $projectDueDate;

        /**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','communication','job','task','create','toggle','downloadFile'),
				'users'=>array('@'),
			),
			//array('allow', // allow authenticated user to perform 'create' and 'update' actions
				//'actions'=>array('create','update'),
				//'users'=>array('@'),
			//),
			//array('allow', // allow admin user to perform 'admin' and 'delete' actions
				//'actions'=>array('admin','delete','create'),
				//'users'=>array('admin'),
			//),
			//array('deny',  // deny all users
				//'users'=>array('*'),
			//),
		);
	}
	
	public function actionView($id)
{
    EQuickDlgs::render('view',array('model'=>$this->loadModel($id)));
}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['HomeForm']))
		{
			$model->attributes=$_POST['HomeForm'];
			if($model->save())
			{
				EQuickDlgs::checkDialogJsScript();
				$this->redirect(array('admin','id'=>$model->project_id));
			}
				
		}

		EQuickDlgs::render('update',array(
			'model'=>$model,
		));
		
		
        	
    	
 
    
	}
	
	
	public function actionCreate()
	{
		$model=new HomeForm;
		$this->performAjaxValidation($model);
		
		if(isset($_POST['HomeForm']))
		{
			$model->attributes=$_POST['HomeForm'];
			if($model->save())
	        {
	            //close the dialog and update the grid instead of redirect if called by the create-dialog
	            EQuickDlgs::checkDialogJsScript();
	            $this->redirect(array('admin'));
	        }
		}
		
 
        //check if the iframe layout (or renderPartial) has to be used
        EQuickDlgs::render('create',array('model'=>$model));
        //$this->render('create',array('model'=>$model));
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	/*public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}*/

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	/*public function actionCreate()
	{
		$model=new HomeForm;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['HomeForm']))
		{
			$model->attributes=$_POST['HomeForm'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->project_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}*/

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	/*public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['HomeForm']))
		{
			$model->attributes=$_POST['HomeForm'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->project_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}*/

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	/*public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}*/

	/**
	 * Lists all models.
	 */
	/*public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('HomeForm');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}*/

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		
		//change status before loading the grid
		$projects=Project::model()->findAll();
		foreach($projects as $project)
		{
			$pid=$project->project_id;
			$status=$project->project_status;
			
			$tasks=Yii::app()->db->createCommand()
				->select('t.task_status,t.task_id')
				->from('pm_project AS p')
				->join('pm_communication c', 'p.project_id = c.project_id')
				->join('pm_task t', 'c.communication_id = t.communication_id')
    			->where('c.`status`=0')
				->andWhere('t.task_status!=26')
				->andWhere('p.project_id=:id', array(':id'=>$pid))
    			->queryAll();
				
				
				if(count($tasks) > 0)
				{
					//print_r($tasks);
					$taskArray=array();
					
					$i=0;
				foreach($tasks as $task)
					{
						
						$taskArray[$i]=$task['task_status'];
						$i++;
					}
			
					//if any task done mark project as ready to issue else active
					if(in_array(12,$taskArray))
					{
						Project::model()->updateByPk($pid, array(
				    	'project_status' => 25//ready
						));
				
						
					}
				else{
						Project::model()->updateByPk($pid, array(
				    	'project_status' => 3//active
						));
				
					
					}
				}
				else{
					
					//if no any task then passive but if already done than do nothing
					
					if($status !=5)
					{
						Project::model()->updateByPk($pid, array(
				    	'project_status' => 4//passive
						));
					}
					
					
					
					
					
				}
		}
		//end 
		
		$model=new HomeForm('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['HomeForm']))
			$model->attributes=$_GET['HomeForm'];

		$this->render('admin',array(
			'model'=>$model,
		));
		
		
	}
	
	
	
	public function actionSelectedAdmin($pno)
	{
		$model=new HomeForm('search');
		$model->unsetAttributes();  // clear any default values
		$model->projectCompositeNo="$pno";
		if(isset($_GET['HomeForm']))
			$model->attributes=$_GET['HomeForm'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return HomeForm the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=HomeForm::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param HomeForm $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='home-form-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	
	
	
	/*
	
	*/
	 public function actionCommunication()
	 {
	 		$id=explode("-",Yii::app()->getRequest()->getParam('id'));
	 		//$sno=$id[0];
	 
	 	//$dataProvider =  new CArrayDataProvider('Communication');
		$this->renderPartial('_communication', array(
			
			'gridDataProvider' => $this->CommunicationDataProvider($id[0]),
			'variationGridDataProvider' => $this->variationDataProvider($id[0]),
			'pid' => $id[0],
			//'cno'=>$sno,
			),false,true);
			
			
	 }
	 
	 public function actionChecklist()
	 {
	 	 $id=explode("-",Yii::app()->getRequest()->getParam('id'));
	 	 if(count($id) <= 2)
	 	 {
		 	$sno=$id[0];
		 }
		 else
		 {
		 	
		 	$sno=$id[2]."-".$id[3]."-".$id[4]."-".$id[0];
		 }
		  
		  
		
	 		$this->renderPartial('_checklist', array(
			
			'gridDataProviderChecklist' => $this->ChecklistDataProvider($id[0]),
			'gridDataProviderTaskComm' => $this->TaskCommDataProvider($id[0]),
			//'gridColumns' => $this->getGridColumns()
			'tid' => $id[0],
			'cklno' =>$sno,
			),false,true);
	 }
	  public function actionTaskComm()
	 {
	 	 $id=explode("-",Yii::app()->getRequest()->getParam('id'));
		 $sno=Yii::app()->getRequest()->getParam('sno')."-" .$id[0];
	 		$this->renderPartial('_taskcomm', array(
			
			'gridDataProvider' => $this->TaskCommDataProvider($id[0]),
			//'gridColumns' => $this->getGridColumns()
			'tid' => $id[0],
			'sno' =>$sno,
			),false,true);
	 }
	   public function TaskCommDataProvider($tid)
	 {
	 	
	 	//$list= Yii::app()->db->createCommand('select * from post where category=:category')->bindValue('category',$category)->queryAll();
	 	$criteria=new CDbCriteria;
		$criteria->select='t.*,tu.task_communication_id';  // only select the 'title' column
		$criteria->join='LEFT JOIN `pm_task_communication` as `tu` ON t.communication_id=tu.communication_id';
		$criteria->condition="tu.task_id=$tid AND t.status=0";
		$criteria->order="t.communication_id DESC";
		//$criteria->params=array(':task_id'=>$tid);
		$project_communicates=Communication::model()->findAll($criteria); // $params is not needed
		
		$dataProvider=new CActiveDataProvider(Communication::model(), array(
    	'data'=>$project_communicates,
	));
		
		return $dataProvider;
	 }
	
	 
	  public function actionTask()
	 {
	 	
		
	 	 $id=explode("-",Yii::app()->getRequest()->getParam('id'));
		 
		 $sno=$id[2]."-".$id[3]."-".$id[0];
		 
	 		$this->renderPartial('_task', array(
			
			'gridDataProvider' => $this->TaskDataProvider($id[0]),
			//'gridColumns' => $this->getGridColumns()
			'cid' => $id[0],
			'tno' =>$sno,
			),false,true);
	 }
	 
	 
	 /*
	 get variation details from the given projectId
	 */
	 public function variationDataProvider($pid)
	 {
	 	$criteria=new CDbCriteria;
		$criteria->select='t.*';  // only select the 'title' column
		$criteria -> join = 'INNER JOIN pm_user on t.user_id = pm_user.user_id ';
		$criteria->condition='t.project_id=:project_id AND t.status=:status';
		
		//do not show internal communication to other users
		if(!Yii::app()->user->checkAccess('viewAllRecentCommunication') && Yii::app()->user->hasState("branchId"))
		{
			$criteria->addCondition('t.is_internal=:is_internal or pm_user.branch_id =:branch_id');
			
			$criteria->params=array(':project_id'=>$pid,':status' => '0',':is_internal'=>'No','branch_id'=>Yii::app()->user->getState("branchId"));
		}
		else{
			$criteria->params=array(':project_id'=>$pid,':status' => '0');
		}
			
		
		
		//$criteria->params=array(':project_id'=>$pid,':status' => '0',':is_internal'=>'No','branch_id'=>Yii::app()->user->getState("branchId"));
		$criteria->order="t.communication_id DESC";
		
		$project_communicates=Communication::model()->findAll($criteria); // $params is not needed
		
		
		$dataProvider=new CActiveDataProvider(Communication, array(
    	'data'=>$project_communicates,
	));
		
		return $dataProvider;
	 }
	 
	 /*
	 	
	 	get communication details from the given projectId
	 */
	 public function CommunicationDataProvider($pid)
	 {
	 
	 	$criteria=new CDbCriteria;
		$criteria->select='t.*';  // only select the 'title' column
		$criteria -> join = 'INNER JOIN pm_user on t.user_id = pm_user.user_id ';
		$criteria->condition='t.project_id=:project_id AND t.status=:status';
		
		//do not show internal communication to other users
		if(!Yii::app()->user->checkAccess('viewAllRecentCommunication') && Yii::app()->user->hasState("branchId"))
		{
			$criteria->addCondition('t.is_internal=:is_internal or pm_user.branch_id =:branch_id');
			
			$criteria->params=array(':project_id'=>$pid,':status' => '0',':is_internal'=>'No','branch_id'=>Yii::app()->user->getState("branchId"));
		}
		else{
			$criteria->params=array(':project_id'=>$pid,':status' => '0');
		}
			
		
		
		//$criteria->params=array(':project_id'=>$pid,':status' => '0',':is_internal'=>'No','branch_id'=>Yii::app()->user->getState("branchId"));
		$criteria->order="t.communication_id DESC";
		
		$project_communicates=Communication::model()->findAll($criteria); // $params is not needed
		
		
		$dataProvider=new CActiveDataProvider(Communication, array(
    	'data'=>$project_communicates,
	));
		
		return $dataProvider;
	 }
	 
	
	 
	  public function ChecklistDataProvider($tid)
	 {
	 
	 	$criteria=new CDbCriteria;
		$criteria->select='*';  // only select the 'title' column
		$criteria->condition='task_id=:task_id';
		$criteria->params=array(':task_id'=>$tid);
		$task_checklist=TaskChecklist::model()->findAll($criteria); // $params is not needed
		
		
		$dataProvider=new CActiveDataProvider(TaskChecklist, array(
    	'data'=>$task_checklist,
	));
		
		return $dataProvider;
	 }
	  public function TaskDataProvider($tid)
	 {
	 
	 	$criteria=new CDbCriteria;
		$criteria->select='*';  // only select the 'title' column
		$criteria->condition='communication_id=:communication_id';
		$criteria->params=array(':communication_id'=>$tid);
		//$criteria->addCondition('task_status !=12');
		$criteria->addCondition('is_hidden =0');
		
		if(!Yii::app()->user->checkAccess('viewAllTasks'))
			$criteria->addCondition('sharable =1 or crtd_by ='.Yii::app()->user->id.'');
		
		$comm_task=Task::model()->findAll($criteria); // $params is not needed
		
		
		$dataProvider=new CActiveDataProvider(Task, array(
    	'data'=>$comm_task,
	));
		
		return $dataProvider;
	 }
	
	public function renderCoordRps($data,$row)
	{
		 $pid=$data->project_id;
		
		
		//we can findall the records using method 1 or method 2
		$criteria=new CDbCriteria;
		$criteria->select='*';  // only select the 'title' column
		$criteria->condition="project_id=:project_id and type='rps'";
		
		$criteria->params=array(':project_id'=>$pid);
		$project_associates=ProjectCoordinates::model()->findAll($criteria); // $params is not needed
		
		
		$rps=array();
		
		$i=0;
		foreach($project_associates as $records)
		{
			
				$user_id=$records->user_id;
				
				
				//method 2 for find
				$user=User::model()->find(array(
				    'select'=>'user_id,user_name,branch_id',
				    'condition'=>'user_id=:userID',
				    'params'=>array(':userID'=>$user_id),
				));
				
				
				$rps[$i]=$user->user_name;
				
				
			
			$i++;
		}
		//$this->_ao=implode(",",$aos);
		return implode(",",$rps);
	}
	
	//get combined project no of alias project_no and project_type
	public function projectNo($data,$row)
	{
		
		
		$criteria=new CDbCriteria;
		$criteria->select='*'; 
		$criteria->condition='code_id=:code_id';
		$criteria->params=array(':code_id'=>$data->project_type);
		$code=CodeValue::model()->find($criteria);
		
		return ($data->project_no <=0 ? $data->project_id:$data->project_no)."-".$code->code_lbl;
	}
	
	
	public function actionDownloadFile($file)
	{
		$upload_path = Yii::getPathOfAlias('webroot').'/images/uploads/commfiles/';

		if( file_exists( $upload_path.$file ) ){
		Yii::app()->getRequest()->sendFile( $file , file_get_contents( $upload_path.$file ) );
		}
		else{
			$this->render('download404');
		} 
	}
	
	//get project status from task.done list them as ready
	public function projectStatus($data,$row)
	{
		$pstatus=$data->project_status;
		
		
		$pid=$data->project_id;
		
		if($pstatus==25)//if ready
		{
			$tasks=Yii::app()->db->createCommand()
				->select('p.project_no,t.task_status,t.task_id,c.communication_id,cv.code_lbl')
				->from('pm_project AS p')
				->join('pm_communication c', 'p.project_id = c.project_id')
				->join('pm_task t', 'c.communication_id = t.communication_id')
				->join('pm_code_value cv', 'p.project_type = cv.code_id')
    			->where('c.`status`=0')
				->andWhere('t.task_status=12')
				->andWhere('p.project_id=:id', array(':id'=>$pid))
    			->queryAll();
				
				$i=0;
				
			$readyToIssueArry=array();
			foreach($tasks as $task)
			{
				$tid=$task['task_id'];
				$table=$task['project_no']."-".$task['code_lbl']."-".$task['communication_id']."-".$task['task_id'];
				$readyToIssueArry[$i]=$table;
				
				$i++;
			}
			return $data->projectStatus->code_lbl." (". implode(',',$readyToIssueArry) ." )";	
		}
		else{
			return $data->projectStatus->code_lbl;
		}
		
		
		
		
		
		
		 
	}
	
	//customize comments column to show the user name and date
	/*
	public function commentsColumn($data,$row)
	{
		if($data->project_comments != "")
		{
			//return $data->updtUser->user_name."(".date("Y,m,d",$data->updt_dt).")".$data->project_comments;
		}
		else{
			return "";
		}
	}*/
	
	//get recent nearest due date of task
	public function projectDueDate($data,$row)
	{
		
		$pid=$data->project_id;
		
		$taskCount=Yii::app()->db->createCommand()
    			->select('pm_task.job_due_date as due_date')
    			->from('pm_task')
			    ->join('pm_communication', 'pm_task.communication_id = pm_communication.communication_id')
			    ->where("pm_communication.project_id = $pid")
			    ->queryAll();
               
		
		$duedate = Yii::app()->db->createCommand()
    			->select('MIN(pm_task.job_due_date) as due_date,pm_task.task_id,pm_task.job_client_due_date')
    			->from('pm_task')
			    ->join('pm_communication', 'pm_task.communication_id = pm_communication.communication_id')
			    ->where('pm_task.task_status not in (12,26) AND pm_communication.project_id = '.$pid)
				->group('pm_task.job_due_date,pm_task.task_id')
				->limit(1)
			    ->queryRow();
                
               
		$insertdate="0000-00-00";
		if($duedate['due_date'] != "")
		{
			$insertdate=$duedate['due_date'];
		}
		
		
		//update project status
		Project::model()->updateByPk($pid, array(
		    'job_due_date' => $insertdate
		));	
		
		if(count($taskCount) <= 0)
		{
			return "...no task...";
		}
		
		if($duedate['due_date'] == "")
		{
			
			return "all tasks done";
		}
		
		$tid=$duedate['task_id'];
				
				$task = Yii::app()->db->createCommand()
    			->select('pm_task.task_id,
                                  pm_task.assigned_to,
                                  pm_task.task_status,
                                  pm_communication.communication_id,
                                  pm_project.project_no,
                                  pm_code_value.code_lbl')
    			->from('pm_task')
			    ->join('pm_communication', 'pm_task.communication_id = pm_communication.communication_id')
				  ->join('pm_project', 'pm_communication.project_id = pm_project.project_id')
				    ->join('pm_code_value', 'pm_project.project_type = pm_code_value.code_id')
			    ->where("pm_task.task_id = $tid")
			    ->queryRow();
				
				$table="No:".$task['project_no']."-".$task['code_lbl']."-".$task['communication_id']."-".$task['task_id'];
		
		//$now=date("Y-m-d");
		//$customClass=new MyCustomClass();
		
		//compare due dates
		
		//if($duedate['due_date'] <= $now)
		//{
			//return '<font color="#FF0000">'.$duedate['due_date'].'</font>';
		//}
		//else if($customClass->GetDateDifference($duedate['due_date'],$now)==1)
		//{
			//return '<font color="#FFA500">'.$duedate['due_date'].'</font>';
		//}
		//else if($customClass->GetDateDifference($duedate['due_date'],$now)==2)
		//{
			//return '<font color="#ffff00">'.$duedate['due_date'].'</font>';
		//}
		//else{
			//return $duedate['due_date']."</br>(".$table.")";   
                       return $duedate['due_date']."/<br/>".$duedate['job_client_due_date']."</br>(".$table.")";    //return only job due date
		//}
		
	}
	
	/*
	to get values for drop down editable field
	*/
	public function actionGetTaskStatusValues()
	{
		$criteria = new CDbCriteria;
		$criteria->select='*';  // only select the 'title' column
		$criteria->condition="code_type='job_status' and code_id not in (27)";
		$models=CodeValue::model()->findAll($criteria);
		
		/* Extract only the attributes that you need from the models
       into a new array */
    	$data = array_map(
        create_function('$m', 
            'return $m->getAttributes(array(\'code_id\',\'code_lbl\'));'), 
            $models);
			
		/* Replace the keys in the new array with 'value' and 'text' */
	    foreach($data as $k => $v )
	    {
	        $data[$k]['value'] = $data[$k]['code_id'];
	        unset($data[$k]['code_id']);
	 
	        $data[$k]['text'] = $data[$k]['code_lbl'];
	        unset($data[$k]['code_lbl']);
	    }
		echo json_encode($data);
	}
	
	/*
	to hide unhide communication,change status
	*/
	
	
	public function actions()
	{
		return array(
                'toggleInnerStatus' => array(
                        'class'=>'bootstrap.actions.TbToggleAction',
                        'modelName' => 'Communication',
                        ),
						'toggleInnerBookmark' => array(
                        'class'=>'bootstrap.actions.TbToggleAction',
                        'modelName' => 'Communication',
                        )
                );

	}
	
	
}
