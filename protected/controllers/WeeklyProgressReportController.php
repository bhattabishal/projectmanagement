<?php
     class WeeklyProgressReportController extends Controller
    {
	public $layout='//layouts/column1';
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','weeklyReport'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionView($id)
	{
	EQuickDlgs::render('view',array(
	        'model'=>$this->loadModel($id)
		));
	}
	
	public function actionIndex()
	{

	    $model=new WeeklyProgressReport;
		$model->unsetAttributes();  // clear any default values
	  //  $model->startDt=MyCustomClass::subractDays(date('Y-m-d'),7);
	//    $model->endDt=MyCustomClass::addDays(date('Y-m-d'),0);
	    $max = Yii::app()->db->createCommand('SELECT max(project_id) as project_id FROM pm_project');
		$a=$max->queryRow();
	//	$model->branch_id=Yii::app()->user->getState('branchId');
		$model->projectId=$a['project_id'];
	//	$model->projectId=0;
		if(isset($_POST['WeeklyProgressReport']))
		{
			$model->attributes=$_POST['WeeklyProgressReport'];
					
		}
		$projectRecords=$model->getProjectWiseDetails($model->projectId,$model->branch_id);
		$this->render('_weeklyProgressReport',array('projectRecords'=>$projectRecords,'model'=>$model));
	//	$this->render('_weeklyProgressReport',array('model'=>$model));
	}
	
public function actionWeeklyReport()
	{
			$model=new WeeklyProgressReport;
		  $projects=Project::model()->getAllProjectsNo(1);
	    	$pid=0;
			foreach($projects as $key=>$val)
			{
				$pid=$key;
			}
			
			$model->projectId=$pid;
		
			
		    //$first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
			//$last_day_this_month  = date('Y-m-t');
			
			
			$model->startDt=date('Y-m-d');
			$model->endDt=date('Y-m-d');

			$projectUsers=User::model()->getUsersArrayList();
		
			
			if(isset($_POST['WeeklyProgressReport']))
			{
			
				$model->attributes=$_POST['WeeklyProgressReport'];
				if($model->projectId > 0)
				{
					$projects=Project::model()->getAllProjectsNo();
				}
				else
				{
					$projects=Project::model()->getProjectsNoById($model->projectId);
				}
				
			
			}
			
			$this->render('_weeklyProgressReport',array('projects'=>$projects,'projectUsers'=>$projectUsers,'model'=>$model));
	}
	
	}
?>