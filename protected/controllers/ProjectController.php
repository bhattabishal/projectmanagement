<?php

class ProjectController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','admin','update','dynamicClientContact','updateComments','dynamicProjectByBranch','dynamicRegularProjectByBranch','dynamicProjectByBranchUsers'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		 EQuickDlgs::render('view',array('model'=>$this->loadModel($id)));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
         * while creating new project,one communication,one task and checklist are auto inserted
	 */
	public function actionCreate()
	{
		$model=new Project;
		$model->project_date=date('Y-m-d');
		$model->job_due_date=MyCustomClass::addDays($model->project_date,2);
		$model->project_status=3;
		
		$commModel=new Communication();
		//$commModel->isNewRecord=TRUE;
		$commModel->scenario = 'auto_insert';
		$commModel->project_id=0;
		
		$taskModel=new Task();
		$taskModel->scenario = 'auto_insert';
		$taskModel->communication_id=0;
		//$clientModel=new Client;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Project']))
		{
			
			$model->attributes=$_POST['Project'];
			
			$uid=Yii::app()->user->id;
			
            $user=User::model()->getUserDetailsById($uid);
			//communication
			
			
			$commModel->communication_details="Please do checklist before drawing and also create and assign necessary tasks";
			$commModel->communication_date=$model->project_date;
			$commModel->user_id=Yii::app()->user->id;
			$commModel->status=0;
			$commModel->bookmark=0;
			$commModel->is_internal="No";
			$commModel->mail_sent="No";
			$commModel->communication_subject=$commModel->communication_details;
			$commModel->comm_from=$user->branch_id;
			
			
			//task model
			
			
			
			//$taskModel->isNewRecord=TRUE;
			$taskModel->job_details=$commModel->communication_details;
			$taskModel->job_start_date=$model->project_date;
			$taskModel->sharable=1;
			$taskModel->done_percentage=0;
			$taskModel->piority="Normal";
			$taskModel->est_rps_cost=$model->est_rps_wis;
			$taskModel->est_ao_cost=$model->est_ao_wis;
			$taskModel->actual_rps_cost=0;
			$taskModel->actual_ao_cost=0;
			$taskModel->job_due_date=$model->job_due_date;
			$taskModel->job_client_due_date=$model->job_due_date;
			$taskModel->task_type='normal';
			$taskModel->assigned_to=$model->coord_rps;
			$taskModel->task_status=11;//WIP
			$taskModel->task_cat=6;//need architecture
			$taskModel->job_type=8;//due
			$taskModel->is_hidden=0;//not hidden
			$taskModel->task_variation='No';//not hidden
			
			//print_r($taskModel);
			//die;
			
			$valid=$model->validate();
			$valid=$commModel->validate() && $valid;
			$valid=$taskModel->validate() && $valid;
			
			if($valid)
			{
				
			
			$transaction = $model->getDbConnection()->beginTransaction();
			try
				{
					
					if($model->save())
					{
						$model->refresh();
						//insert automatic before starting checklists
							
							$commModel->project_id=$model->project_id;
							$commModel->save();
							$commModel->refresh();
					
							$taskModel->communication_id=$commModel->communication_id;
							$taskModel->save();
							$taskModel->refresh();
						
						
						
							//get task list from project type for before starting
							$checklists=ChecklistScheme::model()->findAll("job_type_id=$model->project_type and checklist_stages='23'");
							
							foreach($checklists as $chk)
							{
								$chklist=Checklist::model()->find(array(
				    				'select'=>'checklist_details',
				    				'condition'=>'checklist_id=:checklist_id',
				    				'params'=>array(':checklist_id'=>$chk->chklist_id),
								));
								
								$checklistModel=new TaskChecklist();
								$checklistModel->isNewRecord=TRUE;
								$checklistModel->task_id=$taskModel->task_id;
								$checklistModel->checklist_details=$chklist->checklist_details;
								$checklistModel->status=13;//undone
								$checklistModel->save();
							}
						
					
					
					
				//end checklist
				
				//save project associates
				$pass=array();
				$pass=$model->project_associates_id;
				foreach($pass as $key=>$value)
				{	
					$projectAssoc=new ProjectAssociates();
					$projectAssoc->project_id=$model->project_id;
					$projectAssoc->user_id=$value;
					$projectAssoc->is_coordinator=1;//need to change dynamic later
					$projectAssoc->save();	
				}
				
				
				//save project  contact
					$pcontact=array();
					$pcontact=$model->client_contact_id;
					foreach($pcontact as $key=>$value)
					{
						$projectCont=new ProjectContact();
						$projectCont->project_id=$model->project_id;
						$projectCont->contact_id=$value;
						$projectCont->save();
					
					}
					
					//save coordinates
					//$pcoord=array();
					$pcoord=$model->coord_ao;
					//foreach($pcoord as $key=>$value)
					{
						$projectCoord=new ProjectCoordinates();
						$projectCoord->project_id=$model->project_id;
						$projectCoord->user_id=$pcoord;
						$projectCoord->type="ao";
						$projectCoord->save();
						
					}
					
					//$pcoord=array();
					$pcoord=$model->coord_rps;
					//foreach($pcoord as $key=>$value)
					{
						$projectCoord=new ProjectCoordinates();
						$projectCoord->project_id=$model->project_id;
						$projectCoord->user_id=$pcoord;
						$projectCoord->type="rps";
						$projectCoord->save();
					}
					
					//end save coordiantes
					$transaction->commit();
					Yii::app()->user->setFlash('success', "Project created successfully");
					EQuickDlgs::checkDialogJsScript();
					$this->redirect(array('home/admin','id'=>$model->project_id));
			}
					
					 
					
				}
			catch(Exception $e)
				{
					$transaction->rollback();
					throw new CHttpException(404, 'Some error occured.Please try again');
				}
				
				}
			
		}

		EQuickDlgs::render('create',array(
			'model'=>$model,
			'commModel'=>$commModel,
			'taskModel'=>$taskModel
			//'clientModel'=>$clientModel,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		
		$model=$this->loadModel($id);
		
		
		
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Project']))
		{
			$model->attributes=$_POST['Project'];
			if($model->save())
			{
				
				//save update client contacts
				$pcontact=array();
				$pcontact=$model->client_contact_id;
				$oldProjectContact=ProjectContact::model()->findAll(array(
					    'select'=>'project_contact_id',
					    'condition'=>'project_id=:project_id',
					    'params'=>array(':project_id'=>$id),
					));
					foreach($oldProjectContact as $oldid)
					{
						//$id=$oldid->contact_id;
						$pid=$oldid->project_contact_id;
								
						ProjectContact::model()->deleteByPk($pid);
						
					}
				
					
				foreach($pcontact as $key=>$value)
					{
						$projectCont=new ProjectContact();
						$projectCont->isNewRecord=TRUE;
						
						//get old project contact
					
					
					//$oldContact=ProjectContact::model()->find(array(
					   // 'select'=>'project_contact_id',
					   // 'condition'=>'project_id=:project_id',
						//'condition'=>'contact_id=:contact_id',
					   // 'params'=>array(':project_id'=>$model->project_id,':contact_id'=>$value),
					//));
				
						//if(count($oldContact) > 0 && $oldContact->project_contact_id > 0)
						//{
							//$projectCont->project_contact_id=$oldContact->project_contact_id;
							//$projectCont->isNewRecord=FALSE;
						//}
						
						$projectCont->project_id=$id;
						$projectCont->contact_id=$value;

						if($projectCont->save())
						{
							
						}
						else{
							throw new CHttpException(404, 'Some error occured while saving.Please try again'); 
						}
					}
					//in case fewer selected,need to delete the old ones
					
					//foreach($oldProjectContact as $oldid)
					//{
						//$id=$oldid->contact_id;
						//$pid=$oldid->project_contact_id;
						
						//if(in_array($id, $pcontact))
						//{
						//	
						//}
						//else
						//{				
							//ProjectContact::model()->deleteByPk($pid);
						//}
					//}
				//end client contacts
			
				
				//project associates start
				
				$pass=array();
				$pass=$model->project_associates_id;
				$oldProjectAssociates=ProjectAssociates::model()->findAll(array(
					    'select'=>'project_associates_id,project_id',
					    'condition'=>'project_id=:project_id',
					    'params'=>array(':project_id'=>$id),
					));
				foreach($oldProjectAssociates as $oldid)
				{
					$pid=$oldid->project_associates_id;
					//$id=$oldid->user_id;
					
					//if(in_array($id, $pass))
					//{
					//	
					//}
					//else
					//{
						
						ProjectAssociates::model()->deleteByPk($pid);
					//}
				}
					
				foreach($pass as $key=>$value)
				{
					$projectAssoc=new ProjectAssociates();
					$projectAssoc->isNewRecord=TRUE;
					
					//get old project associates
					
					
					//$oldPass=ProjectAssociates::model()->find(array(
					   // 'select'=>'project_associates_id,user_id',
					   // 'condition'=>'project_id=:project_id',
						//'condition'=>'user_id=:user_id',
					    //'params'=>array(':project_id'=>$id,':user_id'=>$value),
					//));
				
					//if(count($oldPass) > 0 && $oldPass->project_associates_id > 0)
					//{
						//$projectAssoc->project_associates_id=$oldPass->project_associates_id;
						//$projectAssoc->isNewRecord=FALSE;
					//}
					
					$projectAssoc->project_id=$id;
					$projectAssoc->user_id=$value;
					$projectAssoc->is_coordinator=1;//need to change dynamic later
					if($projectAssoc->save())
					{
						
					}
					else{
						throw new CHttpException(404, 'Some error occured while saving.Please try again'); 
					}										
					
				}
				//delete extra project associates ids
				
				//foreach($oldProjectAssociates as $oldid)
				//{
					//$pid=$oldid->project_associates_id;
					//$id=$oldid->user_id;
					//
					//if(in_array($id, $pass))
					//{
						
					//}
					//else
					//{
						
						//ProjectAssociates::model()->deleteByPk($pid);
					//}
				//}
				//end project associates
				
				
				//coordinates ao
				//$pcooao=array();
				$pcooao=$model->coord_ao;
				$oldProjectCoordinatesAo=ProjectCoordinates::model()->find(array(
					    'select'=>'coord_id,user_id',
					    'condition'=>"project_id=:project_id and type='ao'",
					    //'condition'=>'type=:type',
					    'params'=>array(':project_id'=>$id),
					));
				//foreach($oldProjectCoordinatesAo as $oldid)
				{
					$idao=$oldProjectCoordinatesAo->coord_id;				
					ProjectCoordinates::model()->deleteByPk($idao);
					
				}
					//foreach($pcooao as $key=>$value)
				{
					$projectCoordinates=new ProjectCoordinates();
					$projectCoordinates->isNewRecord=TRUE;
					
					//get old project associates
					
					
					//$oldPcoo=$projectCoordinates::model()->find(array(
					  //  'select'=>'coord_id,user_id',
					   // 'condition'=>'project_id=:project_id',
						//'condition'=>'user_id=:user_id',
						//'condition'=>'type=:type',
					    //'params'=>array(':project_id'=>$model->project_id,':user_id'=>$value,':type'=>'ao'),
					//));
				
					//if(count($oldPcoo) > 0 && $oldPcoo->coord_id > 0)
					//{
						//$projectCoordinates->coord_id=$oldPcoo->coord_id;
						//$projectCoordinates->isNewRecord=FALSE;
					//}
					
					$projectCoordinates->project_id=$id;
					$projectCoordinates->user_id=$pcooao;
					$projectCoordinates->type="ao";
				
					if($projectCoordinates->save())
					{
						
					}
					else{
						throw new CHttpException(404, 'Some error occured while saving.Please try again'); 
					}
					
				}
				//delete extra project coordinates ao
				//foreach($oldProjectCoordinatesAo as $oldid)
				//{
					//$idao=$oldid->coord_id;
					//$uidao=$oldid->user_id;
					
					//if(in_array($uidao, $pcooao))
					//{
						
					//}
					//else
					//{
						
						//ProjectCoordinates::model()->deleteByPk($idao);
					//}
				//}
				//end coordinates ao
				
				//coordinates rps
				//$pcoorps=array();
				$pcoorps=$model->coord_rps;
				$oldProjectCoordinatesRps=ProjectCoordinates::model()->find(array(
					    'select'=>'coord_id,user_id',
					    'condition'=>"project_id=:project_id and type='rps'",
						//'condition'=>'type=:type',
					    'params'=>array(':project_id'=>$id),
					));
					
				//foreach($oldProjectCoordinatesRps as $oldid)
				{
					$idrps=$oldProjectCoordinatesRps->coord_id;
					ProjectCoordinates::model()->deleteByPk($idrps);
					
				}
					
					//foreach($pcoorps as $key=>$value)
				{
					$projectCoordinates=new ProjectCoordinates();
					$projectCoordinates->isNewRecord=TRUE;
					
					//get old project associates
					
					
					//$oldPcrps=$projectCoordinates::model()->find(array(
					  //  'select'=>'coord_id',
					  //  'condition'=>'project_id=:project_id',
						//'condition'=>'user_id=:user_id',
					   //'condition'=>'type=:type',
					   // 'params'=>array(':project_id'=>$model->project_id,':user_id'=>$value,':type'=>'rps'),
					//));
				
					//if(count($oldPcrps) > 0 && $oldPcrps->coord_id > 0)
					//{
						//$projectCoordinates->coord_id=$oldPcrps->coord_id;
						//$projectCoordinates->isNewRecord=FALSE;
					//}
					
					$projectCoordinates->project_id=$id;
					$projectCoordinates->user_id=$pcoorps;
					$projectCoordinates->type="rps";
				
					if($projectCoordinates->save())
					{
						
					}
					else{
						throw new CHttpException(404, 'Some error occured while saving.Please try again'); 
					}
				
				}
				
				
				
				
				//delete extra project coordinates rps
				//foreach($oldProjectCoordinatesRps as $oldid)
				//{
					//$idrps=$oldid->coord_id;
					//$uidrps=$oldid->user_id;
					
					//if(in_array($uidrps, $pcoorps))
					//{
						
					//}
					//else
					//{
						
						//ProjectCoordinates::model()->deleteByPk($idrps);
					//}
				//}
				//end coordinates rps
					
					
				EQuickDlgs::checkDialogJsScript();
				$this->redirect(array('home/admin','id'=>$model->project_id));
			}
				
		}

		EQuickDlgs::render('update',array(
			'model'=>$model,
		));
		
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		try{
    			
					if($this->loadModel($id)->delete())
					{
						
					}
					else{
						throw new CHttpException(400, "Cannot delete.The project is in use");
					}
			}catch(CDbException $e)
			{
    			throw new CHttpException(400, "Some error occured while deleting.Please try again");
			}
			
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
		    	$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Project');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Project('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Project']))
			$model->attributes=$_GET['Project'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Project the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Project::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Project $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='project-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionDynamicClientContact()
	{
		
		if(isset($_POST['client_id']) &&  $_POST['client_id'] > 0) 
		{
				$cid=$_POST['client_id'];
				
			 $data = ClientContact::model()->byName()->findAll("client_id=$cid");
				$list = CHtml::listData($data, 'client_contact_id', 'contact_name');
			// echo "<option value=''>Select Client contact</option>";
			 foreach($list as $value=>$name)
            {
                echo CHtml::tag('option',
                           array('value'=>$value),CHtml::encode($name),true);
            }
		}
	}
        
        public function actionDynamicProjectByBranchUsers()
	{
		
		if(isset($_POST['branch_id']) &&  $_POST['branch_id'] > 0) 
		{
				$bid=$_POST['branch_id'];
				
				 $list = Project::model()->getAllBranchUsersProjectsNo(0,0,0,$bid);
				  if(count($list) > 0)
				 {
				 	 echo CHtml::tag('option',
                           array('value'=>''),CHtml::encode('All'),true);
				 }
				 foreach($list as $value=>$name)
           {
               echo CHtml::tag('option',
                           array('value'=>$value),CHtml::encode($name),true);
            }
			/*
			 $data = Project::model()->findAll("branch_id=$bid");
				$list = CHtml::listData($data, 'project_id', 'projectNoTitle');
			// echo "<option value=''>Select Client contact</option>";
			 foreach($list as $value=>$name)
            {
                echo CHtml::tag('option',
                           array('value'=>$value),CHtml::encode($name),true);
            }*/
		}
	}
           public function actionDynamicProjectByBranch()
	{
		
		if(isset($_POST['branch_id']) &&  $_POST['branch_id'] > 0) 
		{
				$bid=$_POST['branch_id'];
				
				 $list = Project::model()->getAllBranchUsersProjectsNo1(0,0,0,$bid);
				 if(count($list) > 0)
				 {
				 	 echo CHtml::tag('option',
                           array('value'=>''),CHtml::encode('All'),true);
				 }
				 foreach($list as $value=>$name)
           {
               echo CHtml::tag('option',
                           array('value'=>$value),CHtml::encode($name),true);
            }
			/*
			 $data = Project::model()->findAll("branch_id=$bid");
				$list = CHtml::listData($data, 'project_id', 'projectNoTitle');
			// echo "<option value=''>Select Client contact</option>";
			 foreach($list as $value=>$name)
            {
                echo CHtml::tag('option',
                           array('value'=>$value),CHtml::encode($name),true);
            }*/
		}
	}
        /*
         * Loads projects drop down options based on branch id
         * used to load drop down based on drop down value of other drop down box
         */
         public function actionDynamicRegularProjectByBranch()
	{
		
		if(isset($_POST['branch_id']) &&  $_POST['branch_id'] > 0) 
		{
				$bid=$_POST['branch_id'];
				
			 $data = Project::model()->findAll("branch_id=:bid and project_id > 0",array(':bid'=>$bid));
				$list = CHtml::listData($data, 'project_id', 'projectNoTitle');
			// echo "<option value=''>Select Client contact</option>";
                         echo CHtml::tag('option',
                           array('value'=>""),CHtml::encode("All"),true);
			 foreach($list as $value=>$name)
            {
                echo CHtml::tag('option',
                           array('value'=>$value),CHtml::encode($name),true);
            }
		}
	}
	
	public function actionUpdateComments()
	{
		if(Yii::app()->request->isAjaxRequest)
	    {
	        Yii::import('bootstrap.widgets.MyCommentsTbEditableSaver');
	        $es=new MyCommentsTbEditableSaver('ProjectComments');
			//$id=Yii::app()->user->id;
			//$this->updt_cnt=$this->updt_cnt+1;
			//$this->updt_by=$id;
	        $es->insert();
	    }
	    else
	        throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	
}
