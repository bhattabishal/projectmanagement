<?php

class FixedTaskTimelineBriefController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
        
        public function actions() {
        return array
            (
            
            'getRowForm1' => array(
                'class' => 'ext.dynamictabularform.actions.GetRowForm1',
                'view' => '_timelineForm',
                'modelClass' => 'FixedTaskTimeline'
            ),
        );
    }

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','getRowForm1','searchTimeline'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
        
        public function actionSearchTimeline()
        {
           
             $model=new FixedTaskTimelineBrief;
             $model->entry_dt=$date;
             $model->user_id=$uid;
             
            // print_r($_POST);
             if(isset($_POST['FixedTaskTimelineBrief']))
             {
                 $model->attributes=$_POST['FixedTaskTimelineBrief'];
                  $valid=$model->validate();
                  if($valid)
                  {
                      $this->redirect(array('create','date'=>$model->entry_dt, 'uid'=>$model->user_id));
                  }
             }
             
             $this->render('_searchTimeline',array(
			'model'=>$model,
		));
        }
    /*   
	public function actionCreateWithSearch()
	{
           
                 $model=new FixedTaskTimelineBrief;
                
                 $date=date('Y-m-d');
                 $uid=Yii::app()->user->id;
                 
                 if(isset($_POST['yt0']) && $_POST['yt0']=='View' )
                 {
                     $model->attributes=$_POST['FixedTaskTimelineBrief'];
                     $date=$model->entry_dt;
                     $uid=$model->user_id;
                     $valid=$model->validate();
                 }
            
           
            $model= FixedTaskTimelineBrief::model()->find('entry_dt=:entry_dt and user_id=:uid',array(':entry_dt'=>$date,':uid'=>$uid));   
            if($model=="")
            {
                $model=new FixedTaskTimelineBrief;
                $model->entry_dt=$date;
                $model->user_id=$uid;
                $modelTimeline=array(new FixedTaskTimeline);
            }
            else
            {
                $modelTimeline=FixedTaskTimeline::model()->findAll('fxt_brief_id=:id',array(':id'=>$model->id));
                foreach($modelTimeline as $timelinedetail) 
                        {
                            $timelinedetail->project_id= $timelinedetail->project_id.'-'.$timelinedetail->type;
                        }
            }
            
		
               
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		  if(isset($_POST['btnSave']) && ($_POST['btnSave']=='Create' || $_POST['btnSave']=='Save' ))
		{
                    
			$model->attributes=$_POST['FixedTaskTimelineBrief'];
                        
                        $totot=0;
                        $totnh=0;
                        if (isset($_POST['FixedTaskTimeline']))
                        {
                                 $modelTimeline = array();
                                 foreach ($_POST['FixedTaskTimeline'] as $key => $value) 
                                 {
                                                if ($value['updateType'] == DynamicTabularForm::UPDATE_TYPE_CREATE)
                                $modelTimeline[$key] = new FixedTaskTimeline();
                                                else if ($value['updateType'] == DynamicTabularForm::UPDATE_TYPE_UPDATE)
                                $modelTimeline[$key] = FixedTaskTimeline::model()->findByPk($value['fxt_id']);
                                                        else if ($value['updateType'] == DynamicTabularForm::UPDATE_TYPE_DELETE) 
                                                        {
                                        $delete = FixedTaskTimeline::model()->findByPk($value['fxt_id']);
                                        if ($delete->delete()) 
                                                                {
                                            unset($modelTimeline[$key]);
                                            continue;
                                        }
                                }
                                                         $idarray=explode('-',$value['project_id']);
                                                         $value['project_id']=$idarray[0];
                                                         $value['type']=$idarray[1];
                                                         
                                                        $modelTimeline[$key]->attributes = $value;
                                                       
                                                        //$modelTimeline[$key]->project_id=$idarray[0];
                                                        // $modelTimeline[$key]->type=$idarray[1];
                                               $totnh+=$value['nt_hours'];
                                               $totot+=$value['ot_hours'];
                                 }
                                  $model->tot_normal_hrs=$totnh;
                                  $model->tot_ot_hrs=$totot;
                        }
                
                        $valid=$model->validate();
                        foreach($modelTimeline as $timelinedetail) 
                        {
                            $valid = $timelinedetail->validate() && $valid;
                        }
                                //if($model->save())
				//$this->redirect(array('view','id'=>$model->id));
                         if ($valid)
                         {
                              $transaction = $model->getDbConnection()->beginTransaction();
                               try 
                                {
                                   
                                    $model->save();
                                    $model->refresh();
                                    
                                     foreach ($modelTimeline as $timelinedetail)
                                    {
                                           $timelinedetail->fxt_brief_id = $model->id;
                                           $timelinedetail->user_id= $model->user_id;
                                            $timelinedetail->entry_dt=$model->entry_dt;
                                           $timelinedetail->save();
                                           
                                           
                                           //also save in actual timeline table
                                           //check if insert or update
                                           
                                           $id=$timelinedetail->fxt_id;
                                           $record=ActualTimeline::model()->find('admin_timeline_id=:id and task_type=:type',array(':id'=>$timelinedetail->fxt_id,':type'=>'admin'));
                                           
                                           if($record->timeline_id > 0)
                                           {
										   		$model=ActualTimeline::model()->findByPk($record->timeline_id);
										   }
										   else
										   {
										   		$model=new ActualTimeline;
										   }
										   
                                                                                   $pid=$timelinedetail->project_id;
										   $adminProjectTask=Task::model()->find('admin_task_id=:id',array(':id'=>$timelinedetail->project_id));
										   
										   $model->task_id=$adminProjectTask->task_id;
										   $model->user_id=$timelinedetail->user_id;
										   $model->normal_hour=$timelinedetail->nt_hours;
										   $model->cusion_hour=0;
										   $model->overtime_hour=$timelinedetail->ot_hours;
										   $model->task_date=$timelinedetail->entry_dt;
										   $model->percentage_completed=0;
										   $model->schedule_id_completed=0;
										   $model->remarks=$timelinedetail->remarks;
										   $model->task_type='admin';
										   $model->admin_timeline_id=$timelinedetail->fxt_id;
										   $model->save(FALSE);
                                           
                                    }
                                    $transaction->commit();
                                } 
                               catch (Exception $ex) 
                               {
                                   $transaction->rollback();
                                    throw new CHttpException(404, 'Some error occured while saving.Please try again'); 
                               }
                               EQuickDlgs::checkDialogJsScript();
                                $this->redirect(array('create'));
                         }
		}
                

		EQuickDlgs::render('create',array(
			'model'=>$model,
                        'modelTimeline'=>$modelTimeline,
                        
		));
	}
	*/
	
	public function actionCreate()
	{
           $model=new FixedTaskTimelineBrief;               
           $date=date('Y-m-d');
           $uid=Yii::app()->user->id;
           $model->entry_dt=$date;
           $model->user_id=$uid;
           $modelTimeline=array(new FixedTaskTimeline);
           
           // Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
			if(isset($_POST['FixedTaskTimelineBrief']))
			{
				 $totot=0;
                 $totnh=0;
                 $model->attributes=$_POST['FixedTaskTimelineBrief'];
                 
                  if (isset($_POST['FixedTaskTimeline']))
                  {
                    $modelTimeline = array();
                     foreach ($_POST['FixedTaskTimeline'] as $key => $value)
                     {
                            $modelTimeline[$key] = new FixedTaskTimeline();

                           
                            $value['project_id']=$value['project_id'];
                            $value['type']='fixedtasks';
                            $value['entry_dt']=$model->entry_dt;
                             
                            $modelTimeline[$key]->attributes = $value;
                            $totnh+=$value['nt_hours'];
                            $totot+=$value['ot_hours'];
                    }
						  
                           $model->tot_normal_hrs=$totnh;
                           $model->tot_ot_hrs=$totot;
                           $valid=$model->validate();
                           
                       foreach($modelTimeline as $timelinedetail) 
                    	{
                        	$valid = $timelinedetail->validate() && $valid;
                    	}
                        	
                    	 if ($valid)
                    	 {
						 	 $transaction = $model->getDbConnection()->beginTransaction();
						 	 try 
                                {
                                	$model->save();
                                	$model->refresh();
                                	
                                	foreach ($modelTimeline as $timelinedetail)
                                	{
										$timelinedetail->fxt_brief_id = $model->id;
                                        $timelinedetail->user_id= $model->user_id;
                                        $timelinedetail->entry_dt=$model->entry_dt;
                                        $timelinedetail->save();
                                        
                                        $timelinedetail->refresh();
                                        
                                        //also save in actual timeline table
                                        //check if insert or update
                                           
                                        $id=$timelinedetail->fxt_id;
                                      
									    $actualModel=new ActualTimeline;
									   
									   
									   $pid=$timelinedetail->project_id;
									   $adminProjectTask=Task::model()->find('admin_task_id=:id',array(':id'=>$pid));
									   $actualModel->task_id=$adminProjectTask->task_id;
									   $actualModel->user_id=$timelinedetail->user_id;
									   $actualModel->normal_hour=$timelinedetail->nt_hours;
									   $actualModel->cusion_hour=0;
									   $actualModel->overtime_hour=$timelinedetail->ot_hours;
									   $actualModel->task_date=$timelinedetail->entry_dt;
									   $actualModel->percentage_completed=0;
									   $actualModel->schedule_id_completed=0;
									   $actualModel->remarks=$timelinedetail->remarks;
									   $actualModel->task_type='admin';
									   $actualModel->admin_timeline_id=$timelinedetail->fxt_id;
									   $actualModel->save(FALSE);
									   
									   
									   
									}
                                	
									 $transaction->commit();
								}
								catch(Exception $ex)
								{
									$transaction->rollback();
                                    throw new CHttpException(404, 'Some error occured while saving.Please try again'); 
								}
								
								EQuickDlgs::checkDialogJsScript();
                                $this->redirect(array('create'));
						 } 
				  }
			}
			EQuickDlgs::render('create',array(
			'model'=>$model,
                        'modelTimeline'=>$modelTimeline,
                        
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
        /*
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['FixedTaskTimelineBrief']))
		{
			$model->attributes=$_POST['FixedTaskTimelineBrief'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
         * */
         

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		//$dataProvider=new CActiveDataProvider('FixedTaskTimelineBrief');
		//$this->render('index',array(
			//'dataProvider'=>$dataProvider,
		//));
            $this->actionCreate();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new FixedTaskTimelineBrief('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['FixedTaskTimelineBrief']))
			$model->attributes=$_GET['FixedTaskTimelineBrief'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return FixedTaskTimelineBrief the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=FixedTaskTimelineBrief::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param FixedTaskTimelineBrief $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='fixed-task-timeline-brief-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
