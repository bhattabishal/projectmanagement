<?php
class ScheduleReportController extends Controller
{
	public $layout='//layouts/column1';
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','performanceReport','remainingReport','dataTableTest'),
				'users'=>array('@'),
			),
		array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionIndex()
	{
	
		$model=new ScheduleReport;
		
		$model->startDt=MyCustomClass::subractDays(date('Y-m-d'),7);
		$model->endDt=MyCustomClass::addDays(date('Y-m-d'),7);
		$model->projectId=0;
                $model->branch_id=Yii::app()->user->getState('branchId');
		
		if(isset($_POST['ScheduleReport']))
		{
			
			$model->attributes=$_POST['ScheduleReport'];
		}
		
		$projectRecords=$model->getProjectWiseDetails($model->projectId,$model->branch_id,$model->adminTasks);
		$this->render('_projectWiseScheduleReport',array('projectRecords'=>$projectRecords,'model'=>$model));
	}
	
	
	public function actionDataTableTest()
	{
		$this->render('_dataTableTest');
	}
	
	public function actionPerformanceReport()
	{
		$model=new PerformanceReport;
		$time = mktime(0, 0, 0, date('n') - 0, 1); 
     
        $model->tomonth=date('Y-m-d', $time);
        
        
        $time = mktime(0, 0, 0, date('n') - 5, 1); 
       
        $model->frommonth=date('Y-m-d', $time);
        $model->userId=Yii::app()->user->id;
        
        
		if(isset($_POST['PerformanceReport']))
		{
			$model->attributes=$_POST['PerformanceReport'];
		}
		$this->render('_performanceReport',array('model'=>$model));
	}
	
	
	public function actionRemainingReport()
	{
			$model=new ScheduleReport;
			
			$model->projectId=0;
                         $model->branch_id=Yii::app()->user->getState('branchId');
			
			//$first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
			//$last_day_this_month  = date('Y-m-t');
			
			//$model->startDt=$first_day_this_month;
			//$model->endDt=$last_day_this_month;
			$model->endDt=date('Y-m-d');

			$projectUsers=User::model()->getUsersArrayList();
			
			if(isset($_POST['ScheduleReport']))
			{
			
				$model->attributes=$_POST['ScheduleReport'];
				if($model->projectId > 0)
				{
					$projectUsers=User::model()->getUsersArrayListByProjectId($model->projectId);
				}
				
			}
			
                        $projects=$model->getProjectWiseDetails($model->projectId,$model->branch_id,0);
			$this->render('_remainingHour',array('projects'=>$projects,'projectUsers'=>$projectUsers,'model'=>$model));
	}
}
?>