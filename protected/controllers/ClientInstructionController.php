<?php

class ClientInstructionController extends Controller
{
	public $layout='//layouts/column1';
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','excel'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionIndex()
	{
		$model=new ClientInstruction('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ClientInstruction']))
			$model->attributes=$_GET['ClientInstruction'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
}

?>