<?php

class ActualTimelineController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','getAuditedValues','updateAuditedValues','updateErrors','taskTransferPopUp'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	
	

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		//$this->render('view',array(
			//'model'=>$this->loadModel($id),
		//));
		EQuickDlgs::render('view',array('model'=>$this->loadModel($id)));
	}

	/**
	 * 
	 * inserts the timeline and update the % completed work in schedule
	 */
	public function actionCreate()
	{
		$model=new ActualTimeline;
		$model->task_date=date('Y-m-d');
		$model->user_id=Yii::app()->user->id;
		$model->isAlert=0;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['ActualTimeline']))
		{
			$model->attributes=$_POST['ActualTimeline'];
			
			if($model->validate())
			{
				$transaction = $model->getDbConnection()->beginTransaction();
				try
				{
					$alertSaved=FALSE;
					if($model->isAlert > 0)
					{
						$alert=TaskAlert::model()->find('task_id=:task_id and fixed_by=:fixed_by',array(':task_id'=>$model->task_id,':fixed_by'=>'0'));
						if($alert->alert_id > 0)
						{
							$alertSaved=TRUE;
						}
						else
						{
							$alert=new TaskAlert;
							$alert->fixed_by=0;
							$alert->task_id=$model->task_id;
							$alert->save();
							$alertSaved=true;
							
							$todo=new TodoList;
							$tid=$model->task_id;
							$todo->user_id=Task::model()->getRpsCoordinatorOfTask($tid);
							$todo->from_id=Yii::app()->user->id;
							
							$todo->task="The task no $tid exceeds client due date.";
							$todo->date_time=date('Y-m-d');
							$todo->actual_date=date('Y-m-d');
							$todo->status=2;
							
							$todo->save(FALSE);
						}
					}
					
					
					$model->save();
					$model->refresh();
					
					/*
					update task completed percentage
					*/
					$task_id=$model->task_id;
					$completedPerc=$model->percentage_completed;
					
					$t=Yii::app()->db->createCommand("update pm_task set done_percentage='$completedPerc' where task_id='$task_id'")->execute();
					
					$task=Task::model()->findByPk($task_id);
					
					if($completedPerc >=100)
					{			
			$r=Yii::app()->db->createCommand("update pm_task set error_known='$model->error_known',error_unknown='$model->error_unknown' where task_id='$task_id'")->execute();
			
                        
                                                $todo=new TodoList;
                                                $todo->user_id=Task::model()->getRpsCoordinatorOfTask($task_id);
                                                $todo->from_id=Yii::app()->user->id;
                                                $todo->task="The task no $task_id is marked completed 100% with error known $model->error_known and error unknown $model->error_unknown.";
                                                $todo->date_time=date('Y-m-d');
                                                $todo->actual_date=date('Y-m-d');
                                                $todo->status=2;

                                                $todo->save(FALSE);
                                                
						$remainingTaskSchedules = Yii::app()->db->createCommand()
						->select('*')
						->from('pm_task_schedule')
						->where("task_id =:task_id",array(':task_id'=>$model->task_id))
						->andWhere("user_id = :user_id",array(':user_id' => $model->user_id))
						->andWhere("completed_status = :completed_status",array(':completed_status' => '0'))
						->order('schedule_date ASC')
						->queryAll();
						
						foreach($remainingTaskSchedules as $rec)
						{
							
							$id=$rec['schedule_id'];
							$schedule=TaskSchedule::model()->findByPk($id);		
							
							$schedule->worked_normal_hr=$schedule->normal_hour;
							$schedule->worked_ot_hr=$schedule->overtime_hour;
							$schedule->completed_status=1;
							$schedule->save(false);
						}
                                                
                                                
					}
					else
					{
						//get total schedule sum
						$taskScheduleSum = Yii::app()->db->createCommand()
						->select('sum(normal_hour+overtime_hour) as tothour')
						->from('pm_task_schedule')
						->where("task_id =:task_id",array(':task_id'=>$model->task_id))
						->andWhere("user_id =:user_id",array(':user_id'=>$model->user_id))
						->queryRow();
								    
						$totHr=$taskScheduleSum['tothour'];
						$hourCompleted=$completedPerc*$totHr/100;
						
						$completedTaskHour = Yii::app()->db->createCommand()
						->select('IFNULL(sum(worked_normal_hr+worked_ot_hr),0) as totWorked')
						->from('pm_task_schedule')
						->where("task_id =:task_id",array(':task_id'=>$model->task_id))
						->andWhere("user_id = :user_id",array(':user_id' => $model->user_id))
						//->andWhere("completed_status = :completed_status",array(':completed_status' => '1'))
						->queryRow();
						
						$hourCompleted=$hourCompleted-$completedTaskHour['totWorked'];
						$hourCompleted=round($hourCompleted,1);
						
						if($hourCompleted > 0)
						{
							//update todays record
							$todaysRecord= Yii::app()->db->createCommand()
							->select('*')
							->from('pm_task_schedule')
							->where("task_id =:task_id",array(':task_id'=>$model->task_id))
							->andWhere("user_id =:user_id",array(':user_id'=>$model->user_id))
							->andWhere("schedule_date = :schedule_date",array(':schedule_date'=>$model->task_date))
							->andWhere("completed_status = :completed_status",array(':completed_status' => '0'))
							->queryRow();
							
							//update todays record. if work done is more than add the work in other dates.if less than add the reamining work the next day but make sure the task due date donot exceeds
							//if not donot mark the task as completed and alert the message for admin
							if($todaysRecord['schedule_id'] > 0)
							{
								$existingRec=new TaskSchedule;
								$existingRec->isNewRecord=FALSE;
								$existingRec->schedule_id=$todaysRecord['schedule_id'];
								$existingRec->task_id=$todaysRecord['task_id'];
								$existingRec->user_id=$todaysRecord['user_id'];
								$existingRec->schedule_date=$todaysRecord['schedule_date'];
								
								//for ot
								$otcompleted=$todaysRecord['worked_ot_hr'];
								$otassigned=$todaysRecord['overtime_hour'];
								$remainingot=$otassigned-$otcompleted;
								$otToAddForNextDay=0;
								
								if($remainingot > 0)
								{
									if($hourCompleted > $remainingot )
									{
										$existingRec->worked_ot_hr=$otcompleted+$remainingot;
										$hourCompleted=$hourCompleted-$remainingot;
									}
									else
									{
										$existingRec->worked_ot_hr=$otcompleted+$hourCompleted;
										$otToAddForNextDay=$otassigned-$existingRec->worked_ot_hr;
									}
									$existingRec->overtime_hour=$existingRec->worked_ot_hr;
								}
								
								//for normal
								$normalcompleted=$todaysRecord['worked_normal_hr'];
								$normalassigned=$todaysRecord['normal_hour'];
								$reaminingnormal=$normalassigned-$normalcompleted;
								$normalToAddForNextDay=0;
								
								if($reaminingnormal > 0)
								{
									if($hourCompleted > $reaminingnormal )
									{
										$existingRec->worked_normal_hr=$normalcompleted+$reaminingnormal;
										$hourCompleted=$hourCompleted-$reaminingnormal;
									}
									else
									{
										$existingRec->worked_normal_hr=$normalcompleted+$hourCompleted;
										$normalToAddForNextDay=$normalassigned-$existingRec->worked_normal_hr;
									}
									$existingRec->normal_hour=$existingRec->worked_normal_hr;
								}
								
								$existingRec->cusion_hour=$todaysRecord['cusion_hour'];
								$existingRec->schedule_nr_hr=$todaysRecord['schedule_nr_hr'];
								$existingRec->schedule_ot_hr=$todaysRecord['schedule_ot_hr'];
								$existingRec->remarks=$todaysRecord['remarks'];
								$existingRec->completed_status='1';
								$existingRec->extended_status=$todaysRecord['extended_status'];
								
								if(($otToAddForNextDay+$normalToAddForNextDay) > 0)
								{
									$nextdate=MyCustomClass::addDays($todaysRecord['schedule_date'],1);
									//check if added next date task due date do not exceeds.
									$saveNextRecord=TRUE;
									if( $nextdate > ($task->job_client_due_date==""?$task->job_due_date:$task->job_client_due_date))
									{
										$existingRec->completed_status=0;
										$existingRec->normal_hour+=$normalToAddForNextDay;
										$existingRec->overtime_hour+=$otToAddForNextDay;
										$existingRec->save();
										$saveNextRecord=FALSE;
										
										
										//alert task;
										if($alertSaved==FALSE)
										{
											$alert=TaskAlert::model()->find('task_id=:task_id and fixed_by=:fixed_by',array(':task_id'=>$model->task_id,':fixed_by'=>'0'));
											if($alert->alert_id <=0 || $alert->alert_id=="" )
											{
												$alert=new TaskAlert;
												$alert->fixed_by=0;
												$alert->task_id=$model->task_id;
												$alert->save();
												
												$alertSaved=TRUE;
												
												$todo=new TodoList;
												$tid=$model->task_id;
												$todo->user_id=Task::model()->getRpsCoordinatorOfTask($tid);
												$todo->from_id=Yii::app()->user->id;
												
												$todo->task="The task no $tid exceeds client due date.";
												$todo->date_time=date('Y-m-d');
												$todo->actual_date=date('Y-m-d');
												$todo->status=2;
												
												$todo->save(FALSE);
											}
											
											
										}
									}
									else
									{
										//check for saturday
										if(date('w', strtotime($nextdate)) == 6)
										{
											$nextdate=MyCustomClass::addDays($nextdate,1);
											if($nextdate > $task->job_due_date)
											{
												$nextdate=MyCustomClass::subractDays($nextdate,1);
											}
										}
										$existingRec->completed_status=1;
										$existingRec->save();
									}
									
									//for next date check if already record
									$nextDayRecord = Yii::app()->db->createCommand()
									->select('*')
									->from('pm_task_schedule')
									->where("task_id = :task_id",array(':task_id'=>$model->task_id))
									->andWhere("user_id = :user_id",array(':user_id'=>$model->user_id))
									->andWhere("schedule_date = :schedule_date",array(':schedule_date'=>$nextdate))
									->queryRow();
									
									$nextRec=new TaskSchedule;
									
									if($saveNextRecord)
									{
										
									
									
									if($nextDayRecord['schedule_id'] != "" && $nextDayRecord['schedule_id'] > 0)
									{
										$nextRec->isNewRecord=FALSE;
										$nextRec->schedule_id=$nextDayRecord['schedule_id'];
										$nextRec->task_id=$nextDayRecord['task_id'];
										$nextRec->user_id=$nextDayRecord['user_id'];
										$nextRec->schedule_date=$nextDayRecord['schedule_date'];
										$nextRec->normal_hour=$nextDayRecord['normal_hour']+$normalToAddForNextDay;
										$nextRec->cusion_hour=$nextDayRecord['cusion_hour'];
										$nextRec->overtime_hour=$nextDayRecord['overtime_hour']+$otToAddForNextDay;
										$nextRec->worked_normal_hr=$nextDayRecord['worked_normal_hr'];
										$nextRec->worked_ot_hr=$nextDayRecord['worked_ot_hr'];
										$nextRec->schedule_nr_hr=$nextDayRecord['schedule_nr_hr'];
										$nextRec->schedule_ot_hr=$nextDayRecord['schedule_ot_hr'];
										$nextRec->remarks=$nextDayRecord['remarks'];
										$nextRec->completed_status=0;
										$nextRec->extended_status=$nextDayRecord['extended_status'];
										$nextRec->save();
									}
									else
									{
										$nextRec->isNewRecord=true;
										$nextRec->task_id=$model->task_id;
										$nextRec->user_id=$model->user_id;
										$nextRec->schedule_date=$nextdate;
										$nextRec->normal_hour=$normalToAddForNextDay;
										$nextRec->cusion_hour=CodeValue::model()->getCodeTypeValue('work_hour',29);
										$nextRec->overtime_hour=$otToAddForNextDay;
										$nextRec->worked_normal_hr=0;
										$nextRec->worked_ot_hr=0;
										$nextRec->schedule_nr_hr=$normalToAddForNextDay;
										$nextRec->schedule_ot_hr=$otToAddForNextDay;
										$nextRec->remarks="";
										$nextRec->completed_status=0;
										$nextRec->extended_status=0;
										$nextRec->save();
									}
									}
								}
								else
								{
									$existingRec->save();
									
									//now in this else means the selected date work is completed.now if more completed hour than assign to the same task of same user for another dates;
									
									$remainingTaskSchedules = Yii::app()->db->createCommand()
									->select('*')
									->from('pm_task_schedule')
									->where("task_id =:task_id",array(':task_id'=>$model->task_id))
									->andWhere("user_id = :user_id",array(':user_id' => $model->user_id))
									->andWhere("completed_status = :completed_status",array(':completed_status' => '0'))
									->andWhere("schedule_date > $model->task_date")
									
									->order('schedule_date ASC')
									->queryAll();
									
									foreach($remainingTaskSchedules as $rec)
									{
										if($hourCompleted <= 0)
										{
											break;
										}
										$id=$rec['schedule_id'];
										$schedule=TaskSchedule::model()->findByPk($id);		
										$overtime=$rec['overtime_hour']-$rec['worked_ot_hr'];
										$normal=$rec['normal_hour']-$rec['worked_normal_hr'];
										
										$remaining=$overtime+$normal;
										
										$addInOt=0;
										$addInNormal=0;
										if($hourCompleted >= $remaining)
										{
											$addInOt=$overtime;
											$addInNormal=$normal;
											$schedule->completed_status=1;
											$hourCompleted=$hourCompleted-$remaining;
										}
										else
										{
											if($hourCompleted >= $overtime)
											{
												$addInOt=$overtime;
												$hourCompleted=$hourCompleted-$overtime;
												
												$addInNormal=$hourCompleted;
												$hourCompleted=0;
											}
											else
											{
												$addInOt=$hourCompleted;
												$hourCompleted=0;
												$addInNormal=0;
												break;
											}
											$schedule->completed_status=0;
										}
										$addInNormal=round($addInNormal,1);
										$addInOt=round($addInOt,1);
										
										$schedule->worked_normal_hr=$schedule->worked_normal_hr+$addInNormal;
										$schedule->worked_ot_hr=$schedule->worked_ot_hr+$addInOt;
										$schedule->save(false);
									}
								}
							}
						}
					}
					
					
					
					$transaction->commit();
				}
				catch(Exception $e)
				{
					$transaction->rollback();
					throw new CHttpException(404, 'Some error occured.Please try again');
				}
				
				EQuickDlgs::checkDialogJsScript();
				$this->redirect(array('view','id'=>$model->timeline_id));
			}
				
		}

		EQuickDlgs::render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                
                $task=Task::model()->findByPk($model->task_id);
                $model->error_known=$task->error_known;
                $model->error_unknown=$task->error_unknown;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['ActualTimeline']))
		{
			$model->attributes=$_POST['ActualTimeline'];
			if($model->save())
			{
                            $model->refresh();
                            $task_id=$model->task_id;
                            if($model->percentage_completed >=100)
                            {
                                $r=Yii::app()->db->createCommand("update pm_task set error_known='$model->error_known',error_unknown='$model->error_unknown' where task_id='$task_id'")->execute();
                            }
				EQuickDlgs::checkDialogJsScript();
				$this->redirect(array('admin'));
			}
				
		}

		EQuickDlgs::render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		//$dataProvider=new CActiveDataProvider('ActualTimeline');
		//$this->render('index',array(
			//'dataProvider'=>$dataProvider,
		//));
		
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$actualmodel=new ActualTimeline('search');
		
		$fixedmodel=new FixedTaskTimeline('search');
		
		$actualmodel->unsetAttributes();  // clear any default values
		$fixedmodel->unsetAttributes();
		
		if(isset($_GET['ActualTimeline']))
			$actualmodel->attributes=$_GET['ActualTimeline'];
			
		if(isset($_GET['FixedTaskTimeline']))
			$fixedmodel->attributes=$_GET['FixedTaskTimeline'];	
		

		$this->render('actual_fixed_form',array(
			'actualmodel'=>$actualmodel,
			'fixedmodel'=>$fixedmodel,
		));
	}
	
	
	public function actionGetAuditedValues()
	{


		$data = array(
			array("code_id" => 'Yes',"code_lbl"=> "Yes"),
			array("code_id" => 'No',"code_lbl"=> "No"),
		);
		/* Replace the keys in the new array with 'value' and 'text' */
		foreach($data as $k => $v)
		{
			$data[$k]['value'] = $data[$k]['code_id'];
			unset($data[$k]['code_id']);

			$data[$k]['text'] = $data[$k]['code_lbl'];
			unset($data[$k]['code_lbl']);
		}

		echo json_encode($data);
	}

	public function actionUpdateAuditedValues()
	{

		if(Yii::app()->request->isAjaxRequest)
		{
			Yii::import('bootstrap.widgets.TbEditableSaver');
			$es = new TbEditableSaver('ActualTimeline');
			//$id = Yii::app()->user->id;
			//$this->updt_cnt = $this->updt_cnt + 1;
			//$this->updt_by = $id;
			$es->update();
		}
		else
		throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
	}
	
	
	public function actionUpdateErrors()
	{
		if(Yii::app()->request->isAjaxRequest)
	    {
	        Yii::import('bootstrap.widgets.MyErrorTbEditableSaver');
	        $es=new MyErrorTbEditableSaver('Task');
	      
			//$id=Yii::app()->user->id;
			//$this->updt_cnt=$this->updt_cnt+1;
			//$this->updt_by=$id;
	      $es->update();
	    }
	    else
	        throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ActualTimeline the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ActualTimeline::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ActualTimeline $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='actual-timeline-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
       /*
        * transfer the task to another user 
        * while transfering the task new task is created with the remaining hours
        * @param task_id
        */  
     public function actionTaskTransferPopUp($id)
	{
		$model    = new ActualTimeline;
                $model->scenario='transfer';
                //$taskschedulemodel    = new TaskSchedule;
		$taskmodel= Task::model()->findByPk($id);
                
               // $criteria=new CDbCriteria;
                //$criteria->select='max(timeline_id) AS maxColumn';

               
               
               $model->task_id=$taskmodel->task_id;
              // $model->user_id=$taskmodel->assigned_to;
               

		//$oldModel = $model;
		// Uncomment the following line if AJAX validation is needed
                //$con=new TaskController();
                   $taskSchedules = Yii::app()->db->createCommand()
                               ->select('pm_task_schedule.task_id,pm_task_schedule.schedule_date,pm_task_schedule.normal_hour,pm_task_schedule.overtime_hour')
                               ->from('pm_task_schedule')
                              ->join('pm_task', 'pm_task_schedule.task_id = pm_task.task_id')
                               ->where("pm_task.task_id = $id")
                               ->queryAll();
                   
                   //   echo $tasktransfer['task_id'];
                     
                      
		$this->performAjaxValidation($model);

		if(isset($_POST['ActualTimeline']))
		{
                        
			$model->attributes = $_POST['ActualTimeline'];
                        
                        
                        $transaction = $model->getDbConnection()->beginTransaction();
                        try
                        {
                            $valid=$model->validate();
                            if($valid)
                            {
                                $perc=$model->percentage_completed;
                                $uid=$model->user_id;
                                $completedHour=($perc/100)*$taskmodel->est_rps_cost;
                                $model->user_id=$taskmodel->assigned_to;
                                $model->percentage_completed=100;
                             if($model->save())
                                {
                                //$task=Task::model()->findByPk($this->task_id);
                                    Task::model()->updateByPk($id, array(
                                                    'est_rps_cost' => $completedHour,
                                                    'done_percentage' => '100',   
                                                    'error_known' => $this->error_known,
                                                    'error_unknown' => $this->error_unknown
                                              ));
                                
                                /*
                                 * now we need to check the percentage completed and update the old schedule
                                 */
                                    $task_id=$model->task_id;
                                    if($perc >=100)
					{			
			
                        
                                                $todo=new TodoList;
                                                $todo->user_id=Task::model()->getRpsCoordinatorOfTask($task_id);
                                                $todo->from_id=Yii::app()->user->id;
                                                $todo->task="The task no $task_id is marked completed 100% with error known $model->error_known and error unknown $model->error_unknown.";
                                                $todo->date_time=date('Y-m-d');
                                                $todo->actual_date=date('Y-m-d');
                                                $todo->status=2;

                                                $todo->save(FALSE);
                                                
						$remainingTaskSchedules = Yii::app()->db->createCommand()
						->select('*')
						->from('pm_task_schedule')
						->where("task_id =:task_id",array(':task_id'=>$model->task_id))
						->andWhere("user_id = :user_id",array(':user_id' => $model->user_id))
						->andWhere("completed_status = :completed_status",array(':completed_status' => '0'))
						->order('schedule_date ASC')
						->queryAll();
						
						foreach($remainingTaskSchedules as $rec)
						{
							
							$id=$rec['schedule_id'];
							$schedule=TaskSchedule::model()->findByPk($id);		
							
							$schedule->worked_normal_hr=$schedule->normal_hour;
							$schedule->worked_ot_hr=$schedule->overtime_hour;
							$schedule->completed_status=1;
							$schedule->save(false);
						}
                                                
                                                
					}
					else
					{
						//get total schedule sum
						$taskScheduleSum = Yii::app()->db->createCommand()
						->select('sum(normal_hour+overtime_hour) as tothour')
						->from('pm_task_schedule')
						->where("task_id =:task_id",array(':task_id'=>$model->task_id))
						->andWhere("user_id =:user_id",array(':user_id'=>$model->user_id))
						->queryRow();
								    
						$totHr=$taskScheduleSum['tothour'];
						$hourCompleted=$perc*$totHr/100;
						
						$completedTaskHour = Yii::app()->db->createCommand()
						->select('IFNULL(sum(worked_normal_hr+worked_ot_hr),0) as totWorked')
						->from('pm_task_schedule')
						->where("task_id =:task_id",array(':task_id'=>$model->task_id))
						->andWhere("user_id = :user_id",array(':user_id' => $model->user_id))
						//->andWhere("completed_status = :completed_status",array(':completed_status' => '1'))
						->queryRow();
						
						$hourCompleted=$hourCompleted-$completedTaskHour['totWorked'];
						$hourCompleted=round($hourCompleted,1);
						
						if($hourCompleted > 0)
						{
							//update todays record
							$todaysRecord= Yii::app()->db->createCommand()
							->select('*')
							->from('pm_task_schedule')
							->where("task_id =:task_id",array(':task_id'=>$model->task_id))
							->andWhere("user_id =:user_id",array(':user_id'=>$model->user_id))
							->andWhere("completed_status = :completed_status",array(':completed_status' => '0'))
                                                        ->order("schedule_date asc")        
							->queryRow();
							
							//update todays record. if work done is more than add the work in other dates.if less than add the reamining work the next day but make sure the task due date donot exceeds
							//if not donot mark the task as completed and alert the message for admin
							if($todaysRecord['schedule_id'] > 0)
							{
								$existingRec=new TaskSchedule;
								$existingRec->isNewRecord=FALSE;
								$existingRec->schedule_id=$todaysRecord['schedule_id'];
								$existingRec->task_id=$todaysRecord['task_id'];
								$existingRec->user_id=$todaysRecord['user_id'];
								$existingRec->schedule_date=$todaysRecord['schedule_date'];
								
								//for ot
								$otcompleted=$todaysRecord['worked_ot_hr'];
								$otassigned=$todaysRecord['overtime_hour'];
								$remainingot=$otassigned-$otcompleted;
								$otToAddForNextDay=0;
								
								if($remainingot > 0)
								{
									if($hourCompleted > $remainingot )
									{
										$existingRec->worked_ot_hr=$otcompleted+$remainingot;
										$hourCompleted=$hourCompleted-$remainingot;
									}
									else
									{
										$existingRec->worked_ot_hr=$otcompleted+$hourCompleted;
										$otToAddForNextDay=$otassigned-$existingRec->worked_ot_hr;
									}
									$existingRec->overtime_hour=$existingRec->worked_ot_hr;
								}
								
								//for normal
								$normalcompleted=$todaysRecord['worked_normal_hr'];
								$normalassigned=$todaysRecord['normal_hour'];
								$reaminingnormal=$normalassigned-$normalcompleted;
								$normalToAddForNextDay=0;
								
								if($reaminingnormal > 0)
								{
									if($hourCompleted > $reaminingnormal )
									{
										$existingRec->worked_normal_hr=$normalcompleted+$reaminingnormal;
										$hourCompleted=$hourCompleted-$reaminingnormal;
									}
									else
									{
										$existingRec->worked_normal_hr=$normalcompleted+$hourCompleted;
										$normalToAddForNextDay=$normalassigned-$existingRec->worked_normal_hr;
									}
									$existingRec->normal_hour=$existingRec->worked_normal_hr;
								}
								
								$existingRec->cusion_hour=$todaysRecord['cusion_hour'];
								$existingRec->schedule_nr_hr=$todaysRecord['schedule_nr_hr'];
								$existingRec->schedule_ot_hr=$todaysRecord['schedule_ot_hr'];
								$existingRec->remarks=$todaysRecord['remarks'];
								$existingRec->completed_status='1';
								$existingRec->extended_status=$todaysRecord['extended_status'];
								
								if(($otToAddForNextDay+$normalToAddForNextDay) > 0)
								{
									$nextdate=MyCustomClass::addDays($todaysRecord['schedule_date'],1);
									//check if added next date task due date do not exceeds.
									$saveNextRecord=TRUE;
									if( $nextdate > ($task->job_client_due_date==""?$task->job_due_date:$task->job_client_due_date))
									{
										$existingRec->completed_status=0;
										$existingRec->normal_hour+=$normalToAddForNextDay;
										$existingRec->overtime_hour+=$otToAddForNextDay;
										$existingRec->save();
										$saveNextRecord=FALSE;
										
										
										//alert task;
										if($alertSaved==FALSE)
										{
											$alert=TaskAlert::model()->find('task_id=:task_id and fixed_by=:fixed_by',array(':task_id'=>$model->task_id,':fixed_by'=>'0'));
											if($alert->alert_id <=0 || $alert->alert_id=="" )
											{
												$alert=new TaskAlert;
												$alert->fixed_by=0;
												$alert->task_id=$model->task_id;
												$alert->save();
												
												$alertSaved=TRUE;
												
												$todo=new TodoList;
												$tid=$model->task_id;
												$todo->user_id=Task::model()->getRpsCoordinatorOfTask($tid);
												$todo->from_id=Yii::app()->user->id;
												
												$todo->task="The task no $tid exceeds client due date.";
												$todo->date_time=date('Y-m-d');
												$todo->actual_date=date('Y-m-d');
												$todo->status=2;
												
												$todo->save(FALSE);
											}
											
											
										}
									}
									else
									{
										//check for saturday
										if(date('w', strtotime($nextdate)) == 6)
										{
											$nextdate=MyCustomClass::addDays($nextdate,1);
											if($nextdate > $task->job_due_date)
											{
												$nextdate=MyCustomClass::subractDays($nextdate,1);
											}
										}
										$existingRec->completed_status=1;
										$existingRec->save();
									}
									
									//for next date check if already record
									$nextDayRecord = Yii::app()->db->createCommand()
									->select('*')
									->from('pm_task_schedule')
									->where("task_id = :task_id",array(':task_id'=>$model->task_id))
									->andWhere("user_id = :user_id",array(':user_id'=>$model->user_id))
									->andWhere("schedule_date = :schedule_date",array(':schedule_date'=>$nextdate))
									->queryRow();
									
									$nextRec=new TaskSchedule;
									
									if($saveNextRecord)
									{
										
									
									
									if($nextDayRecord['schedule_id'] != "" && $nextDayRecord['schedule_id'] > 0)
									{
										$nextRec->isNewRecord=FALSE;
										$nextRec->schedule_id=$nextDayRecord['schedule_id'];
										$nextRec->task_id=$nextDayRecord['task_id'];
										$nextRec->user_id=$nextDayRecord['user_id'];
										$nextRec->schedule_date=$nextDayRecord['schedule_date'];
										$nextRec->normal_hour=$nextDayRecord['normal_hour']+$normalToAddForNextDay;
										$nextRec->cusion_hour=$nextDayRecord['cusion_hour'];
										$nextRec->overtime_hour=$nextDayRecord['overtime_hour']+$otToAddForNextDay;
										$nextRec->worked_normal_hr=$nextDayRecord['worked_normal_hr'];
										$nextRec->worked_ot_hr=$nextDayRecord['worked_ot_hr'];
										$nextRec->schedule_nr_hr=$nextDayRecord['schedule_nr_hr'];
										$nextRec->schedule_ot_hr=$nextDayRecord['schedule_ot_hr'];
										$nextRec->remarks=$nextDayRecord['remarks'];
										$nextRec->completed_status=0;
										$nextRec->extended_status=$nextDayRecord['extended_status'];
										$nextRec->save();
									}
									else
									{
										$nextRec->isNewRecord=true;
										$nextRec->task_id=$model->task_id;
										$nextRec->user_id=$model->user_id;
										$nextRec->schedule_date=$nextdate;
										$nextRec->normal_hour=$normalToAddForNextDay;
										$nextRec->cusion_hour=CodeValue::model()->getCodeTypeValue('work_hour',29);
										$nextRec->overtime_hour=$otToAddForNextDay;
										$nextRec->worked_normal_hr=0;
										$nextRec->worked_ot_hr=0;
										$nextRec->schedule_nr_hr=$normalToAddForNextDay;
										$nextRec->schedule_ot_hr=$otToAddForNextDay;
										$nextRec->remarks="";
										$nextRec->completed_status=0;
										$nextRec->extended_status=0;
										$nextRec->save();
									}
									}
								}
								else
								{
									$existingRec->save();
									
									//now in this else means the selected date work is completed.now if more completed hour than assign to the same task of same user for another dates;
									
									$remainingTaskSchedules = Yii::app()->db->createCommand()
									->select('*')
									->from('pm_task_schedule')
									->where("task_id =:task_id",array(':task_id'=>$model->task_id))
									->andWhere("user_id = :user_id",array(':user_id' => $model->user_id))
									->andWhere("completed_status = :completed_status",array(':completed_status' => '0'))
									->andWhere("schedule_date > $model->task_date")
									
									->order('schedule_date ASC')
									->queryAll();
									
									foreach($remainingTaskSchedules as $rec)
									{
										if($hourCompleted <= 0)
										{
											break;
										}
										$id=$rec['schedule_id'];
										$schedule=TaskSchedule::model()->findByPk($id);		
										$overtime=$rec['overtime_hour']-$rec['worked_ot_hr'];
										$normal=$rec['normal_hour']-$rec['worked_normal_hr'];
										
										$remaining=$overtime+$normal;
										
										$addInOt=0;
										$addInNormal=0;
										if($hourCompleted >= $remaining)
										{
											$addInOt=$overtime;
											$addInNormal=$normal;
											$schedule->completed_status=1;
											$hourCompleted=$hourCompleted-$remaining;
										}
										else
										{
											if($hourCompleted >= $overtime)
											{
												$addInOt=$overtime;
												$hourCompleted=$hourCompleted-$overtime;
												
												$addInNormal=$hourCompleted;
												$hourCompleted=0;
											}
											else
											{
												$addInOt=$hourCompleted;
												$hourCompleted=0;
												$addInNormal=0;
												break;
											}
											$schedule->completed_status=0;
										}
										$addInNormal=round($addInNormal,1);
										$addInOt=round($addInOt,1);
										
										$schedule->worked_normal_hr=$schedule->worked_normal_hr+$addInNormal;
										$schedule->worked_ot_hr=$schedule->worked_ot_hr+$addInOt;
										$schedule->save(false);
									}
								}
							}
						}
					}
                                    
                                    /*
                                     * old schedule update finished
                                     */
                                
                                }
                            $transaction->commit();
                            EQuickDlgs::checkDialogJsScript();
			$this->redirect(array('//taskSchedule/scheduleReport'));   
                            }
                            
                            
                        } catch (Exception $ex) {
                                $transaction->rollback();
                            throw new CHttpException(404, 'Some error occured.Please try again');
                        }
                     
		}

		//EQuickDlgs::render('_updateSchedulePopUp', array(
				//'model'=> $model,
			//));
               
               EQuickDlgs::render('_updateTaskSchedule', array('model' => $model,'taskmodel'=>$taskmodel,'oldSchedule'=>$taskSchedules));
	}
        
        
        
}
