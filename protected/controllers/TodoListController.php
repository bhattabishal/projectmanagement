<?php

class TodoListController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','getTaskStatusValues','editTaskStatus'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 *  public function actionView($id)
	{
		EQuickDlgs::render('view', array(
				'model'=> $this->loadModel($id),
			));
	}
	 */
	

	public function actionView($id)
{
    EQuickDlgs::render('view',array(
	        'model'=>$this->loadModel($id)
			));
}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new TodoList;
		$model->date_time=date('Y-m-d');
		$model->user_id=Yii::app()->user->id;
		$model->status=2;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TodoList']))
		{
			$model->attributes=$_POST['TodoList'];
			$model->actual_date=$model->date_time;
			$model->from_id = Yii::app()->user->id;
			
			if($model->save())
			{
				EQuickDlgs::checkDialogJsScript();
				$this->redirect(array('admin','id'=>$model->to_do_id));
			}
				
		}

		EQuickDlgs::render('create',array(
			'model'=>$model,
		));
	}


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TodoList']))
		{
			$model->attributes=$_POST['TodoList'];
			if($model->save())
			{
				EQuickDlgs::checkDialogJsScript();
				$this->redirect(array('admin','id'=>$model->to_do_id));
			}
			
				
		}

		EQuickDlgs::render('update',array(
			'model'=>$model,
		));
	}
	
	

	/**	$command = $connection->createCommand(
	        'UPDATE pm_todo_list SET t.date_time = t.current_date WHERE status=undone');
         	$command->execute();
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->actionAdmin();
		
       // $criteria=new CDbCriteria(array(
        //'condition'=>'status='.Post::done,
       // 'order'=>'date_time DESC',
       //'with'=>'commentCount',
    //));
	/*
       if(isset($_GET['tag']))
        $criteria->addSearchCondition('tags',$_GET['tag']);
 
    $dataProvider=new CActiveDataProvider('Post', array(
        'pagination'=>array(
            'pageSize'=>5,
        ),
        'criteria'=>$criteria,
    ));
	 $this->render('index',array(
       'dataProvider'=>$dataProvider,
      ));*/
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		
	$id=Yii::app()->user->id;
	
	$command =Yii::app()->db->createCommand(
	
	"UPDATE pm_todo_list SET date_time=CURDATE()
	              WHERE status='2' AND date_time < CURDATE() and user_id='$id'"
	);
	$command->execute();
	
		$model=new TodoList('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TodoList']))
			$model->attributes=$_GET['TodoList'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionGetTaskStatusValues()
	{
		$criteria = new CDbCriteria;
		$criteria->select='*';  // only select the 'title' column
		$criteria->condition="code_type='status'";
		$models=CodeValue::model()->findAll($criteria);
		
		/* Extract only the attributes that you need from the models
       into a new array */
    	$data = array_map(
        create_function('$m', 
            'return $m->getAttributes(array(\'code_id\',\'code_lbl\'));'), 
            $models);
			
		/* Replace the keys in the new array with 'value' and 'text' */
	    foreach($data as $k => $v )
	    {
	        $data[$k]['value'] = $data[$k]['code_id'];
	        unset($data[$k]['code_id']);
	 
	        $data[$k]['text'] = $data[$k]['code_lbl'];
	        unset($data[$k]['code_lbl']);
	    }
		echo json_encode($data);
	}
	
	 public function actionEditTaskStatus() {

        if (Yii::app()->request->isAjaxRequest) {
            Yii::import('bootstrap.widgets.TbEditableSaver');
            $es = new TbEditableSaver('TodoList');

            //$additionalAttr=array();
            //$attribute = yii::app()->request->getParam('name');



            $es->update();
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PmTodoList the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=TodoList::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PmTodoList $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='todo-list-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}