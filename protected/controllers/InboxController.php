<?php

class InboxController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','loadReply','downloadFile'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		//$this->render('view',array(
			//'model'=>$this->loadModel($id),
		//));
		EQuickDlgs::render('view',array('model'=>$this->loadModel($id)));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Inbox;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Inbox']))
		{
			$model->attributes=$_POST['Inbox'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->inbox_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Inbox']))
		{
			$model->attributes=$_POST['Inbox'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->inbox_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		//$dataProvider=new CActiveDataProvider('Inbox');
		//$this->render('index',array(
			//'dataProvider'=>$dataProvider,
		//));
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	 /*
	public function actionAdmin()
	{
		$model=new Inbox('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Inbox']))
			$model->attributes=$_GET['Inbox'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}*/
	public function actionAdmin()
	{
		$inboxmodel=new Inbox('search');
		$mailmodel=new Reply('search');
		$inboxmodel->unsetAttributes();  // clear any default values
		$mailmodel->unsetAttributes();
		if(isset($_GET['Inbox']))
			$inboxmodel->attributes=$_GET['Inbox'];
			if(isset($_GET['Reply']))
			$mailmodel->attributes=$_GET['Reply'];

		$this->render('inbox_send',array(
			'inboxmodel'=>$inboxmodel,
			'mailmodel'=>$mailmodel,
		));
	}
	
	public function actionLoadReply()
	{
		$id=explode("-",Yii::app()->getRequest()->getParam('id'));
		
		$criteria=new CDbCriteria;
		$criteria->select='*';  // only select the 'title' column
		$criteria->condition='inbox_id=:inbox_id';
		$criteria->params=array(':inbox_id'=>$id[0]);
		
		$replys=Reply::model()->findAll($criteria); // $params is not needed
		
		
		$dataProvider=new CActiveDataProvider(Reply, array(
    	'data'=>$replys,
		));
	 		
		$this->renderPartial('reply', array(
			
			'gridDataProvider' => $dataProvider,
			),false,true);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Inbox the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Inbox::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	public function actionDownloadFile($file)
	{
		$upload_path = Yii::getPathOfAlias('webroot').'/project_files/archive/';

		if( file_exists( $upload_path.$file ) ){
		Yii::app()->getRequest()->sendFile( $file , file_get_contents( $upload_path.$file ) );
		}
		else{
			$this->render('download404');
		} 
	}

	/**
	 * Performs the AJAX validation.
	 * @param Inbox $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='inbox-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
