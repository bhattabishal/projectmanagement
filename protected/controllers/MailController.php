<?php
Yii::import('ext.EIMap.EIMap', true);
Yii::import('ext.runactions.components.ERunActions');
class MailController extends CController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
	
	

	/**
	 * @return array action filters
	 */
	 public function actions() {
        return array(
           'getRowForm1' => array(
                'class' => 'ext.dynamictabularform.actions.GetRowForm1',
                'view' => 'reply_form',
                'modelClass' => 'Reply'
            ),
			'getRowForm2' => array(
                'class' => 'ext.dynamictabularform.actions.GetRowForm2',
                'view' => 'comm_form',
                'modelClass' => 'Communication'
            ),
        );
    }
	
	 
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','delete','create','update','test','messageDialog','downloadAttachment','loadEmails','reply','deleteMail','getRowForm1','getRowForm2','bulkDelete','archive'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	
	
	
	public function actionIndex()
	{
		$uid=Yii::app()->user->id;
		$commEmails=Communication::model()->findAll("emailId > 0 and user_id=$uid");
		$commEmailsArray=array();
		if(count($commEmails) > 0)
		{
			$i=0;
			foreach($commEmails as $cMail)
			{
				$commEmailsArray[$i]=$cMail->emailId;
				$i++;
			}
		}
		$archiveEmails=Inbox::model()->findAll("crtd_by=$uid");
		$archiveEmailsArray=array();
		if(count($archiveEmails) > 0)
		{
			$i=0;
			foreach($archiveEmails as $eMail)
			{
				$archiveEmailsArray[$i]=$eMail->msg_no;
				$i++;
			}
		}
		$this->render('maillist',array('commEmailsArray'=>$commEmailsArray,'archiveEmailsArray'=>$archiveEmailsArray));
	}
	public function actionLoadEmails()
	{
		if (ERunActions::runBackground())
        {
           //do all the stuff that should work in background
           //mail->send() ....
		   $imap=new EIMap('{imap.gmail.com:993/imap/ssl}',  "bishal.testing@gmail.com", '21l@hsiB');
		   $list = array();
			if($imap->connect())
		{
			$folders=$imap->listFolders();
		
			$week_emails=$imap->searchMails('SINCE '. date('d-M-Y',strtotime("-2 week")));
			if($week_emails && is_array($week_emails))
			{
				rsort($week_emails);
				foreach($week_emails as $email_id)
				{
					$message=$imap->getMail($email_id,null,false,true);
					$mailObject=new Mail();
					$mailObject->messageId=$email_id;
					$mailObject->attachment=$message->attachments;
					$from=$message->fromName==""?$message->fromAddress:$message->fromName;
					$mailObject->from=$from;
					$mailObject->subject=$message->subject;
					$mailObject->date=$message->date;
					$mailObject->messageOriginal=$message->textHtmlOriginal;
					$mailObject->messagePlain=$message->textPlain;
					array_push($list, $mailObject);
				}
			}
			$this->render('maillist',array('mails'=>$list));
			$imap->close();
		}
		}
        else
        {
            //this code will be executed immediately
           // echo 'Time-consuming process has been started';
			//$this->render('maillist',array('mails'=>$list));
            //user->setFlash ...render ... redirect,
			
        }
	}

	
	
	public function actionTest()
	{
		$this->render('test');
		 
	}
	
	public function actionReply()
	{
		//print_r($_POST['Reply']);
		//die;
		//$model=new Inbox();
		
		//$this->performAjaxValidation($messageObj);
		//die;
		
		if(isset($_POST['Reply']))
		{
			$reply=$_POST['Reply'];
			//print_r($reply);
			$errors=array();
			$emailTo=array();
			$emailCc=array();
			
			$emailTo=$reply['email_to'];
			$emailCc=$reply['email_cc'];
			if($reply['email_to'] != "" && strpos($reply['email_to'],',') !== false)
			{
				$emailTo=explode(',',trim($reply['email_to']));
				//print_r($emailTo);
				
				foreach($emailTo as $val)
				{
					if(!filter_var($val, FILTER_VALIDATE_EMAIL))
					{
						$errors[]="To Email address is invalid";
						
					}
				}
				//print_r($errors);
			//die;
			}
			else
			{
				
				$emailTo=$reply['email_to'];
				if(!filter_var($emailTo, FILTER_VALIDATE_EMAIL))
				{
					$errors[]="To Email address is invalid";
				}
			}
			
			if($reply['email_cc'] != "")
			{
				if(strpos($reply['email_cc'],',') !== false)
				{
					
				
					$emailCc=explode(',',trim($reply['email_cc']));
					foreach($emailCc as $val)
					{
						if(!filter_var($val, FILTER_VALIDATE_EMAIL))
						{
							$errors[]="CC Email address is invalid";
						}
					}
				}
				else
				{
					$emailCc=$reply['email_cc'];
					if(!filter_var($emailCc, FILTER_VALIDATE_EMAIL))
					{
						$errors[]="CC Email address is invalid";
					}
				}
			}
			
			
			//check if already saved in inbox.i.e already replied once.If so no need to save in inbox
			$userId=Yii::app()->user->id;
			$uid=$reply['uid'];
			$archiveEmails=Inbox::model()->find("crtd_by=$userId and uid=$uid");
			
			
			$model=new Reply();
			//$model->attributes=$_POST['Reply'];
			$model->email_from=trim($reply['email_from']);
			$model->email_to=trim($reply['email_to']);
			$model->email_cc=trim($reply['email_cc']);
			$model->subject=trim($reply['subject']);
			$model->message=$reply['message'];
			$model->setIsNewRecord(true);
			
			if($archiveEmails==null || $archiveEmails->inbox_id <= 0)
			{
				$messageObj=new Inbox();
				$messageObj->uid=trim($reply['uid']);
				$messageObj->msg_no=trim($reply['msg_no']);
				$messageObj->fromName=trim($reply['fromName']);
				$messageObj->fromAddress=trim($reply['fromAddress']);
				$messageObj->mail_to=trim($reply['mail_to']);
				$messageObj->mail_cc=trim($reply['mail_cc']);
				 $messageObj->subject=trim($reply['mainSubject']);
				 $messageObj->message=$reply['mainMessage'];
				 $messageObj->mail_date=trim($reply['mail_date']);
				 $messageObj->setIsNewRecord(true);
			}
			
			
			
			
			
			if($model->subject=="")
			{
				$errors[]="Subject cannot be blank";
			}
			
			if($model->message=="")
			{
				$errors[]="Message cannot be blank";
			}
			
			
			
			if(count($errors) <= 0)
	       {
	          
			//print_r($_POST['Reply']);
			//die;
			//$reply=$_POST['Reply'];
			
			

			 
			 
			 	
				
				

				
				$message= new YiiMailMessage;
				$message->view = "test_reply";
				$params= array('myMail'=>$model);
				$message->subject = $model->subject;
				$message->setBody($params, 'text/html');
				$message->from = trim($reply['email_from']);
				//$message->from = "contact@magnus.com.np";
				//$message->addTo(trim($reply['email_to']));
				
				if($reply['email_to'] != "" && strpos($reply['email_to'],',') !== false)
				{
					$emailTo=explode(',',trim($reply['email_to']));
					foreach($emailTo as $val)
					{
						$message->addTo($val);
					}
				}
				else
				{
					$message->addTo($reply['email_to']);
				}
				
				if($reply['email_cc'] != "")
				{
					if(strpos($reply['email_cc'],',') !== false)
					{
						$emailCc=explode(',',trim($reply['email_cc']));
						foreach($emailCc as $val)
						{
							$message->addCc($val);
						}
					}
					else
					{
						$message->addCc($reply['email_cc']);
					}
				}
				
				
				
				
				//$message->addTo('bishalbhatta@magnus.com.np');
				
				//print_r($messageObj);
				//die;
				
				//print_r(CHtml::errorSummary($messageObj));
				//die;
				
				 $transaction = $model->getDbConnection()->beginTransaction();
				  try 
			 		{
						if(Yii::app()->mail->send($message))
					  {
					  	if($archiveEmails==null || $archiveEmails->inbox_id <= 0)
					  	{
							$messageObj->save();
							$messageObj->refresh();
							$model->inbox_id=$messageObj->inbox_id;
						}
						else
						{
							$model->inbox_id=$archiveEmails->inbox_id;
						}
					  	
						
						
						
						$model->save();
						
						echo "yes";
						
					  }
					  else
					  {
					  	
					  	echo "Some error occured.Please try again";
					  }
					   $transaction->commit();
					}
					catch (Exception $e)
			 		{
			 			$transaction->rollback();
						throw new CHttpException(404, 'Some error occured.Please try again');
			 		}
				
			 
			 
				  
			 
	           return;
	        }
			else
			{
				foreach($errors as $val)
				{
					echo $val;
				}
			}
		}
	}
	
	public function actionDownloadAttachment()
	{
		
		$msg=$_REQUEST['msgId'];
		$pid=$_REQUEST['pid'];
		$attachment;
		
		$user_details=EmailUser::model()->getEmailDetailsById(Yii::app()->user->id);
		$user=$user_details->email_id;
		$pass=MyCustomClass::decrypt($user_details->password);
		$imap=new EIMap('{vmcp09.digitalpacific.com.au:993/imap/ssl/novalidate-cert}', $user, $pass);
		
		if($imap->connect())
		{
			$message=$imap->getMail($msg,null,false,true);
			$attachment=$message->attachments;
			$imap->close();
		}
	
	
	$fileData;
	foreach($attachment as $file)
	{
		if($file['pid']==$pid)
		{
			$fileData=$file;
			break;
		}
	}
	
		//print_r($fileData);
		//die;
		header('Content-Description: File Transfer');
		header('Content-Type: ' .$fileData['mimetype']);
		header('Content-Disposition: attachment; filename='.$fileData['filename']);
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		//header('Content-Length: ' . filesize($fileData['data']));
		ob_clean();
		flush();
		echo $fileData['data'];
	}

 
  	public function actionMessageDialog($emailId)
	{
			EQuickDlgs::render('message_dialog',array(
			'emailId'=>$emailId,
		));
		
		
		//$this->renderPartial('message_dialog',array('id'=>$msgId),false,true);
	}
	
	/*
	Delete selected mail
	@param int message no
	*/
	public function actionDeleteMail($emailId)
	{
		if($emailId > 0)
		{
			$user_details=EmailUser::model()->getEmailDetailsById(Yii::app()->user->id);
			$user=$user_details->email_id;
			$pass=MyCustomClass::decrypt($user_details->password);
			
			$imap=new EIMap('{vmcp09.digitalpacific.com.au:993/imap/ssl/novalidate-cert}', $user, $pass);
			
			if($imap->connect())
			{
				$imap->deleteMail($emailId);
			}
		
			$imap->close();
		}
		$this->redirect(array('index'));
		
	}
	
	/*
	Delete selected mails in bulk
	*/
	public function actionBulkDelete()
	{
		//print_r($_POST);
		//die;
		if(Yii::app()->request->isAjaxRequest)
	    {
			if(isset($_POST['data1']))
			{
				$msgsArry=$_POST['data1'];
				if(count($msgsArry) > 0)
				{
					$user_details=EmailUser::model()->getEmailDetailsById(Yii::app()->user->id);
					$user=$user_details->email_id;
					$pass=MyCustomClass::decrypt($user_details->password);
					$imap=new EIMap('{vmcp09.digitalpacific.com.au:993/imap/ssl/novalidate-cert}', $user, $pass);
					if($imap->connect())
					{
						
						foreach($msgsArry as $key=>$val)
						{
							$imap->deleteMail(trim($val));
						}
						$imap->close();
						echo "Selected emails has been deleted successfully.";
					}
					else
					{
						echo "Error in connection.Please try again.";
					}
					
				}
				else
				{
					echo "Please select row to delete.";
				}
				
			}
			else
			{
				echo "Please select row to delete.";
			}
		}
		else
		{
			echo "The request is invalid.";
		}
		
	}
	
	public function actionArchive()
	{
		//print_r($_POST);
		//die;
		if(Yii::app()->request->isAjaxRequest)
	    {
			if(isset($_POST['data1']))
			{
				$msgsArry=$_POST['data1'];
				if(count($msgsArry) > 0)
				{
					
					
					$user_details=EmailUser::model()->getEmailDetailsById(Yii::app()->user->id);
					$user=$user_details->email_id;
					$pass=MyCustomClass::decrypt($user_details->password);
					$imap=new EIMap('{vmcp09.digitalpacific.com.au:993/imap/ssl/novalidate-cert}', $user, $pass);
					if($imap->connect())
					{
						 $transaction= Yii::app()->db->beginTransaction();
						 $msg="";
						  try 
			 				{
								foreach($msgsArry as $key=>$val)
								{
									$uid=Yii::app()->user->id;
									$val=trim($val);
									//validate so that same message cannot be archived twice
									$archiveEmails=Inbox::model()->find("crtd_by=$uid and msg_no=$val");
									$commEmails=Communication::model()->find("emailId=$val and user_id=$uid");
									$valid=true;
									if(!($commEmails==null || $commEmails->communication_id <= 0))
									{
										$valid=false;
									}
									
									if(!($archiveEmails==null || $archiveEmails->inbox_id <= 0))
									{
										$valid=false;
									}
									
									if($valid)
									{
											$message=$imap->getMail($val,null,false,true);
											$inbox=new Inbox;
											$inbox->uid=$message->UID;
											$inbox->msg_no=$val;
											$inbox->fromName=$message->fromName;
											$inbox->fromAddress=$message->fromAddress;
											$inbox->mail_to=$message->toString;
											if(is_array($message->cc))
											{
												$inbox->mail_cc=implode(',',$message->cc);
											}
											else
											{
												$inbox->mail_cc=$message->cc;
											}
											
											$inbox->subject=$message->subject;
											$inbox->message=$message->textHtmlOriginal==""?"<pre>".$message->textPlain."</pre>":$message->textHtmlOriginal;
											$inbox->mail_date=date('Y-m-d',strtotime($message->date));
											 $inbox->setIsNewRecord(true);
											 $inbox->save();
											 $inbox->refresh();
											 
											 $attachment=$message->attachments;
											 if(!empty($attachment))
											 {
											 	foreach($attachment as $file)
												{
													$uniqid=MyCustomClass::uniqueRandom();
													$filename = Yii::getPathOfAlias('webroot').'/project_files/archive/'.$uniqid.$file['filename'];
													$fp = fopen($filename, 'w');
													fputs($fp, $file['data']);
													fclose($fp);
													
													$file_add = new EmailAttachment();
													$file_add->setIsNewRecord(true);
													$file_add->file = $uniqid.$file['filename'];
													$file_add->id=$inbox->inbox_id;
													$file_add->id_type='inbox';
													$file_add->save();
												}
											 }
									}
								
								}
								$transaction->commit();
								$imap->close();
								echo "Selected emails has been archived successfully.Emails already in archive or communication are omitted while saving.";
							}
							catch (Exception $e)
							 {
							 	$transaction->rollback();
								$imap->close();
								throw new CHttpException(404, 'Some error occured while saving.Please try again'); 
							 }
							
							 	
							 
							
						
						
						
					}
					else
					{
						echo "Error in connection.Please try again.";
					}
					
				}
				else
				{
					echo "Please select rows to archive.";
				}
				
			}
			else
			{
				echo "Please select rows to archive.";
			}
		}
		else
		{
			echo "The request is invalid.";
		}
		
	}
	
	
	

	
protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='reply-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	


	


}
