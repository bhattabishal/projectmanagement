<?php
class UserWiseScheduleReportController extends Controller
{
	public $layout='//layouts/column1';
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','dynamicRegularProjectByUser'),
				'users'=>array('@'),
			),
		array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionIndex()
	{
		$model=new UserWiseScheduleReport;
		$model->startDt=MyCustomClass::subractDays(date('Y-m-d'),7);
		$model->endDt=MyCustomClass::addDays(date('Y-m-d'),7);
		$model->projectId=0;
        $model->user_id=Yii::app()->user->id;
	
		if(isset($_POST['UserWiseScheduleReport']))
		{
			
			$model->attributes=$_POST['UserWiseScheduleReport'];
		}
		
		$projectRecords=$model->getInvolvedProjectsByUser($model->projectId,$model->user_id,$model->adminTasks,$model->task_id);
		$this->render('_userWiseScheduleReport',array('projectRecords'=>$projectRecords,'model'=>$model));
			
	}
	
		//UserWiseScheduleReport
	
 public function actionDynamicRegularProjectByUser()
  {
  if(isset($_POST['user_id']) &&  $_POST['user_id'] > 0) 
		{
				$uid=$_POST['user_id'];
				
  		$records=MyCustomClass::getInvolvedProjectsByUserId($uid);

	 echo CHtml::tag('option',
                           array('value'=>""),CHtml::encode("All"),true);
			 foreach($records as $project)
            {
			
			//	$lbl=$project['pno']."(".$project['project_title'].")";
                echo CHtml::tag('option',
                           array('value'=>$project['project_id']),CHtml::encode($project['pno']),true);
            }
  	}
  }

}
?>