<?php

/**
 * This is the model class for table "pm_variation".
 *
 * The followings are the available columns in table 'pm_variation':
 * @property string $variation_id
 * @property integer $project_id
 * @property integer $task_id
 * @property string $description
 * @property string $variation_hours
 * @property integer $status
 * @property string $comments
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 */
class Variation extends PMActiveRecord
{
	
	public $projectCompositeNo;
	public 	$status_search;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_variation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('task_id, description,status, variation_hours', 'required','on'=>array('update', 'create'),'except'=>'batchSave'),
			array('task_id, status,status_search, crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			array('variation_hours', 'length', 'max'=>50),
			
			array('description, variation_hours', 'required', 'on'=>'batchSave'),
			
			array('comments', 'safe'),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('variation_id, task_id,projectCompositeNo,variation_no,variation_dt, variation_hours, status, comments,status_search', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'task' => array(self::BELONGS_TO, 'Task', 'task_id'),
			'communication' => array(self::BELONGS_TO, 'Communication',array('communication_id'=>'communication_id'), 'through'=>'task'),
			
			'project' => array(self::BELONGS_TO, 'Project', array('project_id'=>'project_id'),'through'=>'communication'),
			'codeValueProjectType' => array(self::HAS_ONE, 'CodeValue',array('project_type'=>'code_id'), 'through'=>'project'),
			
		);
	}
	
	
	public function getUniqueVariationByProject($commId)
	{
		$comm=Communication::model()->findByPk($commId);
		$projectId=$comm->project_id;
		$project=Project::model()->findByPk($projectId);
		$tasks=$project->pmTasks;
		
		$taskIds="";
		foreach($tasks as $task)
		{
			if($taskIds=="")
			{
				$taskIds=$task->task_id;
			}
			else
			{
				$taskIds=$taskIds.",".$task->task_id;
			}
			
		}
		
		
		$result=Yii::app()->db->createCommand("SELECT max(variation_no) as varnumber FROM pm_variation WHERE task_id IN ($taskIds)")->queryRow();
		
		return $result['varnumber']+1;
		
	}
	
	/*
	get status of variation
	*/
	public function getStatus()
	{
		if($this->status==0)
			{
				return "Pending";
			}
			else if($this->status==1)
			{
				return "Partial Approved";
			}
			else if($this->status==2)
			{
				return "Approved";
			}
			else
			{
				return "Not Defined";
			}
	}
	
	/*
	get project no (combination of project no and project type)
	*/
	
	public function getProjectCompositeNo()
	{
		$task=Task::model()->findByPk($this->task_id);
		$comm=Communication::model()->findByPk($task->communication_id);
		$projectId=$comm->project_id;
		$project=Project::model()->findByPk($projectId);
		//print_r($this->project_id);
		//die;
		
		return ($project->project_no <=0 ? $project->project_id:$project->project_no)."-".$project->projectType->code_lbl;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'variation_id' => 'Variation',
			'task_id' => 'Task',
			'projectCompositeNo'=>'Project No',
		//	'description' => 'Description',
			'variation_hours' => 'Hours',
			'variation_dt' =>'Date',
			'variation_no' =>'Variation Number',
			'status' => 'Status',
			'comments' => 'Comments',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
		//	'variationStatus.code_lbl' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.variation_id',$this->variation_id,true);
		
	
		$criteria->compare('t.task_id',$this->task_id,true);
//		$criteria->compare('t.description',$this->description,true);
		$criteria->compare('t.variation_hours',$this->variation_hours,true);
		$criteria->compare('t.variation_no',$this->variation_no,true);
		$criteria->compare('t.variation_dt',$this->variation_dt,true);
		$criteria->compare('t.status',$this->status,true);
		$criteria->compare('t.comments',$this->comments,true);

		
	//	$criteria->array(
	//	->join('pm_task','pm_variation.task_id = pm_task.task_id')
	//	->join('pm_communication','pm_task.communication_id = pm_communication.communication_id')
	//	->join('pm_project','pm_communication.project_id = pm_project.project_id')
	//	);
  $criteria->join = 'INNER JOIN pm_task task on t.task_id = task.task_id ';
  $criteria->join .= 'INNER JOIN pm_communication communication  on task.communication_id = communication.communication_id ';
  $criteria->join .= 'INNER JOIN pm_project project on communication.project_id = project.project_id ';
  
  $criteria->join .= 'INNER JOIN pm_code_value codeValueProjectType on project.project_type = codeValueProjectType.code_id ';
  
 $criteria->compare('concat(project.project_no,"-",codeValueProjectType.code_lbl)',$this->projectCompositeNo,true);
					 
			
			
		if($this->comments != "")
		{
			
		}
		else
		{
			$data2=new CActiveDataProvider(Variation, array('pagination'=>FALSE,'criteria'=>$criteria));
			$_SESSION['variation-excel']=$data2; // get all data and filtered data
		}			
  
  
		$data = new CActiveDataProvider(get_class($this), array(
                       'pagination'=>array('pageSize'=> Yii::app()->user->getState('pageSize',
                                                   Yii::app()->params['defaultPageSize']),),
                        'criteria'=>$criteria,
						   'sort'=>array(
			 'defaultOrder' => 'concat(project.project_no,"-",codeValueProjectType.code_lbl) ASC',
		        'attributes'=>array(
		           '*',
		        ),
		    ),
                ));
                 return $data;

	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Variation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
