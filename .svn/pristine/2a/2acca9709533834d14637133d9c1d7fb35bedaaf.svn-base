<?php

/**
 * This is the model class for table "pm_project_associates".
 *
 * The followings are the available columns in table 'pm_project_associates':
 * @property integer $project_associates_id
 * @property integer $project_id
 * @property integer $user_id
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 * @property integer $is_coordinator
 *
 * The followings are the available model relations:
 * @property PmCodeValue $isCoordinator
 * @property PmProject $project
 * @property PmUser $user
 */
class ProjectAssociates extends PMActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_project_associates';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, user_id', 'required'),
			array('project_id, user_id, crtd_by, updt_by, updt_cnt, is_coordinator', 'numerical', 'integerOnly'=>true),
			array('crtd_dt, updt_dt', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('project_associates_id, project_id, user_id, crtd_by, crtd_dt, updt_by, updt_dt, updt_cnt, is_coordinator', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'isCoordinator' => array(self::BELONGS_TO, 'PmCodeValue', 'is_coordinator'),
			'project' => array(self::BELONGS_TO, 'PmProject', 'project_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'project_associates_id' => 'Project Associates',
			'project_id' => 'Project',
			'user_id' => 'User',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
			'is_coordinator' => 'Is Coordinator',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('project_associates_id',$this->project_associates_id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('crtd_by',$this->crtd_by);
		$criteria->compare('crtd_dt',$this->crtd_dt,true);
		$criteria->compare('updt_by',$this->updt_by);
		$criteria->compare('updt_dt',$this->updt_dt,true);
		$criteria->compare('updt_cnt',$this->updt_cnt);
		$criteria->compare('is_coordinator',$this->is_coordinator);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/*
	Get all project associates by project id
	*/	
	
	public function getProjectAssociatesUsers($pid)
	{
		//$criteria2=new CDbCriteria;
		//$criteria2->select='u.user_id,u.user_name';  // only select the 'title' column
		//$criteria2->join='inner JOIN `pm_user` as `u` ON t.user_id=u.user_id';
		//$criteria2->condition="t.project_id=$pid";
		
		$project_associates = Yii::app()->db->createCommand()
		->select('u.user_id,u.user_name')
		 ->from('pm_project_associates as t')
		 ->join('pm_user as u', 't.user_id=u.user_id')
		 ->where('t.project_id=:project_id', array(':project_id'=>$pid))
		 ->queryAll();
		
		//$criteria->params=array(':task_id'=>$tid);
		//$project_associates=ProjectAssociates::model()->findAll($criteria2);
		//print_r($project_associates);
		//die;
		
		$arry=array();
		foreach($project_associates as $val)
		{
			$id=$val['user_id'];
			$text=$val['user_name'];
			$arry[$id]=$text;
		}
		
		
		return $arry;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProjectAssociates the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
