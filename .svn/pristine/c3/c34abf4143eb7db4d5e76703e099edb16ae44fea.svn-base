<?php

/**
 * This is the model class for table "pm_task".
 *
 * The followings are the available columns in table 'pm_task':
 * @property integer $task_id
 * @property integer $job_id
 * @property string $job_details
 * @property integer $sharable
 * @property string $job_due_date
 * @property string $est_rps_cost
 * @property string $est_ao_cost
 * @property string $actual_rps_cost
 * @property string $actual_ao_cost
 * @property string $done_percentage
 * @property integer $assigned_to
 * @property integer $task_status
 * @property integer $crtd_by
 * @property string $crtd_dt
 * @property integer $updt_by
 * @property string $updt_dt
 * @property integer $updt_cnt
 *
 * The followings are the available model relations:
 * @property CodeValue $taskStatus
 * @property Job $job
 * @property CodeValue $sharable0
 * @property User $assignedTo
 * @property TaskCommunication[] $taskCommunications
 */
class Task extends PMActiveRecord
{
	//public  $project_search;
	public  $project_text_search;
	public $assigned_to_search;
	public 	$status_search;
	public $projectCompositeNo;
	public $validation;
	public $extendedTask;
	
	//public $taskSchedules;
	//public $totalNormalHours;
	//public $totalCusionHours;
	public $totalOverTimeHours;
	
	
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm_task';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('communication_id,piority, job_details, sharable, done_percentage, assigned_to, task_status,task_cat,job_type,est_rps_cost,job_client_due_date', 'required'),
			
			array('communication_id, sharable, assigned_to, task_status,task_cat, crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			array('est_rps_cost, est_ao_cost, actual_rps_cost, actual_ao_cost, done_percentage', 'length', 'max'=>18),
			array('task_variation,crtd_dt, updt_dt,job_due_date,job_start_date,validation,piority,totalOverTimeHours,is_planned,error_known,error_unknown,parent_task_id,task_type,admin_task_id', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('task_id, communication_id,project_text_search,assigned_to_search,status_search, job_details, sharable, job_due_date, est_rps_cost, est_ao_cost, actual_rps_cost, actual_ao_cost, assigned_to, task_status,task_cat,is_hidden,projectCompositeNo,task_variation', 'safe', 'on'=>'search'),
			
			//array('taskSchedules', 'checkTaskScheduled', 'on' => 'batchSave'),
			
			
			array('est_rps_cost', 'checkIfHourAvailable', 'on' => 'insert,update'),
			array('is_hidden', 'ifTaskCompleted', 'on' => 'toggle'),
			array('task_status', 'ifCheckListCompleted', 'on' => 'editable,update'),
			
		);
	}
	
	
	/*
	check if assigned hour is available
	*/
	public function checkIfHourAvailable($attribute,$params)
	{
		$startDate=date('Y-m-d');
		$task_id=$this->task_id;
		if($this->extendedTask == 1)
		{
			$task=Task::model()->findByPk($task_id);
			$startDate=$task->job_due_date;
			$task_id=0;
		}
		
		if($this->assigned_to=="" || $this->job_due_date=="" || !MyCustomClass::is_date($this->job_due_date) || $this->est_rps_cost =="" )
		{
			$this->addError('job_due_date', "Enter valid job due date or select user");
			return FALSE;
		}
		elseif(floatval($this->est_rps_cost) <= 0)
		{
			$this->addError($attribute, "Please enter valid work hours.");
			return FALSE;
		}
		else if(floatval($this->est_rps_cost) < 0.5)
		{
			$this->addError($attribute, "Work hour must be greater than 0.5 hours");
			return FALSE;
		}
		else if(floatval($this->totalOverTimeHours) > 0)
		{
			$dateDiff=MyCustomClass::GetDateDifference($this->job_due_date,$startDate);
			$dateDiff+=1;
			
			$definedOH=CodeValue::model()->getCodeTypeValue('work_hour',30);
			if($this->totalOverTimeHours > ($definedOH*$dateDiff))
			{
				$this->addError('totalOverTimeHours', "Invalid over time hours.");
				return FALSE;
			}
			
			
		}
		else
		{
			return TRUE;
		}
		/*
		else
		{
			
			$totHour=floatval($this->est_rps_cost);
			$otHour=floatval($this->totalOverTimeHours);
			
			//$currentTime=date('H');
			//$officeLastHour=07;
			//$officeFirstHour=15;
		
			 $endDate=$this->job_due_date;
			//$hourLeftToday=$officeFirstHour-$currentTime;
			//$totHourWorkLeft=$totHour;
			
			
			//get the user availability from
			
				
			
				if(!$this->normalWorkHourAvailable($startDate,$endDate,$this->assigned_to,$totHour,$otHour,$task_id))
				{
					$this->addError($attribute,"The selected user is not available to complete the task.");
					return FALSE;
				}
			
			
			
		}
		
		
		return TRUE;
		*/
	}
	
	/*
	validation for task schedule array.task dates should be in asending order and for  urgent tasks the schedule should not affect the project deadline if schedule shifted
	*/
	
	/*
	public function checkTaskScheduled($attribute,$params)
	{
		$definedNH=CodeValue::model()->getCodeTypeValue('work_hour',28);
		$definedCH=CodeValue::model()->getCodeTypeValue('work_hour',29);
		$definedOH=CodeValue::model()->getCodeTypeValue('work_hour',30);
		
		
		$schedules=$this->taskSchedules;
		 $oldDate="";
		 $shift_days=0;//no of days shifted if urgent forced schedule
				 foreach($schedules as $schedule)
				 {
				 	
				 	$record=Yii::app()->db->createCommand()
		->select('IFNULL(sum(normal_hour),0) as nh,IFNULL(sum(cusion_hour),0) as ch,IFNULL(sum(overtime_hour),0) as oh')
		->from('pm_task_schedule')
		->where('pm_task_schedule.user_id=:userid',array(':userid'=>$schedule->user_id))
		->andWhere('pm_task_schedule.schedule_date=:schedule_date',array(':schedule_date'=>$schedule->schedule_date))
		->group('schedule_date')
		->queryRow();
		
		if((floatval($record['nh'])+floatval($schedule->normal_hour)) > floatval($definedNH))
		{
			$shift_days+=1;
		}
		else if((floatval($record['ch'])+floatval($schedule->cusion_hour)) > floatval($definedCH))
		{
			$shift_days+=1;
		}
		else if((floatval($record['oh'])+floatval($schedule->overtime_hour)) > floatval($definedOH))
		{
			$shift_days+=1;
		}
		
		
				 	
				 	
					//validation: the date should be in acending order
					if($oldDate != "")
					{
						
						 if( MyCustomClass::GetDateDifference($schedule->schedule_date,$oldDate) <= 0)
		                    {
								 $this->addError('validation', "The scheduled dates must be in ascending order.");
								 return FALSE;
							}	
					}
		                   		
						
				 	 $oldDate=$schedule->schedule_date;
				 	 $i++;
				 }
				 
				 //if above validation passed then check if project deadline shifts or not if we push the task on piority
				 if($this->piority=="Urgent")
				 {
				 	$comm=Communication::model()->findByPk($this->communication_id);
					$project=$comm->project;
					$deadlineDate=$project->job_due_date;
					$shiftedDate=MyCustomClass::addDays($deadlineDate,$shift_days);
					if(MyCustomClass::GetDateDifference($shiftedDate,$deadlineDate) > 0)
					{
						$this->addError($attribute, "The project deadline exceeds by $dateDiff days.");
					}
				 }
				 
	}
	
	*/
	public function ifAlreadyInUse($attribute,$params)
	{
	
		
		
		$checklistMaster=new Checklist();
		$taskChecklist=new TaskChecklist();
		
		$checklists=$checklistMaster->findAll("job_type_id=$this->task_cat");
		$presentCheckList=$taskChecklist->findAll("task_id=$this->task_id");
		
		if(count($presentCheckList) > count($checklists))
		{
			$this->addError($attribute, count($presentCheckList).",".count($checklists)."You cannot change the category.Already in progress ");
		}
		
		
		
	}
	
	/*
	check if all checklist complted before changing status of task as done
	also check if schedule completed.if schedule available
	*/
	public function ifCheckListCompleted($attribute,$params)
	{
		//12-Done,26-Completed(Task)
		//27-NA not aplicable (Task Checklist)
		if($this->task_status == 12 || $this->task_status == 26)
		{
			$checklist=new TaskChecklist();
			$list=$checklist->findAll("task_id=$this->task_id and status!='12' and status!='27'");
			$schedule=TaskSchedule::model()->findAll("task_id=$this->task_id and completed_status='0'");
			if(count($list)>0 || count($schedule) > 0)
			{
				$this->addError($attribute,"Cannot update the status.Checklists or task schedule not completed");
				return FALSE;
			}
		}
		return TRUE;
		
	}
	
	
	
	
	/*
	before save validation.sice updating status validation above(ifCheckListCompleted) did not
	worked for tbeditable column.
	needed to add the before save function
	*/
	/*
	public function beforeSave()
        {
                if(parent::beforeSave())
                {		
					if(!$this->isNewRecord)
					{						
						if($this->task_status == 12)
						{
							$checklist=new TaskChecklist();
							$list=$checklist->findAll("task_id=$this->task_id and status!='12'");
							if(count($list)>0)
							{
								$this->addError('task_status',"Cannot update the status.Checklists not completed");
							}
						}
						else{
							return true;
						}
					}
					else{
						 return true;
					}
					
                }
       
        }
	*/
	public function ifTaskCompleted($attribute,$params)
	{
		
		//$task=new Task();
		
		
		//$taskList=$task->find("task_id=$this->task_id");
		
		if(isset($this->is_hidden) && $this->is_hidden == 1)
		{
			if($this->task_status != 26)
			{
				$this->addError($attribute,"Only completed(done) task can be hidden");
				return FALSE;
			}
		}
		return TRUE;
		
		
	
		
		//$this->addError($attribute, $this->$attribute);
		
	}
	
	/*
	before saving hide the record that are completed,ie status==12
	*/
	/*
	public function beforeSave()
        {
                if(parent::beforeSave())
                {
					if($this->isNewRecord)
					{
						if($this->task_status==12)
						{
							$this->setAttribute('is_hidden', '1');
						}
					}
					else{
							if($this->task_status==12)
							{
								$task=new Task();
								$taskList=$task->find("task_id=$this->task_id");
								if($taskList->is_hidden > 0 )
								{
									return true;
								}
								else{
									$this->setAttribute('is_hidden', '1');
								}
							}
							else{
								$this->setAttribute('is_hidden', '0');
							}
							
					}
					
					
					
                        
                }
        return true;
        }

	*/

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'taskStatus' => array(self::BELONGS_TO, 'CodeValue', 'task_status'),
			'taskCat' => array(self::BELONGS_TO, 'CodeValue', 'task_cat'),
			'communication' => array(self::BELONGS_TO, 'Communication', 'communication_id'),
			'sharable0' => array(self::BELONGS_TO, 'CodeValue', 'sharable'),
			'jobType' => array(self::BELONGS_TO, 'CodeValue', 'job_type'),
			'assignedTo' => array(self::BELONGS_TO, 'User', 'assigned_to'),
			'project' => array(self::HAS_ONE, 'Project',array('project_id'=>'project_id'), 'through'=>'communication'),
			'codeValueProjectType' => array(self::HAS_ONE, 'CodeValue',array('project_type'=>'code_id'), 'through'=>'project'),
			'taskCommunications' => array(self::HAS_MANY, 'TaskCommunication', 'task_id'),
		);
	}
	
	/**
	 * @return array relational rules.
	 */
	public function getTaskList($id)
	{
		$val = Yii::app()->db->createCommand()
		->select('*')
		 ->from('pm_task ')
		 ->where('task_id=:task_id', array(':task_id'=>$id))
		 ->queryRow();
		 return $val;
	}
	
	/*
	all assigned active task lists for dropdown box according to user id
	*/
	public function getUserAssignedActiveTaskList($userId)
	{
		if($userId > 0)
		{
			$results=Yii::app()->db->createCommand()
		->select("CONCAT(if(pm_project.project_no IS NULL OR pm_project.project_no='',pm_project.project_id,pm_project.project_no),'-',pm_code_value.code_lbl,
'-',pm_communication.communication_id,'-',pm_task.task_id,'(',pm_task.est_rps_cost,'hr)') AS task_no,
pm_task.task_id")
		->from('pm_task')
		->join('pm_communication','pm_task.communication_id = pm_communication.communication_id')
		->join('pm_project','pm_communication.project_id = pm_project.project_id')
		->join('pm_code_value','pm_project.project_type = pm_code_value.code_id')
		->where('pm_task.assigned_to=:uid', array(':uid'=>$userId))
		->andWhere(array('not in', 'pm_task.task_status', array(12,26)))//not done or completed
		->queryAll();
		}
		else
		{
			$results=Yii::app()->db->createCommand()
		->select("CONCAT(if(pm_project.project_no IS NULL OR pm_project.project_no='',pm_project.project_id,pm_project.project_no),'-',pm_code_value.code_lbl,
'-',pm_communication.communication_id,'-',pm_task.task_id) AS task_no,
pm_task.task_id")
		->from('pm_task')
		->join('pm_communication','pm_task.communication_id = pm_communication.communication_id')
		->join('pm_project','pm_communication.project_id = pm_project.project_id')
		->join('pm_code_value','pm_project.project_type = pm_code_value.code_id')
		->where(array('not in', 'pm_task.task_status', array(12,26)))//not done or completed
		->queryAll();
		
		}
		
		
		$rtnarry=array();
		
		if(count($results) > 0)
		{
			foreach($results as $result)
			{
				$taskId=$result['task_id'];
				$rtnarry[$taskId]=$result['task_no'];
			}
		}
		
		return $rtnarry;
	}
	
	public function getTaskName($id)
	{
		if($id=="" || $id <= 0)
		{
			$id=$this->task_id;
		}
		
		
		$val = Yii::app()->db->createCommand()
		->select('t.task_id,c.communication_id,p.project_no,p.project_id,p.project_type,cv.code_lbl')
		 ->from('pm_task t')
		 ->join('pm_communication c','t.communication_id = c.communication_id')
		 ->join('pm_project p', 'c.project_id = p.project_id')
		 ->join('pm_code_value cv', 'p.project_type = cv.code_id')
		 ->where('t.task_id=:task_id', array(':task_id'=>$id))
		 ->queryRow();
		
		 $project_code = $val['project_no']==""?$val['project_id']:$val['project_no'];
		 $project_type = $val['code_lbl'];
		 $comm_id = $val['communication_id'];
		 $task_id = $val['task_id'];
		 $project_name = $project_code."-".$project_type."-".$comm_id."-".$task_id; 
		return $project_name;
		 
	}
	public function getProjectCoordinates($id)
	{
		$projectcoordinatesList = Yii::app()->db->createCommand()
		->select('t.task_id,c.communication_id,p.project_no,p.project_type,cv.code_lbl,pc.user_id,')
		 ->from('pm_task t')
		 ->join('pm_communication c','t.communication_id = c.communication_id')
		 ->join('pm_project p', 'c.project_id = p.project_id')
		 ->join('pm_code_value cv', 'p.project_type = cv.code_id')
		 ->join('pm_project_coordinates pc', 'p.project_id = pc.project_id')
		 ->where('t.task_id=:task_id', array(':task_id'=>$id))
		 ->andWhere('pc.type=:type', array(':type'=>'ao'))
		 ->queryAll();
         return $projectcoordinatesList;
	}
	
	public function setRowColor()
	{
		
		
		$duedate=$this->job_due_date;
		$now=date("Y-m-d");
		$customClass=new MyCustomClass();
		
		//compare due dates
		
		if($duedate <= $now)
		{
			return "#fb7474";
		}
		else if($customClass->GetDateDifference($duedate,$now)==1)
		{
			return "#fdc152";
		}
		else if($customClass->GetDateDifference($duedate,$now)==2)
		{
			return "#ffff00";
		}
		else{
			return "#fffbf0";
		}
		
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'task_id' => 'Task No',
			'communication_id' => 'Comm No',
			'project.project_id' =>'Prj No',
			'projectCompositeNo' =>'Prj No',
			'project_search' =>'Prj No',
			'project_text_search' =>'Project',
			'communication.communication_details' => 'Comm Details',
			'job_details' => 'Details',
			'sharable' => 'Sharable',
			'sharable0.code_lbl'=>'Sharable',
			'job_due_date' => 'Due Date',
			'job_client_due_date' => 'Client Due Date',
			'est_rps_cost' => 'Est Rps Hrs',
			'est_ao_cost' => 'Est Ao Hrs',
			'actual_rps_cost' => 'Actual Rps Hrs',
			'actual_ao_cost' => 'Actual Ao Hrs',
			'done_percentage' => 'Done %',
			'assigned_to' => 'Assigned To',
			'assignedTo.user_name' => 'Assigned To',
			'assigned_to_search' => 'Assigned To',
			'taskStatus.code_lbl' => 'Status',
			'task_variation'=>'variation',
			'status_search'=>'Status',
			'jobType.code_lbl' => 'Job Type',
			'taskCat.code_lbl' => 'Category',
			'totalNormalHours'=>'Tot Normal Hrs',
			'totalCusionHours'=>'Tot Cusion Hrs',
			'totalOverTimeHours'=>'Tot OT Hrs',
			'crtd_by' => 'Crtd By',
			'crtd_dt' => 'Crtd Dt',
			'updt_by' => 'Updt By',
			'updt_dt' => 'Updt Dt',
			'updt_cnt' => 'Updt Cnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		//$criteria->with=array('project,taskStatus,assignedTo,codeValueProjectType');
		//$criteria->together = true; 
		
		$criteria->compare('t.task_id',$this->task_id);
		$criteria->compare('t.task_variation',$this->task_variation);
		$criteria->compare('t.communication_id',$this->communication_id);
		$criteria->compare('t.job_details',$this->job_details,true);
		$criteria->compare('t.sharable',$this->sharable);
		$criteria->compare('t.job_due_date',$this->job_due_date,true);
		$criteria->compare('t.est_rps_cost',$this->est_rps_cost,true);
		$criteria->compare('t.est_ao_cost',$this->est_ao_cost,true);
		$criteria->compare('t.actual_rps_cost',$this->actual_rps_cost,true);
		$criteria->compare('t.actual_ao_cost',$this->actual_ao_cost,true);
		//$criteria->order = 't.job_due_date ASC';
		
		
		$criteria->compare('project.project_title',$this->project_text_search,true);
		$criteria->with = array('project'=>array('alias'=>'project','select'=>'project.project_title','together'=>true));
		
		$criteria->addCondition('project.project_status in (3,25)');//active and ready to issue
		
		$criteria->compare('assignedTo.user_name',$this->assigned_to_search,true);
		//$criteria->with = array('assignedTo'=>array('alias'=>'assignedTo','select'=>'assignedTo.user_name','together'=>true));
		$criteria -> join = 'INNER JOIN pm_user assignedTo on t.assigned_to = assignedTo.user_id ';
		
		
		
		$criteria->compare('taskStatus.code_lbl',$this->status_search,true);
		//$criteria->compare('taskStatus.code_lbl',$this->task_status,true);
		$criteria -> join .= 'INNER JOIN pm_code_value taskStatus on t.task_status = taskStatus.code_id ';
		
		
		
		
		$criteria->compare('concat(project.project_no,"-",codeValueProjectType.code_lbl)',$this->projectCompositeNo,true);
		$criteria->with = array('codeValueProjectType'=>array('alias'=>'codeValueProjectType','select'=>'codeValueProjectType.code_lbl','together'=>true));
		
		
		//$criteria->condition='t.task_status !=12';
		
		
		if(!Yii::app()->user->checkAccess('viewAllTask'))
		{
			$model=new MyCustomClass();
		   	$pids=$model->getInvolvedProjectsByUser();
			//$criteria->condition='project.project_id in ('.$pids.')';
			$criteria->addCondition('project.project_id in ('.$pids.')');
		}
		 if($this->status_search =="" )
		 {
		 	$criteria->addCondition('t.is_hidden =0');
		 }
		
		//return new CActiveDataProvider($this, array(
			//'criteria'=>$criteria,
		//));
		if(!Yii::app()->user->checkAccess('viewAllTasks'))
		$criteria->addCondition('t.sharable =1 or t.crtd_by ='.Yii::app()->user->id.'');
		
		
		$criteria->addCondition('t.communication_id > 0');
		
		return new CActiveDataProvider( $this, array(
		    'criteria'=>$criteria,
		    'sort'=>array(
			 'defaultOrder' => 't.job_due_date ASC',
		        'attributes'=>array(
		            //'project_search'=>array(
		               // 'asc'=>'project.project_id',
		               // 'desc'=>'project.project_id DESC',
		            //),
					///'project_text_search'=>array(
		               // 'asc'=>'project.project_title',
		               // 'desc'=>'project.project_title DESC',
		            //),
					//'status_search'=>array(
		               // 'asc'=>'taskStatus.code_lbl',
		                //'desc'=>'taskStatus.code_lbl DESC',
		            //),
					//'projectCompositeNo'=>array(
		               // 'asc'=>'concat(project.project_no,"-",codeValueProjectType.code_lbl)',
		               // 'desc'=>'concat(project.project_no,"-",codeValueProjectType.code_lbl) DESC',
		            //),
					//'assigned_to_search'=>array(
		                //'asc'=>'assignedTo.user_name',
		                //'desc'=>'assignedTo.user_name DESC',
		           // ),
		           '*',
		        ),
		    ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Task the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getProjectCompositeNo()
	{
		$criteria=new CDbCriteria;
		$criteria->select='*'; 
		$criteria->condition='project_id=:project_id';
		$criteria->params=array(':project_id'=>$this->project->project_id);
		$proj=Project::model()->find($criteria);
		
		//$criteria=new CDbCriteria;
		$criteria->select='*'; 
		$criteria->condition='code_id=:code_id';
		$criteria->params=array(':code_id'=>$proj->project_type);
		$code=CodeValue::model()->find($criteria);
		
		return ($proj->project_no <=0 ? $proj->project_id:$proj->project_no)."-".$code->code_lbl;
	}
	
	
	/*
	check if task has checklist and schedule created.It it has any checklist do not allow to delete
	*/
	public function beforeDelete()
        {
                if(parent::beforeDelete())
                {
					$checklist=TaskChecklist::model()->findAll(array(
					    'select'=>'task_checklist_id',
					    'condition'=>'task_id=:task_id',
					
					    'params'=>array(':task_id'=>$this->task_id),
						));
						
						$schedule=TaskSchedule::model()->findAll(array(
					    'select'=>'schedule_id',
					    'condition'=>'task_id=:task_id',
					
					    'params'=>array(':task_id'=>$this->task_id),
						));
						
						
						
					if(count($checklist) > 0 || count($schedule) > 0)
					{
						return false;
					}
					else{
						
						return true;
					}
                        
                }
       
        }
        
        /*
        dont let the task update if work started
        */
        
        public function beforeSave()
        {
        	
        	
			 if(parent::beforeSave())
			 {
			 	if(!$this->isNewRecord)
			 	{
			 		if($this->scenario != "editable" && $this->scenario != "toggle")
			 		{
						$timeline=ActualTimeline::model()->findAll(array(
					    'select'=>'timeline_id',
					    'condition'=>'task_id=:task_id',
					
					    'params'=>array(':task_id'=>$this->task_id),
						));
						
						if(count($timeline) > 0)
						{
							return FALSE;
						}
						else
						{
							return TRUE;
						}
					}
					
				}
				return TRUE;
			 	
			 }
		}
        
        public function normalWorkHourAvailable($startDate,$endDate,$user,$workHour,$ot_hour=0,$task_id=0)
        {
        
			$definedNH=CodeValue::model()->getCodeTypeValue('work_hour',28);
			$definedOH=CodeValue::model()->getCodeTypeValue('work_hour',30);
			$dateDiff=MyCustomClass::GetDateDifference($endDate,$startDate);
			$dateDiff+=1;
			$totAvailableHours=0;
			if($ot_hour > 0)
			{
				$totAvailableHours=($definedNH+$definedOH)*$dateDiff;
			}
			else
			{
				$totAvailableHours=$definedNH*$dateDiff;
			}
			
			
			
				if($task_id <= 0)
				{
					$record=Yii::app()->db->createCommand()
					->select('IFNULL(sum(normal_hour),0) as nh,IFNULL(sum(cusion_hour),0) as ch,IFNULL(sum(overtime_hour),0) as oh')
					->from('pm_task_schedule')
					->where('pm_task_schedule.user_id=:userid',array(':userid'=>$user))
					->andWhere("pm_task_schedule.completed_status='0'")
					->andWhere("pm_task_schedule.schedule_date between :startdt and :enddt" ,array(':startdt'=>$startDate,':enddt'=>$endDate))
					->queryRow();
					
					//echo "task id is $task_id";
					//print_r($record);
				}
				else
				{
					$record=Yii::app()->db->createCommand()
					->select('IFNULL(sum(normal_hour),0) as nh,IFNULL(sum(cusion_hour),0) as ch,IFNULL(sum(overtime_hour),0) as oh')
					->from('pm_task_schedule')
					->where('pm_task_schedule.user_id=:userid',array(':userid'=>$user))
					->andWhere("pm_task_schedule.completed_status='0'")
					->andWhere("pm_task_schedule.task_id != '".$task_id."'")
					->andWhere("pm_task_schedule.schedule_date between :startdt and :enddt" ,array(':startdt'=>$startDate,':enddt'=>$endDate))
					->queryRow();
				}
					
					
					$assignedHours=0;
						if($ot_hour > 0)
						{
							$assignedHours=floatval($record['nh'])+floatval($record['oh']);
						}
						else
						{
							$assignedHours=floatval($record['nh']);
						}
					
				
					$actualAvailable=$totAvailableHours-$assignedHours;
					//echo $actualAvailable;
					
					if($actualAvailable < $workHour)
					{
						
						return FALSE;
					}
					
					
				return TRUE;
		}
		
		
	public function getRpsCoordinatorWorkingHour($proj_type,$esthours)
	{
		
		
		switch($proj_type)
		{
			case '15'://ENMK
				$perc=20;
				break;
			case '16'://ENSK
				$perc=20;
				break;
			case '17'://ENABK
				$perc=20;
				break;
			case '19'://ENMP
				$perc=20;
				break;
			case '20'://ENSP
				$perc=20;
				break;
			case '21'://ABP
				$perc=10;
				break;
			case '22'://ABK
				$perc=10;
				break;
			default:
				$perc=10;
				break;
			
		}
		
		
		$totHour=$esthours;
      	$totHour=($perc/100)*$totHour;
      	$totHour=round($totHour,1);
      	
      	return $totHour;
	}
		
	
	public function generateRpsCoordinatorSchedule($model)
	{
		
		$definedNH=CodeValue::model()->getCodeTypeValue('work_hour',28);
                $definedCH=CodeValue::model()->getCodeTypeValue('work_hour',29);
                $definedOH=CodeValue::model()->getCodeTypeValue('work_hour',30);
		
		$schedule=new TaskSchedule('novalidation');
		$schedule->isNewRecord=TRUE;
		$schedule->user_id=$model->assigned_to;
		$schedule->schedule_date=$model->job_due_date;
		$schedule->normal_hour=$model->est_rps_cost;
		$schedule->overtime_hour=0;
		$schedule->task_id=$model->task_id;
		$schedule->cusion_hour=$actualCH;//cusion hour as it is.managed internally
		$schedule->actualNH=$actualNH;
		$schedule->actualCH=$actualCH;
		$schedule->actualOH=$actualOT;
		$schedule->overtime_hour=0;
		$schedule->schedule_nr_hr=$schedule->normal_hour;
		$schedule->schedule_ot_hr=$schedule->overtime_hour;
		$schedule->remarks="";
		$schedule->completed_status=0;
		$schedule->extended_status=$model->extendedTask;
		
		
		return $schedule;
		
		
	}
		
	public function generateSchedule($model,$type='val')
	{
			$taskScheduleModel = array();
			$rps_hour=$model->est_rps_cost;
			$ot_hour=$model->totalOverTimeHours;
			$totHour=$rps_hour;
            $totNormalHour=$rps_hour-$ot_hour;

             $totOTHours=$ot_hour;

             $definedNH=CodeValue::model()->getCodeTypeValue('work_hour',28);
             $definedCH=CodeValue::model()->getCodeTypeValue('work_hour',29);
             $definedOH=CodeValue::model()->getCodeTypeValue('work_hour',30);


			$date=$model->job_start_date;
			$due_date=$model->job_due_date;
			$user_id=$model->assigned_to;
			$task_id=$model->task_id;
			
			
		
		while (strtotime($due_date) >= strtotime($date)) 
		{
			$currentDate=$due_date;
			if(date('w', strtotime($currentDate)) != 6)
			{
			
			
			if($totHour <= 0)
			{
				break;
			}
			
			$record=Yii::app()->db->createCommand()
				->select('IFNULL(sum(normal_hour),0) as nh,IFNULL(sum(cusion_hour),0) as ch,IFNULL(sum(overtime_hour),0) as oh')
				->from('pm_task_schedule')
				->where('pm_task_schedule.user_id=:userid',array(':userid'=>$user_id))
				->andWhere('pm_task_schedule.completed_status=0')
				->andWhere('pm_task_schedule.schedule_date=:schedule_date',array(':schedule_date'=>$currentDate))
				->group('schedule_date')
				->queryRow();
				
				if($task_id > 0)
				{
					$record=Yii::app()->db->createCommand()
					->select('IFNULL(sum(normal_hour),0) as nh,IFNULL(sum(cusion_hour),0) as ch,IFNULL(sum(overtime_hour),0) as oh')
					->from('pm_task_schedule')
					->where('pm_task_schedule.user_id=:userid',array(':userid'=>$user_id))
					->andWhere('pm_task_schedule.completed_status=0')
					->andWhere("pm_task_schedule.task_id !=$task_id")
					->andWhere('pm_task_schedule.schedule_date=:schedule_date',array(':schedule_date'=>$currentDate))
					->group('schedule_date')
					->queryRow();
				}
				
				$actualNH=floatval($definedNH)-floatval($record['nh']);
				$remainingNormal=floatval($definedNH);
				
				$actualCH=floatval($definedCH)-floatval($record['ch']);
				$remainingCusion=floatval($definedCH);
				
				$actualOT=floatval($definedOH)- floatval($record['oh']);
				$remainingOT=floatval($definedOH);
				
				
				if($ot_hour > 0)
				{
					$remaining=$remainingNormal+$remainingOT;
				}
				else
				{
					$remaining=$remainingNormal;
				}
				
				
				if($remaining > 0 && ($totNormalHour > 0 || $totOTHours > 0))
				{
                                    $j=0;
                                    $k=0;
                                    if($totNormalHour <= $remainingNormal)
                                    {
                                        $j=$totNormalHour;
                                        $totNormalHour=0;
                                    }
                                    else
                                    {
                                         $j=$remainingNormal;
                                         $totNormalHour=$totNormalHour-$remainingNormal;
                                         
                                    }
                                    
                                    if($totOTHours <= $remainingOT)
                                    {
                                        $k=$totOTHours;
                                        $totOTHours=0;
                                    }
                                    else
                                    {
                                         $k=$remainingOT;
                                         $totOTHours=$totOTHours-$remainingOT;
                                    }
					
                                       
					
					$schedule=new TaskSchedule('novalidation');
					$schedule->isNewRecord=TRUE;
					$schedule->user_id=$model->assigned_to;
					$schedule->schedule_date=$currentDate;
					$schedule->normal_hour=$j;
					$schedule->task_id=$model->task_id;
					$schedule->cusion_hour=$actualCH;//cusion hour as it is.managed internally
					$schedule->actualNH=$actualNH;
					$schedule->actualCH=$actualCH;
					$schedule->actualOH=$actualOT;
					$schedule->overtime_hour=$k;
					$schedule->schedule_nr_hr=$schedule->normal_hour;
					$schedule->schedule_ot_hr=$schedule->overtime_hour;
					$schedule->remarks="";
					$schedule->completed_status=0;
					$schedule->extended_status=$model->extendedTask;
					$taskScheduleModel[]=$schedule;
					
				}
				
				
			}
			
			$due_date = date ("Y-m-d", strtotime("-1 day", strtotime($due_date)));
			
		}
		
		
		if($type=='val')
			{
				return $taskScheduleModel;
			}
			else
			{
				
				if($totHour  != 0)
				{
					return FALSE;
				}
				else
				{
					return TRUE;
				}
			}
	}
	
	public function getTotalTaskHourByProjectId($pid)
	{
		$records=Yii::app()->db->createCommand("
		SELECT
COALESCE(sum(tothour),'0') as tottaskhour
FROM
scheduled_total_by_date WHERE project_id = '$pid'")->queryRow();


		return $records;
	}
	
	public function getRpsCoordinatorOfTask($tid)
	{
		$records=Yii::app()->db->createCommand("
		SELECT
pm_project_coordinates.user_id,
pm_project_coordinates.type,
pm_project_coordinates.project_id
FROM
pm_task
INNER JOIN pm_communication ON pm_task.communication_id = pm_communication.communication_id
INNER JOIN pm_project ON pm_communication.project_id = pm_project.project_id
INNER JOIN pm_project_coordinates ON pm_project.project_id = pm_project_coordinates.project_id
WHERE
pm_project_coordinates.type = 'rps' AND
pm_task.task_id = '$tid'")->queryRow();


		return $records['user_id'];
	}
	
		
		public  function todoListSave()
		{
			
				$id=$this->task_id;
				$eknown=$this->error_known;
				$eunknown=$this->error_unknown;
				if($this->task_status == 12)
				{
					$todo=new TodoList;
					
					
					
					$todo->user_id=$this->getRpsCoordinatorOfTask($id);
					$todo->from_id=Yii::app()->user->id;
					
					$todo->task="The task no $id is marked as done with error known $eknown and error unknown $eunknown.";
					$todo->date_time=date('Y-m-d');
					$todo->actual_date=date('Y-m-d');
					$todo->status=2;
					
					$todo->save(FALSE);
				}
				
					
				
			

		}
                
                
                
                public function getFixedTasks()
                {
                    $qry=Task::model()->findAll('communication_id=:cid',array(':cid'=>'-1'));
                    return $qry;
                }

		
	
	
}
