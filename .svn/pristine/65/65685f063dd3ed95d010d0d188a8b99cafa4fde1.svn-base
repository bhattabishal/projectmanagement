<?php

/**
* This is the model class for table "pm_task_schedule".
*
* The followings are the available columns in table 'pm_task_schedule':
* @property string $schedule_id
* @property integer $task_id
* @property string $schedule_date
* @property string $normal_hour
* @property string $cusion_hour
* @property string $overtime_hour
* @property string $remarks
* @property string $crtd_dt
* @property integer $crtd_by
* @property string $updt_dt
* @property integer $updt_by
* @property integer $updt_cnt
*/
class TaskSchedule extends PMActiveRecord{
	public $updateType;
	public $piority;
	public $user_search;
	public $projectCompositeNo;
	public  $project_text_search;
	public $communication_no;
	public $actualNH;
	public $actualCH;
	public $actualOH;
	public $changeDate;
	public $changeHour;
        public $projectSearch;
        public $userSearch;
        public $stDtSearch;
        public $enDtSearch;
        public $days;
        public $branch_id;
        public $ispost;
	
	
	/**
	* @return string the associated database table name
	*/
	public function tableName(){
		return 'pm_task_schedule';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules(){
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('schedule_date', 'required'),
			array('task_id, crtd_by, updt_by, updt_cnt', 'numerical', 'integerOnly'=>true),
			array('remarks', 'length', 'max'=>250),
			array('normal_hour, cusion_hour, overtime_hour','match','pattern'=>'/^[0-9]{1,2}(\.[0-9]{0,2})?$/'),
			array('user_id,remarks,task_id,extended_status,completed_status,schedule_id_completed,schedule_nr_hr,schedule_ot_hr,changeHour,projectSearch,userSearch,changeDate,stDtSearch,enDtSearch,days,branch_id,ispost','safe'),
			
			array('schedule_date', 'validateHour', 'on' => 'batchSave,insert,update'),
			array('normal_hour', 'validateHour', 'on' => 'editable'),
			array('overtime_hour', 'validateHour', 'on' => 'editable'),
			array('task_id,user_id','required', 'on' => 'insert,update,editable'),
			
			array('schedule_date', 'validateDate', 'on' => 'update'),
			
			array('schedule_date', 'validateDateShift', 'on' => 'update'),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('schedule_id,user_id,user_search, task_id, schedule_date, normal_hour, cusion_hour, overtime_hour, remarks,projectCompositeNo,communication_no, crtd_dt, crtd_by, updt_dt, updt_by, updt_cnt,branch_id,ispost', 'safe', 'on'=>'search'),
		);
	}
	
	
	/*
	if days shift checked than date must be shifted
	*/
	public function validateDateShift($attribute,$params)
	{
		if($this->changeDate > 0)
		{
			$schedule=TaskSchedule::model()->findByPk($this->schedule_id);
			
			if($this->schedule_date == $schedule->schedule_date)
			{
				$this->addError($attribute, "Please change date or uncheck the change date checkbox");
				return FALSE;
			}
		}	
		
		return TRUE;
	}
	
	
	/*
		check the condition for taskSchedule 
		the schedule cannot be assigned if task due date exceeds.
		in such condition check if it has authority.if so allow the change.
		Also no need to check for insert condition
		
		also the date cannot be changed before current date
	*/
	
	public function validateDate($attribute,$params)
	{
		/*
		if($this->isNewRecord==FALSE)
		{
			$task=Task::model()->findByPk($this->task_id);
			if($this->schedule_date > $task->job_due_date)
			{
				$id=Yii::app()->user->id;
				
				if(User::model()->checkIfCoordinatorsFromTask($id,$this->task_id))
				{
					
					return True;
				}
				else
				{
					$this->addError($attribute, "You don't have authority to change the task due date.Please contact the administrator.");
					return FALSE;
				}
				
			}
			
		}
		*/
		return TRUE;
		
	}
	
	
	
	/*
	check if n/c/w hour available on the selected date
	*/
	public function  validateHour($attribute,$params){
		
		
		
		//cannot select date previous than today
		
		$dateDifference=MyCustomClass::GetDateDifference($this->schedule_date);
		//$dateDifference=1;
		if($dateDifference < 0){
			$this->addError($attribute, "Cannot schedule for previous days.");
			return FALSE;
		}
		
		if(floatval($this->normal_hour) <= 0  && floatval($this->overtime_hour) <= 0){
			$this->addError($attribute, "Please enter work hours.");
			return FALSE;
		}
		
		
		if($this->schedule_id > 0){
			$record=Yii::app()->db->createCommand()
			->select('IFNULL(sum(normal_hour),0) as nh,IFNULL(sum(cusion_hour),0) as ch,IFNULL(sum(overtime_hour),0) as oh')
			->from('pm_task_schedule')
			->where('pm_task_schedule.user_id=:userid',array(':userid'=>$this->user_id))
			->andWhere('pm_task_schedule.schedule_date=:schedule_date',array(':schedule_date'=>$this->schedule_date))
			->andWhere("pm_task_schedule.schedule_id !=$this->schedule_id")
			->group('schedule_date')
			->queryRow();
		}
		else{
			$record=Yii::app()->db->createCommand()
			->select('IFNULL(sum(normal_hour),0) as nh,IFNULL(sum(cusion_hour),0) as ch,IFNULL(sum(overtime_hour),0) as oh')
			->from('pm_task_schedule')
			->where('pm_task_schedule.user_id=:userid',array(':userid'=>$this->user_id))
			->andWhere('pm_task_schedule.schedule_date=:schedule_date',array(':schedule_date'=>$this->schedule_date))
			->group('schedule_date')
			->queryRow();
		}
		
		$definedNH=CodeValue::model()->getCodeTypeValue('work_hour',28);
		$definedCH=CodeValue::model()->getCodeTypeValue('work_hour',29);
		$definedOH=CodeValue::model()->getCodeTypeValue('work_hour',30);
		
		
		//if(floatval($this->normal_hour) > floatval($definedNH)){
			//$this->addError($attribute, "Normal Hour is not available.");
							
			//return FALSE;
		//}
		
		
			//$this->cusion_hour=floatval($definedCH)-floatval($record['ch']);
			return TRUE;
		
		
		
	}

	/**
	* @return array relational rules.
	*/
	public function relations(){
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'task' => array(self::BELONGS_TO, 'Task', 'task_id'),
			'communication' => array(self::BELONGS_TO, 'Communication', array('communication_id'=>'communication_id'), 'through'=>'task'),
			'project' => array(self::HAS_ONE, 'Project',array('project_id'=>'project_id'), 'through'=>'communication'),
			'codeValueProjectType' => array(self::HAS_ONE, 'CodeValue',array('project_type'=>'code_id'), 'through'=>'project'),
		);
	}

	/**
	* @return array customized attribute labels (name=>label)
	*/
	public function attributeLabels(){
		return array(
			'schedule_id' => 'Schedule',
			'task_id' => 'Task',
			'user_id'=>'Assigned To',
			'user.user_name'=>'Assigned To',
			'piority'=>'Piority',
			'schedule_date' => 'Date',
			'normal_hour' => 'Normal Hour',
			'cusion_hour' => 'Cus Hour',
			'overtime_hour' => 'OT Hour',
			'remarks' => 'Remarks',
			'crtd_dt' => 'Crtd Dt',
			'crtd_by' => 'Crtd By',
			'updt_dt' => 'Updt Dt',
			'updt_by' => 'Updt By',
			'updt_cnt' => 'Updt Cnt',
                        'branch_id'=>'Branch',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	*
	* Typical usecase:
	* - Initialize the model fields with values from filter form.
	* - Execute this method to get CActiveDataProvider instance which will filter
	* models according to data in model fields.
	* - Pass data provider to CGridView, CListView or any similar widget.
	*
	* @return CActiveDataProvider the data provider that can return the models
	* based on the search/filter conditions.
	*/
	public function search(){
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.schedule_id',$this->schedule_id,true);
		$criteria->compare('t.task_id',$this->task_id,true);
		//$criteria->compare('user_id',$this->user_id);
		$criteria->compare('communication.communication_id',$this->communication_no,true);
		$criteria->compare('concat(project.project_no,"-",codeValueProjectType.code_lbl)',$this->projectCompositeNo,FALSE);
		$criteria->compare('user.user_name',$this->user_search,true);
		$criteria->compare('t.piority',$this->piority);
		$criteria->compare('t.schedule_date',$this->schedule_date,true);
		$criteria->compare('t.normal_hour',$this->normal_hour,true);
		$criteria->compare('t.cusion_hour',$this->cusion_hour,true);
		$criteria->compare('t.overtime_hour',$this->overtime_hour,true);
		$criteria->compare('t.remarks',$this->remarks,true);
		$criteria->compare('t.crtd_dt',$this->crtd_dt,true);
		$criteria->compare('t.crtd_by',$this->crtd_by);
		$criteria->compare('t.updt_dt',$this->updt_dt,true);
		$criteria->compare('t.updt_by',$this->updt_by);
		$criteria->compare('t.updt_cnt',$this->updt_cnt);
		
		$criteria->with = array(
			'user'=>array('select'=>'user.user_name','together'=>true),
			'project'=>array('select'=>'project.*','together'=>true),
			'codeValueProjectType'=>array('select'=>'codeValueProjectType.*','together'=>true),
		);
		
		if(!Yii::app()->user->checkAccess('viewAllTask'))
		{
			$model=new MyCustomClass();
			$pids=$model->getInvolvedProjectsByUser();
			//$criteria->condition='project.project_id in ('.$pids.')';
			$criteria->addCondition('project.project_id in ('.$pids.')');
		}
		
		$criteria->addCondition('project.project_id > 0');

		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize' => 50),
				'sort'=>array(
					'defaultOrder' => 't.schedule_date DESC,t.task_id,t.user_id',
					'attributes'=>array(
						//'project_search'=>array(
						//'asc'=>'project.project_title',
						//'desc'=>'project.project_title DESC',
						// ),
					
						'*',
					),
				),
			));
	}
	
	
	/*
	get taskschedule summary from taskschedule table of selected user groupby taskid
	*/
	
	public function getActiveTaskScheduleSummary($userId){
		//CONCAT('(',Min(pm_task_schedule.schedule_date),') - (',Max(pm_task_schedule.schedule_date),')') AS task_date,
		$result = Yii::app()->db->createCommand()
		->select("CONCAT_WS('-', IF(pm_project.project_no IS NULL or pm_project.project_no = '', pm_project.project_id, pm_project.project_no), pm_code_value.code_lbl, pm_communication.communication_id, pm_task.task_id) AS project_number,pm_task_schedule.schedule_date as task_date,

			SUM(pm_task_schedule.normal_hour) AS sum_normal_hour,
			SUM(pm_task_schedule.cusion_hour) AS sum_cusion_hour,
			SUM(pm_task_schedule.overtime_hour) AS sum_overtime_hour")
		->from('pm_task_schedule')
		->join('pm_task','pm_task_schedule.task_id = pm_task.task_id')
		->join('pm_communication','pm_task.communication_id = pm_communication.communication_id')
		->join('pm_project','pm_communication.project_id = pm_project.project_id')
		->join('pm_code_value','pm_project.project_type = pm_code_value.code_id')
		->where('pm_task.assigned_to=:uid', array(':uid'=>$userId))
		->andWhere(array('not in', 'pm_task.task_status', array(12,26)))//not done or completed
		->group('pm_task_schedule.schedule_date,pm_task_schedule.task_id')
		->order('pm_task_schedule.schedule_date DESC')->queryAll();
		
		
		return $result;
	}
	
	/*
	check if selected user has task scheduled for the mentioned date or not
	*/
	public function checkIfUserFree($date,$userId){
		return "";
	}
	
	
	
	
	/*
	check the total working hour the user is busy(active tasks)
	normal_hour,cusion_hour and overtime_hour
	*/
	public function checkWorkingHour($taskId,$user,$hour_type){
		
		$record=Yii::app()->db->createCommand()
		->select('IFNULL(sum(normal_hour),0) as nh,IFNULL(sum(cusion_hour),0) as ch,IFNULL(sum(overtime_hour),0) as oh')
		->from('pm_task_schedule')
		->where('pm_task_schedule.user_id=:userid',array(':userid'=>$user))
		->andWhere("pm_task_schedule.completed_status='0'")
		->andWhere("pm_task_schedule.task_id=:task_id",array(':task_id'=>$taskId))
		->queryRow();
					
					
		if($hour_type=='normal'){
			return $record['nh'];
		}
		else if($hour_type=='cusion'){
			return $record['ch'];
		}
		else{
			return $record['oh'];
		}
	}
	
	public function getProjectCompositeNo(){
		
		
		return ($this->project->project_no <=0 ? $this->project->project_id:$this->project->project_no)."-".$this->codeValueProjectType->code_lbl;
	}
	
	
	



	/*
	check the date to shift the task due date
	and total work hours of task if added
	*/
	protected function afterSave(){
		parent::afterSave();
		$task=Task::model()->findByPk($this->task_id);
		if($this->schedule_date > $task->job_due_date){
			Task::model()->updateByPk($task->task_id, array(
					'job_due_date' => $this->schedule_date
				));
		}
		
		$taskSum=Yii::app()->db->createCommand()
		->select('sum(normal_hour+overtime_hour) as tothr')
		->from('pm_task_schedule')
		->where("task_id=$this->task_id")->queryRow();
		
		$hr=$taskSum['tothr'];
		
		Task::model()->updateByPk($task->task_id, array(
				'est_rps_cost' => $hr
			));
		
	}
	
	
	/*
	while shifting the schedule, the schedule may have been started or not 
	and the date in which we are shifting may already have schedule/worked schedule or no schedule
	*/
	
	public function shiftScheduleWorkNextDate($schedule_id,$nextDate){
		
		$taskScheduleRecord = TaskSchedule::model()->findByPk($schedule_id);
		
		$nextTaskSchedule=TaskSchedule::model()->find('task_id=:task_id and user_id = :user_id and schedule_date = :schedule_date', array(':task_id'=>$taskScheduleRecord->task_id,':user_id'=>$taskScheduleRecord->user_id,':schedule_date'=>$nextDate));
		
		
		
		$worked_nr_hr=floatval($taskScheduleRecord->worked_normal_hr);
		$worked_ot_hr=floatval($taskScheduleRecord->worked_ot_hr);				    
		$worked_hour=$worked_nr_hr + $worked_ot_hr;
		
		$assigned_nr_hr=floatval($taskScheduleRecord->normal_hour);
		$assigned_ot_hr=floatval($taskScheduleRecord->overtime_hour);
		$assignedHr=$assigned_nr_hr + $assigned_ot_hr;
						    
		//find addition hours to be added
		$additionalHr=$assignedHr-$worked_hour;
		$additionalHr=round($additionalHr,1);
		//end
		
		
		$newSchedule="";
		
		
		
		if($nextTaskSchedule->schedule_id > 0)//already task in shifting date
		{
			$newSchedule=$nextTaskSchedule;
			$definedNH=CodeValue::model()->getCodeTypeValue('work_hour',28);
			$nextScheduleRemainingNormal=$definedNH-($nextTaskSchedule->normal_hour-$nextTaskSchedule->worked_normal_hr);
			
			if($additionalHr > $nextScheduleRemainingNormal)
			{
				$newSchedule->normal_hour=$newSchedule->normal_hour+$nextScheduleRemainingNormal;
				$newSchedule->overtime_hour=$newSchedule->overtime_hour+($additionalHr-$nextScheduleRemainingNormal);
			}
			else
			{
				$newSchedule->normal_hour=$newSchedule->normal_hour+$additionalHr;
			}
			
		}
		else//no task on shifting date
		{
			
			$newSchedule=new TaskSchedule;
			$newSchedule->normal_hour=$assigned_nr_hr-$worked_nr_hr;
			$newSchedule->overtime_hour=$assigned_ot_hr-$worked_ot_hr;
			$newSchedule->task_id=$taskScheduleRecord->task_id;
			$newSchedule->user_id=$taskScheduleRecord->user_id;
			$newSchedule->schedule_date=$nextDate;
			$newSchedule->cusion_hour=2;
			
			
			$newSchedule->schedule_nr_hr=$newSchedule->normal_hour;
			$newSchedule->schedule_ot_hr=$newSchedule->overtime_hour;
			
			
			
			
			
			
		}
		
		
		$taskScheduleRecord->normal_hour=$worked_nr_hr;
		$taskScheduleRecord->overtime_hour=$worked_ot_hr;
		$taskScheduleRecord->completed_status=1;
		
		$newSchedule->save(false);
		
		
		$taskScheduleRecord->save(false);
		
					   
	}
	
	public function getRemainingHourByUserTaskDate($uid,$pid,$date)
	{
		$records=Yii::app()->db->createCommand("select (sum(tothour-totworkedhour)) as  remaining_hour
		FROM scheduled_total_by_date

		WHERE
		project_id = '$pid' AND
		user_id = '$uid' AND
		schedule_date <= '$date'")->queryRow();


		return $records;
	}
	
	public function getActualTakenHourByUserProjectDate($uid,$pid,$date)
	{
		$records=Yii::app()->db->createCommand("select (sum(tothour)) as  remaining_hour,(sum(shootingtothour)) as shootinghour
		FROM actual_total_by_date

		WHERE
		project_id = '$pid' AND
		user_id = '$uid' AND
		task_date <= '$date'")->queryRow();


		return $records;
	}
	
	public function getRemainingHourByUserTaskDateRange($uid,$pid,$stdt,$endt)
	{
		$records=Yii::app()->db->createCommand("
		SELECT
		
		sum(((pm_task_schedule.normal_hour + pm_task_schedule.overtime_hour)-(pm_task_schedule.worked_normal_hr+pm_task_schedule.worked_ot_hr))) AS remaining_hour
		
		FROM
		pm_project
		INNER JOIN pm_communication ON pm_project.project_id = pm_communication.project_id
		INNER JOIN pm_task ON pm_communication.communication_id = pm_task.communication_id
		INNER JOIN pm_task_schedule ON pm_task.task_id = pm_task_schedule.task_id
		INNER JOIN pm_user ON pm_task_schedule.user_id = pm_user.user_id
		INNER JOIN pm_code_value ON pm_project.project_type = pm_code_value.code_id
		LEFT JOIN pm_shooting_factor ON pm_user.user_id = pm_shooting_factor.user_id

		WHERE
		pm_project.project_id = '$pid' AND
		pm_task_schedule.user_id = '$uid' AND
		pm_task_schedule.schedule_date >= '$stdt'
		and pm_task_schedule.schedule_date <= '$endt'

		GROUP BY pm_task_schedule.schedule_date

		 ")->queryRow();


		return $records;
	}
	
	
	
	
	
	public function getAllocatedHourByProjectUser($uid,$pid)
	{
		$records=Yii::app()->db->createCommand("SELECT

		sum(pm_task_schedule.normal_hour + pm_task_schedule.overtime_hour) AS allocated_hour

		FROM
		pm_project
		INNER JOIN pm_communication ON pm_project.project_id = pm_communication.project_id
		INNER JOIN pm_task ON pm_communication.communication_id = pm_task.communication_id
		INNER JOIN pm_task_schedule ON pm_task.task_id = pm_task_schedule.task_id
		INNER JOIN pm_user ON pm_task_schedule.user_id = pm_user.user_id
		INNER JOIN pm_code_value ON pm_project.project_type = pm_code_value.code_id
		LEFT JOIN pm_shooting_factor ON pm_user.user_id = pm_shooting_factor.user_id
		WHERE
		pm_project.project_id = '$pid' AND
		pm_task_schedule.user_id = '$uid' 

		");
		
		return $records;
		
		
	}

	/**
	* Returns the static model of the specified AR class.
	* Please note that you should have this exact method in all your CActiveRecord descendants!
	* @param string $className active record class name.
	* @return TaskSchedule the static model class
	*/
	public static function model($className=__CLASS__){
		return parent::model($className);
	}
}
