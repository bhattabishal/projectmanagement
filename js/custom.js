//for dialog width and height

function initialiseDialog(dialog, iframe, url)
{
    $(dialog).dialog("option", "title", "Loading...");
    $(dialog).dialog("option", "width", $(window).width()-20);
    $(dialog).dialog("option", "height", $(window).height()-20);
    $(dialog).css("position","middle","top");
 
    $(window).scrollTop(0); //Scroll to the main page's top 
    $(dialog).dialog("open");
 
    //Set iframe size after dialog was opened   
    $(iframe).attr("src",url);  
    $(iframe).css("width", $(dialog).width()-16);   
    $(iframe).css("height", $(dialog).height()-10);
    $(iframe).css("position","middle","top");
}

//end dialog width and height

function checkIfEmailToClient()
{
	var y=bootbox.confirm("Do you want to send email to client?");
	if(y)
	{
		document.getElementById("emailtoclient").val("1");
	}
	return;
}